// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:tube/utils/colors.dart';

class ServiceIconCustomPainter extends CustomPainter {
  final Color color;

  const ServiceIconCustomPainter({
    this.color = ColorData.service,
    Listenable? repaint,
  }) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.3333313, size.height * 0.08333333);
    path_0.lineTo(size.width * 0.8333312, size.height * 0.08333333);
    path_0.cubicTo(
        size.width * 0.8791646,
        size.height * 0.08333333,
        size.width * 0.9166646,
        size.height * 0.1208333,
        size.width * 0.9166646,
        size.height * 0.1666667);
    path_0.lineTo(size.width * 0.9166646, size.height * 0.6666667);
    path_0.cubicTo(
        size.width * 0.9166646,
        size.height * 0.7125000,
        size.width * 0.8791646,
        size.height * 0.7500000,
        size.width * 0.8333312,
        size.height * 0.7500000);
    path_0.lineTo(size.width * 0.3333313, size.height * 0.7500000);
    path_0.cubicTo(
        size.width * 0.2874979,
        size.height * 0.7500000,
        size.width * 0.2499979,
        size.height * 0.7125000,
        size.width * 0.2499979,
        size.height * 0.6666667);
    path_0.lineTo(size.width * 0.2499979, size.height * 0.1666667);
    path_0.cubicTo(
        size.width * 0.2499979,
        size.height * 0.1208333,
        size.width * 0.2874979,
        size.height * 0.08333333,
        size.width * 0.3333313,
        size.height * 0.08333333);
    path_0.close();
    path_0.moveTo(size.width * 0.08333333, size.height * 0.2500000);
    path_0.lineTo(size.width * 0.1666667, size.height * 0.2500000);
    path_0.lineTo(size.width * 0.1666667, size.height * 0.8333333);
    path_0.lineTo(size.width * 0.7500000, size.height * 0.8333333);
    path_0.lineTo(size.width * 0.7500000, size.height * 0.9166667);
    path_0.lineTo(size.width * 0.1666667, size.height * 0.9166667);
    path_0.cubicTo(
        size.width * 0.1208333,
        size.height * 0.9166667,
        size.width * 0.08333333,
        size.height * 0.8791667,
        size.width * 0.08333333,
        size.height * 0.8333333);
    path_0.lineTo(size.width * 0.08333333, size.height * 0.2500000);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = color;
    canvas.drawPath(path_0, paint_0_fill);
  }

  @override
  bool shouldRepaint(covariant ServiceIconCustomPainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
