// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

//Copy this CustomPainter code to the Bottom of the File
class FacebookCustomPainter extends CustomPainter {
  final Color color;

  const FacebookCustomPainter({
    this.color = Colors.black,
    Listenable? repaint,
  }) : super(repaint: repaint);
  // const CustomPainter({ Listenable? repaint }) : _repaint = repaint;
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width, size.height * 0.5030542);
    path_0.cubicTo(size.width, size.height * 0.2252254, size.width * 0.7761417,
        0, size.width * 0.5000000, 0);
    path_0.cubicTo(size.width * 0.2238575, 0, 0, size.height * 0.2252254, 0,
        size.height * 0.5030542);
    path_0.cubicTo(0, size.height * 0.7541417, size.width * 0.1828417,
        size.height * 0.9622625, size.width * 0.4218750, size.height);
    path_0.lineTo(size.width * 0.4218750, size.height * 0.6484708);
    path_0.lineTo(size.width * 0.2949217, size.height * 0.6484708);
    path_0.lineTo(size.width * 0.2949217, size.height * 0.5030542);
    path_0.lineTo(size.width * 0.4218750, size.height * 0.5030542);
    path_0.lineTo(size.width * 0.4218750, size.height * 0.3922262);
    path_0.cubicTo(
        size.width * 0.4218750,
        size.height * 0.2661479,
        size.width * 0.4965250,
        size.height * 0.1965062,
        size.width * 0.6107333,
        size.height * 0.1965062);
    path_0.cubicTo(
        size.width * 0.6654208,
        size.height * 0.1965062,
        size.width * 0.7226583,
        size.height * 0.2063312,
        size.width * 0.7226583,
        size.height * 0.2063312);
    path_0.lineTo(size.width * 0.7226583, size.height * 0.3301304);
    path_0.lineTo(size.width * 0.6596083, size.height * 0.3301304);
    path_0.cubicTo(
        size.width * 0.5975000,
        size.height * 0.3301304,
        size.width * 0.5781250,
        size.height * 0.3689108,
        size.width * 0.5781250,
        size.height * 0.4087325);
    path_0.lineTo(size.width * 0.5781250, size.height * 0.5030542);
    path_0.lineTo(size.width * 0.7167958, size.height * 0.5030542);
    path_0.lineTo(size.width * 0.6946292, size.height * 0.6484708);
    path_0.lineTo(size.width * 0.5781250, size.height * 0.6484708);
    path_0.lineTo(size.width * 0.5781250, size.height);
    path_0.cubicTo(size.width * 0.8171583, size.height * 0.9622625, size.width,
        size.height * 0.7541417, size.width, size.height * 0.5030542);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = color;
    canvas.drawPath(path_0, paint_0_fill);
  }

  @override
  bool shouldRepaint(covariant FacebookCustomPainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
