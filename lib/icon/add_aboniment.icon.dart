// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

class AddAbonementCustomPainter extends CustomPainter {
  final Color color;

  const AddAbonementCustomPainter({
    this.color = Colors.black,
    super.repaint,
  });

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.3750000, size.height * 0.8333333);
    path_0.cubicTo(size.width * 0.5590944, size.height * 0.8333333, size.width * 0.7083333, size.height * 0.6840944,
        size.width * 0.7083333, size.height * 0.5000000);
    path_0.cubicTo(size.width * 0.7083333, size.height * 0.3159050, size.width * 0.5590944, size.height * 0.1666667,
        size.width * 0.3750000, size.height * 0.1666667);
    path_0.cubicTo(size.width * 0.1909050, size.height * 0.1666667, size.width * 0.04166667, size.height * 0.3159050,
        size.width * 0.04166667, size.height * 0.5000000);
    path_0.cubicTo(size.width * 0.04166667, size.height * 0.6840944, size.width * 0.1909050, size.height * 0.8333333,
        size.width * 0.3750000, size.height * 0.8333333);
    path_0.close();
    path_0.moveTo(size.width * 0.5312450, size.height * 0.5312600);
    path_0.lineTo(size.width * 0.4062450, size.height * 0.5312600);
    path_0.lineTo(size.width * 0.4062450, size.height * 0.6562611);
    path_0.lineTo(size.width * 0.3437450, size.height * 0.6562611);
    path_0.lineTo(size.width * 0.3437450, size.height * 0.5312600);
    path_0.lineTo(size.width * 0.2187450, size.height * 0.5312600);
    path_0.lineTo(size.width * 0.2187450, size.height * 0.4687600);
    path_0.lineTo(size.width * 0.3437450, size.height * 0.4687600);
    path_0.lineTo(size.width * 0.3437450, size.height * 0.3437600);
    path_0.lineTo(size.width * 0.4062450, size.height * 0.3437600);
    path_0.lineTo(size.width * 0.4062450, size.height * 0.4687600);
    path_0.lineTo(size.width * 0.5312450, size.height * 0.4687600);
    path_0.lineTo(size.width * 0.5312450, size.height * 0.5312600);
    path_0.close();
    path_0.moveTo(size.width * 0.7083333, size.height * 0.2645833);
    path_0.lineTo(size.width * 0.7083333, size.height * 0.1775000);
    path_0.cubicTo(size.width * 0.8520833, size.height * 0.2145833, size.width * 0.9583333, size.height * 0.3445833,
        size.width * 0.9583333, size.height * 0.5000000);
    path_0.cubicTo(size.width * 0.9583333, size.height * 0.6554167, size.width * 0.8520833, size.height * 0.7854167,
        size.width * 0.7083333, size.height * 0.8225000);
    path_0.lineTo(size.width * 0.7083333, size.height * 0.7354167);
    path_0.cubicTo(size.width * 0.8054167, size.height * 0.7012500, size.width * 0.8750000, size.height * 0.6087500,
        size.width * 0.8750000, size.height * 0.5000000);
    path_0.cubicTo(size.width * 0.8750000, size.height * 0.3912500, size.width * 0.8054167, size.height * 0.2987500,
        size.width * 0.7083333, size.height * 0.2645833);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = color;
    canvas.drawPath(path_0, paint_0_fill);
  }

  @override
  bool shouldRepaint(covariant AddAbonementCustomPainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
