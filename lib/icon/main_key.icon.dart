// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

class InitialKeyCustomPainter extends CustomPainter {
  const InitialKeyCustomPainter({Listenable? repaint}) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.5688978, size.height * 0.5230535);
    path_0.lineTo(size.width * 0.5688978, size.height * 0.5289294);
    path_0.lineTo(size.width * 0.5741290, size.height * 0.5316010);
    path_0.cubicTo(size.width * 0.6293820, size.height * 0.5598200, size.width * 0.6645255, size.height * 0.6166569,
        size.width * 0.6645255, size.height * 0.6790584);
    path_0.cubicTo(size.width * 0.6645255, size.height * 0.7704307, size.width * 0.5901776, size.height * 0.8447786,
        size.width * 0.4988054, size.height * 0.8447786);
    path_0.cubicTo(size.width * 0.4074331, size.height * 0.8447786, size.width * 0.3330827, size.height * 0.7704307,
        size.width * 0.3330827, size.height * 0.6790584);
    path_0.lineTo(size.width * 0.3330827, size.height * 0.6790560);
    path_0.cubicTo(size.width * 0.3330633, size.height * 0.6167421, size.width * 0.3681290, size.height * 0.5615547,
        size.width * 0.4234769, size.height * 0.5333090);
    path_0.lineTo(size.width * 0.4286715, size.height * 0.5306569);
    path_0.lineTo(size.width * 0.4287105, size.height * 0.5248224);
    path_0.lineTo(size.width * 0.4290925, size.height * 0.4674039);
    path_0.lineTo(size.width * 0.4290949, size.height * 0.4673698);
    path_0.lineTo(size.width * 0.4290949, size.height * 0.4673382);
    path_0.cubicTo(size.width * 0.4290925, size.height * 0.4656545, size.width * 0.4297591, size.height * 0.4640462,
        size.width * 0.4309489, size.height * 0.4628564);
    path_0.lineTo(size.width * 0.4515499, size.height * 0.4422555);
    path_0.lineTo(size.width * 0.4583382, size.height * 0.4354672);
    path_0.lineTo(size.width * 0.4515499, size.height * 0.4286813);
    path_0.lineTo(size.width * 0.4305669, size.height * 0.4076959);
    path_0.cubicTo(size.width * 0.4293017, size.height * 0.4064307, size.width * 0.4286229, size.height * 0.4046715,
        size.width * 0.4287178, size.height * 0.4028467);
    path_0.cubicTo(size.width * 0.4288443, size.height * 0.4008929, size.width * 0.4297494, size.height * 0.3992895,
        size.width * 0.4309757, size.height * 0.3983577);
    path_0.lineTo(size.width * 0.4310438, size.height * 0.3983041);
    path_0.lineTo(size.width * 0.4311144, size.height * 0.3982482);
    path_0.lineTo(size.width * 0.4502287, size.height * 0.3831168);
    path_0.lineTo(size.width * 0.4579221, size.height * 0.3770268);
    path_0.lineTo(size.width * 0.4516667, size.height * 0.3694696);
    path_0.lineTo(size.width * 0.4301727, size.height * 0.3435061);
    path_0.lineTo(size.width * 0.4301703, size.height * 0.3435061);
    path_0.cubicTo(size.width * 0.4282263, size.height * 0.3411582, size.width * 0.4282263, size.height * 0.3377810,
        size.width * 0.4301703, size.height * 0.3354331);
    path_0.lineTo(size.width * 0.4301727, size.height * 0.3354307);
    path_0.lineTo(size.width * 0.4516667, size.height * 0.3094696);
    path_0.lineTo(size.width * 0.4579173, size.height * 0.3019173);
    path_0.lineTo(size.width * 0.4502336, size.height * 0.2958273);
    path_0.lineTo(size.width * 0.4311290, size.height * 0.2806837);
    path_0.cubicTo(size.width * 0.4296058, size.height * 0.2794745, size.width * 0.4287226, size.height * 0.2776448,
        size.width * 0.4287226, size.height * 0.2757129);
    path_0.lineTo(size.width * 0.4287226, size.height * 0.2757105);
    path_0.lineTo(size.width * 0.4287105, size.height * 0.2438273);
    path_0.cubicTo(size.width * 0.4287105, size.height * 0.2438273, size.width * 0.4287105, size.height * 0.2438273,
        size.width * 0.4287105, size.height * 0.2438273);
    path_0.cubicTo(size.width * 0.4287105, size.height * 0.2421433, size.width * 0.4293771, size.height * 0.2405367,
        size.width * 0.4305669, size.height * 0.2393474);
    path_0.lineTo(size.width * 0.4943212, size.height * 0.1755922);
    path_0.cubicTo(size.width * 0.4968029, size.height * 0.1731114, size.width * 0.5008054, size.height * 0.1731114,
        size.width * 0.5032871, size.height * 0.1755922);
    path_0.lineTo(size.width * 0.5670414, size.height * 0.2393474);
    path_0.cubicTo(size.width * 0.5682311, size.height * 0.2405375, size.width * 0.5688954, size.height * 0.2421440,
        size.width * 0.5688954, size.height * 0.2438297);
    path_0.lineTo(size.width * 0.5784696, size.height * 0.2438297);
    path_0.lineTo(size.width * 0.5688954, size.height * 0.2438297);
    path_0.lineTo(size.width * 0.5688978, size.height * 0.5230535);
    path_0.close();

    Paint paint_0_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.01919679;
    paint_0_stroke.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_stroke);

    // Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    // paint_0_fill.color = Color(0xff000000).withOpacity(1.0);
    // canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.4650073, size.height * 0.6986034);
    path_1.cubicTo(size.width * 0.4650073, size.height * 0.6799684, size.width * 0.4801752, size.height * 0.6648005,
        size.width * 0.4988102, size.height * 0.6648005);
    path_1.cubicTo(size.width * 0.5174453, size.height * 0.6648005, size.width * 0.5326156, size.height * 0.6799684,
        size.width * 0.5326156, size.height * 0.6986034);
    path_1.cubicTo(size.width * 0.5326156, size.height * 0.7172384, size.width * 0.5174453, size.height * 0.7324063,
        size.width * 0.4988102, size.height * 0.7324063);
    path_1.cubicTo(size.width * 0.4801776, size.height * 0.7324063, size.width * 0.4650073, size.height * 0.7172384,
        size.width * 0.4650073, size.height * 0.6986034);
    path_1.close();

    Paint paint_1_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.02800998;
    paint_1_stroke.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_1, paint_1_stroke);

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_1, paint_1_fill);

    Paint paint_2_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.02288000;
    paint_2_stroke.color = Colors.white.withOpacity(1.0);
    canvas.drawRRect(
        RRect.fromRectAndCorners(
            Rect.fromLTWH(
                size.width * 0.01144000, size.height * 0.01144000, size.width * 0.9746959, size.height * 0.9746959),
            bottomRight: Radius.circular(size.width * 0.06383552),
            bottomLeft: Radius.circular(size.width * 0.06383552),
            topLeft: Radius.circular(size.width * 0.06383552),
            topRight: Radius.circular(size.width * 0.06383552)),
        paint_2_stroke);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
