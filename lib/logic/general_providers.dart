import 'package:tube/logic/blocs/active_tariff/active_tariff_bloc.dart';
import 'package:tube/logic/blocs/admin/big_service/big_service_bloc.dart';
import 'package:tube/logic/blocs/admin/booking_admin/booking_admin_bloc.dart';
import 'package:tube/logic/blocs/admin/booking_detail_data_bloc/booking_detail_data_bloc.dart';
import 'package:tube/logic/blocs/admin/clients/client_bloc.dart';
import 'package:tube/logic/blocs/admin/tariff_admin_bloc/tariff_admin_bloc.dart';
import 'package:tube/logic/blocs/admin/workers/worker_bloc.dart';
import 'package:tube/logic/blocs/big_event/big_event_bloc.dart';
import 'package:tube/logic/blocs/client/big_event_client/big_event_client_bloc.dart';
import 'package:tube/logic/blocs/club_client/club_client_bloc.dart';
import 'package:tube/logic/blocs/gifts/gift_bloc.dart';
import 'package:tube/logic/blocs/notification/notification_bloc.dart';
import 'package:tube/logic/blocs/tariff/tariff_bloc.dart';
import 'package:tube/logic/blocs/trainer/big_event_trainer/big_event_trainer_bloc.dart';
import 'package:tube/logic/blocs/trainer/tariff_trainer_bloc/tariff_trainer_bloc.dart';
import 'package:tube/logic/blocs/trainer/work_time/work_time_bloc.dart';
import 'package:tube/logic/model/status_enum.dart';

class AdminBlocMap {
  final Map<String, WorkerBloc> _mapWorkerBlocs = {};
  final Map<String, BookingAdminBloc> _bookingAdminBloc = {};
  final Map<String, TariffAdminBloc> _mapTariffAdminBloc = {};
  final Map<String, ControllerWorkTimeBloc> _workTrainerTimeBloc = {};
  final Map<String, BookingDetailDataBloc> _bookingDetailDataBloc = {};
  final Map<String, NotificationBloc> _notifyMap = {};
  final Map<String, ClubClientBloc> _clubClientMap = {};
  final Map<String, AdminBigEventBloc> _adminBigEvent = {};
  final Map<String, AdminBigServiceBloc> _adminBigServiceBloc = {};
  final Map<String, AdminClientBloc> _adminClientBloc = {};

  AdminBlocMap();

  T? getByType<T>(String clubId) {
    if (T == WorkerBloc) {
      return _mapWorkerBlocs[clubId] ??= WorkerBloc(
        clubId: clubId,
      ) as dynamic;
    }
    if (T == BookingAdminBloc) {
      return _bookingAdminBloc[clubId] ??= BookingAdminBloc(
        clubId: clubId,
      ) as dynamic;
    }
    if (T == TariffAdminBloc) {
      return _mapTariffAdminBloc[clubId] ??= TariffAdminBloc(
        clubId: clubId,
      ) as dynamic;
    }
    if (T == ControllerWorkTimeBloc) {
      return _workTrainerTimeBloc[clubId] ??= ControllerWorkTimeBloc(
        clubId: clubId,
      ) as dynamic;
    }
    // if (T == BookingDetailDataBloc) {
    //   return _bookingDetailDataBloc[clubId] ??= BookingDetailDataBloc(
    //     clubId: clubId,
    //   ) as dynamic;
    // }
    // if (T == NotificationBloc) {
    //   return _notifyMap[clubId] ??= NotificationBloc(
    //     clubId: clubId,
    //   ) as dynamic;
    // }
    if (T == ClubClientBloc) {
      return _clubClientMap[clubId] ??= ClubClientBloc(
        clubId: clubId,
      ) as dynamic;
    }
    if (T == AdminBigEventBloc) {
      return _adminBigEvent[clubId] ??= AdminBigEventBloc(
        clubId: clubId,
      ) as dynamic;
    }
    if (T == AdminBigServiceBloc) {
      return _adminBigServiceBloc[clubId] ??= AdminBigServiceBloc(
        clubId: clubId,
      ) as dynamic;
    }

    if (T == AdminClientBloc) {
      return _adminClientBloc[clubId] ??= AdminClientBloc(
        clubId: clubId,
      ) as dynamic;
    }
    throw 'Not found';
  }

  AdminBigEventBloc getAdminBigEventBloc(String clubId) {
    return _adminBigEvent[clubId] ??= AdminBigEventBloc(
      clubId: clubId,
    );
  }

  WorkerBloc getWorkerBloc(String clubId) {
    return _mapWorkerBlocs[clubId] ??= WorkerBloc(
      clubId: clubId,
    );
  }

  BookingAdminBloc getBookingAdminBloc(String clubId) {
    return _bookingAdminBloc[clubId] ??= BookingAdminBloc(
      clubId: clubId,
    );
  }

  TariffAdminBloc getTariffAdminBloc(String clubId) {
    return _mapTariffAdminBloc[clubId] ??= TariffAdminBloc(
      clubId: clubId,
    );
  }

  BookingDetailDataBloc getBookingDetailDataBloc(String bookingId) {
    return _bookingDetailDataBloc[bookingId] ??= BookingDetailDataBloc(
      bookingId: bookingId,
    );
  }

  ControllerWorkTimeBloc getControllerWorkTimeBloc({
    required String trainerId,
    required String clubId,
  }) {
    return _workTrainerTimeBloc[trainerId + clubId] ??= ControllerWorkTimeBloc(
      clubId: clubId,
      trainerId: trainerId,
    );
  }

  NotificationBloc getNotifcationBloc(String clubId) {
    return _notifyMap[clubId] ??= NotificationBloc(
      clubId: clubId,
      participance: StatusEnum.admin,
    );
  }

  ClubClientBloc getClubClient(String clubId) {
    return _clubClientMap[clubId] ??= ClubClientBloc(
      clubId: clubId,
    );
  }

  AdminBigServiceBloc getAdminBigServiceBloc(String clubId) {
    return _adminBigServiceBloc[clubId] ??= AdminBigServiceBloc(
      clubId: clubId,
    );
  }

  AdminClientBloc getAdminClientBloc(String clubId) {
    return _adminClientBloc[clubId] ??= AdminClientBloc(
      clubId: clubId,
    );
  }

  void close() {
    for (final element in _adminBigEvent.values) {
      element.close();
    }
    for (final element in _mapWorkerBlocs.values) {
      element.close();
    }
    for (final element in _bookingAdminBloc.values) {
      element.close();
    }
    for (final element in _mapTariffAdminBloc.values) {
      element.close();
    }
    for (final element in _workTrainerTimeBloc.values) {
      element.close();
    }
    for (final element in _bookingDetailDataBloc.values) {
      element.close();
    }
    for (final element in _notifyMap.values) {
      element.close();
    }
    for (final element in _clubClientMap.values) {
      element.close();
    }
    for (final element in _adminBigServiceBloc.values) {
      element.close();
    }
    for (final element in _adminClientBloc.values) {
      element.close();
    }

    _adminBigServiceBloc.clear();
    _adminBigEvent.clear();
    _mapWorkerBlocs.clear();
    _bookingAdminBloc.clear();
    _mapTariffAdminBloc.clear();
    _workTrainerTimeBloc.clear();
    _bookingDetailDataBloc.clear();
    _notifyMap.clear();
    _clubClientMap.clear();
    _adminClientBloc.clear();
  }
}

class TrainerBlocMap {
  final Map<String, ControllerWorkTimeBloc> _workTimeBloc = {};
  final Map<String, TariffTrainerBloc> _tariffTrainerBloc = {};
  final Map<String, NotificationBloc> _notifyMap = {};
  final Map<String, BigEventTrainerBloc> _trainerBlocMap = {};

  TrainerBlocMap();

  ControllerWorkTimeBloc getWorkTimeBloc(String clubId) {
    return _workTimeBloc[clubId] ??= ControllerWorkTimeBloc(
      clubId: clubId,
    );
  }

  TariffTrainerBloc getTariffTrainerBloc(String clubId) {
    return _tariffTrainerBloc[clubId] ??= TariffTrainerBloc(
      clubId: clubId,
    );
  }

  NotificationBloc getNotifcationBloc(String clubId) {
    return _notifyMap[clubId] ??= NotificationBloc(
      clubId: clubId,
      participance: StatusEnum.trainer,
    );
  }

  BigEventTrainerBloc getBigEvent(String clubId) {
    return _trainerBlocMap[clubId] ??= BigEventTrainerBloc(clubId: clubId);
  }

  void close() {
    for (final element in _tariffTrainerBloc.values) {
      element.close();
    }
    for (final element in _workTimeBloc.values) {
      element.close();
    }
    for (final element in _notifyMap.values) {
      element.close();
    }
    for (final element in _trainerBlocMap.values) {
      element.close();
    }
    _tariffTrainerBloc.clear();
    _workTimeBloc.clear();
    _notifyMap.clear();
    _trainerBlocMap.clear();
  }
}

class UserBlocMap {
  final Map<String, ActiveTariffBloc> _activeTariffBlocMap = {};
  final Map<String, TariffBloc> _getTariffBloc = {};
  final Map<String, NotificationBloc> _notifyMap = {};
  final Map<String, BigEventClientBloc> _bigEventMap = {};
  // final Map<String, TariffTrainerBloc> _tariffTrainerBloc = {};
  final Map<String, GiftBloc> _giftBloc = {};

  UserBlocMap();

  ActiveTariffBloc getActiveBlocMap(String clubId) {
    return _activeTariffBlocMap[clubId] ??= ActiveTariffBloc(
      clubId: clubId,
    );
  }

  TariffBloc getTariffBloc(String clubId) {
    return _getTariffBloc[clubId] ??= TariffBloc(
      clubId: clubId,
    );
  }

  NotificationBloc getNotifcationBloc(String clubId) {
    return _notifyMap[clubId] ??= NotificationBloc(
      clubId: clubId,
      participance: StatusEnum.participance,
    );
  }

  BigEventClientBloc getBigEventClientBloc(String clubId) {
    return _bigEventMap[clubId] ??= BigEventClientBloc(
      clubId: clubId,
    );
  }

  // final Map<String, GiftBloc> _giftBloc = {};
  GiftBloc getGiftBloc(String clubId) {
    return _giftBloc[clubId] ??= GiftBloc(
      clubId: clubId,
    );
  }

  void close() {
    for (final element in _activeTariffBlocMap.values) {
      element.close();
    }
    for (final element in _getTariffBloc.values) {
      element.close();
    }
    for (final element in _notifyMap.values) {
      element.close();
    }
    for (final element in _bigEventMap.values) {
      element.close();
    }
    for (final elem in _giftBloc.values) {
      elem.close();
    }
    _bigEventMap.clear();
    _activeTariffBlocMap.clear();
    _getTariffBloc.clear();
    _notifyMap.clear();
    _giftBloc.clear();
  }
}
