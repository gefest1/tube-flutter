import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:provider/provider.dart';

import 'package:tube/logic/blocs/admin/worker_time/work_time_wrapper.dart';
import 'package:tube/logic/blocs/admin/workers/worker_bloc.dart';

import 'package:tube/logic/blocs/finance/finance_bloc.dart';

import 'package:tube/logic/blocs/participance/participance_bloc.dart';

import 'package:tube/logic/blocs/user/user_bloc.dart';
import 'package:tube/logic/blocs/club/club_bloc.dart';
import 'package:tube/logic/general_providers.dart';

class BlocWrapper extends StatefulWidget {
  final Widget child;

  const BlocWrapper({
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  State<BlocWrapper> createState() => _BlocWrapperState();
}

UserBloc? userBloc;
ParticipanceBloc? participanceBloc;
ClubBloc? clubBloc;
WorkerBloc? workerBloc;
AdminBlocMap? adminBlocMap;
TrainerBlocMap? trainerBlocMap;
UserBlocMap? userBlocMap;
FinanceBloc? financeBloc;

class _BlocWrapperState extends State<BlocWrapper> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<UserBloc>(
          create: (_) {
            return userBloc ??= UserBloc();
          },
        ),
        BlocProvider<ParticipanceBloc>(
          create: (_) {
            return participanceBloc ??= ParticipanceBloc();
          },
        ),
        BlocProvider<ClubBloc>(
          create: (_) {
            return clubBloc ??= ClubBloc();
          },
        ),
      ],
      child: RolesWrapper(
        child: WorkTimeWrapper(
          child: widget.child,
        ),
      ),
    );
  }
}

// ///
// ///
// ///
// ///
// ///

class RolesWrapper extends StatefulWidget {
  final Widget child;

  const RolesWrapper({
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  State<RolesWrapper> createState() => _RolesWrapperState();
}

class _RolesWrapperState extends State<RolesWrapper> {
  TrainerBlocMap? _trainerBlocMap;
  UserBlocMap? _userBlocMap;
  AdminBlocMap? _adminBlocMap;

  @override
  void dispose() {
    _trainerBlocMap?.close();
    _userBlocMap?.close();
    _adminBlocMap?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<TrainerBlocMap>(
          create: (_) => trainerBlocMap ??= TrainerBlocMap(),
        ),
        Provider<UserBlocMap>(
          create: (_) => userBlocMap ??= UserBlocMap(),
        ),
        Provider<AdminBlocMap>(
          create: (_) => adminBlocMap ??= AdminBlocMap(),
        ),
      ],
      child: widget.child,
    );
  }
}
