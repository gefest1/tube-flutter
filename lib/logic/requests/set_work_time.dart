import 'package:graphql/client.dart';

final setWorkTime = gql('''
mutation (
  \$clubId:String!
	\$workTime: [WorkTimeInput!]!
	\$trainerId: String
){
  setWorkTime(
    clubId: \$clubId
  	workTime: \$workTime
    trainerId: \$trainerId
  ) {
    _id
    clubId
    userId
    workTime{
      startTime
      endTime
		}
  }
}
''');

// final getWorkTime = gql('''
// query (
//   \$clubId:String!
// 	\$trainerId: String
// ){
//   getWorkTime( 
//     clubId: \$clubId
//     trainerId: \$trainerId
//   ) {
//     _id
//     clubId
//     userId
//     workTime{
//       startTime
//       endTime
// 		}
//   }
// }''');
