import 'package:graphql/client.dart';

final startSession = gql('''
mutation (
	\$bookingId: String!
	\$trainTimeId: String!
){
  startTraining(
    bookingId: \$bookingId
    trainTimeId: \$trainTimeId
  ){
    trainTimes {
    	_id  
      come
      date
      endTime      
    }
  }
}''');
