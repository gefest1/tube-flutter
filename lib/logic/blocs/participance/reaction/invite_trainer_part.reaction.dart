part of '../participance_bloc.dart';

extension InvitieTrainerReaction on ParticipanceBloc {
  void inviteTrainerParticipanceEventReaction(
    InviteParticipanceEvent event,
    Emitter<ParticipanceState> emitter,
  ) async {
    try {
      final mutationResult = await graphqlClient.mutate$inviteUserMy(
        Options$Mutation$inviteUserMy(
          variables: Variables$Mutation$inviteUserMy(
            clubId: event.clubId,
            status: event.participance.genratedValue,
          ),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (mutationResult.hasException) throw mutationResult.exception!;
      final data = mutationResult.parsedData!;

      final List<Participance> parts = data.inviteUserMy
          .map((e) => Participance.fromMap(e.toJson()))
          .toList();
      if (parts.isNotEmpty) {
        MainDrawerState.currentParticipance =
            parts[MainDrawerState.index ??= 0];
        emitter(DataParticipanceState(parts, {
          for (Participance part in parts) part.clubId!: part,
        }));
      }
      if (parts.isEmpty) emitter(const DataParticipanceState([], {}));
      event.completer.complete(null);

      add(const GetDataParticipanceEvent());
    } catch (e, s) {
      event.completer.completeError(e, s);
      log(e.toString());
    }
  }
}
