part of '../participance_bloc.dart';

extension AcceptEventExtension on ParticipanceBloc {
  void acceptParticipanceEventReaction(
    AcceptParticipanceEvent event,
    Emitter<ParticipanceState> emitter,
  ) async {
    if (state is! LoadingParticipanceState) {
      emitter(LoadingParticipanceState(state));
    }
    try {
      final queryResult = await graphqlClient.mutate$acceptUser(
        Options$Mutation$acceptUser(
          variables: Variables$Mutation$acceptUser(
            clubId: event.clubId,
            status: event.participance.genratedValue,
            accept: event.accept,
            iinData: event.iin,
          ),
        ),
      );

      if (queryResult.hasException) queryResult.exception!;

      final List<Participance> parts = queryResult.parsedData!.acceptUser
          .map((e) => Participance.fromMap(e.toJson()))
          .toList();
      if (parts.isNotEmpty) {
        MainDrawerState.index ??= 0;
        emitter(DataParticipanceState(parts, {
          for (Participance part in parts) part.clubId!: part,
        }));
      }
      if (parts.isEmpty) emitter(const DataParticipanceState([], {}));
      event.completer?.complete(null);
    } catch (e, s) {
      event.completer?.completeError(e, s);
      if (state is LoadingParticipanceState) {
        emitter(
          (state as LoadingParticipanceState).prevState ??
              const LoadingParticipanceState(),
        );
      }
      log(e.toString());
    }
  }
}
