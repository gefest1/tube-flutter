part of './participance_bloc.dart';

abstract class ParticipanceEvent {
  const ParticipanceEvent();
}

class GetDataParticipanceEvent extends ParticipanceEvent {
  final int recursion;
  const GetDataParticipanceEvent({this.recursion = 0});
}

class AcceptParticipanceEvent extends ParticipanceEvent {
  final String clubId;
  final StatusEnum participance;
  final bool accept;
  final String? iin;
  final Completer? completer;

  const AcceptParticipanceEvent({
    required this.clubId,
    required this.participance,
    required this.accept,
    this.iin,
    this.completer,
  });
}

abstract class InviteParticipanceEvent extends ParticipanceEvent {
  final String clubId;
  final Completer completer;

  StatusEnum get participance;

  const InviteParticipanceEvent({
    required this.clubId,
    required this.completer,
  });
}

class InviteTrainerParticipanceEvent extends InviteParticipanceEvent {
  @override
  StatusEnum get participance => StatusEnum.trainer;

  const InviteTrainerParticipanceEvent({
    required String clubId,
    required Completer completer,
  }) : super(
          clubId: clubId,
          completer: completer,
        );
}

class InviteClientParticipanceEvent extends InviteParticipanceEvent {
  @override
  StatusEnum get participance => StatusEnum.participance;

  const InviteClientParticipanceEvent({
    required String clubId,
    required Completer completer,
  }) : super(
          clubId: clubId,
          completer: completer,
        );
}

class AddTagParticipanceEvent extends ParticipanceEvent {
  final String clubId;
  final String tagName;
  final Completer? completer;

  const AddTagParticipanceEvent({
    required this.clubId,
    required this.tagName,
    this.completer,
  });
}

class EditTagParticipanceEvent extends ParticipanceEvent {
  final String clubId;
  final String tagName;
  final String oldTagName;
  final Completer? completer;

  const EditTagParticipanceEvent({
    required this.clubId,
    required this.tagName,
    required this.oldTagName,
    this.completer,
  });
}

class RemoveTagParticipanceEvent extends ParticipanceEvent {
  final String clubId;
  final String tagName;
  final Completer? completer;

  const RemoveTagParticipanceEvent({
    required this.clubId,
    required this.tagName,
    this.completer,
  });
}
