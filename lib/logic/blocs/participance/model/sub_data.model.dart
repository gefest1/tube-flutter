import 'dart:convert';

import 'package:equatable/equatable.dart';

class SubData extends Equatable {
  final String? facebook;
  final String? telegram;
  final String? whatsapp;
  final String? instagram;
  final String? description;

  const SubData({
    this.facebook,
    this.telegram,
    this.whatsapp,
    this.instagram,
    this.description,
  });

  SubData copyWith({
    String? facebook,
    String? telegram,
    String? whatsapp,
    String? instagram,
    String? description,
  }) {
    return SubData(
      facebook: facebook ?? this.facebook,
      telegram: telegram ?? this.telegram,
      whatsapp: whatsapp ?? this.whatsapp,
      instagram: instagram ?? this.instagram,
      description: description ?? this.description,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'facebook': facebook,
      'telegram': telegram,
      'whatsapp': whatsapp,
      'instagram': instagram,
      'description': description,
    };
  }

  factory SubData.fromMap(Map<String, dynamic> map) {
    return SubData(
      facebook: map['facebook'],
      telegram: map['telegram'],
      whatsapp: map['whatsapp'],
      instagram: map['instagram'],
      description: map['description'],
    );
  }

  String toJson() => json.encode(toMap());

  factory SubData.fromJson(String source) =>
      SubData.fromMap(json.decode(source));

  @override
  String toString() {
    return 'SubData(facebook: $facebook, telegram: $telegram, whatsapp: $whatsapp, instagram: $instagram, description: $description)';
  }

  @override
  List<Object?> get props {
    return [
      facebook,
      telegram,
      whatsapp,
      instagram,
      description,
    ];
  }
}
