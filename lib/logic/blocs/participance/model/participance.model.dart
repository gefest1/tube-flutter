import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:tube/logic/blocs/club/club.model.dart';
import 'package:tube/logic/blocs/participance/model/sub_data.model.dart';
import 'package:tube/logic/model/status_enum.dart';

class Participance extends Equatable {
  final bool? accepted;
  final String? clubId;
  final StatusEnum? status;
  final Club? club;
  final SubData? subData;

  const Participance({
    this.accepted,
    this.clubId,
    this.status,
    this.club,
    this.subData,
  });

  Participance copyWith({
    bool? accepted,
    String? clubId,
    StatusEnum? status,
    Club? club,
    SubData? subData,
  }) {
    return Participance(
      accepted: accepted ?? this.accepted,
      clubId: clubId ?? this.clubId,
      status: status ?? this.status,
      club: club ?? this.club,
      subData: subData ?? this.subData,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'accepted': accepted,
      'clubId': clubId,
      'status': status?.toMap(),
      'club': club?.toMap(),
      'subData': subData?.toMap(),
    };
  }

  factory Participance.fromMap(Map<String, dynamic> map) {
    return Participance(
      accepted: map['accepted'],
      clubId: map['clubId'],
      status: map['status'] != null ? StatusEnum.fromMap(map['status']) : null,
      club: map['club'] != null ? Club.fromMap(map['club']) : null,
      subData: map['subData'] != null ? SubData.fromMap(map['subData']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Participance.fromJson(String source) =>
      Participance.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Participance(accepted: $accepted, clubId: $clubId, status: $status, club: $club)';
  }

  @override
  List<Object?> get props => [
        accepted,
        clubId,
        status,
        club,
        subData,
      ];
}
