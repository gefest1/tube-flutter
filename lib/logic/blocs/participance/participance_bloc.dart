import 'dart:async';
import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/blocs/club/club.model.dart';

import 'package:tube/logic/blocs/participance/model/participance.model.dart';
import 'package:tube/logic/model/status_enum.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';

part './participance_state.dart';
part './participance_event.dart';
part 'reaction/invite_trainer_part.reaction.dart';
part 'reaction/get_data.dart';
part 'reaction/accept_club.dart';
part 'reaction/add_tag_reaction.dart';

class ParticipanceBloc
    extends HydratedBloc<ParticipanceEvent, ParticipanceState> {
  StreamSubscription? _str;

  ParticipanceBloc() : super(const LoadingParticipanceState()) {
    on<GetDataParticipanceEvent>(getDataParticipanceEventReaction);
    on<AcceptParticipanceEvent>(acceptParticipanceEventReaction);
    on<InviteParticipanceEvent>(inviteTrainerParticipanceEventReaction);
    on<AddTagParticipanceEvent>(addTagReaction);
    on<EditTagParticipanceEvent>(editTagReaction);
    on<RemoveTagParticipanceEvent>(removeTagReaction);
    _str = stream.listen((state) {
      if (state is DataParticipanceState) {
        // log(message)
        if (state.participances.isEmpty) return;
        MainDrawerState.currentParticipance =
            state.participances[MainDrawerState.index ??= 0];
      }
    });

    if (sharedBox.containsKey('token')) {
      add(const GetDataParticipanceEvent());
    }
  }

  @override
  Future<void> close() {
    _str?.cancel();
    _str = null;
    return super.close();
  }

  @override
  ParticipanceState? fromJson(Map<String, dynamic> json) {
    if (json['type'] == (DataParticipanceState).toString()) {
      final List<Participance> list = (json['data'] as List<dynamic>)
          .map((e) => Participance.fromMap(e))
          .toList();
      final map = {
        for (Participance part in list) part.clubId!: part,
      };
      return DataParticipanceState(
        (json['data'] as List<dynamic>)
            .map((e) => Participance.fromMap(e))
            .toList(),
        map,
      );
    }
    return const LoadingParticipanceState();
  }

  @override
  Map<String, dynamic>? toJson(ParticipanceState state) {
    if (state is LoadingParticipanceState) {
      if (state.prevState != null) {
        state = state.prevState!;
      }
    }
    if (state is DataParticipanceState) {
      return {
        "type": "DataParticipanceState",
        "data": state.participances.map((e) => e.toMap()).toList(),
      };
    }

    return null;
  }
}
