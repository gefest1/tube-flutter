part of '../gift_bloc.dart';

extension GetGiftReaction on GiftBloc {
  void getGiftReaction(
    GetGiftEvent event,
    Emitter<GiftState> emit,
  ) async {
    try {
      final queryRes = await graphqlClient.query$getGifts(
        Options$Query$getGifts(
          fetchPolicy: FetchPolicy.networkOnly,
          variables: Variables$Query$getGifts(clubId: clubId),
        ),
      );

      if (queryRes.hasException) throw queryRes.exception!;
      emit(
        DataGiftState(
          queryRes.parsedData!.getGifts
              .map((e) => GiftModel.fromMap(e.toJson()))
              .toList(),
        ),
      );
    } catch (e, s) {
      log(e.toString(), stackTrace: s);
    }
  }
}
