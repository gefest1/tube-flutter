part of '../gift_bloc.dart';

// final _doc = gql('''
// fragment TariffFragment on TariffGraph {
//   _id
//   clubId
//   trainTimes {
//       date
//       come
//       _id
//       endTime
//       trainerId
//     }
//     bookingId
//     booking {
//       _id
//       name
//       count
//       price
//       percent
//       timePerMinute
//     }
//     user {
//       _id
//       iin
//       date
//       fullName
//       email
//       phoneNumber
//       photoUrl {
//         M
//         XL
//         thumbnail
//       }
//     }
// }
// mutation (
//   \$giftId: String!
//   \$inputTrain: [TrainTimesInput!]!
//   \$dateTime: DateTime

// ){
//   useGift(
//     giftId: \$giftId
//     dateTime: \$dateTime
//     inputTrain: \$inputTrain
//   ){
//     ...TariffFragment
//   }
// }
// ''');

extension UseGiftReaction on GiftBloc {
  void useGiftReaction(
    UseGiftEvent event,
    Emitter<GiftState> emit,
  ) async {
    try {
      final mutationResult = await graphqlClient.mutate$useGift(
        Options$Mutation$useGift(
          variables: Variables$Mutation$useGift(
            giftId: event.giftId,
            dateTime: event.dateTime,
            inputTrain: event.inputTrains.map((e) {
              return Input$TrainTimesInput(
                date: e.date,
                endTime: e.endTime,
                trainerId: e.trainerId,
              );
            }).toList(),
          ),
        ),
      );

      if (mutationResult.hasException) throw mutationResult.exception!;
      event.completer?.complete();
    } catch (e, s) {
      event.completer?.completeError(e, s);
    }
  }
}
