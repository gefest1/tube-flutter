part of '../finance_bloc.dart';

extension GetReactionBloc on FinanceBloc {
  void getReaction(
    GetFinanceEvent event,
    Emitter<FinanceState> emit,
  ) async {
    try {
      var variables = Variables$Query$getPeriodFinances(
        endTime: event.endTime,
        startTime: event.startTime,
      );
      if (status == StatusEnum.admin) {
        variables = variables.copyWith(adminClubId: clubId);
      } else if (status == StatusEnum.trainer) {
        variables = variables.copyWith(trainerClubId: clubId);
      }
      final queryResult = await graphqlClient.query$getPeriodFinances(
        Options$Query$getPeriodFinances(
          variables: variables,
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (queryResult.hasException) throw queryResult.exception!;

      final newDocs = queryResult.parsedData!.getPeriodFinances
          .map((e) => Finance.fromMap(e.toJson()))
          .toList();
      feedData(newDocs, emit);
    } catch (e) {
      log(e.toString());
    }
  }
}
