part of './finance_bloc.dart';

abstract class FinanceState extends Equatable {
  const FinanceState();
}

class LoadingFinanceState extends FinanceState {
  @override
  List<Object?> get props => [];
}

class DataFinanceState extends FinanceState {
  final List<Finance> data;

  const DataFinanceState(this.data);

  @override
  List<Object?> get props => [data];
}
