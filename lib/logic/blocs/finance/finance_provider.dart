import 'package:tube/logic/blocs/finance/finance_bloc.dart';
import 'package:tube/logic/model/status_enum.dart';

class FinanceProvider {
  FinanceProvider();

  final Map<String, FinanceBloc> _map = {};

  FinanceBloc getFinance(
    StatusEnum status,
    String clubId,
  ) {
    return _map['${status.toString()}' '_' '$clubId'] ??= FinanceBloc(
      clubId: clubId,
      status: status,
    );
  }

  void cancel() {
    for (final element in _map.values) {
      element.close();
    }
    _map.clear();
  }
}
