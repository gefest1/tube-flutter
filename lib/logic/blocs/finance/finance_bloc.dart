import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/finance/finance_provider.dart';
import 'package:tube/logic/blocs/finance/model/finance.model.dart';
import 'package:tube/logic/model/status_enum.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';

part './finance_state.dart';
part './finance_event.dart';

part './reaction/get_reaction.dart';

class FinanceBloc extends HydratedBloc<FinanceEvent, FinanceState> {
  final String clubId;
  final StatusEnum status;
  List<Finance> finances = [];
  final Map<String, bool> financeMap = {};

  FinanceBloc({
    required this.clubId,
    required this.status,
  }) : super(LoadingFinanceState()) {
    on<GetFinanceEvent>(getReaction);
  }

  @override
  Future<void> close() {
    finances.clear();

    return super.close();
  }

  @override
  Map<String, dynamic>? toJson(FinanceState state) => null;

  @override
  FinanceState? fromJson(Map<String, dynamic> json) => null;
}

extension FeedData on FinanceBloc {
  void feedData(List<Finance> finances, Emitter emit) {
    final newData = [
      ...this.finances,
      ...finances.where((element) {
        if (!financeMap.containsKey(element.id)) {
          financeMap[element.id!] = true;
          return true;
        }
        return false;
      })
    ];

    newData.sort(
      (a, b) => b.createdAt!.compareTo(a.createdAt!),
    );

    emit(DataFinanceState(newData));
    this.finances = newData;
  }
}

FinanceBloc getFinanceBloc(BuildContext context) {
  final part = MainDrawerState.currentParticipanceValue.value!;
  return Provider.of<FinanceProvider>(context, listen: false)
      .getFinance(part.status!, part.clubId!);
}
