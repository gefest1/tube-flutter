part of './club_client_bloc.dart';

abstract class ClubClientState extends Equatable {
  const ClubClientState();
}

class InitClubClientState extends ClubClientState {
  const InitClubClientState();
  @override
  List<Object?> get props => [];
}

class DataClubClientState extends ClubClientState {
  final List<User> users;

  const DataClubClientState({
    required this.users,
  });
  @override
  List<Object?> get props => [users];
}
