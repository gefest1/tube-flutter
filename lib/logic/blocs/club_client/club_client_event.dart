part of './club_client_bloc.dart';

abstract class ClubClientEvent {
  const ClubClientEvent();
}

class GetClubClientEvent extends ClubClientEvent {
  const GetClubClientEvent();
}
