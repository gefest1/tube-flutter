import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:tube/logic/model/photourl.dart';
import 'package:tube/utils/date_time_parse.dart';

class User extends Equatable {
  final String? id;
  final DateTime? date;
  final String? email;
  final String? fullName;
  final String? iin;
  final String? phoneNumber;
  final PhotoUrl? photoUrl;
  final bool? isAdmin;

  const User({
    this.id,
    this.date,
    this.email,
    this.fullName,
    this.iin,
    this.phoneNumber,
    this.photoUrl,
    this.isAdmin,
  });

  User copyWith({
    String? id,
    DateTime? date,
    String? email,
    String? fullName,
    String? iin,
    String? phoneNumber,
    PhotoUrl? photoUrl,
    bool? isAdmin,
  }) {
    return User(
      id: id ?? this.id,
      date: date ?? this.date,
      email: email ?? this.email,
      fullName: fullName ?? this.fullName,
      iin: iin ?? this.iin,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      photoUrl: photoUrl ?? this.photoUrl,
      isAdmin: isAdmin ?? this.isAdmin,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'date': date?.millisecondsSinceEpoch,
      'email': email,
      'fullName': fullName,
      'iin': iin,
      'phoneNumber': phoneNumber,
      'photoUrl': photoUrl,
      'isAdmin': isAdmin,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      id: map['_id'],
      date: dateTimeParse(map['date']),
      email: map['email'],
      fullName: map['fullName'],
      iin: map['iin'],
      phoneNumber: map['phoneNumber'],
      photoUrl: map['photoUrl'] == null
          ? null
          : PhotoUrl.fromMap(
              map['photoUrl'],
            ),
      isAdmin: map['isAdmin'],
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) => User.fromMap(json.decode(source));

  @override
  String toString() {
    return 'User(id: $id, date: $date, email: $email, fullName: $fullName, iin: $iin, phoneNumber: $phoneNumber, photoUrl: $photoUrl)';
  }

  @override
  List<Object?> get props {
    return [id, date, email, fullName, iin, phoneNumber, photoUrl, isAdmin];
  }
}
