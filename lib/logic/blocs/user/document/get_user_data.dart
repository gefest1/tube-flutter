import 'package:graphql/client.dart';

final getDataDocument = gql('''
{
  getUser {
    _id
    email
    iin
    photoUrl {
      M
      XL
      thumbnail
    }
    date
    fullName
    phoneNumber
    isAdmin
  }
}
''');

// final editData = gql('''
//     mutation (
//       \$fullName: String
//       \$phoneNumber: String
//       \$photo: Upload
//     ) {
//       editUser(
//         fullName: \$fullName
//         phoneNumber: \$phoneNumber
//         photo: \$photo
//       ){
//         _id
//         email
//         iin
//         photoUrl {
//           M
//           XL
//           thumbnail
//         }
//         date
//         fullName
//         phoneNumber
//         isAdmin
//       }
//     }
// ''');
