part of './user_bloc.dart';

abstract class UserEvent {
  const UserEvent();
}

class InitUserEvent extends UserEvent {
  final User user;

  const InitUserEvent(this.user);
}

class GetDataUserEvent extends UserEvent {
  final int recursion;
  const GetDataUserEvent({this.recursion = 0});
}

class EditUserEvent extends UserEvent {
  final User user;
  final Uint8List? file;

  const EditUserEvent({
    required this.user,
    this.file,
  });
}
