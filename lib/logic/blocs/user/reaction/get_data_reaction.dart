part of '../user_bloc.dart';

extension GetDataReacion on UserBloc {
  void getData(
    GetDataUserEvent event,
    Emitter<UserState> emit,
  ) async {
    try {
      if (event.recursion == 5) return;
      final queryResult = await graphqlClient.query$getUser(
        Options$Query$getUser(
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (queryResult.hasException) throw queryResult.exception!;

      final user = User.fromMap(queryResult.parsedData!.getUser.toJson());
      emit(DataUserState(user));
    } on OperationException catch (_) {
      log(_.toString(), name: 'OperationException');
      add(GetDataUserEvent(recursion: event.recursion + 1));
      rethrow;
    } catch (e) {
      add(GetDataUserEvent(recursion: event.recursion + 1));
      log(e.toString());
    }
  }
}
