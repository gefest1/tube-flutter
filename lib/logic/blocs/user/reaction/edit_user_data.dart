part of '../user_bloc.dart';

extension UserEditData on UserBloc {
  void editUserData(
    EditUserEvent event,
    Emitter<UserState> emit,
  ) async {
    emit(const LoadingUserState());
    var variables = Variables$Mutation$editUser(
      fullName: event.user.fullName,
      phoneNumber: event.user.phoneNumber,
    );

    if (event.file != null) {
      variables = variables.copyWith(
        photo: http.MultipartFile.fromBytes(
          '',
          await byeImageExif(event.file!),
        ),
      );
    }
    final mutationResult = await graphqlClient.mutate$editUser(
      Options$Mutation$editUser(
        variables: variables,
      ),
    );

    if (mutationResult.hasException) throw mutationResult.exception!;

    final user = User.fromMap(mutationResult.parsedData!.editUser.toJson());
    emit(DataUserState(user));
  }
}
