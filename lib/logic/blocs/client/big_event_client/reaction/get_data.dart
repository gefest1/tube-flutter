part of '../big_event_client_bloc.dart';

extension GetDataReaction on BigEventClientBloc {
  void getDataReaction(
    GetBigEventClientEvent event,
    Emitter<BigEventClientState> emit,
  ) async {
    try {
      final queryRes = await graphqlClient.query$getBigEventsClub(
        Options$Query$getBigEventsClub(
          variables: Variables$Query$getBigEventsClub(clubId: clubId),
          fetchPolicy: graphql.FetchPolicy.networkOnly,
        ),
      );

      if (queryRes.hasException) throw queryRes.exception!;

      final events = queryRes.parsedData!.getBigEventsClub
          .map((e) => BigEventEntity.fromMap(e.toJson()))
          .toList();
      emit(
        DataBigEventClientState(events: events),
      );
    } catch (e, s) {
      log(e.toString(), stackTrace: s);
      rethrow;
    }
  }
}
