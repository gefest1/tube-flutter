part of 'big_event_client_bloc.dart';

abstract class BigEventClientEvent {
  const BigEventClientEvent();
}

class GetBigEventClientEvent extends BigEventClientEvent {
  const GetBigEventClientEvent();
}
