part of '../tariff_bloc.dart';

extension AddTariff on TariffBloc {
  void addTariff(
    AddTariffEvent event,
    Emitter<TariffState> emit,
  ) async {
    if (event.owner != null) {
      addTariffSpecial(event, emit);
      return;
    }

    try {
      final mutationResult = await graphqlClient.mutate$createTariff(
        Options$Mutation$createTariff(
          variables: Variables$Mutation$createTariff(
            clubId: event.booking.clubId!,
            subBooking: event.subBooking == null
                ? null
                : Input$SubBookingInput(
                    count: event.subBooking!.count,
                    price: event.subBooking!.price,
                    deadlineDays: event.subBooking!.deadlineDays,
                  ),
            inputTrain: event.inputTrains.map((e) {
              return Input$TrainTimesInput(
                date: e.date,
                endTime: e.endTime,
                trainerId: e.trainerId,
              );
            }).toList(),
            bookingId: event.booking.id!,
            dateTime: event.dateTime!,
          ),
        ),
      );

      if (mutationResult.hasException) throw mutationResult.exception!;
      final parsedData = mutationResult.parsedData!;

      if (parsedData.createTariff.urlPayment != null) {
        final ans = await Navigator.push(
          navigatorKeyGlobal.currentContext!,
          MaterialPageRoute(
            builder: (_) => PaymentPage(
              initialUrl: parsedData.createTariff.urlPayment!,
            ),
          ),
        );
        if (ans == true && state is DataTariffState) {
          final listTariff = [...(state as DataTariffState).tariffs];
          emit(
            DataTariffState(
              tariffs: listTariff
                ..add(
                  Tariff.fromMap(parsedData.createTariff.toJson())
                      .copyWith(booking: event.booking),
                ),
              stopTariff: (state as DataTariffState).stopTariff,
            ),
          );
        } else {
          throw 'Something went wrong';
        }
      } else if (state is DataTariffState) {
        final listTariff = [...(state as DataTariffState).tariffs];
        emit(
          DataTariffState(
            tariffs: listTariff
              ..add(
                Tariff.fromMap(parsedData.createTariff.toJson())
                    .copyWith(booking: event.booking),
              ),
            stopTariff: (state as DataTariffState).stopTariff,
          ),
        );
      } else {
        add(const GetDataTariffEvent());
      }

      event.completer.complete();
    } catch (e, s) {
      event.completer.completeError(e, s);
    }
  }

  void addTariffSpecial(
    AddTariffEvent event,
    Emitter<TariffState> emit,
  ) async {
    try {
      final mutationResult = await graphqlClient.mutate$createTariff(
        Options$Mutation$createTariff(
          variables: Variables$Mutation$createTariff(
            clubId: event.booking.clubId!,
            inputTrain: event.inputTrains.map((e) {
              return Input$TrainTimesInput(
                date: e.date,
                endTime: e.endTime,
                trainerId: e.trainerId,
              );
            }).toList(),
            bookingId: event.booking.id!,
            email: event.owner,
          ),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (mutationResult.hasException) throw mutationResult.exception!;
      // if (state is DataTariffState) {
      //   final listTariff = [...(state as DataTariffState).tariffs];
      //   emit(
      //     DataTariffState(
      //       listTariff
      //         ..add(
      //           Tariff.fromMap(_mutationResult.data!['createTariff'])
      //               .copyWith(booking: event.booking),
      //         ),
      //     ),
      //   );
      // } else {
      //   add(const GetDataTariffEvent());
      // }

      event.completer.complete();
    } catch (e, s) {
      event.completer.completeError(e, s);
    }
  }
}
