part of '../tariff_bloc.dart';

extension EditTrain on TariffBloc {
  void editTrainReaction(
    EditTariffEvent event,
    Emitter<TariffState> emit,
  ) async {
    final state1 = state;
    try {
      List<Tariff>? tariff;
      List<StopTariff>? stopTariff;
      if (state1 is DataTariffState) {
        tariff = state1.tariffs;
        stopTariff = state1.stopTariff;
      }
      final mutRes = await graphqlClient.mutate$setTrainTimeByDirtInput(
        Options$Mutation$setTrainTimeByDirtInput(
          variables: Variables$Mutation$setTrainTimeByDirtInput(
            tariffId: event.tariffId,
            inputTrain: event.inputs
                .map((e) => Input$TrainTimesInput(
                      date: e.date,
                      endTime: e.endTime,
                      trainerId: e.trainerId,
                    ))
                .toList(),
          ),
        ),
      );

      if (mutRes.hasException) throw mutRes.exception!;

      final trainTimes = mutRes.parsedData!.setTrainTimeByDirtInput
          .map((e) => TrainTime.fromMap(e.toJson()))
          .toList();
      if (tariff != null) {
        emit(
          DataTariffState(
            tariffs: tariff
                .map(
                  (e) => e.id == event.tariffId
                      ? e.copyWith(trainTimes: trainTimes)
                      : e,
                )
                .toList(),
            stopTariff: stopTariff ?? [],
          ),
        );
      }

      event.completer?.complete();
    } catch (e, s) {
      if (event.completer == null) rethrow;
      event.completer?.completeError(e, s);
    }
  }
}
