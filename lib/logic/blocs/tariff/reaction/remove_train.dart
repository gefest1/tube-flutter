part of '../tariff_bloc.dart';

extension RemoveTrainReaction on TariffBloc {
  void removeTrainReaction(
    RemoveTrainTimeEvent event,
    Emitter<TariffState> emit,
  ) {
    if (this.state is! DataTariffState) return;
    final state = this.state as DataTariffState;

    final newTarriff = [...state.tariffs];

    final tariffIndex = newTarriff.indexWhere(
      (element) => element.id == event.tariffId,
    );

    final List<TrainTime> trainTimes = [...newTarriff[tariffIndex].trainTimes!];
    final trainTimesIndex = trainTimes.indexWhere(
      (element) => element.id == event.trainTimeId,
    );
    trainTimes[trainTimesIndex] =
        trainTimes[trainTimesIndex].copyWith(come: true);

    newTarriff[tariffIndex].copyWith(trainTimes: trainTimes);
    emit(
      DataTariffState(
        tariffs: newTarriff,
        stopTariff: state.stopTariff,
      ),
    );
  }

  void setNewTrainReaction(
    SetNewTrainTimeEvent event,
    Emitter<TariffState> emit,
  ) {
    if (this.state is! DataTariffState) return;
    final state = this.state as DataTariffState;

    final newTarriffs = [...state.tariffs];
    final tariffIndex = newTarriffs.indexWhere(
      (element) => element.id == event.tariffId,
    );
    if (tariffIndex == -1) return;
    newTarriffs[tariffIndex] =
        newTarriffs[tariffIndex].copyWith(trainTimes: event.trainTime);

    emit(DataTariffState(
      tariffs: newTarriffs,
      stopTariff: state.stopTariff,
    ));
  }
}
