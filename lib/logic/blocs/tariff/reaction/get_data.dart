part of '../tariff_bloc.dart';

extension GetDataTariffBlocExtension on TariffBloc {
  void getData(GetDataTariffEvent event, Emitter<TariffState> emitter) async {
    try {
      final queryResult = await graphqlClient.query$getTariff(
        Options$Query$getTariff(fetchPolicy: FetchPolicy.networkOnly),
      );
      if (queryResult.hasException) throw queryResult.exception!;

      List<Tariff> tariffs = queryResult.parsedData!.getTarrif
          .map((v) => Tariff.fromMap(v.toJson()))
          .where((element) {
        return element.clubId == clubId;
      }).where((e) {
        final notActive = (e.trainTimes ?? []).where(
          (element) {
            return DateTime.now().microsecondsSinceEpoch >
                    element.endTime!.microsecondsSinceEpoch ||
                element.come == true;
          },
        ).toList();
        return e.trainCount! - notActive.length != 0;
      }).toList();
      emitter(
        DataTariffState(
          tariffs: tariffs,
          stopTariff: queryResult.parsedData!.getStopTarrif
              .map<StopTariff>((e) => StopTariff.fromMap(e.toJson()))
              .toList(),
        ),
      );
    } catch (e) {
      if (event.count <= 5) {
        add(
          GetDataTariffEvent(count: event.count + 1),
        );
      } else {
        rethrow;
      }

      log(e.toString());
    }
  }
}
