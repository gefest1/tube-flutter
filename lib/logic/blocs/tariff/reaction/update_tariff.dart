part of '../tariff_bloc.dart';

extension UpdateTariff on TariffBloc {
  void updateTrainTimeReaction(
    UpdateTrainTimeEvent event,
    Emitter<TariffState> emit,
  ) async {
    try {
      final mutationRes = await graphqlClient.mutate$setTrainTime(
        Options$Mutation$setTrainTime(
          variables: Variables$Mutation$setTrainTime(
            inputTrain: event.trainTime
                .map(
                  (e) => Input$TrainTimesInput(
                    date: e.date!,
                    endTime: e.endTime!,
                    trainerId: e.trainerId!,
                  ),
                )
                .toList(),
            tariffId: event.tariffId,
          ),
        ),
      );

      if (mutationRes.hasException) throw mutationRes.exception!;
      event.completer.complete();

      final newTrainTime = mutationRes.parsedData!.setTrainTime
          .map((e) => TrainTime.fromMap(e.toJson()))
          .toList();
      if (state is DataTariffState) {
        final List<Tariff> newTariff =
            (state as DataTariffState).tariffs.map((e) {
          if (e.id == event.tariffId) {
            return e.copyWith(
              trainTimes: newTrainTime,
            );
          }
          return e;
        }).toList();

        emit(
          DataTariffState(
            tariffs: newTariff,
            stopTariff: (state as DataTariffState).stopTariff,
          ),
        );
      }
    } catch (e, s) {
      event.completer.completeError(e, s);
    }
  }
}
