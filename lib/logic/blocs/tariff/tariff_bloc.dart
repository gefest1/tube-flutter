import 'dart:async';
import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/blocs/tariff/model/stop_tariff.model.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/tariff/model/train_time.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/logic/model/create_tarriff.dart';
import 'package:tube/logic/model/sub_booking.model.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/common/page/payment_page.dart';

part 'tariff_event.dart';
part 'tariff_state.dart';
part 'reaction/get_data.dart';
part 'reaction/add_tariff.dart';
part 'reaction/remove_train.dart';
part 'reaction/update_tariff.dart';
part 'reaction/edit_train.dart';

class TariffBloc extends HydratedBloc<TariffEvent, TariffState> {
  final String clubId;

  TariffBloc({
    required this.clubId,
  }) : super(const LoadingTariffState()) {
    on<GetDataTariffEvent>(getData);
    on<AddTariffEvent>(addTariff);
    on<RemoveTrainTimeEvent>(removeTrainReaction);
    on<SetNewTrainTimeEvent>(setNewTrainReaction);
    on<UpdateTrainTimeEvent>(updateTrainTimeReaction);
    on<EditTariffEvent>(editTrainReaction);

    if (sharedBox.containsKey('token')) add(const GetDataTariffEvent());
  }

  @override
  String get id => clubId;

  @override
  TariffState? fromJson(Map<String, dynamic> json) {
    if (json['type'] == (DataTariffState).toString()) {
      final data = (json['data'] as List<dynamic>).map((v) {
        return Tariff.fromMap(v);
      }).toList();
      return DataTariffState(
        tariffs: data,
        stopTariff: [],
      );
    }
    return const LoadingTariffState();
  }

  @override
  Map<String, dynamic>? toJson(TariffState state) {
    if (state is DataTariffState) {
      return {
        "type": (DataTariffState).toString(),
        "data": state.tariffs.map((e) => e.toMap()).toList(),
      };
    }
    return null;
  }
}
