part of 'tariff_bloc.dart';

abstract class TariffState extends Equatable {
  const TariffState();
}

class LoadingTariffState extends TariffState {
  const LoadingTariffState();

  @override
  List<Object?> get props => [];
}

class DataTariffState extends TariffState {
  final List<Tariff> tariffs;
  final List<StopTariff> stopTariff;

  const DataTariffState({
    required this.tariffs,
    required this.stopTariff,
  });

  @override
  List<Object?> get props => [tariffs];
}
