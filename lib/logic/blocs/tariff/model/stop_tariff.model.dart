import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:tube/logic/blocs/tariff/model/train_time.model.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/utils/date_time_parse.dart';

class StopTariff extends Equatable {
  final String? id;
  final String? clientId;
  final String? clubId;
  final String? bookingId;
  final String? boughtId;
  final List<TrainTime>? trainTimes;
  final User? user;
  final Booking? booking;
  final List<User>? trainers;
  final DateTime? deadlineDate;
  final int? trainCount;
  final DateTime? startDate;

  final DateTime? stopStartDate;
  final DateTime? stopEndDate;

  const StopTariff({
    this.id,
    this.clientId,
    this.clubId,
    this.trainTimes,
    this.user,
    this.bookingId,
    this.booking,
    this.trainers,
    this.boughtId,
    this.deadlineDate,
    this.trainCount,
    this.startDate,
    this.stopStartDate,
    this.stopEndDate,
  });

  StopTariff copyWith({
    String? id,
    String? clientId,
    String? clubId,
    String? trainerId,
    List<TrainTime>? trainTimes,
    User? user,
    String? bookingId,
    Booking? booking,
    List<User>? trainers,
    String? boughtId,
    DateTime? deadlineDate,
    int? trainCount,
    DateTime? startDate,
    DateTime? stopStartDate,
    DateTime? stopEndDate,
  }) {
    return StopTariff(
      id: id ?? this.id,
      clientId: clientId ?? this.clientId,
      clubId: clubId ?? this.clubId,
      trainTimes: trainTimes ?? this.trainTimes,
      user: user ?? this.user,
      bookingId: bookingId ?? this.bookingId,
      booking: booking ?? this.booking,
      trainers: trainers ?? this.trainers,
      boughtId: boughtId ?? this.boughtId,
      deadlineDate: deadlineDate ?? this.deadlineDate,
      trainCount: trainCount ?? this.trainCount,
      startDate: startDate ?? this.startDate,

      //
      stopStartDate: stopStartDate ?? this.stopStartDate,
      stopEndDate: stopEndDate ?? this.stopEndDate,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'clientId': clientId,
      'clubId': clubId,
      'trainTimes': trainTimes?.map((x) => x.toMap()).toList(),
      'user': user?.toMap(),
      'bookingId': bookingId,
      'booking': booking?.toMap(),
      'trainers': trainers?.map((x) => x.toMap()).toList(),
      'boughtId': boughtId,
      'trainCount': trainCount,
      //
      'stopStartDate': stopStartDate?.toUtc().toIso8601String(),
      'stopEndDate': stopEndDate?.toUtc().toIso8601String(),
    };
  }

  factory StopTariff.fromMap(Map<String, dynamic> map) {
    return StopTariff(
      id: map['_id'],
      clientId: map['clientId'],
      clubId: map['clubId'],
      bookingId: map['bookingId'],
      boughtId: map['boughtId'],
      trainCount: map['trainCount'],
      trainTimes: map['trainTimes'] != null
          ? List<TrainTime>.from(
              map['trainTimes']?.map((x) => TrainTime.fromMap(x)))
          : null,
      user: map['user'] != null ? User.fromMap(map['user']) : null,
      booking: map['booking'] == null ? null : Booking.fromMap(map['booking']),
      trainers: map['trainers'] == null
          ? null
          : List<User>.from(
              map['trainers']?.map(
                (x) => User.fromMap(x),
              ),
            ),
      deadlineDate: dateTimeParse(map['deadlineDate']),
      startDate: dateTimeParse(map['startDate']),
      stopStartDate: dateTimeParse(map['stopStartDate']),
      stopEndDate: dateTimeParse(map['stopEndDate']),
    );
  }

  String toJson() => json.encode(toMap());

  factory StopTariff.fromJson(String source) =>
      StopTariff.fromMap(json.decode(source));

  @override
  String toString() {
    return 'StopTariff(id: $id, clientId: $clientId, clubId: $clubId, trainTimes: $trainTimes, bookingId: $bookingId)';
  }

  @override
  List<Object?> get props {
    return [
      id,
      clientId,
      clubId,
      trainTimes,
      user,
      bookingId,
      booking,
      boughtId,
      trainCount,
      deadlineDate?.toUtc().toIso8601String(),
      startDate?.toUtc().toIso8601String(),
      stopStartDate,
      stopEndDate,
    ];
  }
}

class StopTariffSingle extends StopTariff {
  final TrainTime trainTimesSingle;

  const StopTariffSingle({
    super.boughtId,
    super.id,
    super.clientId,
    super.clubId,
    super.bookingId,
    super.user,
    super.booking,
    super.trainTimes,
    super.trainers,
    required this.trainTimesSingle,
    required super.deadlineDate,
    required super.trainCount,
    super.startDate,
    super.stopStartDate,
    super.stopEndDate,
  });

  @override
  StopTariffSingle copyWith({
    String? id,
    String? clientId,
    String? clubId,
    String? trainerId,
    List<TrainTime>? trainTimes,
    User? user,
    String? bookingId,
    Booking? booking,
    TrainTime? trainTimesSingle,
    List<User>? trainers,
    String? boughtId,
    DateTime? deadlineDate,
    int? trainCount,
    DateTime? startDate,
    DateTime? stopStartDate,
    DateTime? stopEndDate,
  }) {
    return StopTariffSingle(
      id: id ?? this.id,
      clientId: clientId ?? this.clientId,
      clubId: clubId ?? this.clubId,
      trainTimes: trainTimes ?? this.trainTimes,
      user: user ?? this.user,
      bookingId: bookingId ?? this.bookingId,
      booking: booking ?? this.booking,
      trainTimesSingle: trainTimesSingle ?? this.trainTimesSingle,
      trainers: trainers ?? this.trainers,
      boughtId: boughtId ?? this.boughtId,
      deadlineDate: deadlineDate ?? this.deadlineDate,
      trainCount: trainCount ?? this.trainCount,
      startDate: startDate ?? this.startDate,

      //
      stopStartDate: stopStartDate ?? this.stopStartDate,
      stopEndDate: stopEndDate ?? this.stopEndDate,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'clientId': clientId,
      'clubId': clubId,
      'trainTimes': trainTimes?.map((x) => x.toMap()).toList(),
      'user': user?.toMap(),
      'bookingId': bookingId,
      'booking': booking?.toMap(),
      'trainTimesSingle': trainTimesSingle.toMap(),
      'trainers': trainers?.map((x) => x.toMap()).toList(),
      'boughtId': boughtId,
      'deadlineDate': deadlineDate?.toUtc().toIso8601String(),
      'trainCount': trainCount,
      'startDate': startDate?.toUtc().toIso8601String(),
    };
  }

  factory StopTariffSingle.fromMap(Map<String, dynamic> map) {
    return StopTariffSingle(
      id: map['_id'],
      clientId: map['clientId'],
      clubId: map['clubId'],
      bookingId: map['bookingId'],
      trainTimes: map['trainTimes'] != null
          ? List<TrainTime>.from(
              map['trainTimes']?.map((x) => TrainTime.fromMap(x)))
          : null,
      user: map['user'] != null ? User.fromMap(map['user']) : null,
      booking: map['booking'] == null ? null : Booking.fromMap(map['booking']),
      trainTimesSingle: TrainTime.fromMap(map['trainTimesSingle']),
      boughtId: map['boughtId'],
      deadlineDate: dateTimeParse(map['deadlineDate'])!,
      startDate: dateTimeParse(map['startDate']),
      trainCount: map['trainCount'],
      trainers: map['trainers'] == null
          ? null
          : List<User>.from(
              map['trainers']?.map(
                (x) => User.fromMap(x),
              ),
            ),
    );
  }

  factory StopTariffSingle.fromMapStopTariff(
      StopTariff tariff, TrainTime trainTime) {
    return StopTariffSingle(
      id: tariff.id,
      clientId: tariff.clientId,
      clubId: tariff.clubId,
      bookingId: tariff.bookingId,
      trainTimes: tariff.trainTimes,
      user: tariff.user,
      booking: tariff.booking,
      trainers: tariff.trainers,
      trainTimesSingle: trainTime,
      boughtId: tariff.boughtId,
      deadlineDate: tariff.deadlineDate,
      trainCount: tariff.trainCount,
      startDate: tariff.startDate,
    );
  }

  @override
  String toJson() => json.encode(toMap());

  factory StopTariffSingle.fromJson(String source) =>
      StopTariffSingle.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Tariff(id: $id,\n clientId: $clientId,\n clubId: $clubId,\n trainTimes: $trainTimes,\n bookingId: $bookingId\n)';
  }

  @override
  List<Object?> get props {
    return [
      id,
      clientId,
      clubId,
      trainTimes,
      user,
      bookingId,
      booking,
    ];
  }
}
