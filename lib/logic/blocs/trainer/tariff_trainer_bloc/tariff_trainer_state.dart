part of 'tariff_trainer_bloc.dart';

abstract class TariffTrainerState extends Equatable {
  const TariffTrainerState();
}

class LoadingTariffTrainerState extends TariffTrainerState {
  const LoadingTariffTrainerState();

  @override
  List<Object?> get props => [];
}

@immutable
class GetTariffTrainerState extends TariffTrainerState {
  static int dayCount = 1000;
  final List<Tariff> dirtTariffList;

  late final Map<int, List<TariffSingle>> tariffList;
  late final Map<String, Booking> bookingMap;
  late final Map<String, List<TariffSingle>> uniqueTariffList;
  late final List<MapEntry<String, List<TariffSingle>>> sortedEntries;

  GetTariffTrainerState({
    required this.dirtTariffList,
  }) {
    _parse();
  }

  _parse() {
    final Map<int, List<TariffSingle>> tariffTabDay = {};
    final Map<String, Booking> bookingMap = {};
    final Map<String, List<TariffSingle>> uniqueTariff = {};

    final currentDate = DateTime.now();

    for (final res in dirtTariffList) {
      bookingMap[res.bookingId!] = res.booking!;
      if (res.trainTimes == null) continue;
      for (final trainTime in res.trainTimes!) {
        final diff = trainTime.date!.difference(currentDate);
        final diff1 = trainTime.endTime!.difference(currentDate);
        //

        if ((!diff.isNegative || !diff1.isNegative) &&
            ((!diff.isNegative && diff.inDays < dayCount) ||
                (!diff1.isNegative && diff1.inDays < dayCount))) {
          //

          final newSingle = TariffSingle.fromMapTariff(res, trainTime);
          if (res.user?.email == "hellomik2002work@gmail.com") {
            log("START DEBUG ");
          }
          //
          (tariffTabDay[diff.inDays] ??= []).add(newSingle);
          (uniqueTariff[
                  "${newSingle.bookingId!}_${newSingle.trainTimesSingle.date!.toUtc().toIso8601String()}"] ??= [])
              .add(newSingle);
        }
      }
    }
    for (final single in tariffTabDay.entries) {
      single.value.sort((a, b) {
        return a.trainTimesSingle.date!.compareTo(b.trainTimesSingle.date!);
      });
    }

    List<MapEntry<String, List<TariffSingle>>> newEntriesSorted = [];
    final List<DateTime> listDate = [];

    final listSorted = [...tariffTabDay.entries]..sort((a, b) {
        return a.key.compareTo(b.key);
      });

    for (final single in listSorted) {
      listDate.add(
        single.value.first.trainTimesSingle.date!,
      );
    }

    newEntriesSorted = [...uniqueTariff.entries];
    newEntriesSorted.sort((a, b) {
      final val = dateTimeParse(a.key.split("_").last)!
          .compareTo(dateTimeParse(b.key.split("_").last)!);
      if (val == 0) {
        return a.key.split("_").first.compareTo(b.key.split("_").first);
      }
      return val;
    });
    tariffList = tariffTabDay;
    this.bookingMap = bookingMap;
    uniqueTariffList = uniqueTariff;
    sortedEntries = newEntriesSorted;
  }

  @override
  List<Object?> get props => [dirtTariffList];
}
