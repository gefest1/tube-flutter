part of '../tariff_trainer_bloc.dart';

extension GetSoonTariffReaction on TariffTrainerBloc {
  // static int dayCount = 1000;
  void getSoonTariffReaction(
    GetSoonTariffTrainerEvent event,
    Emitter<TariffTrainerState> emit,
  ) async {
    try {
      final queryResult = await graphqlClient.query$getSoonTariff(
        Options$Query$getSoonTariff(
          variables: Variables$Query$getSoonTariff(
            clubId: clubId,
            dayEnd: event.endDate,
            dayStart: event.startDate,
          ),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (queryResult.hasException) queryResult.exception!;

      final newResult = queryResult.parsedData!.getSoonTariff
          .map((e) => Tariff.fromMap(e.toJson()))
          .toList();

      emit(
        GetTariffTrainerState(dirtTariffList: newResult),
      );
    } catch (e) {
      log(e.toString());
    }
  }

  void getTariffByIdReaction() async {}

  void setNewTrainTimeReaction(
    SetNewTrainTimeEvent event,
    Emitter<TariffTrainerState> emit,
  ) {
    final List<Tariff> dirtTariffList;
    if (state is GetTariffTrainerState) {
      dirtTariffList = (state as GetTariffTrainerState).dirtTariffList;
    } else {
      dirtTariffList = event.dirtTariffList;
    }
    final newList = dirtTariffList.map((e) {
      if (e.id == event.tariff.id) {
        return e.copyWith(trainTimes: event.tariff.trainTimes);
      }
      return e;
    }).toList();
    emit(GetTariffTrainerState(dirtTariffList: newList));
  }
}
