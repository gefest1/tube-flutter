part of 'big_event_trainer_bloc.dart';

abstract class BigEventTrainerEvent {
  const BigEventTrainerEvent();
}

class GetBigEventTrainerEvent extends BigEventTrainerEvent {
  const GetBigEventTrainerEvent();
}
