part of '../big_event_trainer_bloc.dart';

extension GetReaction on BigEventTrainerBloc {
  void getReactionEvent(
    GetBigEventTrainerEvent event,
    Emitter<BigEventTrainerState> emit,
  ) async {
    try {
      final queryRes = await graphqlClient.query$getBigEventsTrainerClub(
        Options$Query$getBigEventsTrainerClub(
          variables: Variables$Query$getBigEventsTrainerClub(clubId: clubId),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (queryRes.hasException) throw queryRes.exception!;
      final data = queryRes.parsedData!.getBigEventsTrainerClub
          .map((e) => BigEventEntity.fromMap(e.toJson()))
          .toList();
      emit(
        DataBigEventTrainerState(data: data),
      );
    } catch (e, s) {
      log(e.toString(), stackTrace: s);
    }
  }
}
