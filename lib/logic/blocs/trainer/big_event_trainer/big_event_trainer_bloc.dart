import 'dart:developer';

import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/big_event/entities/big_event_entity.dart';

part 'big_event_trainer_event.dart';
part 'big_event_trainer_state.dart';
part 'reaction/get_reaction_event.dart';

class BigEventTrainerBloc
    extends HydratedBloc<BigEventTrainerEvent, BigEventTrainerState> {
  final String clubId;

  BigEventTrainerBloc({
    required this.clubId,
  }) : super(const LoadingBigEventTrainerState()) {
    on<GetBigEventTrainerEvent>(getReactionEvent);
    add(const GetBigEventTrainerEvent());
  }

  @override
  BigEventTrainerState? fromJson(Map<String, dynamic> json) => null;
  @override
  Map<String, dynamic>? toJson(BigEventTrainerState state) => null;
}
