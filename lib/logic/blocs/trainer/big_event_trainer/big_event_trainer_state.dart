part of 'big_event_trainer_bloc.dart';

abstract class BigEventTrainerState {
  const BigEventTrainerState();
}

class LoadingBigEventTrainerState extends BigEventTrainerState {
  const LoadingBigEventTrainerState();
}

class DataBigEventTrainerState extends BigEventTrainerState {
  final List<BigEventEntity> data;

  const DataBigEventTrainerState({
    required this.data,
  });
}
