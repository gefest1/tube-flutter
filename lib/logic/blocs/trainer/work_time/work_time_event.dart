part of './work_time_bloc.dart';

abstract class WorkTimeEvent {
  const WorkTimeEvent();
}

class GetDataWorkTimeEvent extends WorkTimeEvent {
  const GetDataWorkTimeEvent();
}

class SetDataWorkTimeEvent extends WorkTimeEvent {
  final List<WorkTimeGraph> workTimes;

  final Completer? completer;

  const SetDataWorkTimeEvent({
    required this.workTimes,
    this.completer,
  });
}
