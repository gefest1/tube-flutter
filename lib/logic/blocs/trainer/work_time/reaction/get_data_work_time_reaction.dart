part of '../work_time_bloc.dart';

extension GetDataWorkTimeReaction on ControllerWorkTimeBloc {
  void getDataWorkTimeReaction(
    GetDataWorkTimeEvent event,
    Emitter<WorkTimeState> emit,
  ) async {
    try {
      final mutationResult = await graphqlClient.query$getWorkTime(
        Options$Query$getWorkTime(
          variables:
              Variables$Query$getWorkTime(clubId: clubId, trainerId: trainerId),
        ),
      );

      if (mutationResult.hasException) throw mutationResult.exception!;
      final data = mutationResult.parsedData!;
      Iterable<MapEntry<int, WorkTimeGraph>> workTimeGraphList =
          data.getWorkTime.workTime.map((e) {
        final data = WorkTimeGraph.fromMap(e.toJson());
        return MapEntry((data.startTime!.weekday + 6) % 7, data);
      });

      emit(
        SuccessWorkTimeState(
          workTime: Map.fromEntries(workTimeGraphList),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
  }
}
