import 'dart:async';
import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/model/work_time_graph.dart';

part 'work_time_state.dart';
part 'work_time_event.dart';
part 'reaction/set_data_work_time_reaction.dart';
part 'reaction/get_data_work_time_reaction.dart';

class ControllerWorkTimeBloc
    extends HydratedBloc<WorkTimeEvent, WorkTimeState> {
  final String clubId;
  final String? trainerId;

  ControllerWorkTimeBloc({
    required this.clubId,
    this.trainerId,
  }) : super(LoadingWorkTimeState()) {
    on<SetDataWorkTimeEvent>(setDataWorkTimeReaction);
    on<GetDataWorkTimeEvent>(getDataWorkTimeReaction);
    add(const GetDataWorkTimeEvent());
  }

  @override
  Map<String, dynamic>? toJson(WorkTimeState state) {
    return null;
  }

  @override
  WorkTimeState? fromJson(Map<String, dynamic> json) {
    return null;
  }
}
