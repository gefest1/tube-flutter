import 'dart:developer';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/tariff/model/train_time.model.dart';

part './active_tariff_state.dart';
part './active_tariff_event.dart';
part './reaction/get_data_reaction.dart';

class ActiveTariffBloc
    extends HydratedBloc<ActiveTariffEvent, ActiveTariffState> {
  final String clubId;

  ActiveTariffBloc({
    required this.clubId,
  }) : super(const LoadingActiveTariffState()) {
    on<GetDataActiveTariffEvent>(getDataActiveTariffReaction);
    add(const GetDataActiveTariffEvent());
  }

  @override
  String get id => clubId;

  @override
  Map<String, dynamic>? toJson(ActiveTariffState state) {
    return null;
  }

  @override
  ActiveTariffState? fromJson(Map<String, dynamic> json) {
    return null;
  }
}
