part of './active_tariff_bloc.dart';

abstract class ActiveTariffEvent {
  const ActiveTariffEvent();
}

class GetDataActiveTariffEvent extends ActiveTariffEvent {
  const GetDataActiveTariffEvent();
}
