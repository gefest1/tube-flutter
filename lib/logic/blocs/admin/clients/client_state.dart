part of './client_bloc.dart';

abstract class AdminClientState extends Equatable {
  const AdminClientState();
}

class LoadingAdminClientState extends AdminClientState {
  const LoadingAdminClientState();

  @override
  List<Object?> get props => [];
}

class DataAdminClientState extends AdminClientState {
  final List<User> users;

  const DataAdminClientState({
    required this.users,
  });

  @override
  List<Object?> get props => [];
}
