part of './client_bloc.dart';

abstract class AdminClientEvent {
  const AdminClientEvent();
}

class GetAdminClientEvent extends AdminClientEvent {
  const GetAdminClientEvent();
}
