import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/photourl.dart';

part 'client_event.dart';
part 'client_state.dart';
part 'reaction/get_admin_client_reaction.dart';

class AdminClientBloc extends Bloc<AdminClientEvent, AdminClientState> {
  final String clubId;

  AdminClientBloc({
    required this.clubId,
  }) : super(const LoadingAdminClientState()) {
    on<GetAdminClientEvent>(getAdminClientEvent);
    add(const GetAdminClientEvent());
  }
}
