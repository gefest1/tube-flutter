part of '../client_bloc.dart';

extension GetAdminClientReaction on AdminClientBloc {
  void getAdminClientEvent(
    GetAdminClientEvent event,
    Emitter<AdminClientState> emit,
  ) async {
    final result = await graphqlClient.query$getUsersAsAdmin(
      Options$Query$getUsersAsAdmin(
        variables: Variables$Query$getUsersAsAdmin(clubId: clubId),
      ),
    );
    if (result.hasException) throw result.exception!;
    final users = result.parsedData!.getUserByClubId.map(
      (e) {
        return User(
          date: e.date,
          email: e.email,
          fullName: e.fullName,
          id: e.$_id,
          iin: e.iin,
          phoneNumber: e.phoneNumber,
          photoUrl: PhotoUrl(
            m: e.photoUrl?.M,
            thumbnail: e.photoUrl?.thumbnail,
            xl: e.photoUrl?.XL,
          ),
        );
      },
    ).toList();
    emit(
      DataAdminClientState(users: users),
    );
  }
}
