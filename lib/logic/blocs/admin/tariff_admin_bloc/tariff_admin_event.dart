part of 'tariff_admin_bloc.dart';

abstract class TariffAdminEvent {
  const TariffAdminEvent();
}

class GetSoonTariffAdminEvent extends TariffAdminEvent {
  final DateTime? startDate;
  final DateTime? endDate;

  const GetSoonTariffAdminEvent({
    this.startDate,
    this.endDate,
  });
}

class EditSingleTariffAdminEvent extends TariffAdminEvent {
  final Tariff? tariff;
  final ({
    String id,
    int lastCount,
    DateTime startDate,
  })? data;
  final Completer? completer;

  const EditSingleTariffAdminEvent({
    this.tariff,
    this.data,
    this.completer,
  });
}
