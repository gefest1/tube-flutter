part of '../tariff_admin_bloc.dart';

extension TariffReaction on TariffAdminBloc {
  void getSoonTariffReaction(
    GetSoonTariffAdminEvent event,
    Emitter<TariffAdminState> emit,
  ) async {
    try {
      final queryResult = await graphqlClient.query$getSoonTariffClubId(
        Options$Query$getSoonTariffClubId(
          variables: Variables$Query$getSoonTariffClubId(
            clubId: clubId,
            dayStart: event.startDate,
            dayEnd: event.endDate,
          ),
        ),
      );

      if (queryResult.hasException) queryResult.exception!;

      tariffMap.addEntries(queryResult.parsedData!.getSoonTariffClubId
          .map((e) => MapEntry(e.$_id, Tariff.fromMap(e.toJson()))));

      emit(
        GetTariffAdminState(
          dirtTariffList: tariffMap.entries.map((e) => e.value).toList(),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
  }
}
