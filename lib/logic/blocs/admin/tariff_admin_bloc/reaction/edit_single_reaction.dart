part of '../tariff_admin_bloc.dart';

extension EditSingleTariff on TariffAdminBloc {
  void editTariffSingle(
    EditSingleTariffAdminEvent event,
    Emitter<TariffAdminState> emit,
  ) async {
    try {
      if (event.data != null && event.tariff != null) throw 'Error';
      final state = this.state;
      if (state is GetTariffAdminState) {
        Tariff? newTariff = event.tariff;
        if (event.data != null) {
          final result = await graphqlClient.mutate$editTariffAdmin(
            Options$Mutation$editTariffAdmin(
              variables: Variables$Mutation$editTariffAdmin(
                id: event.data!.id,
                lastCount: event.data!.lastCount,
                startDate: event.data!.startDate,
              ),
            ),
          );
          if (result.hasException) {
            throw result.exception!;
          }
          final parsedData = result.parsedData!.editTariff;
          newTariff = Tariff.fromMap(parsedData.toJson());
        }
        if (newTariff != null) {
          emit(
            GetTariffAdminState(
              dirtTariffList: state.dirtTariffList.map(
                (tar) {
                  if (tar.id == newTariff!.id) {
                    return tar.copyWith(
                      trainCount: newTariff.trainCount,
                      trainTimes: newTariff.trainTimes,
                      startDate: newTariff.startDate,
                      deadlineDate: newTariff.deadlineDate,
                    );
                  }
                  return tar;
                },
              ).toList(),
            ),
          );
        }
      }
      event.completer?.complete();
    } catch (e, s) {
      event.completer?.completeError(e, s);
      log(e.toString(), stackTrace: s);
    }
  }
}
