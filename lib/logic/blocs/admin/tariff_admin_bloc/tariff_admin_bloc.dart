import 'dart:async';
import 'dart:developer';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/utils/date_time_parse.dart';

part 'tariff_admin_event.dart';
part 'tariff_admin_state.dart';
part 'reaction/get_soon_tariff_reaction.dart';
part 'reaction/edit_single_reaction.dart';

class TariffAdminBloc extends HydratedBloc<TariffAdminEvent, TariffAdminState> {
  final String clubId;
  final Map<String, Tariff> tariffMap = {};

  TariffAdminBloc({required this.clubId})
      : super(const LoadingTariffAdminState()) {
    on<GetSoonTariffAdminEvent>(getSoonTariffReaction);
    on<EditSingleTariffAdminEvent>(editTariffSingle);
    add(const GetSoonTariffAdminEvent());
  }

  @override
  Map<String, dynamic>? toJson(TariffAdminState state) => null;

  @override
  TariffAdminState? fromJson(Map<String, dynamic> json) => null;
}
