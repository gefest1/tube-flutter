part of 'worker_bloc.dart';

abstract class WorkerEvent {
  const WorkerEvent();
}

// class AddWorkerWorkerEvent extends WorkerEvent {
//   final List<User> workers;
//   final String clubId;

//   const AddWorkerWorkerEvent(
//     this.clubId,
//     this.workers,
//   );
// }

class GetWorkWorkerEvent extends WorkerEvent {
  final Completer? completer;

  const GetWorkWorkerEvent({this.completer});
}
