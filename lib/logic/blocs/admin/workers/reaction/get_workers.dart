part of '../worker_bloc.dart';

extension GetWorkerDatalazy on WorkerBloc {
  // static bool _gettingWorkes = false;

  // void getWorker() async {
  //   if (_gettingWorkes == true) return;
  //   _gettingWorkes = true;
  //   try {
  //     final mutationResult = await client.query(
  //       QueryOptions(
  //         document: gql(getWorkes),
  //         variables: {
  //           "clubId": clubId,
  //         },
  //       ),
  //     );
  //     if (mutationResult.hasException) throw mutationResult.exception!;
  //     final List<User> workers =
  //         (mutationResult.data!['getWorkers'] as List<dynamic>)
  //             .map((e) => User.fromMap(e))
  //             .toList();

  //     add(AddWorkerWorkerEvent(clubId, workers));
  //   } catch (e) {
  //     log(e.toString());
  //   }
  //   _gettingWorkes = false;
  // }
}

extension GetWorker on WorkerBloc {
  void getWorkReaction(
    GetWorkWorkerEvent event,
    Emitter<WorkerState> emit,
  ) async {
    final firstState = state;

    try {
      emit(const LoadginWorkerState());
      final mutationResult = await graphqlClient.query$getWorkerData(
        Options$Query$getWorkerData(
          variables: Variables$Query$getWorkerData(clubId: clubId),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (mutationResult.hasException) throw mutationResult.exception!;

      final partEntries = mutationResult.parsedData!.getParticipanceClubId
          .where((element) => element.participances.status == 'trainer')
          .map(
            (e) => MapEntry<String, Participance>(
              e.userId,
              Participance.fromMap(
                e.participances.toJson(),
              ),
            ),
          );
      final mapParticipance = Map.fromEntries(partEntries);

      final List<User> workers = mutationResult.parsedData!.getWorkers
          .map((e) => User.fromMap(e.toJson()))
          .toList();

      emit(
        ClubWorkerState(
          mapParticipance: mapParticipance,
          mapWorkers: workers,
          workUserMap: Map<String, UserWorkerTime>.fromEntries(
            mutationResult.parsedData!.getWorkTimeByClub.map((val) {
              final userWorkerTime = UserWorkerTime.fromMap(val.toJson());

              return MapEntry<String, UserWorkerTime>(
                userWorkerTime.userId!,
                userWorkerTime,
              );
            }),
          ),
        ),
      );
      event.completer?.complete(null);
    } catch (e, s) {
      emit(firstState);
      event.completer?.completeError(e, s);
      rethrow;
    }
  }
}
