part of 'worker_bloc.dart';

abstract class WorkerState extends Equatable {
  const WorkerState();
}

class LoadginWorkerState extends WorkerState {
  final WorkerState? prevState;
  const LoadginWorkerState({this.prevState});

  @override
  List<Object?> get props => [prevState];
}

class ClubWorkerState extends WorkerState {
  final List<User> mapWorkers;
  final Map<String, UserWorkerTime> workUserMap;
  final Map<String, Participance> mapParticipance;

  const ClubWorkerState({
    required this.mapWorkers,
    required this.workUserMap,
    required this.mapParticipance,
  });

  @override
  List<Object?> get props => [mapWorkers, workUserMap];

  ClubWorkerState copyWith({
    List<User>? mapWorkers,
    Map<String, UserWorkerTime>? workUserMap,
    Map<String, Participance>? mapParticipance,
  }) {
    return ClubWorkerState(
      mapWorkers: mapWorkers ?? this.mapWorkers,
      workUserMap: workUserMap ?? this.workUserMap,
      mapParticipance: mapParticipance ?? this.mapParticipance,
    );
  }
}
