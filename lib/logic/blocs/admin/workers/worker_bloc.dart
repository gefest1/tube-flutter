import 'dart:async';

import 'package:equatable/equatable.dart';

import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';

import 'package:tube/logic/blocs/participance/model/participance.model.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/user_worker_time.dart';

part 'worker_event.dart';
part 'worker_state.dart';
part 'reaction/get_workers.dart';

class WorkerBloc extends HydratedBloc<WorkerEvent, WorkerState> {
  static WorkerBloc? currentBloc;

  final String clubId;

  WorkerBloc({
    required this.clubId,
  }) : super(const LoadginWorkerState()) {
    on<GetWorkWorkerEvent>(getWorkReaction);
    add(const GetWorkWorkerEvent());
  }

  @override
  WorkerState? fromJson(Map<String, dynamic> json) {
    return null;
  }

  @override
  Map<String, dynamic>? toJson(WorkerState? state) {
    return null;
  }
}
