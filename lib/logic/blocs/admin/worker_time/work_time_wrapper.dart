import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/admin/worker_time/worker_time_bloc.dart';

class WorkTimeMap {
  final Map<String, WorkerTimeBloc> _workTimeBloc = {};

  WorkTimeMap();

  WorkerTimeBloc getWorkTimeBloc({
    required String clubId,
    required String userId,
  }) {
    return _workTimeBloc[clubId + userId] ??= WorkerTimeBloc(
      clubId: clubId,
      userId: userId,
    );
  }
}

class WorkTimeWrapper extends StatefulWidget {
  final Widget child;

  const WorkTimeWrapper({
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  State<WorkTimeWrapper> createState() => _WorkTimeWrapperState();
}

class _WorkTimeWrapperState extends State<WorkTimeWrapper> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<WorkTimeMap>(
          create: (_) => WorkTimeMap(),
        ),
      ],
      child: widget.child,
    );
  }
}
