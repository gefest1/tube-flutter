part of '../worker_time_bloc.dart';

extension GetWorkerReaction on WorkerTimeBloc {
  void getDataAboutUser(
    GetDataWorkTimeEvent event,
    Emitter<WorkTimeState> emit,
  ) async {
    emit(const LoadingWorkTimeState());
    try {
      final quertRes = await graphqlClient.query$getWorkTimeUser(
        Options$Query$getWorkTimeUser(
          variables: Variables$Query$getWorkTimeUser(
            clubId: event.clubId,
            userId: event.userId,
          ),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      Iterable<MapEntry<int, WorkTimeGraph>> workTimeGraphList =
          quertRes.parsedData!.getWorkTimeUser.workTime.map(
        (e) {
          final data = WorkTimeGraph.fromMap(e.toJson());
          return MapEntry((data.startTime!.weekday + 6) % 7, data);
        },
      );

      emit(
        DataWorkTimeState(
          clubId: event.clubId,
          userId: event.userId,
          workTime: Map.fromEntries(workTimeGraphList),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
  }
}
