part of '../big_service_bloc.dart';

extension GetAdminServiceReaction on AdminBigServiceBloc {
  void getAdminServiceReaction(
    GetAdminBigServiceEvent event,
    Emitter<AdminBigServiceState> emit,
  ) async {
    try {
      final queryRes = await graphqlClient.query$getBigServicesClub(
        Options$Query$getBigServicesClub(
          variables: Variables$Query$getBigServicesClub(clubId: clubId),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );
      if (queryRes.hasException) throw queryRes.exception!;
      final data = (queryRes.data!['getBigServicesClub'] as List<dynamic>)
          .map((e) => BigServiceEntity.fromMap(e))
          .toList();

      emit(DataAdminBigServiceState(data: data));
    } catch (e, s) {
      log(e.toString(), stackTrace: s);
    }
  }
}
