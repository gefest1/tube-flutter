part of './big_service_bloc.dart';

abstract class AdminBigServiceEvent {
  const AdminBigServiceEvent();
}

class GetAdminBigServiceEvent extends AdminBigServiceEvent {
  const GetAdminBigServiceEvent();
}

class AddAdminBigServiceEvent extends AdminBigServiceEvent {
  final Completer? completer;
  final CreateBigServiceEntity entity;

  const AddAdminBigServiceEvent({
    this.completer,
    required this.entity,
  });
}
