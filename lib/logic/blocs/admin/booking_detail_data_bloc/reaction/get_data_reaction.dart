part of '../booking_detail_data_bloc.dart';

extension GetDataReaction on BookingDetailDataBloc {
  void getData(
    GetBookingDetailDataEvent event,
    Emitter<BookingDetailDataState> emit,
  ) async {
    try {
      final result = await graphqlClient.query$getNumberClubId(
        Options$Query$getNumberClubId(
          variables: Variables$Query$getNumberClubId(bookingId: bookingId),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (result.hasException) throw result.exception!;
      final data = result.parsedData!.getNumberClubId;
      data.booking;
      emit(
        DataBookingDetailDataState(
          activeTariffs: data.activeTariffs
              .map((e) => Tariff.fromMap(e.toJson()))
              .toList(),
          booking: Booking.fromMap(data.booking.toJson()),
          trainers: data.trainers.map((e) => User.fromMap(e.toJson())).toList(),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
  }
}
