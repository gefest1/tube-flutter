import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/booking.model.dart';

part './booking_detail_data_event.dart';
part './booking_detail_data_state.dart';

part './reaction/get_data_reaction.dart';

class BookingDetailDataBloc
    extends HydratedBloc<BookingDetailDataEvent, BookingDetailDataState> {
  final String bookingId;

  BookingDetailDataBloc({
    required this.bookingId,
  }) : super(const LoadingBookingDetailDataState()) {
    on<GetBookingDetailDataEvent>(getData);
    add(const GetBookingDetailDataEvent());
  }

  @override
  BookingDetailDataState? fromJson(Map<String, dynamic> json) {
    return null;
  }

  @override
  Map<String, dynamic>? toJson(BookingDetailDataState state) {
    return null;
  }
}
