part of './booking_detail_data_bloc.dart';

abstract class BookingDetailDataState extends Equatable {
  const BookingDetailDataState();
}

class LoadingBookingDetailDataState extends BookingDetailDataState {
  const LoadingBookingDetailDataState();

  @override
  List<Object?> get props => [];
}

class DataBookingDetailDataState extends BookingDetailDataState {
  final List<Tariff> activeTariffs;
  final Booking booking;
  final List<User> trainers;

  const DataBookingDetailDataState({
    required this.activeTariffs,
    required this.booking,
    required this.trainers,
  });

  @override
  List<Object?> get props => [activeTariffs, booking, trainers];
}
