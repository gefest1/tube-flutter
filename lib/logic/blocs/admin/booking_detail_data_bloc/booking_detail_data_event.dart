part of './booking_detail_data_bloc.dart';

abstract class BookingDetailDataEvent {
  const BookingDetailDataEvent();
}

class GetBookingDetailDataEvent extends BookingDetailDataEvent {
  const GetBookingDetailDataEvent();
}
