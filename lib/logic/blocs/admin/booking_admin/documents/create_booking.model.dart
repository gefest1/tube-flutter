import 'package:graphql/client.dart';

const bookingFragment = '''
fragment BookingFragment on BookingGraph {
    _id
  	clubId
  	count
  	description
  	name
  	percent
  	price
  	deadlineDays
  	timePerMinute
    studentNumber
    workTimesObj {
      now
      max
      date
    }
    workTimesUserIds
  	tags
}
''';

final creatBookingDocument = gql(''' 
$bookingFragment
mutation (
  \$name: String!
	\$count: Int!
	\$price: Float!
	\$clubId: String! 
	\$percent: Float!
	\$description: String
	\$timePerMinute: Int!
  \$workTimesObj: [[WorkTimeObjInput!]!]!
  \$workTimesUserIds: [String!]!
  \$deadlineDays: Int!
  \$studentNumber: Int!
  \$tags: [String!]
){
  createBooking(
    studentNumber: \$studentNumber
    deadlineDays: \$deadlineDays
    name: \$name
    count: \$count
    price: \$price
    clubId: \$clubId
    percent: \$percent
    description: \$description
    timePerMinute: \$timePerMinute
    workTimesObj: \$workTimesObj
    workTimesUserIds: \$workTimesUserIds
    tags: \$tags
  ){
      ...BookingFragment
  }
}''');
