import 'package:graphql/client.dart';
import 'package:tube/logic/blocs/admin/booking_admin/documents/create_booking.model.dart';

final editBookingDoc = gql('''
$bookingFragment
mutation editBooking(
  \$editBooking: EditBookingInput!
  \$bookingId: String!
  \$applyToOld: Boolean
){
  editBooking(
    editBooking: \$editBooking
    bookingId: \$bookingId
    applyToOld:\$applyToOld
  ){
    ...BookingFragment
  }
}
''');
