part of './booking_admin_bloc.dart';

class BookingAdminEvent {
  const BookingAdminEvent();
}

class CreateBookingAdminEvent extends BookingAdminEvent {
  final Booking booking;
  final Map<String, Map<int, List<DateTime>>> workTime;
  final Completer? completer;

  CreateBookingAdminEvent({
    required this.booking,
    required this.workTime,
    this.completer,
  });
}

class GetBookingAdminEvent extends BookingAdminEvent {
  const GetBookingAdminEvent();
}

class EditBookingEvent extends BookingAdminEvent {
  final String bookingId;
  final Completer? completer;
  final EditDescription? editDescription;
  final EditTags? editTags;
  final EditPrice? editPrice;
  final EditSchedule? editSchedule;
  final EditDuration? editDuration;
  final List<SubBooking>? subBookings;

  final bool? applyToOld;

  const EditBookingEvent({
    required this.bookingId,
    this.editDescription,
    this.completer,
    this.editTags,
    this.editPrice,
    this.editSchedule,
    this.editDuration,
    this.applyToOld,
    this.subBookings,
  });
}

class RemoveBookingAdminEvent extends BookingAdminEvent {
  final String bookingId;
  final Completer? completer;

  const RemoveBookingAdminEvent({required this.bookingId, this.completer});
}

//
//
//

class StopBookingDetailDataEvent extends BookingAdminEvent {
  final Completer? completer;
  final String bookingId;
  final DateTime? date;

  const StopBookingDetailDataEvent({
    this.completer,
    required this.bookingId,
    required this.date,
  });
}

class ResumeBookingDetailDataEvent extends BookingAdminEvent {
  final Completer? completer;
  final String bookingId;

  const ResumeBookingDetailDataEvent({
    this.completer,
    required this.bookingId,
  });
}
