part of '../booking_admin_bloc.dart';

extension EditBookingReaction on BookingAdminBloc {
  void editBookingReaction(
    EditBookingEvent event,
    Emitter<BookingAdminState> emit,
  ) async {
    try {
      final oldState = state;
      List<Booking>? preBookings;
      if (oldState is DataBookingAdminState) {
        preBookings = oldState.bookings;
      }

      Input$EditBookingInput inputs = Input$EditBookingInput();

      if (event.editDescription != null) {
        inputs = inputs.copyWith(
          name: event.editDescription!.name,
          description: event.editDescription!.description,
        );
      }
      if (event.editTags != null) {
        inputs = inputs.copyWith(tags: event.editTags!.tags);
      }
      if (event.editPrice != null) {
        inputs = inputs.copyWith(
          price: event.editPrice!.price,
          percent: event.editPrice!.percent,
          subBooking: (event.subBookings ?? [])
              .map(
                (e) => Input$SubBookingInput(
                  count: 1,
                  price: e.price,
                  deadlineDays: 30,
                ),
              )
              .toList(),
        );
      }
      if (event.editSchedule != null) {
        final parsedData = parseFromMap(event.editSchedule!.workTime);
        inputs = inputs.copyWith(
          workTimesObj: parsedData.workTimesDates
              .map(
                (e) => e
                    .map(
                      (e) => Input$WorkTimeObjInput.fromJson(e),
                    )
                    .toList(),
              )
              .toList(),
          workTimesUserIds: parsedData.workTimesUserIds,
        );
      }
      if (event.editDuration != null) {
        inputs = inputs.copyWith(
          studentNumber: event.editDuration!.studentNumber,
          count: event.editDuration!.count,
          deadlineDays: event.editDuration!.deadlineDays,
          timePerMinute: event.editDuration!.timePerMinute,
        );
      }
      final res = await graphqlClient.mutate$editBooking(
        Options$Mutation$editBooking(
          variables: Variables$Mutation$editBooking(
            editBooking: inputs,
            bookingId: event.bookingId,
            applyToOld: event.applyToOld,
          ),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (res.hasException) throw res.exception!;
      final newBooking = Booking.fromMap(res.parsedData!.editBooking.toJson());
      if (preBookings != null) {
        emit(
          DataBookingAdminState(
            bookings: preBookings
                .map((e) => e.id == event.bookingId ? newBooking : e)
                .toList(),
          ),
        );
      }
      event.completer?.complete();
    } catch (e, s) {
      event.completer?.completeError(e, s);
    }
  }
}
