part of '../booking_admin_bloc.dart';

extension StopReaction on BookingAdminBloc {
  void stop(
    StopBookingDetailDataEvent event,
    Emitter<BookingAdminState> emit,
  ) async {
    try {
      DataBookingAdminState? state1;
      if (state is DataBookingAdminState) {
        state1 = state as DataBookingAdminState;
      }
      final responce = await graphqlClient.mutate$stopBooking(
        Options$Mutation$stopBooking(
          variables: Variables$Mutation$stopBooking(
            bookingId: event.bookingId,
            date: event.date,
          ),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );
      if (responce.hasException) throw responce.exception!;
      if (state1 != null) {
        emit(
          DataBookingAdminState(
            bookings: state1.bookings
                .map(
                  (e) => e.id == event.bookingId
                      ? e.copyWith(
                          stopDate: responce.parsedData?.stopBooking.stopDate,
                        )
                      : e,
                )
                .toList(),
          ),
        );
      }
      event.completer?.complete();
    } catch (e, s) {
      event.completer?.completeError(e, s);
      log(e.toString(), stackTrace: s);
    }
  }
}
