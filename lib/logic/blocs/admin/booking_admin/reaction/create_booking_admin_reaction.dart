part of '../booking_admin_bloc.dart';

class ParseMap {
  final List<List<Map<String, dynamic>>> workTimesDates;
  final List<String> workTimesUserIds;
  const ParseMap({
    required this.workTimesDates,
    required this.workTimesUserIds,
  });
}

ParseMap parseFromMap(WorkTimeType workTime) {
  final List<List<WorkTimeObj>> workTimesDates = [];
  final List<String> workTimesUserIds = [];

  for (final element in workTime.entries) {
    workTimesUserIds.add(element.key);
    final newValues = element.value.values.fold<List<DateTime>>(
      [],
      (List<DateTime> previousValue, List<DateTime> element) {
        return [...previousValue, ...element];
      },
    );
    //  TODO
    workTimesDates.add(
      newValues.map((e) {
        return WorkTimeObj(
          date: e,
        );
      }).toList(),
    );
  }

  final reqWorkTimeObj = workTimesDates
      .map((e) => e
          .map(
            (e) => Map.fromEntries(
                e.toMap().entries.where((element) => element.value != null)),
          )
          .toList())
      .toList();
  return ParseMap(
    workTimesDates: reqWorkTimeObj,
    workTimesUserIds: workTimesUserIds,
  );
}

extension CreateBookingAdminReaction on BookingAdminBloc {
  void createBookingAdminReaction(
    CreateBookingAdminEvent event,
    Emitter<BookingAdminState> emit,
  ) async {
    final DataBookingAdminState? previousState = state is DataBookingAdminState
        ? (state as DataBookingAdminState)
        : null;

    emit(const LoadingBookingAdminState());
    try {
      final booking = event.booking.copyWith(clubId: clubId);
      final prasedData = parseFromMap(event.workTime);

      final mutationResult = await graphqlClient.mutate$createBooking(
        Options$Mutation$createBooking(
          variables: Variables$Mutation$createBooking(
            name: booking.name!,
            count: booking.count!.toInt(),
            price: booking.price!.toDouble(),
            clubId: clubId,
            percent: booking.percent!.toDouble(),
            description: booking.description!,
            timePerMinute: booking.timePerMinute!.toInt(),
            workTimesObj: prasedData.workTimesDates
                .map((e) =>
                    e.map((e) => Input$WorkTimeObjInput.fromJson(e)).toList())
                .toList(),
            workTimesUserIds: prasedData.workTimesUserIds,
            deadlineDays: booking.deadlineDays!,
            studentNumber: booking.studentNumber!,
            tags: booking.tags,
            subBooking: booking.subBooking
                .map((e) => Input$SubBookingInput(
                      count: e.count,
                      price: e.price,
                      deadlineDays: e.deadlineDays,
                    ))
                .toList(),
          ),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );
      if (mutationResult.hasException) throw mutationResult.exception!;

      final newBooking =
          Booking.fromMap(mutationResult.parsedData!.createBooking.toJson());
      event.completer?.complete(newBooking);

      emit(
        DataBookingAdminState(
          bookings: [
            ...(previousState?.bookings ?? []),
            newBooking,
          ],
        ),
      );
    } catch (e, s) {
      emit(previousState ?? const LoadingBookingAdminState());
      log(e.toString());
      event.completer?.completeError(e, s);
    }
  }
}
