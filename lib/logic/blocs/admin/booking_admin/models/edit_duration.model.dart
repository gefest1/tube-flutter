class EditDuration {
  final int count;
  final int deadlineDays;
  final int timePerMinute;
  final int studentNumber;

  const EditDuration({
    required this.count,
    required this.deadlineDays,
    required this.timePerMinute,
    required this.studentNumber,
  });
}
