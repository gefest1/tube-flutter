import 'dart:async';
import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

import 'package:tube/logic/blocs/admin/booking_admin/models/edit_description.model.dart';
import 'package:tube/logic/blocs/admin/booking_admin/models/edit_duration.model.dart';
import 'package:tube/logic/blocs/admin/booking_admin/models/edit_price.model.dart';
import 'package:tube/logic/blocs/admin/booking_admin/models/edit_schedule.model.dart';
import 'package:tube/logic/blocs/admin/booking_admin/models/edit_tags.model.dart';
import 'package:tube/logic/model/admin/edit_booking.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/logic/model/sub_booking.model.dart';
import 'package:tube/main.dart';

part './booking_admin_event.dart';
part './booking_admin_state.dart';
part 'reaction/create_booking_admin_reaction.dart';
part 'reaction/get_booking_admin_reaction.dart';
part 'reaction/edit_booking_reaction.dart';
//
//
//

part './reaction/stop_reaction.dart';

class BookingAdminBloc
    extends HydratedBloc<BookingAdminEvent, BookingAdminState> {
  final String clubId;

  Booking? getBooking(String bookingId) {
    final state = this.state;
    if (state is DataBookingAdminState) {
      return <Booking?>[
        ...state.bookings
      ].firstWhere((element) => element?.id == bookingId, orElse: () => null);
    }
    return null;
  }

  BookingAdminBloc({
    required this.clubId,
  }) : super(const LoadingBookingAdminState()) {
    on<GetBookingAdminEvent>(getBookingAdminReaction);
    on<CreateBookingAdminEvent>(createBookingAdminReaction);
    on<EditBookingEvent>(editBookingReaction);
    on<RemoveBookingAdminEvent>(removeBooking);

    on<StopBookingDetailDataEvent>(stop);

    add(const GetBookingAdminEvent());
  }

  @override
  BookingAdminState? fromJson(Map<String, dynamic> json) {
    return null;
  }

  @override
  Map<String, dynamic>? toJson(BookingAdminState state) {
    return null;
  }
}
