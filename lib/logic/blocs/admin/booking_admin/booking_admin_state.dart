part of './booking_admin_bloc.dart';

abstract class BookingAdminState extends Equatable {
  const BookingAdminState();
}

class LoadingBookingAdminState extends BookingAdminState {
  const LoadingBookingAdminState();
  @override
  List<Object?> get props => [];
}

class DataBookingAdminState extends BookingAdminState {
  final List<Booking> bookings;

  const DataBookingAdminState({
    required this.bookings,
  });

  @override
  List<Object?> get props => [bookings];
}
