part of './notification_bloc.dart';

abstract class NotificationEvent {
  const NotificationEvent();
}

class GetNotificationEvent extends NotificationEvent {
  const GetNotificationEvent();
}
