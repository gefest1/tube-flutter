part of '../notification_bloc.dart';

extension GetDataReaction on NotificationBloc {
  void getData(
    GetNotificationEvent event,
    Emitter<NotificationState> emit,
  ) async {
    final notifyRes = await graphqlClient.query$getNotification(
      Options$Query$getNotification(
        fetchPolicy: FetchPolicy.networkOnly,
        variables: Variables$Query$getNotification(
          status: participance.genratedValue,
          clubId: clubId,
        ),
      ),
    );

    if (notifyRes.hasException) throw notifyRes.exception!;
    final data = notifyRes.parsedData!.getNotification
        .map((e) => NotificationEntity.fromMap(e.toJson()))
        .toList();
    emit(DataNotificationState(notificationEntity: data));
  }
}
