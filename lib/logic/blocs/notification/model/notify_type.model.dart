enum Water {
  frozen(32);

  final int tempInFahrenheit;
  const Water(this.tempInFahrenheit);
}

enum NotificationType {
  boughtBookingTrainer("bought_booking_trainer"),
  joiningTrainer("joining_trainer"),
  joiningParticipance("joining_participance"),
  boughtBookingParticipance("bought_booking_participance"),
  inviteParticipance("invite_participance"),
  createCompanyAdmin("create_company_admin"),
  createBookingAdmin("create_booking_admin"),
  createServiceAdmin("create_service_admin"),
  createEventAdmin("create_event_admin"),
  deleteBookingAdmin("delete_booking_admin"),
  deleteServiceAdmin("delete_service_admin"),
  deleteEventAdmin("delete_event_admin"),
  boughtBookingAdmin("bought_booking_admin"),
  boughtServiceAdmin("bought_service_admin"),
  joiningCompanyAdmin("joining_company_admin");

  final String value;
  const NotificationType(this.value);

  @override
  String toString() => value;
  String toMap() => value;

  static NotificationType? fromMap(String? value) {
    if (boughtBookingTrainer.value == value) return boughtBookingTrainer;
    if (joiningTrainer.value == value) return joiningTrainer;
    if (joiningParticipance.value == value) return joiningParticipance;
    if (boughtBookingParticipance.value == value) return boughtBookingParticipance;
    if (inviteParticipance.value == value) return inviteParticipance;
    if (createCompanyAdmin.value == value) return createCompanyAdmin;
    if (createBookingAdmin.value == value) return createBookingAdmin;
    if (createServiceAdmin.value == value) return createServiceAdmin;
    if (createEventAdmin.value == value) return createEventAdmin;
    if (deleteBookingAdmin.value == value) return deleteBookingAdmin;
    if (deleteServiceAdmin.value == value) return deleteServiceAdmin;
    if (deleteEventAdmin.value == value) return deleteEventAdmin;
    if (boughtBookingAdmin.value == value) return boughtBookingAdmin;
    if (boughtServiceAdmin.value == value) return boughtServiceAdmin;
    if (joiningCompanyAdmin.value == value) return joiningCompanyAdmin;
    return null;
  }
}

// const trainerTypes = [
//   NotificationType.bought_booking_trainer,
//   NotificationType.joining_trainer,
// ];
// const participanceTypes = [
//   NotificationType.joining_participance,
//   NotificationType.bought_booking_participance,
//   NotificationType.invite_participance,
// ];
// const adminTypes = [
//   NotificationType.create_company_admin,
//   NotificationType.create_booking_admin,
//   NotificationType.create_service_admin,
//   NotificationType.create_event_admin,
//   NotificationType.delete_booking_admin,
//   NotificationType.delete_service_admin,
//   NotificationType.delete_event_admin,
//   NotificationType.bought_booking_admin,
//   NotificationType.bought_service_admin,
//   NotificationType.joining_company_admin,
// ];
