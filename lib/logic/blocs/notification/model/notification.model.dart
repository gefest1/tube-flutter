// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:tube/logic/blocs/club/club.model.dart';
import 'package:tube/logic/blocs/notification/model/notify_type.model.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/logic/model/photourl.dart';
import 'package:tube/utils/date_time_parse.dart';

class NotificationEntity extends Equatable {
  final String? id;
  final User? admin;
  final Booking? booking;
  final Club? club;
  final DateTime? date;
  final Tariff? tariff;
  final User? trainer;
  final User? user;
  final NotificationType? type;
  final PhotoUrl? avatar;

  const NotificationEntity({
    this.id,
    this.admin,
    this.booking,
    this.club,
    this.date,
    this.tariff,
    this.trainer,
    this.user,
    this.type,
    this.avatar,
  });

  NotificationEntity copyWith({
    String? id,
    User? admin,
    Booking? booking,
    Club? club,
    DateTime? date,
    Tariff? tariff,
    User? trainer,
    User? user,
    NotificationType? type,
    PhotoUrl? avatar,
  }) {
    return NotificationEntity(
      id: id ?? this.id,
      admin: admin ?? this.admin,
      booking: booking ?? this.booking,
      club: club ?? this.club,
      date: date ?? this.date,
      tariff: tariff ?? this.tariff,
      trainer: trainer ?? this.trainer,
      user: user ?? this.user,
      type: type ?? this.type,
      avatar: avatar ?? this.avatar,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      '_id': id,
      'admin': admin?.toMap(),
      'booking': booking?.toMap(),
      'club': club?.toMap(),
      'date': date?.millisecondsSinceEpoch,
      'tariff': tariff?.toMap(),
      'trainer': trainer?.toMap(),
      'user': user?.toMap(),
      'type': type?.toMap(),
      'avatar': avatar?.toMap(),
    };
  }

  factory NotificationEntity.fromMap(Map<String, dynamic> map) {
    return NotificationEntity(
      id: map['_id'] != null ? map['_id'] as String : null,
      admin: map['admin'] != null ? User.fromMap(map['admin'] as Map<String, dynamic>) : null,
      booking: map['booking'] != null ? Booking.fromMap(map['booking'] as Map<String, dynamic>) : null,
      club: map['club'] != null ? Club.fromMap(map['club'] as Map<String, dynamic>) : null,
      date: map['date'] != null ? dateTimeParse(map['date']) : null,
      tariff: map['tariff'] != null ? Tariff.fromMap(map['tariff'] as Map<String, dynamic>) : null,
      trainer: map['trainer'] != null ? User.fromMap(map['trainer'] as Map<String, dynamic>) : null,
      user: map['user'] != null ? User.fromMap(map['user'] as Map<String, dynamic>) : null,
      type: map['type'] != null ? NotificationType.fromMap(map['type']) : null,
      avatar: map['avatar'] != null ? PhotoUrl.fromMap(map['avatar']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory NotificationEntity.fromJson(String source) =>
      NotificationEntity.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [
        id,
        admin,
        booking,
        club,
        date,
        tariff,
        trainer,
        user,
        type,
        avatar,
      ];
}
