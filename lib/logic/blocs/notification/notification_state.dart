part of './notification_bloc.dart';

abstract class NotificationState extends Equatable {
  const NotificationState();
}

class InitNotificationState extends NotificationState {
  const InitNotificationState();
  @override
  List<Object?> get props => [];
}

class DataNotificationState extends NotificationState {
  final List<NotificationEntity> notificationEntity;

  const DataNotificationState({required this.notificationEntity});

  @override
  List<Object?> get props => [notificationEntity];
}
