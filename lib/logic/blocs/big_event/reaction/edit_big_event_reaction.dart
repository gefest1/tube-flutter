part of '../big_event_bloc.dart';

extension EditBigEventReaction on AdminBigEventBloc {
  void editBigEventReaction(
    EditAdminBigEventEvent event,
    Emitter<AdminBigEventState> emit,
  ) async {
    final state = this.state;
    try {
      final preData = <BigEventEntity>[];
      if (state is DataAdminBigEventState) {
        preData.addAll(state.bigEvents);
      }
      final mutResult = await graphqlClient.mutate$editBigEvent(
        Options$Mutation$editBigEvent(
          variables: Variables$Mutation$editBigEvent(
            bigEventId: event.bigEventId,
            editBigEvent: Input$EditBigEvent(
              description: event.description,
            ),
          ),
        ),
      );

      if (mutResult.hasException) throw mutResult.exception!;

      final newData =
          BigEventEntity.fromMap(mutResult.parsedData!.editBigEvent.toJson());
      emit(DataAdminBigEventState(
        bigEvents: preData.map((e) {
          if (e.id != event.bigEventId) return e;
          return newData;
        }).toList(),
      ));
      event.completer?.complete(newData);
    } catch (e, s) {
      if (event.completer == null) rethrow;
      event.completer?.completeError(e, s);
    }
  }
}
