part of '../big_event_bloc.dart';

extension GetBigEventReaction on AdminBigEventBloc {
  void getBigEventReaction(
    GetAdminBigEventEvent event,
    Emitter<AdminBigEventState> emit,
  ) async {
    try {
      final queryRes = await graphqlClient.query$getBigEventAdmin(
        Options$Query$getBigEventAdmin(
          variables: Variables$Query$getBigEventAdmin(clubId: clubId),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      emit(
        DataAdminBigEventState(
          bigEvents: queryRes.parsedData!.getBigEventAdmin
              .map((e) => BigEventEntity.fromMap(e.toJson()))
              .toList(),
        ),
      );
      event.completer?.complete();
    } catch (e, s) {
      if (event.completer == null) rethrow;
      event.completer?.completeError(e, s);
    }
  }
}
