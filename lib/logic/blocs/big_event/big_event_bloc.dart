import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/big_event/entities/big_event_entity.dart';
import 'package:tube/logic/blocs/big_event/entities/create_big_entity.dart';

part 'big_event_event.dart';
part 'big_event_state.dart';
part 'reaction/add_big_event_reaction.dart';
part 'reaction/get_big_event_reaction.dart';
part 'reaction/remove_big_event_reaction.dart';
part 'reaction/edit_big_event_reaction.dart';

class AdminBigEventBloc
    extends HydratedBloc<AdminBigEventEvent, AdminBigEventState> {
  final String clubId;
  AdminBigEventBloc({
    required this.clubId,
  }) : super(const LoadingAdminBigEventState()) {
    on<AddBigEventEvent>(addBigEventReaction);
    on<GetAdminBigEventEvent>(getBigEventReaction);
    on<RemoveAdminBigEventEvent>(removeBigEventReaction);
    on<EditAdminBigEventEvent>(editBigEventReaction);
    add(const GetAdminBigEventEvent());
  }

  @override
  AdminBigEventState? fromJson(Map<String, dynamic> json) => null;

  @override
  Map<String, dynamic>? toJson(AdminBigEventState state) => null;
}
