// import 'package:graphql/client.dart';

// const addressDataGraphFragment = '''
// fragment AddressDataGraphFragment on AddressDataGraph {
//   city
// 	house
// 	street
// 	country
// }''';

// const bigEventGraphFragment = ''' 
// fragment BigEventGraphFragment on BigEventGraph {
//     _id
//     date
//     name
//     clubId
//     clientIds
//     finishDate
//     trainerIds
//     addressData {
//       ...AddressDataGraphFragment
//     }
//     description
//     durationMin
//     guestsEmail
// }''';

// final addBigEventDocument = gql('''
// $addressDataGraphFragment
// $bigEventGraphFragment
// query getBigEventAdmin(\$clubId: String!) {
//   getBigEventAdmin(clubId: \$clubId){
// 		...BigEventGraphFragment
//   }
// }
// mutation createBigEvent(
// 	\$trainerIds: [String!]
// 	\$clientIds: [String!]
// 	\$durationMin: Int!
// 	\$name: String!
// 	\$description: String!
// 	\$addressData: AddressDataInput!
// 	\$date: DateTime!
//   \$clubId: String!
//   \$guestsEmail: [String!]
// ){
// 	createBigEvent(
// 		trainerIds: \$trainerIds
// 		clientIds: \$clientIds
// 		durationMin: \$durationMin
// 		name: \$name
// 		description: \$description
// 		addressData: \$addressData
// 		date: \$date
//     clubId: \$clubId
// 	){
// 		...BigEventGraphFragment
//   }
// }
// mutation removeBigEvent(\$bigEventId: String!){
//   removeBigEvent(bigEventId: \$bigEventId)
// }
// mutation editBigEvent(\$bigEventId: String! \$editBigEvent: EditBigEvent!){
//   editBigEvent(bigEventId: \$bigEventId, editBigEvent: \$editBigEvent){
//     ...BigEventGraphFragment
//     }
// }
// ''');
