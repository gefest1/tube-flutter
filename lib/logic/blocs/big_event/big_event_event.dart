// ignore_for_file: public_member_api_docs, sort_constructors_first

part of 'big_event_bloc.dart';

abstract class AdminBigEventEvent {
  const AdminBigEventEvent();
}

class AddBigEventEvent extends AdminBigEventEvent {
  final CreateBigEventEntity entity;
  final Completer? completer;

  const AddBigEventEvent({
    required this.entity,
    this.completer,
  });
}

class GetAdminBigEventEvent extends AdminBigEventEvent {
  final Completer? completer;

  const GetAdminBigEventEvent({this.completer});
}

class RemoveAdminBigEventEvent extends AdminBigEventEvent {
  final String bigEventId;
  final Completer? completer;

  const RemoveAdminBigEventEvent({required this.bigEventId, this.completer});
}

class EditAdminBigEventEvent extends AdminBigEventEvent {
  final String bigEventId;
  final String? description;
  final Completer<BigEventEntity>? completer;

  const EditAdminBigEventEvent({
    required this.bigEventId,
    this.description,
    this.completer,
  });
}
