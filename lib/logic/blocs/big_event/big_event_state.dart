part of 'big_event_bloc.dart';

abstract class AdminBigEventState extends Equatable {
  const AdminBigEventState();
}

class LoadingAdminBigEventState extends AdminBigEventState {
  const LoadingAdminBigEventState();
  @override
  List<Object?> get props => [];
}

class DataAdminBigEventState extends AdminBigEventState {
  final List<BigEventEntity> bigEvents;

  const DataAdminBigEventState({
    this.bigEvents = const [],
  });

  @override
  List<Object?> get props => [bigEvents];
}
