// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';

class AddressDataEntity extends Equatable {
  final String street;
  final String country;
  final String house;
  final String city;
  const AddressDataEntity({
    required this.street,
    required this.country,
    required this.house,
    required this.city,
  });

  AddressDataEntity copyWith({
    String? street,
    String? country,
    String? house,
    String? city,
  }) {
    return AddressDataEntity(
      street: street ?? this.street,
      country: country ?? this.country,
      house: house ?? this.house,
      city: city ?? this.city,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'street': street,
      'country': country,
      'house': house,
      'city': city,
    };
  }

  factory AddressDataEntity.fromMap(Map<String, dynamic> map) {
    return AddressDataEntity(
      street: (map["street"] ?? '') as String,
      country: (map["country"] ?? '') as String,
      house: (map["house"] ?? '') as String,
      city: (map["city"] ?? '') as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory AddressDataEntity.fromJson(String source) =>
      AddressDataEntity.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [street, country, house, city];
}
