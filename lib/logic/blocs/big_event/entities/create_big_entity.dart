// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class CreateBigEventEntity {
  final List<String> trainerIds; //: [String!]
  final List<String> clientIds; //: [String!]
  final int durationMin; //: Int!
  final String name; //: String!
  final String description; //: String!
  final AddressDataEntity addressData; //: AddressDataInput!
  final DateTime date; //: DateTime!
  final String clubId;
  final List<String> guestsEmail;

  const CreateBigEventEntity({
    this.trainerIds = const [],
    this.clientIds = const [],
    required this.durationMin,
    required this.name,
    required this.description,
    required this.addressData,
    required this.date,
    required this.clubId,
    required this.guestsEmail,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'trainerIds': trainerIds,
      'clientIds': clientIds,
      'durationMin': durationMin,
      'name': name,
      'description': description,
      'addressData': addressData.toMap(),
      'date': date.toUtc().toIso8601String(),
      "clubId": clubId,
      'guestsEmail': guestsEmail,
    };
  }
}

class AddressDataEntity {
  final String country;
  final String city;
  final String street;
  final String house;

  const AddressDataEntity({
    required this.country,
    required this.city,
    required this.street,
    required this.house,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'country': country,
      'city': city,
      'street': street,
      'house': house,
    };
  }

  factory AddressDataEntity.fromMap(Map<String, dynamic> map) {
    return AddressDataEntity(
      country: (map["country"] ?? '') as String,
      city: (map["city"] ?? '') as String,
      street: (map["street"] ?? '') as String,
      house: (map["house"] ?? '') as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory AddressDataEntity.fromJson(String source) =>
      AddressDataEntity.fromMap(json.decode(source) as Map<String, dynamic>);
}
