import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:http/http.dart' show MultipartFile;
import 'package:image_picker/image_picker.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/club/club.model.dart';

import 'package:tube/utils/img_parser.dart';

part 'club_state.dart';
part 'club_event.dart';
part 'reaction/create_club.dart';

class ClubBloc extends HydratedBloc<ClubEvent, ClubState> {
  ClubBloc() : super(InitClubState()) {
    on<CreateClubEvent>(createClubReaction);
    on<RequestClubEvent>(requestClub);
  }

  @override
  ClubState? fromJson(Map<String, dynamic> json) {
    return null;
  }

  @override
  Map<String, dynamic>? toJson(ClubState state) {
    return null;
  }

  void requestClub(
    RequestClubEvent event,
    Emitter<ClubState> emit,
  ) async {
    try {
      final responce = await graphqlClient.mutate$requestClub(
        Options$Mutation$requestClub(
          variables: Variables$Mutation$requestClub(
              bankName: event.bankName,
              bankNumber: event.bankNumber,
              bin: event.bin,
              iikBank: event.iikBank,
              jurAddress: event.jurAddress,
              jurName: event.jurName,
              name: event.name,
              phoneNumber: event.phoneNumber,
              photo: MultipartFile.fromBytes(
                '',
                await byeImageExif(await event.file.readAsBytes()),
                filename: event.file.name,
              )),
        ),
      );
      if (responce.hasException) throw responce.exception!;
      event.completer?.complete();
    } catch (e, s) {
      event.completer?.completeError(e, s);
    }
  }
}
