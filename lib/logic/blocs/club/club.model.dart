// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:tube/logic/model/photourl.dart';

class Club extends Equatable {
  final String? id;
  final String? description;
  final String? fullName;
  final String? type;
  final PhotoUrl? photoUrl;
  final List<String>? tags;

  const Club({
    this.id,
    this.description,
    this.fullName,
    this.type,
    this.photoUrl,
    this.tags,
  });

  Club copyWith({
    String? id,
    String? description,
    String? fullName,
    String? type,
    PhotoUrl? photoUrl,
    List<String>? tags,
  }) {
    return Club(
      id: id ?? this.id,
      description: description ?? this.description,
      fullName: fullName ?? this.fullName,
      type: type ?? this.type,
      photoUrl: photoUrl ?? this.photoUrl,
      tags: tags ?? this.tags,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      '_id': id,
      'description': description,
      'fullName': fullName,
      'type': type,
      'photoUrl': photoUrl?.toMap(),
      'tags': tags,
    };
  }

  factory Club.fromMap(Map<String, dynamic> map) {
    return Club(
      id: map['_id'] != null ? map["_id"] : null,
      description: map['description'] != null ? map["description"] : null,
      fullName: map['fullName'] != null ? map["fullName"] : null,
      type: map['type'] != null ? map["type"] : null,
      photoUrl:
          map['photoUrl'] != null ? PhotoUrl.fromMap(map["photoUrl"]) : null,
      tags: map['tags'] != null ? List<String>.from(map['tags']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Club.fromJson(String source) =>
      Club.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Club(id: $id, description: $description, fullName: $fullName, type: $type, photoUrl: $photoUrl)';
  }

  @override
  List<Object?> get props {
    return [
      id,
      description,
      fullName,
      type,
      photoUrl,
      tags,
    ];
  }

  @override
  bool get stringify => true;
}
