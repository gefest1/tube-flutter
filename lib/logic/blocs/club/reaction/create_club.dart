part of '../club_bloc.dart';

extension CreatClubEventReaction on ClubBloc {
  void createClubReaction(
    CreateClubEvent event,
    Emitter<ClubState> emit,
  ) async {
    try {
      final mutationResult = await graphqlClient.mutate$createClub(
        Options$Mutation$createClub(
          variables: Variables$Mutation$createClub(
            description: event.description,
            type: event.type,
            fullName: event.name,
            photoUpload: MultipartFile.fromBytes(
              '',
              await byeImageExif(await event.file.readAsBytes()),
              filename: event.file.name,
            ),
          ),
        ),
      );
      // final mutationResult = await client.mutate(
      //   MutationOptions(
      //     document: gql('''
      //       mutation (
      //       		\$description: String!
      //           \$type: String!
      //           \$fullName: String!
      //         	\$photoUpload: Upload!
      //       ){
      //         createClub(
      //           description: \$description
      //           type: \$type
      //           fullName: \$fullName
      //           photoUrl: \$photoUpload
      //         ){
      //           _id
      //           fullName
      //           photoUrl {
      //             M
      //             XL
      //             thumbnail
      //           }
      //           description
      //           type
      //         }
      //       }'''),
      //     variables: {
      //       "description": event.description,
      //       "type": event.type,
      //       "fullName": event.name,
      //       "photoUpload": MultipartFile.fromBytes(
      //         '',
      //         await byeImageExif(await event.file.readAsBytes()),
      //         filename: event.file.name,
      //       ),
      //     },
      //   ),
      // );

      if (mutationResult.hasException) throw mutationResult.exception!;
      final club = Club.fromMap(mutationResult.parsedData!.createClub.toJson());
      if (event.completer != null) event.completer!.complete(club);
    } catch (e, s) {
      if (event.completer != null) event.completer!.completeError(e, s);
    }
  }
}
