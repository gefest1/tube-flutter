part of './club_bloc.dart';

abstract class ClubState extends Equatable {}

class InitClubState extends ClubState {
  @override
  List<Object?> get props => [];
}
