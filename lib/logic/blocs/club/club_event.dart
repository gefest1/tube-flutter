part of './club_bloc.dart';

abstract class ClubEvent {
  const ClubEvent();
}

class CreateClubEvent extends ClubEvent {
  final String name;
  final String type;
  final String description;
  final XFile file;
  final Completer<Club>? completer;

  const CreateClubEvent({
    required this.name,
    required this.type,
    required this.description,
    required this.file,
    this.completer,
  });
}

class RequestClubEvent extends ClubEvent {
  final String name;
  final String phoneNumber;
  final String bin;
  final String jurName;
  final String jurAddress;
  final String bankNumber;
  final String bankName;
  final String iikBank;
  final XFile file;

  final Completer? completer;

  const RequestClubEvent({
    required this.name,
    required this.phoneNumber,
    required this.bin,
    required this.jurName,
    required this.jurAddress,
    required this.bankNumber,
    required this.bankName,
    required this.iikBank,
    required this.file,
    this.completer,
  });
}
