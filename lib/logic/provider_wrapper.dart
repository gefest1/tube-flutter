import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:tube/logic/blocs/finance/finance_provider.dart';
import 'package:tube/logic/model/admin/create.booking.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/logic/provider/big_user_map.dart';
import 'package:tube/logic/provider/booking/get_special_booking.dart';
// import 'package:tube/main.dart';

String? fastToken;

class ProviderWrapper extends StatefulWidget {
  final Widget child;

  const ProviderWrapper({
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  State<ProviderWrapper> createState() => _ProviderWrapperState();
}

class _ProviderWrapperState extends State<ProviderWrapper> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        // Provider<GraphQLClient>(
        //   create: (_) => graphQLClient ??= getGraphLink(),
        // ),
        ChangeNotifierProvider<CreateBookingModel>(create: (_) {
          return CreateBookingModel(booking: const Booking());
        }),
        ChangeNotifierProvider<GetSpecialBooking>(
          create: (_) => GetSpecialBooking(),
        ),
        Provider(
          create: (context) => FinanceProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => BigUserMap(),
        )
      ],
      child: widget.child,
    );
  }
}
