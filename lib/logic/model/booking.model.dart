import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:tube/logic/blocs/club/club.model.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/sub_booking.model.dart';
import 'package:tube/logic/model/trainer_booking_graph.dart';
import 'package:tube/utils/date_time_parse.dart';

class WorkTimeObj extends Equatable {
  final num? now;
  final num max;
  final DateTime? date;

  const WorkTimeObj({
    this.now,
    this.max = 10,
    this.date,
  });

  WorkTimeObj copyWith({
    num? now,
    num? max,
    DateTime? date,
  }) {
    return WorkTimeObj(
      now: now ?? this.now,
      max: max ?? this.max,
      date: date ?? this.date,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'now': now,
      'max': max,
      'date': date?.toUtc().toIso8601String(),
    };
  }

  factory WorkTimeObj.fromMap(Map<String, dynamic> map) {
    return WorkTimeObj(
      now: map['now'],
      max: map['max'] ?? 10,
      date: map['date'] != null ? dateTimeParse(map['date']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory WorkTimeObj.fromJson(String source) =>
      WorkTimeObj.fromMap(json.decode(source));

  @override
  String toString() => 'WorkTimeObj(now: $now, max: $max, date: $date)';

  @override
  List<Object?> get props => [now, max, date];
}

class Booking extends Equatable {
  final String? id;
  final Club? club;
  final String? clubId;
  final String? description;
  final String? name;
  final num? price;
  final num? timePerMinute;
  final List<TrainerBookingGraph>? trainerBooking;
  final DateTime? expireDate;
  final num? count;
  final num? percent;
  final List<User>? trainers;
  final Map<String, List<WorkTimeObj>>? workTimes;
  final int? deadlineDays;
  final int? studentNumber;
  final List<String> tags;
  final List<SubBooking> subBooking;

  final DateTime? stopDate;

  const Booking({
    this.id,
    this.club,
    this.clubId,
    this.description,
    this.name,
    this.price,
    this.timePerMinute,
    this.workTimes,
    this.trainerBooking,
    this.expireDate,
    this.count,
    this.percent,
    this.trainers,
    this.deadlineDays,
    this.studentNumber,
    this.tags = const [],
    this.subBooking = const [],
    this.stopDate,
  });

  Booking copyWith({
    String? id,
    Club? club,
    String? clubId,
    String? description,
    String? name,
    num? price,
    num? timePerMinute,
    List<TrainerBookingGraph>? trainerBooking,
    DateTime? expireDate,
    num? count,
    num? percent,
    List<User>? trainers,
    Map<String, List<WorkTimeObj>>? workTimes,
    int? deadlineDays,
    int? studentNumber,
    List<String>? tags,
    List<SubBooking>? subBooking,
    DateTime? stopDate,
  }) {
    return Booking(
      id: id ?? this.id,
      club: club ?? this.club,
      clubId: clubId ?? this.clubId,
      description: description ?? this.description,
      name: name ?? this.name,
      price: price ?? this.price,
      timePerMinute: timePerMinute ?? this.timePerMinute,
      workTimes: workTimes ?? this.workTimes,
      trainerBooking: trainerBooking ?? this.trainerBooking,
      expireDate: expireDate ?? this.expireDate,
      count: count ?? this.count,
      percent: percent ?? this.percent,
      trainers: trainers ?? this.trainers,
      deadlineDays: deadlineDays ?? this.deadlineDays,
      studentNumber: studentNumber ?? this.studentNumber,
      tags: tags ?? this.tags,
      subBooking: subBooking ?? this.subBooking,
      stopDate: stopDate ?? this.stopDate,
    );
  }

  Booking removeStopDate() {
    return Booking(
      id: id,
      club: club,
      clubId: clubId,
      description: description,
      name: name,
      price: price,
      timePerMinute: timePerMinute,
      workTimes: workTimes,
      trainerBooking: trainerBooking,
      expireDate: expireDate,
      count: count,
      percent: percent,
      trainers: trainers,
      deadlineDays: deadlineDays,
      studentNumber: studentNumber,
      tags: tags,
      subBooking: subBooking,
      stopDate: null,
    );
  }

  Map<String, dynamic> toMap() {
    final obj = {
      '_id': id,
      'club': club?.toMap(),
      'clubId': clubId,
      'description': description,
      'name': name,
      'price': price,
      'timePerMinute': timePerMinute,
      'trainerBooking': trainerBooking?.map((x) => x.toMap()).toList(),
      'expireDate': expireDate?.toUtc().toIso8601String(),
      'count': count,
      'percent': percent,
      'trainers': trainers?.map((e) => e.toMap()).toList(),
      'deadlineDays': deadlineDays,
      'studentNumber': studentNumber,
      'tags': tags,
      'subBooking': subBooking.map((e) => e.toMap()).toList(),
      'stopDate': stopDate?.toUtc().toIso8601String(),
    };
    if (workTimes != null) {
      final List<List<WorkTimeObj>> workTimesDates = [];
      final List<String> workTimesUserIds = [];

      for (final MapEntry<String, List<WorkTimeObj>> entry
          in (workTimes?.entries ?? const Iterable.empty())) {
        workTimesUserIds.add(entry.key);
        workTimesDates.add(entry.value);
      }

      obj['workTimesUserIds'] = workTimesUserIds;
      obj['workTimesObj'] =
          workTimesDates.map((e) => e.map((e) => e.toMap()).toList()).toList();
    }

    return obj;
  }

  factory Booking.fromMap(Map<String, dynamic> map) {
    Map<String, List<WorkTimeObj>>? mapWorkTime;

    if (map['workTimesUserIds'] != null && map['workTimesObj'] != null) {
      final workTimesUserIds = List<String>.from(map['workTimesUserIds']);
      final workTimesObj = List<List<WorkTimeObj>>.from(
        (map['workTimesObj']! as List<dynamic>).map(
          (dynamic x) =>
              (x as List<dynamic>).map((e) => WorkTimeObj.fromMap(e)).toList(),
        ),
      );
      mapWorkTime = {};
      for (int index = 0; index < workTimesUserIds.length; ++index) {
        mapWorkTime[workTimesUserIds[index]] = workTimesObj[index];
      }
    }
    return Booking(
      id: map['_id'],
      club: map['club'] != null ? Club.fromMap(map['club']) : null,
      clubId: map['clubId'],
      description: map['description'],
      name: map['name'],
      price: map['price'],
      timePerMinute: map['timePerMinute'],
      workTimes: mapWorkTime,
      trainerBooking: map['trainerBooking'] != null
          ? List<TrainerBookingGraph>.from(
              map['trainerBooking']?.map((x) => TrainerBookingGraph.fromMap(x)))
          : null,
      expireDate: dateTimeParse(map['expireDate']),
      count: map['count'],
      percent: map['percent'],
      trainers: map['trainers'] != null
          ? List<User>.from(map['trainers']?.map((x) => User.fromMap(x)))
          : null,
      deadlineDays: map['deadlineDays'],
      studentNumber: map['studentNumber'],
      tags: List<String>.from(map['tags'] ?? []),
      subBooking: List<SubBooking>.from(
          (map['subBooking'] ?? [])?.map((x) => SubBooking.fromMap(x))),
      stopDate: dateTimeParse(map['stopDate']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Booking.fromJson(String source) =>
      Booking.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Booking(id: $id,\n club: $club,\n clubId: $clubId,\n description: $description,\n name: $name,\n price: $price,\n timePerMinute: $timePerMinute,\n workTimes: $workTimes,\n trainerBooking: $trainerBooking,\n expireDate: $expireDate,\n percent: $percent\n)';
  }

  @override
  List<Object?> get props {
    return [
      id,
      club,
      clubId,
      description,
      name,
      price,
      timePerMinute,
      workTimes,
      trainerBooking,
      expireDate,
      count,
      percent,
      deadlineDays,
      studentNumber,
      tags,
      subBooking,
      stopDate,
    ];
  }
}
