import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:tube/utils/date_time_parse.dart';

class WorkTimeGraph extends Equatable {
  final DateTime? startTime;
  final DateTime? endTime;

  const WorkTimeGraph({
    this.startTime,
    this.endTime,
  });

  WorkTimeGraph copyWith({
    DateTime? startTime,
    DateTime? endTime,
  }) {
    return WorkTimeGraph(
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'startTime': startTime?.toUtc().toIso8601String(),
      'endTime': endTime?.toUtc().toIso8601String(),
    };
  }

  factory WorkTimeGraph.fromMap(Map<String, dynamic> map) {
    return WorkTimeGraph(
      startTime:
          map['startTime'] != null ? dateTimeParse(map['startTime']) : null,
      endTime: map['endTime'] != null ? dateTimeParse(map['endTime']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory WorkTimeGraph.fromJson(String source) =>
      WorkTimeGraph.fromMap(json.decode(source));

  @override
  String toString() =>
      'WorkTimeGraph(startTime: $startTime, endTime: $endTime)';

  @override
  List<Object?> get props => [
        endTime,
        startTime,
      ];
}
