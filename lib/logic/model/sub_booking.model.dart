// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';

class SubBooking extends Equatable {
  final double price;
  final int count;
  final int deadlineDays;

  const SubBooking({
    required this.price,
    required this.count,
    required this.deadlineDays,
  });

  SubBooking copyWith({
    double? price,
    int? count,
    int? deadlineDays,
  }) {
    return SubBooking(
      price: price ?? this.price,
      count: count ?? this.count,
      deadlineDays: deadlineDays ?? this.deadlineDays,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'price': price,
      'count': count,
      'deadlineDays': deadlineDays,
    };
  }

  factory SubBooking.fromMap(Map<String, dynamic> map) {
    return SubBooking(
      price: map['price'] as double,
      count: map['count'] as int,
      deadlineDays: map['deadlineDays'] as int,
    );
  }

  String toJson() => json.encode(toMap());

  factory SubBooking.fromJson(String source) =>
      SubBooking.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [price, count, deadlineDays];
}
