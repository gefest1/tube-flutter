import 'package:flutter/material.dart';

//                         userId  weekDay  dateTime
typedef WorkTimeType = Map<String, Map<int, List<DateTime>>>;

class EditBookingModel extends ChangeNotifier {
  EditBookingModel({
    bool concernsOld = false,
    WorkTimeType? workTime,
    required int timePerMinute,
    int? count,
    int? studentNumber,
  }) {
    _concernsOld = concernsOld;
    _workTime = workTime ?? _workTime;
    _timePerMinute = timePerMinute;
    _count = count;
    _studentNumber = _studentNumber;
  }

  List<ModalRoute> routes = [];
  bool _concernsOld = false;
  bool get concernsOld => _concernsOld;
  set concernsOld(bool newVal) {
    _concernsOld = newVal;
    notifyListeners();
  }

  WorkTimeType? _workTime;
  WorkTimeType? get workTime => _workTime;
  set workTime(WorkTimeType? newVal) {
    _workTime = newVal;
    notifyListeners();
  }

  // IF CHANGED should open new page
  late int _timePerMinute;
  int get timePerMinute => _timePerMinute;
  set timePerMinute(int newVal) {
    _timePerMinute = newVal;
    notifyListeners();
  }

  int? _count;
  int? get count => _count;
  set count(int? newVal) {
    _count = newVal;
    notifyListeners();
  }

  int? _deadlineDays;
  int? get deadlineDays => _deadlineDays;
  set deadlineDays(int? newVal) {
    _deadlineDays = newVal;
    notifyListeners();
  }

  int? _studentNumber;
  int? get studentNumber => _studentNumber;
  set studentNumber(int? newVal) {
    _studentNumber = newVal;
    notifyListeners();
  }
}
