import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:tube/utils/date_time_parse.dart';

class InputTrainGraph extends Equatable {
  final DateTime date;
  final DateTime endTime;
  final String trainerId;
  final String? id;

  const InputTrainGraph({
    required this.date,
    required this.endTime,
    required this.trainerId,
    this.id,
  });

  InputTrainGraph copyWith({
    DateTime? date,
    DateTime? endTime,
    String? trainerId,
    String? id,
  }) {
    return InputTrainGraph(
      date: date ?? this.date,
      endTime: endTime ?? this.endTime,
      trainerId: trainerId ?? this.trainerId,
      id: id ?? this.id,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'date': date.toUtc().toIso8601String(),
      'endTime': endTime.toUtc().toIso8601String(),
      'trainerId': trainerId,
      "_id": id,
    };
  }

  factory InputTrainGraph.fromMap(Map<String, dynamic> map) {
    return InputTrainGraph(
      date: dateTimeParse(map['date'])!,
      endTime: dateTimeParse(map['endTime'])!,
      trainerId: map['trainerId']!,
      id: map['_id'],
    );
  }

  String toJson() => json.encode(toMap());

  factory InputTrainGraph.fromJson(String source) =>
      InputTrainGraph.fromMap(json.decode(source));

  @override
  String toString() =>
      'InputTrainGraph(date: $date, endTime: $endTime, trainerId: $trainerId)';

  @override
  List<Object?> get props => [date, endTime, trainerId, id];
}
