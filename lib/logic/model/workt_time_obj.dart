import 'dart:convert';

import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/utils/date_time_parse.dart';

class WorkTimeObjTrainer extends WorkTimeObj {
  final User trainer;

  const WorkTimeObjTrainer({
    num? now,
    num? max,
    DateTime? date,
    required this.trainer,
  }) : super(
          now: now,
          max: max ?? 10,
          date: date,
        );

  @override
  WorkTimeObjTrainer copyWith({
    num? now,
    num? max,
    DateTime? date,
    User? trainer,
  }) {
    return WorkTimeObjTrainer(
      now: now ?? this.now,
      max: max ?? this.max,
      date: date ?? this.date,
      trainer: trainer ?? this.trainer,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'now': now,
      'max': max,
      'date': date?.toUtc().toIso8601String(),
      'trainer': trainer.toMap(),
    };
  }

  factory WorkTimeObjTrainer.fromMap(Map<String, dynamic> map) {
    return WorkTimeObjTrainer(
      now: map['now'],
      max: map['max'],
      date: map['date'] != null ? dateTimeParse(map['date']) : null,
      trainer: User.fromMap(map['trainer']),
    );
  }

  factory WorkTimeObjTrainer.fromJson(String source) =>
      WorkTimeObjTrainer.fromMap(json.decode(source));

  @override
  String toString() => 'WorkTimeObjTrainer(now: $now, max: $max, date: $date)';

  @override
  List<Object?> get props => [now, max, date];
}
