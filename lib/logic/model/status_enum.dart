import 'package:tube/logic/api/graphql_client.dart';

enum StatusEnum {
  admin('admin'),
  trainer('trainer'),
  participance('participance'),
  waiting('waiting');

  final String _value;

  const StatusEnum(String value) : _value = value;
  String toStrValue() {
    if (this == admin) {
      return 'Админ';
    }
    if (this == participance) {
      return 'Клиент';
    }
    if (this == trainer) {
      return 'Сотрудник';
    }
    if (this == waiting) {
      return 'Ожидание';
    }
    throw 'status not found';
  }

  Enum$StatusEnumParticipance get genratedValue {
    if (_value == 'admin') {
      return Enum$StatusEnumParticipance.admin;
    }
    if (_value == 'trainer') {
      return Enum$StatusEnumParticipance.trainer;
    }
    if (_value == 'participance') {
      return Enum$StatusEnumParticipance.participance;
    }
    if (_value == 'waiting') {
      return Enum$StatusEnumParticipance.waiting;
    }
    return Enum$StatusEnumParticipance.$unknown;
  }

  static StatusEnum? fromMap(String? value) {
    if ('admin' == value) return admin;
    if ('trainer' == value) return trainer;
    if ('participance' == value) return participance;
    if ('waiting' == value) return waiting;

    return null;
  }

  @override
  String toString() => _value;
  String toMap() => _value;
}
