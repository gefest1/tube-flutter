import 'package:graphql/client.dart';
import 'package:tube/logic/provider_wrapper.dart';
import 'package:tube/main.dart';
import 'package:tube/utils/const.dart';

// ignore: unused_import
import 'graphql/__generated/request.graphql.dart';
// ignore: unused_import
import 'graphql/__generated/schema.graphql.dart';

// ignore: unused_import

export 'graphql/__generated/request.graphql.dart';
export 'graphql/__generated/schema.graphql.dart';

GraphQLClient getGraphLink() {
  final AuthLink authLink = AuthLink(
    getToken: () async {
      return sharedBox.get('token') ?? fastToken;
    },
  );
  final uri = Uri.parse('$httpUrlPath/graphql');

  final Link link = Link.split(
    (request) {
      return request.isSubscription;
    },
    WebSocketLink(
      () {
        final url = uri.replace(scheme: 'wss', port: null).toString();

        return url;
      }(),
      config: SocketClientConfig(
        autoReconnect: true,
        inactivityTimeout: const Duration(seconds: 30),
        initialPayload: () async {
          final token = sharedBox.get('token') ?? fastToken;
          return {
            'Authorization': token,
          };
        },
      ),
    ),
    authLink.concat(HttpLink(uri.toString())),
  );
  return GraphQLClient(
    link: link,
    cache: GraphQLCache(),
  );
}

GraphQLClient graphqlClient = getGraphLink();
