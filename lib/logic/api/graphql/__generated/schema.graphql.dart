import 'package:tube/utils/date_time_parse.dart';

class Input$AddressDataInput {
  factory Input$AddressDataInput({
    required String city,
    required String country,
    required String house,
    required String street,
  }) =>
      Input$AddressDataInput._({
        r'city': city,
        r'country': country,
        r'house': house,
        r'street': street,
      });

  Input$AddressDataInput._(this._$data);

  factory Input$AddressDataInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$city = data['city'];
    result$data['city'] = (l$city as String);
    final l$country = data['country'];
    result$data['country'] = (l$country as String);
    final l$house = data['house'];
    result$data['house'] = (l$house as String);
    final l$street = data['street'];
    result$data['street'] = (l$street as String);
    return Input$AddressDataInput._(result$data);
  }

  Map<String, dynamic> _$data;

  String get city => (_$data['city'] as String);

  String get country => (_$data['country'] as String);

  String get house => (_$data['house'] as String);

  String get street => (_$data['street'] as String);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$city = city;
    result$data['city'] = l$city;
    final l$country = country;
    result$data['country'] = l$country;
    final l$house = house;
    result$data['house'] = l$house;
    final l$street = street;
    result$data['street'] = l$street;
    return result$data;
  }

  CopyWith$Input$AddressDataInput<Input$AddressDataInput> get copyWith =>
      CopyWith$Input$AddressDataInput(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$AddressDataInput) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$city = city;
    final lOther$city = other.city;
    if (l$city != lOther$city) {
      return false;
    }
    final l$country = country;
    final lOther$country = other.country;
    if (l$country != lOther$country) {
      return false;
    }
    final l$house = house;
    final lOther$house = other.house;
    if (l$house != lOther$house) {
      return false;
    }
    final l$street = street;
    final lOther$street = other.street;
    if (l$street != lOther$street) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$city = city;
    final l$country = country;
    final l$house = house;
    final l$street = street;
    return Object.hashAll([
      l$city,
      l$country,
      l$house,
      l$street,
    ]);
  }
}

abstract class CopyWith$Input$AddressDataInput<TRes> {
  factory CopyWith$Input$AddressDataInput(
    Input$AddressDataInput instance,
    TRes Function(Input$AddressDataInput) then,
  ) = _CopyWithImpl$Input$AddressDataInput;

  factory CopyWith$Input$AddressDataInput.stub(TRes res) =
      _CopyWithStubImpl$Input$AddressDataInput;

  TRes call({
    String? city,
    String? country,
    String? house,
    String? street,
  });
}

class _CopyWithImpl$Input$AddressDataInput<TRes>
    implements CopyWith$Input$AddressDataInput<TRes> {
  _CopyWithImpl$Input$AddressDataInput(
    this._instance,
    this._then,
  );

  final Input$AddressDataInput _instance;

  final TRes Function(Input$AddressDataInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? city = _undefined,
    Object? country = _undefined,
    Object? house = _undefined,
    Object? street = _undefined,
  }) =>
      _then(Input$AddressDataInput._({
        ..._instance._$data,
        if (city != _undefined && city != null) 'city': (city as String),
        if (country != _undefined && country != null)
          'country': (country as String),
        if (house != _undefined && house != null) 'house': (house as String),
        if (street != _undefined && street != null)
          'street': (street as String),
      }));
}

class _CopyWithStubImpl$Input$AddressDataInput<TRes>
    implements CopyWith$Input$AddressDataInput<TRes> {
  _CopyWithStubImpl$Input$AddressDataInput(this._res);

  TRes _res;

  call({
    String? city,
    String? country,
    String? house,
    String? street,
  }) =>
      _res;
}

class Input$EditBigEvent {
  factory Input$EditBigEvent({
    Input$AddressDataInput? addressData,
    List<String>? clientIds,
    String? clubId,
    DateTime? date,
    String? description,
    int? durationMin,
    List<String>? guestsEmail,
    String? name,
    List<String>? trainerIds,
  }) =>
      Input$EditBigEvent._({
        if (addressData != null) r'addressData': addressData,
        if (clientIds != null) r'clientIds': clientIds,
        if (clubId != null) r'clubId': clubId,
        if (date != null) r'date': date,
        if (description != null) r'description': description,
        if (durationMin != null) r'durationMin': durationMin,
        if (guestsEmail != null) r'guestsEmail': guestsEmail,
        if (name != null) r'name': name,
        if (trainerIds != null) r'trainerIds': trainerIds,
      });

  Input$EditBigEvent._(this._$data);

  factory Input$EditBigEvent.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    if (data.containsKey('addressData')) {
      final l$addressData = data['addressData'];
      result$data['addressData'] = l$addressData == null
          ? null
          : Input$AddressDataInput.fromJson(
              (l$addressData as Map<String, dynamic>));
    }
    if (data.containsKey('clientIds')) {
      final l$clientIds = data['clientIds'];
      result$data['clientIds'] =
          (l$clientIds as List<dynamic>?)?.map((e) => (e as String)).toList();
    }
    if (data.containsKey('clubId')) {
      final l$clubId = data['clubId'];
      result$data['clubId'] = (l$clubId as String?);
    }
    if (data.containsKey('date')) {
      final l$date = data['date'];
      result$data['date'] = l$date == null ? null : dateTimeParseG(l$date);
    }
    if (data.containsKey('description')) {
      final l$description = data['description'];
      result$data['description'] = (l$description as String?);
    }
    if (data.containsKey('durationMin')) {
      final l$durationMin = data['durationMin'];
      result$data['durationMin'] = (l$durationMin as int?);
    }
    if (data.containsKey('guestsEmail')) {
      final l$guestsEmail = data['guestsEmail'];
      result$data['guestsEmail'] =
          (l$guestsEmail as List<dynamic>?)?.map((e) => (e as String)).toList();
    }
    if (data.containsKey('name')) {
      final l$name = data['name'];
      result$data['name'] = (l$name as String?);
    }
    if (data.containsKey('trainerIds')) {
      final l$trainerIds = data['trainerIds'];
      result$data['trainerIds'] =
          (l$trainerIds as List<dynamic>?)?.map((e) => (e as String)).toList();
    }
    return Input$EditBigEvent._(result$data);
  }

  Map<String, dynamic> _$data;

  Input$AddressDataInput? get addressData =>
      (_$data['addressData'] as Input$AddressDataInput?);

  List<String>? get clientIds => (_$data['clientIds'] as List<String>?);

  String? get clubId => (_$data['clubId'] as String?);

  DateTime? get date => (_$data['date'] as DateTime?);

  String? get description => (_$data['description'] as String?);

  int? get durationMin => (_$data['durationMin'] as int?);

  List<String>? get guestsEmail => (_$data['guestsEmail'] as List<String>?);

  String? get name => (_$data['name'] as String?);

  List<String>? get trainerIds => (_$data['trainerIds'] as List<String>?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    if (_$data.containsKey('addressData')) {
      final l$addressData = addressData;
      result$data['addressData'] = l$addressData?.toJson();
    }
    if (_$data.containsKey('clientIds')) {
      final l$clientIds = clientIds;
      result$data['clientIds'] = l$clientIds?.map((e) => e).toList();
    }
    if (_$data.containsKey('clubId')) {
      final l$clubId = clubId;
      result$data['clubId'] = l$clubId;
    }
    if (_$data.containsKey('date')) {
      final l$date = date;
      result$data['date'] = l$date == null ? null : toMapDateTimeParseG(l$date);
    }
    if (_$data.containsKey('description')) {
      final l$description = description;
      result$data['description'] = l$description;
    }
    if (_$data.containsKey('durationMin')) {
      final l$durationMin = durationMin;
      result$data['durationMin'] = l$durationMin;
    }
    if (_$data.containsKey('guestsEmail')) {
      final l$guestsEmail = guestsEmail;
      result$data['guestsEmail'] = l$guestsEmail?.map((e) => e).toList();
    }
    if (_$data.containsKey('name')) {
      final l$name = name;
      result$data['name'] = l$name;
    }
    if (_$data.containsKey('trainerIds')) {
      final l$trainerIds = trainerIds;
      result$data['trainerIds'] = l$trainerIds?.map((e) => e).toList();
    }
    return result$data;
  }

  CopyWith$Input$EditBigEvent<Input$EditBigEvent> get copyWith =>
      CopyWith$Input$EditBigEvent(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$EditBigEvent) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$addressData = addressData;
    final lOther$addressData = other.addressData;
    if (_$data.containsKey('addressData') !=
        other._$data.containsKey('addressData')) {
      return false;
    }
    if (l$addressData != lOther$addressData) {
      return false;
    }
    final l$clientIds = clientIds;
    final lOther$clientIds = other.clientIds;
    if (_$data.containsKey('clientIds') !=
        other._$data.containsKey('clientIds')) {
      return false;
    }
    if (l$clientIds != null && lOther$clientIds != null) {
      if (l$clientIds.length != lOther$clientIds.length) {
        return false;
      }
      for (int i = 0; i < l$clientIds.length; i++) {
        final l$clientIds$entry = l$clientIds[i];
        final lOther$clientIds$entry = lOther$clientIds[i];
        if (l$clientIds$entry != lOther$clientIds$entry) {
          return false;
        }
      }
    } else if (l$clientIds != lOther$clientIds) {
      return false;
    }
    final l$clubId = clubId;
    final lOther$clubId = other.clubId;
    if (_$data.containsKey('clubId') != other._$data.containsKey('clubId')) {
      return false;
    }
    if (l$clubId != lOther$clubId) {
      return false;
    }
    final l$date = date;
    final lOther$date = other.date;
    if (_$data.containsKey('date') != other._$data.containsKey('date')) {
      return false;
    }
    if (l$date != lOther$date) {
      return false;
    }
    final l$description = description;
    final lOther$description = other.description;
    if (_$data.containsKey('description') !=
        other._$data.containsKey('description')) {
      return false;
    }
    if (l$description != lOther$description) {
      return false;
    }
    final l$durationMin = durationMin;
    final lOther$durationMin = other.durationMin;
    if (_$data.containsKey('durationMin') !=
        other._$data.containsKey('durationMin')) {
      return false;
    }
    if (l$durationMin != lOther$durationMin) {
      return false;
    }
    final l$guestsEmail = guestsEmail;
    final lOther$guestsEmail = other.guestsEmail;
    if (_$data.containsKey('guestsEmail') !=
        other._$data.containsKey('guestsEmail')) {
      return false;
    }
    if (l$guestsEmail != null && lOther$guestsEmail != null) {
      if (l$guestsEmail.length != lOther$guestsEmail.length) {
        return false;
      }
      for (int i = 0; i < l$guestsEmail.length; i++) {
        final l$guestsEmail$entry = l$guestsEmail[i];
        final lOther$guestsEmail$entry = lOther$guestsEmail[i];
        if (l$guestsEmail$entry != lOther$guestsEmail$entry) {
          return false;
        }
      }
    } else if (l$guestsEmail != lOther$guestsEmail) {
      return false;
    }
    final l$name = name;
    final lOther$name = other.name;
    if (_$data.containsKey('name') != other._$data.containsKey('name')) {
      return false;
    }
    if (l$name != lOther$name) {
      return false;
    }
    final l$trainerIds = trainerIds;
    final lOther$trainerIds = other.trainerIds;
    if (_$data.containsKey('trainerIds') !=
        other._$data.containsKey('trainerIds')) {
      return false;
    }
    if (l$trainerIds != null && lOther$trainerIds != null) {
      if (l$trainerIds.length != lOther$trainerIds.length) {
        return false;
      }
      for (int i = 0; i < l$trainerIds.length; i++) {
        final l$trainerIds$entry = l$trainerIds[i];
        final lOther$trainerIds$entry = lOther$trainerIds[i];
        if (l$trainerIds$entry != lOther$trainerIds$entry) {
          return false;
        }
      }
    } else if (l$trainerIds != lOther$trainerIds) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$addressData = addressData;
    final l$clientIds = clientIds;
    final l$clubId = clubId;
    final l$date = date;
    final l$description = description;
    final l$durationMin = durationMin;
    final l$guestsEmail = guestsEmail;
    final l$name = name;
    final l$trainerIds = trainerIds;
    return Object.hashAll([
      _$data.containsKey('addressData') ? l$addressData : const {},
      _$data.containsKey('clientIds')
          ? l$clientIds == null
              ? null
              : Object.hashAll(l$clientIds.map((v) => v))
          : const {},
      _$data.containsKey('clubId') ? l$clubId : const {},
      _$data.containsKey('date') ? l$date : const {},
      _$data.containsKey('description') ? l$description : const {},
      _$data.containsKey('durationMin') ? l$durationMin : const {},
      _$data.containsKey('guestsEmail')
          ? l$guestsEmail == null
              ? null
              : Object.hashAll(l$guestsEmail.map((v) => v))
          : const {},
      _$data.containsKey('name') ? l$name : const {},
      _$data.containsKey('trainerIds')
          ? l$trainerIds == null
              ? null
              : Object.hashAll(l$trainerIds.map((v) => v))
          : const {},
    ]);
  }
}

abstract class CopyWith$Input$EditBigEvent<TRes> {
  factory CopyWith$Input$EditBigEvent(
    Input$EditBigEvent instance,
    TRes Function(Input$EditBigEvent) then,
  ) = _CopyWithImpl$Input$EditBigEvent;

  factory CopyWith$Input$EditBigEvent.stub(TRes res) =
      _CopyWithStubImpl$Input$EditBigEvent;

  TRes call({
    Input$AddressDataInput? addressData,
    List<String>? clientIds,
    String? clubId,
    DateTime? date,
    String? description,
    int? durationMin,
    List<String>? guestsEmail,
    String? name,
    List<String>? trainerIds,
  });
  CopyWith$Input$AddressDataInput<TRes> get addressData;
}

class _CopyWithImpl$Input$EditBigEvent<TRes>
    implements CopyWith$Input$EditBigEvent<TRes> {
  _CopyWithImpl$Input$EditBigEvent(
    this._instance,
    this._then,
  );

  final Input$EditBigEvent _instance;

  final TRes Function(Input$EditBigEvent) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? addressData = _undefined,
    Object? clientIds = _undefined,
    Object? clubId = _undefined,
    Object? date = _undefined,
    Object? description = _undefined,
    Object? durationMin = _undefined,
    Object? guestsEmail = _undefined,
    Object? name = _undefined,
    Object? trainerIds = _undefined,
  }) =>
      _then(Input$EditBigEvent._({
        ..._instance._$data,
        if (addressData != _undefined)
          'addressData': (addressData as Input$AddressDataInput?),
        if (clientIds != _undefined) 'clientIds': (clientIds as List<String>?),
        if (clubId != _undefined) 'clubId': (clubId as String?),
        if (date != _undefined) 'date': (date as DateTime?),
        if (description != _undefined) 'description': (description as String?),
        if (durationMin != _undefined) 'durationMin': (durationMin as int?),
        if (guestsEmail != _undefined)
          'guestsEmail': (guestsEmail as List<String>?),
        if (name != _undefined) 'name': (name as String?),
        if (trainerIds != _undefined)
          'trainerIds': (trainerIds as List<String>?),
      }));

  CopyWith$Input$AddressDataInput<TRes> get addressData {
    final local$addressData = _instance.addressData;
    return local$addressData == null
        ? CopyWith$Input$AddressDataInput.stub(_then(_instance))
        : CopyWith$Input$AddressDataInput(
            local$addressData, (e) => call(addressData: e));
  }
}

class _CopyWithStubImpl$Input$EditBigEvent<TRes>
    implements CopyWith$Input$EditBigEvent<TRes> {
  _CopyWithStubImpl$Input$EditBigEvent(this._res);

  TRes _res;

  call({
    Input$AddressDataInput? addressData,
    List<String>? clientIds,
    String? clubId,
    DateTime? date,
    String? description,
    int? durationMin,
    List<String>? guestsEmail,
    String? name,
    List<String>? trainerIds,
  }) =>
      _res;

  CopyWith$Input$AddressDataInput<TRes> get addressData =>
      CopyWith$Input$AddressDataInput.stub(_res);
}

class Input$EditBookingInput {
  factory Input$EditBookingInput({
    String? clubId,
    int? count,
    int? deadlineDays,
    bool? deleteSubBooking,
    String? description,
    DateTime? expireDate,
    String? name,
    double? percent,
    double? price,
    int? studentNumber,
    List<Input$SubBookingInput>? subBooking,
    List<String>? tags,
    int? timePerMinute,
    List<List<Input$WorkTimeObjInput>>? workTimesObj,
    List<String>? workTimesUserIds,
  }) =>
      Input$EditBookingInput._({
        if (clubId != null) r'clubId': clubId,
        if (count != null) r'count': count,
        if (deadlineDays != null) r'deadlineDays': deadlineDays,
        if (deleteSubBooking != null) r'deleteSubBooking': deleteSubBooking,
        if (description != null) r'description': description,
        if (expireDate != null) r'expireDate': expireDate,
        if (name != null) r'name': name,
        if (percent != null) r'percent': percent,
        if (price != null) r'price': price,
        if (studentNumber != null) r'studentNumber': studentNumber,
        if (subBooking != null) r'subBooking': subBooking,
        if (tags != null) r'tags': tags,
        if (timePerMinute != null) r'timePerMinute': timePerMinute,
        if (workTimesObj != null) r'workTimesObj': workTimesObj,
        if (workTimesUserIds != null) r'workTimesUserIds': workTimesUserIds,
      });

  Input$EditBookingInput._(this._$data);

  factory Input$EditBookingInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    if (data.containsKey('clubId')) {
      final l$clubId = data['clubId'];
      result$data['clubId'] = (l$clubId as String?);
    }
    if (data.containsKey('count')) {
      final l$count = data['count'];
      result$data['count'] = (l$count as int?);
    }
    if (data.containsKey('deadlineDays')) {
      final l$deadlineDays = data['deadlineDays'];
      result$data['deadlineDays'] = (l$deadlineDays as int?);
    }
    if (data.containsKey('deleteSubBooking')) {
      final l$deleteSubBooking = data['deleteSubBooking'];
      result$data['deleteSubBooking'] = (l$deleteSubBooking as bool?);
    }
    if (data.containsKey('description')) {
      final l$description = data['description'];
      result$data['description'] = (l$description as String?);
    }
    if (data.containsKey('expireDate')) {
      final l$expireDate = data['expireDate'];
      result$data['expireDate'] =
          l$expireDate == null ? null : dateTimeParseG(l$expireDate);
    }
    if (data.containsKey('name')) {
      final l$name = data['name'];
      result$data['name'] = (l$name as String?);
    }
    if (data.containsKey('percent')) {
      final l$percent = data['percent'];
      result$data['percent'] = (l$percent as num?)?.toDouble();
    }
    if (data.containsKey('price')) {
      final l$price = data['price'];
      result$data['price'] = (l$price as num?)?.toDouble();
    }
    if (data.containsKey('studentNumber')) {
      final l$studentNumber = data['studentNumber'];
      result$data['studentNumber'] = (l$studentNumber as int?);
    }
    if (data.containsKey('subBooking')) {
      final l$subBooking = data['subBooking'];
      result$data['subBooking'] = (l$subBooking as List<dynamic>?)
          ?.map((e) =>
              Input$SubBookingInput.fromJson((e as Map<String, dynamic>)))
          .toList();
    }
    if (data.containsKey('tags')) {
      final l$tags = data['tags'];
      result$data['tags'] =
          (l$tags as List<dynamic>?)?.map((e) => (e as String)).toList();
    }
    if (data.containsKey('timePerMinute')) {
      final l$timePerMinute = data['timePerMinute'];
      result$data['timePerMinute'] = (l$timePerMinute as int?);
    }
    if (data.containsKey('workTimesObj')) {
      final l$workTimesObj = data['workTimesObj'];
      result$data['workTimesObj'] = (l$workTimesObj as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>)
              .map((e) =>
                  Input$WorkTimeObjInput.fromJson((e as Map<String, dynamic>)))
              .toList())
          .toList();
    }
    if (data.containsKey('workTimesUserIds')) {
      final l$workTimesUserIds = data['workTimesUserIds'];
      result$data['workTimesUserIds'] = (l$workTimesUserIds as List<dynamic>?)
          ?.map((e) => (e as String))
          .toList();
    }
    return Input$EditBookingInput._(result$data);
  }

  Map<String, dynamic> _$data;

  String? get clubId => (_$data['clubId'] as String?);

  int? get count => (_$data['count'] as int?);

  int? get deadlineDays => (_$data['deadlineDays'] as int?);

  bool? get deleteSubBooking => (_$data['deleteSubBooking'] as bool?);

  String? get description => (_$data['description'] as String?);

  DateTime? get expireDate => (_$data['expireDate'] as DateTime?);

  String? get name => (_$data['name'] as String?);

  double? get percent => (_$data['percent'] as double?);

  double? get price => (_$data['price'] as double?);

  int? get studentNumber => (_$data['studentNumber'] as int?);

  List<Input$SubBookingInput>? get subBooking =>
      (_$data['subBooking'] as List<Input$SubBookingInput>?);

  List<String>? get tags => (_$data['tags'] as List<String>?);

  int? get timePerMinute => (_$data['timePerMinute'] as int?);

  List<List<Input$WorkTimeObjInput>>? get workTimesObj =>
      (_$data['workTimesObj'] as List<List<Input$WorkTimeObjInput>>?);

  List<String>? get workTimesUserIds =>
      (_$data['workTimesUserIds'] as List<String>?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    if (_$data.containsKey('clubId')) {
      final l$clubId = clubId;
      result$data['clubId'] = l$clubId;
    }
    if (_$data.containsKey('count')) {
      final l$count = count;
      result$data['count'] = l$count;
    }
    if (_$data.containsKey('deadlineDays')) {
      final l$deadlineDays = deadlineDays;
      result$data['deadlineDays'] = l$deadlineDays;
    }
    if (_$data.containsKey('deleteSubBooking')) {
      final l$deleteSubBooking = deleteSubBooking;
      result$data['deleteSubBooking'] = l$deleteSubBooking;
    }
    if (_$data.containsKey('description')) {
      final l$description = description;
      result$data['description'] = l$description;
    }
    if (_$data.containsKey('expireDate')) {
      final l$expireDate = expireDate;
      result$data['expireDate'] =
          l$expireDate == null ? null : toMapDateTimeParseG(l$expireDate);
    }
    if (_$data.containsKey('name')) {
      final l$name = name;
      result$data['name'] = l$name;
    }
    if (_$data.containsKey('percent')) {
      final l$percent = percent;
      result$data['percent'] = l$percent;
    }
    if (_$data.containsKey('price')) {
      final l$price = price;
      result$data['price'] = l$price;
    }
    if (_$data.containsKey('studentNumber')) {
      final l$studentNumber = studentNumber;
      result$data['studentNumber'] = l$studentNumber;
    }
    if (_$data.containsKey('subBooking')) {
      final l$subBooking = subBooking;
      result$data['subBooking'] = l$subBooking?.map((e) => e.toJson()).toList();
    }
    if (_$data.containsKey('tags')) {
      final l$tags = tags;
      result$data['tags'] = l$tags?.map((e) => e).toList();
    }
    if (_$data.containsKey('timePerMinute')) {
      final l$timePerMinute = timePerMinute;
      result$data['timePerMinute'] = l$timePerMinute;
    }
    if (_$data.containsKey('workTimesObj')) {
      final l$workTimesObj = workTimesObj;
      result$data['workTimesObj'] = l$workTimesObj
          ?.map((e) => e.map((e) => e.toJson()).toList())
          .toList();
    }
    if (_$data.containsKey('workTimesUserIds')) {
      final l$workTimesUserIds = workTimesUserIds;
      result$data['workTimesUserIds'] =
          l$workTimesUserIds?.map((e) => e).toList();
    }
    return result$data;
  }

  CopyWith$Input$EditBookingInput<Input$EditBookingInput> get copyWith =>
      CopyWith$Input$EditBookingInput(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$EditBookingInput) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$clubId = clubId;
    final lOther$clubId = other.clubId;
    if (_$data.containsKey('clubId') != other._$data.containsKey('clubId')) {
      return false;
    }
    if (l$clubId != lOther$clubId) {
      return false;
    }
    final l$count = count;
    final lOther$count = other.count;
    if (_$data.containsKey('count') != other._$data.containsKey('count')) {
      return false;
    }
    if (l$count != lOther$count) {
      return false;
    }
    final l$deadlineDays = deadlineDays;
    final lOther$deadlineDays = other.deadlineDays;
    if (_$data.containsKey('deadlineDays') !=
        other._$data.containsKey('deadlineDays')) {
      return false;
    }
    if (l$deadlineDays != lOther$deadlineDays) {
      return false;
    }
    final l$deleteSubBooking = deleteSubBooking;
    final lOther$deleteSubBooking = other.deleteSubBooking;
    if (_$data.containsKey('deleteSubBooking') !=
        other._$data.containsKey('deleteSubBooking')) {
      return false;
    }
    if (l$deleteSubBooking != lOther$deleteSubBooking) {
      return false;
    }
    final l$description = description;
    final lOther$description = other.description;
    if (_$data.containsKey('description') !=
        other._$data.containsKey('description')) {
      return false;
    }
    if (l$description != lOther$description) {
      return false;
    }
    final l$expireDate = expireDate;
    final lOther$expireDate = other.expireDate;
    if (_$data.containsKey('expireDate') !=
        other._$data.containsKey('expireDate')) {
      return false;
    }
    if (l$expireDate != lOther$expireDate) {
      return false;
    }
    final l$name = name;
    final lOther$name = other.name;
    if (_$data.containsKey('name') != other._$data.containsKey('name')) {
      return false;
    }
    if (l$name != lOther$name) {
      return false;
    }
    final l$percent = percent;
    final lOther$percent = other.percent;
    if (_$data.containsKey('percent') != other._$data.containsKey('percent')) {
      return false;
    }
    if (l$percent != lOther$percent) {
      return false;
    }
    final l$price = price;
    final lOther$price = other.price;
    if (_$data.containsKey('price') != other._$data.containsKey('price')) {
      return false;
    }
    if (l$price != lOther$price) {
      return false;
    }
    final l$studentNumber = studentNumber;
    final lOther$studentNumber = other.studentNumber;
    if (_$data.containsKey('studentNumber') !=
        other._$data.containsKey('studentNumber')) {
      return false;
    }
    if (l$studentNumber != lOther$studentNumber) {
      return false;
    }
    final l$subBooking = subBooking;
    final lOther$subBooking = other.subBooking;
    if (_$data.containsKey('subBooking') !=
        other._$data.containsKey('subBooking')) {
      return false;
    }
    if (l$subBooking != null && lOther$subBooking != null) {
      if (l$subBooking.length != lOther$subBooking.length) {
        return false;
      }
      for (int i = 0; i < l$subBooking.length; i++) {
        final l$subBooking$entry = l$subBooking[i];
        final lOther$subBooking$entry = lOther$subBooking[i];
        if (l$subBooking$entry != lOther$subBooking$entry) {
          return false;
        }
      }
    } else if (l$subBooking != lOther$subBooking) {
      return false;
    }
    final l$tags = tags;
    final lOther$tags = other.tags;
    if (_$data.containsKey('tags') != other._$data.containsKey('tags')) {
      return false;
    }
    if (l$tags != null && lOther$tags != null) {
      if (l$tags.length != lOther$tags.length) {
        return false;
      }
      for (int i = 0; i < l$tags.length; i++) {
        final l$tags$entry = l$tags[i];
        final lOther$tags$entry = lOther$tags[i];
        if (l$tags$entry != lOther$tags$entry) {
          return false;
        }
      }
    } else if (l$tags != lOther$tags) {
      return false;
    }
    final l$timePerMinute = timePerMinute;
    final lOther$timePerMinute = other.timePerMinute;
    if (_$data.containsKey('timePerMinute') !=
        other._$data.containsKey('timePerMinute')) {
      return false;
    }
    if (l$timePerMinute != lOther$timePerMinute) {
      return false;
    }
    final l$workTimesObj = workTimesObj;
    final lOther$workTimesObj = other.workTimesObj;
    if (_$data.containsKey('workTimesObj') !=
        other._$data.containsKey('workTimesObj')) {
      return false;
    }
    if (l$workTimesObj != null && lOther$workTimesObj != null) {
      if (l$workTimesObj.length != lOther$workTimesObj.length) {
        return false;
      }
      for (int i = 0; i < l$workTimesObj.length; i++) {
        final l$workTimesObj$entry = l$workTimesObj[i];
        final lOther$workTimesObj$entry = lOther$workTimesObj[i];
        if (l$workTimesObj$entry.length != lOther$workTimesObj$entry.length) {
          return false;
        }
        for (int i = 0; i < l$workTimesObj$entry.length; i++) {
          final l$workTimesObj$entry$entry = l$workTimesObj$entry[i];
          final lOther$workTimesObj$entry$entry = lOther$workTimesObj$entry[i];
          if (l$workTimesObj$entry$entry != lOther$workTimesObj$entry$entry) {
            return false;
          }
        }
      }
    } else if (l$workTimesObj != lOther$workTimesObj) {
      return false;
    }
    final l$workTimesUserIds = workTimesUserIds;
    final lOther$workTimesUserIds = other.workTimesUserIds;
    if (_$data.containsKey('workTimesUserIds') !=
        other._$data.containsKey('workTimesUserIds')) {
      return false;
    }
    if (l$workTimesUserIds != null && lOther$workTimesUserIds != null) {
      if (l$workTimesUserIds.length != lOther$workTimesUserIds.length) {
        return false;
      }
      for (int i = 0; i < l$workTimesUserIds.length; i++) {
        final l$workTimesUserIds$entry = l$workTimesUserIds[i];
        final lOther$workTimesUserIds$entry = lOther$workTimesUserIds[i];
        if (l$workTimesUserIds$entry != lOther$workTimesUserIds$entry) {
          return false;
        }
      }
    } else if (l$workTimesUserIds != lOther$workTimesUserIds) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$clubId = clubId;
    final l$count = count;
    final l$deadlineDays = deadlineDays;
    final l$deleteSubBooking = deleteSubBooking;
    final l$description = description;
    final l$expireDate = expireDate;
    final l$name = name;
    final l$percent = percent;
    final l$price = price;
    final l$studentNumber = studentNumber;
    final l$subBooking = subBooking;
    final l$tags = tags;
    final l$timePerMinute = timePerMinute;
    final l$workTimesObj = workTimesObj;
    final l$workTimesUserIds = workTimesUserIds;
    return Object.hashAll([
      _$data.containsKey('clubId') ? l$clubId : const {},
      _$data.containsKey('count') ? l$count : const {},
      _$data.containsKey('deadlineDays') ? l$deadlineDays : const {},
      _$data.containsKey('deleteSubBooking') ? l$deleteSubBooking : const {},
      _$data.containsKey('description') ? l$description : const {},
      _$data.containsKey('expireDate') ? l$expireDate : const {},
      _$data.containsKey('name') ? l$name : const {},
      _$data.containsKey('percent') ? l$percent : const {},
      _$data.containsKey('price') ? l$price : const {},
      _$data.containsKey('studentNumber') ? l$studentNumber : const {},
      _$data.containsKey('subBooking')
          ? l$subBooking == null
              ? null
              : Object.hashAll(l$subBooking.map((v) => v))
          : const {},
      _$data.containsKey('tags')
          ? l$tags == null
              ? null
              : Object.hashAll(l$tags.map((v) => v))
          : const {},
      _$data.containsKey('timePerMinute') ? l$timePerMinute : const {},
      _$data.containsKey('workTimesObj')
          ? l$workTimesObj == null
              ? null
              : Object.hashAll(
                  l$workTimesObj.map((v) => Object.hashAll(v.map((v) => v))))
          : const {},
      _$data.containsKey('workTimesUserIds')
          ? l$workTimesUserIds == null
              ? null
              : Object.hashAll(l$workTimesUserIds.map((v) => v))
          : const {},
    ]);
  }
}

abstract class CopyWith$Input$EditBookingInput<TRes> {
  factory CopyWith$Input$EditBookingInput(
    Input$EditBookingInput instance,
    TRes Function(Input$EditBookingInput) then,
  ) = _CopyWithImpl$Input$EditBookingInput;

  factory CopyWith$Input$EditBookingInput.stub(TRes res) =
      _CopyWithStubImpl$Input$EditBookingInput;

  TRes call({
    String? clubId,
    int? count,
    int? deadlineDays,
    bool? deleteSubBooking,
    String? description,
    DateTime? expireDate,
    String? name,
    double? percent,
    double? price,
    int? studentNumber,
    List<Input$SubBookingInput>? subBooking,
    List<String>? tags,
    int? timePerMinute,
    List<List<Input$WorkTimeObjInput>>? workTimesObj,
    List<String>? workTimesUserIds,
  });
  TRes subBooking(
      Iterable<Input$SubBookingInput>? Function(
              Iterable<CopyWith$Input$SubBookingInput<Input$SubBookingInput>>?)
          _fn);
  TRes workTimesObj(
      Iterable<Iterable<Input$WorkTimeObjInput>>? Function(
              Iterable<
                  Iterable<
                      CopyWith$Input$WorkTimeObjInput<
                          Input$WorkTimeObjInput>>>?)
          _fn);
}

class _CopyWithImpl$Input$EditBookingInput<TRes>
    implements CopyWith$Input$EditBookingInput<TRes> {
  _CopyWithImpl$Input$EditBookingInput(
    this._instance,
    this._then,
  );

  final Input$EditBookingInput _instance;

  final TRes Function(Input$EditBookingInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? clubId = _undefined,
    Object? count = _undefined,
    Object? deadlineDays = _undefined,
    Object? deleteSubBooking = _undefined,
    Object? description = _undefined,
    Object? expireDate = _undefined,
    Object? name = _undefined,
    Object? percent = _undefined,
    Object? price = _undefined,
    Object? studentNumber = _undefined,
    Object? subBooking = _undefined,
    Object? tags = _undefined,
    Object? timePerMinute = _undefined,
    Object? workTimesObj = _undefined,
    Object? workTimesUserIds = _undefined,
  }) =>
      _then(Input$EditBookingInput._({
        ..._instance._$data,
        if (clubId != _undefined) 'clubId': (clubId as String?),
        if (count != _undefined) 'count': (count as int?),
        if (deadlineDays != _undefined) 'deadlineDays': (deadlineDays as int?),
        if (deleteSubBooking != _undefined)
          'deleteSubBooking': (deleteSubBooking as bool?),
        if (description != _undefined) 'description': (description as String?),
        if (expireDate != _undefined) 'expireDate': (expireDate as DateTime?),
        if (name != _undefined) 'name': (name as String?),
        if (percent != _undefined) 'percent': (percent as double?),
        if (price != _undefined) 'price': (price as double?),
        if (studentNumber != _undefined)
          'studentNumber': (studentNumber as int?),
        if (subBooking != _undefined)
          'subBooking': (subBooking as List<Input$SubBookingInput>?),
        if (tags != _undefined) 'tags': (tags as List<String>?),
        if (timePerMinute != _undefined)
          'timePerMinute': (timePerMinute as int?),
        if (workTimesObj != _undefined)
          'workTimesObj': (workTimesObj as List<List<Input$WorkTimeObjInput>>?),
        if (workTimesUserIds != _undefined)
          'workTimesUserIds': (workTimesUserIds as List<String>?),
      }));

  TRes subBooking(
          Iterable<Input$SubBookingInput>? Function(
                  Iterable<
                      CopyWith$Input$SubBookingInput<Input$SubBookingInput>>?)
              _fn) =>
      call(
          subBooking: _fn(
              _instance.subBooking?.map((e) => CopyWith$Input$SubBookingInput(
                    e,
                    (i) => i,
                  )))?.toList());

  TRes workTimesObj(
          Iterable<Iterable<Input$WorkTimeObjInput>>? Function(
                  Iterable<
                      Iterable<
                          CopyWith$Input$WorkTimeObjInput<
                              Input$WorkTimeObjInput>>>?)
              _fn) =>
      call(
          workTimesObj: _fn(_instance.workTimesObj
              ?.map((e) => e.map((e) => CopyWith$Input$WorkTimeObjInput(
                    e,
                    (i) => i,
                  ))))?.map((e) => e.toList()).toList());
}

class _CopyWithStubImpl$Input$EditBookingInput<TRes>
    implements CopyWith$Input$EditBookingInput<TRes> {
  _CopyWithStubImpl$Input$EditBookingInput(this._res);

  TRes _res;

  call({
    String? clubId,
    int? count,
    int? deadlineDays,
    bool? deleteSubBooking,
    String? description,
    DateTime? expireDate,
    String? name,
    double? percent,
    double? price,
    int? studentNumber,
    List<Input$SubBookingInput>? subBooking,
    List<String>? tags,
    int? timePerMinute,
    List<List<Input$WorkTimeObjInput>>? workTimesObj,
    List<String>? workTimesUserIds,
  }) =>
      _res;

  subBooking(_fn) => _res;

  workTimesObj(_fn) => _res;
}

class Input$StartEndTimeInput {
  factory Input$StartEndTimeInput({
    required DateTime endTime,
    required DateTime startTime,
  }) =>
      Input$StartEndTimeInput._({
        r'endTime': endTime,
        r'startTime': startTime,
      });

  Input$StartEndTimeInput._(this._$data);

  factory Input$StartEndTimeInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$endTime = data['endTime'];
    result$data['endTime'] = dateTimeParseG(l$endTime);
    final l$startTime = data['startTime'];
    result$data['startTime'] = dateTimeParseG(l$startTime);
    return Input$StartEndTimeInput._(result$data);
  }

  Map<String, dynamic> _$data;

  DateTime get endTime => (_$data['endTime'] as DateTime);

  DateTime get startTime => (_$data['startTime'] as DateTime);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$endTime = endTime;
    result$data['endTime'] = toMapDateTimeParseG(l$endTime);
    final l$startTime = startTime;
    result$data['startTime'] = toMapDateTimeParseG(l$startTime);
    return result$data;
  }

  CopyWith$Input$StartEndTimeInput<Input$StartEndTimeInput> get copyWith =>
      CopyWith$Input$StartEndTimeInput(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$StartEndTimeInput) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$endTime = endTime;
    final lOther$endTime = other.endTime;
    if (l$endTime != lOther$endTime) {
      return false;
    }
    final l$startTime = startTime;
    final lOther$startTime = other.startTime;
    if (l$startTime != lOther$startTime) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$endTime = endTime;
    final l$startTime = startTime;
    return Object.hashAll([
      l$endTime,
      l$startTime,
    ]);
  }
}

abstract class CopyWith$Input$StartEndTimeInput<TRes> {
  factory CopyWith$Input$StartEndTimeInput(
    Input$StartEndTimeInput instance,
    TRes Function(Input$StartEndTimeInput) then,
  ) = _CopyWithImpl$Input$StartEndTimeInput;

  factory CopyWith$Input$StartEndTimeInput.stub(TRes res) =
      _CopyWithStubImpl$Input$StartEndTimeInput;

  TRes call({
    DateTime? endTime,
    DateTime? startTime,
  });
}

class _CopyWithImpl$Input$StartEndTimeInput<TRes>
    implements CopyWith$Input$StartEndTimeInput<TRes> {
  _CopyWithImpl$Input$StartEndTimeInput(
    this._instance,
    this._then,
  );

  final Input$StartEndTimeInput _instance;

  final TRes Function(Input$StartEndTimeInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? endTime = _undefined,
    Object? startTime = _undefined,
  }) =>
      _then(Input$StartEndTimeInput._({
        ..._instance._$data,
        if (endTime != _undefined && endTime != null)
          'endTime': (endTime as DateTime),
        if (startTime != _undefined && startTime != null)
          'startTime': (startTime as DateTime),
      }));
}

class _CopyWithStubImpl$Input$StartEndTimeInput<TRes>
    implements CopyWith$Input$StartEndTimeInput<TRes> {
  _CopyWithStubImpl$Input$StartEndTimeInput(this._res);

  TRes _res;

  call({
    DateTime? endTime,
    DateTime? startTime,
  }) =>
      _res;
}

class Input$SubBookingInput {
  factory Input$SubBookingInput({
    required int count,
    int? deadlineDays,
    required double price,
  }) =>
      Input$SubBookingInput._({
        r'count': count,
        if (deadlineDays != null) r'deadlineDays': deadlineDays,
        r'price': price,
      });

  Input$SubBookingInput._(this._$data);

  factory Input$SubBookingInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$count = data['count'];
    result$data['count'] = (l$count as int);
    if (data.containsKey('deadlineDays')) {
      final l$deadlineDays = data['deadlineDays'];
      result$data['deadlineDays'] = (l$deadlineDays as int?);
    }
    final l$price = data['price'];
    result$data['price'] = (l$price as num).toDouble();
    return Input$SubBookingInput._(result$data);
  }

  Map<String, dynamic> _$data;

  int get count => (_$data['count'] as int);

  int? get deadlineDays => (_$data['deadlineDays'] as int?);

  double get price => (_$data['price'] as double);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$count = count;
    result$data['count'] = l$count;
    if (_$data.containsKey('deadlineDays')) {
      final l$deadlineDays = deadlineDays;
      result$data['deadlineDays'] = l$deadlineDays;
    }
    final l$price = price;
    result$data['price'] = l$price;
    return result$data;
  }

  CopyWith$Input$SubBookingInput<Input$SubBookingInput> get copyWith =>
      CopyWith$Input$SubBookingInput(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$SubBookingInput) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$count = count;
    final lOther$count = other.count;
    if (l$count != lOther$count) {
      return false;
    }
    final l$deadlineDays = deadlineDays;
    final lOther$deadlineDays = other.deadlineDays;
    if (_$data.containsKey('deadlineDays') !=
        other._$data.containsKey('deadlineDays')) {
      return false;
    }
    if (l$deadlineDays != lOther$deadlineDays) {
      return false;
    }
    final l$price = price;
    final lOther$price = other.price;
    if (l$price != lOther$price) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$count = count;
    final l$deadlineDays = deadlineDays;
    final l$price = price;
    return Object.hashAll([
      l$count,
      _$data.containsKey('deadlineDays') ? l$deadlineDays : const {},
      l$price,
    ]);
  }
}

abstract class CopyWith$Input$SubBookingInput<TRes> {
  factory CopyWith$Input$SubBookingInput(
    Input$SubBookingInput instance,
    TRes Function(Input$SubBookingInput) then,
  ) = _CopyWithImpl$Input$SubBookingInput;

  factory CopyWith$Input$SubBookingInput.stub(TRes res) =
      _CopyWithStubImpl$Input$SubBookingInput;

  TRes call({
    int? count,
    int? deadlineDays,
    double? price,
  });
}

class _CopyWithImpl$Input$SubBookingInput<TRes>
    implements CopyWith$Input$SubBookingInput<TRes> {
  _CopyWithImpl$Input$SubBookingInput(
    this._instance,
    this._then,
  );

  final Input$SubBookingInput _instance;

  final TRes Function(Input$SubBookingInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? count = _undefined,
    Object? deadlineDays = _undefined,
    Object? price = _undefined,
  }) =>
      _then(Input$SubBookingInput._({
        ..._instance._$data,
        if (count != _undefined && count != null) 'count': (count as int),
        if (deadlineDays != _undefined) 'deadlineDays': (deadlineDays as int?),
        if (price != _undefined && price != null) 'price': (price as double),
      }));
}

class _CopyWithStubImpl$Input$SubBookingInput<TRes>
    implements CopyWith$Input$SubBookingInput<TRes> {
  _CopyWithStubImpl$Input$SubBookingInput(this._res);

  TRes _res;

  call({
    int? count,
    int? deadlineDays,
    double? price,
  }) =>
      _res;
}

class Input$TrainTimesInput {
  factory Input$TrainTimesInput({
    String? $_id,
    required DateTime date,
    required DateTime endTime,
    required String trainerId,
  }) =>
      Input$TrainTimesInput._({
        if ($_id != null) r'_id': $_id,
        r'date': date,
        r'endTime': endTime,
        r'trainerId': trainerId,
      });

  Input$TrainTimesInput._(this._$data);

  factory Input$TrainTimesInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    if (data.containsKey('_id')) {
      final l$$_id = data['_id'];
      result$data['_id'] = (l$$_id as String?);
    }
    final l$date = data['date'];
    result$data['date'] = dateTimeParseG(l$date);
    final l$endTime = data['endTime'];
    result$data['endTime'] = dateTimeParseG(l$endTime);
    final l$trainerId = data['trainerId'];
    result$data['trainerId'] = (l$trainerId as String);
    return Input$TrainTimesInput._(result$data);
  }

  Map<String, dynamic> _$data;

  String? get $_id => (_$data['_id'] as String?);

  DateTime get date => (_$data['date'] as DateTime);

  DateTime get endTime => (_$data['endTime'] as DateTime);

  String get trainerId => (_$data['trainerId'] as String);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    if (_$data.containsKey('_id')) {
      final l$$_id = $_id;
      result$data['_id'] = l$$_id;
    }
    final l$date = date;
    result$data['date'] = toMapDateTimeParseG(l$date);
    final l$endTime = endTime;
    result$data['endTime'] = toMapDateTimeParseG(l$endTime);
    final l$trainerId = trainerId;
    result$data['trainerId'] = l$trainerId;
    return result$data;
  }

  CopyWith$Input$TrainTimesInput<Input$TrainTimesInput> get copyWith =>
      CopyWith$Input$TrainTimesInput(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$TrainTimesInput) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$$_id = $_id;
    final lOther$$_id = other.$_id;
    if (_$data.containsKey('_id') != other._$data.containsKey('_id')) {
      return false;
    }
    if (l$$_id != lOther$$_id) {
      return false;
    }
    final l$date = date;
    final lOther$date = other.date;
    if (l$date != lOther$date) {
      return false;
    }
    final l$endTime = endTime;
    final lOther$endTime = other.endTime;
    if (l$endTime != lOther$endTime) {
      return false;
    }
    final l$trainerId = trainerId;
    final lOther$trainerId = other.trainerId;
    if (l$trainerId != lOther$trainerId) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$$_id = $_id;
    final l$date = date;
    final l$endTime = endTime;
    final l$trainerId = trainerId;
    return Object.hashAll([
      _$data.containsKey('_id') ? l$$_id : const {},
      l$date,
      l$endTime,
      l$trainerId,
    ]);
  }
}

abstract class CopyWith$Input$TrainTimesInput<TRes> {
  factory CopyWith$Input$TrainTimesInput(
    Input$TrainTimesInput instance,
    TRes Function(Input$TrainTimesInput) then,
  ) = _CopyWithImpl$Input$TrainTimesInput;

  factory CopyWith$Input$TrainTimesInput.stub(TRes res) =
      _CopyWithStubImpl$Input$TrainTimesInput;

  TRes call({
    String? $_id,
    DateTime? date,
    DateTime? endTime,
    String? trainerId,
  });
}

class _CopyWithImpl$Input$TrainTimesInput<TRes>
    implements CopyWith$Input$TrainTimesInput<TRes> {
  _CopyWithImpl$Input$TrainTimesInput(
    this._instance,
    this._then,
  );

  final Input$TrainTimesInput _instance;

  final TRes Function(Input$TrainTimesInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? $_id = _undefined,
    Object? date = _undefined,
    Object? endTime = _undefined,
    Object? trainerId = _undefined,
  }) =>
      _then(Input$TrainTimesInput._({
        ..._instance._$data,
        if ($_id != _undefined) '_id': ($_id as String?),
        if (date != _undefined && date != null) 'date': (date as DateTime),
        if (endTime != _undefined && endTime != null)
          'endTime': (endTime as DateTime),
        if (trainerId != _undefined && trainerId != null)
          'trainerId': (trainerId as String),
      }));
}

class _CopyWithStubImpl$Input$TrainTimesInput<TRes>
    implements CopyWith$Input$TrainTimesInput<TRes> {
  _CopyWithStubImpl$Input$TrainTimesInput(this._res);

  TRes _res;

  call({
    String? $_id,
    DateTime? date,
    DateTime? endTime,
    String? trainerId,
  }) =>
      _res;
}

class Input$WorkTimeInput {
  factory Input$WorkTimeInput({
    required DateTime endTime,
    required DateTime startTime,
  }) =>
      Input$WorkTimeInput._({
        r'endTime': endTime,
        r'startTime': startTime,
      });

  Input$WorkTimeInput._(this._$data);

  factory Input$WorkTimeInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$endTime = data['endTime'];
    result$data['endTime'] = dateTimeParseG(l$endTime);
    final l$startTime = data['startTime'];
    result$data['startTime'] = dateTimeParseG(l$startTime);
    return Input$WorkTimeInput._(result$data);
  }

  Map<String, dynamic> _$data;

  DateTime get endTime => (_$data['endTime'] as DateTime);

  DateTime get startTime => (_$data['startTime'] as DateTime);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$endTime = endTime;
    result$data['endTime'] = toMapDateTimeParseG(l$endTime);
    final l$startTime = startTime;
    result$data['startTime'] = toMapDateTimeParseG(l$startTime);
    return result$data;
  }

  CopyWith$Input$WorkTimeInput<Input$WorkTimeInput> get copyWith =>
      CopyWith$Input$WorkTimeInput(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$WorkTimeInput) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$endTime = endTime;
    final lOther$endTime = other.endTime;
    if (l$endTime != lOther$endTime) {
      return false;
    }
    final l$startTime = startTime;
    final lOther$startTime = other.startTime;
    if (l$startTime != lOther$startTime) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$endTime = endTime;
    final l$startTime = startTime;
    return Object.hashAll([
      l$endTime,
      l$startTime,
    ]);
  }
}

abstract class CopyWith$Input$WorkTimeInput<TRes> {
  factory CopyWith$Input$WorkTimeInput(
    Input$WorkTimeInput instance,
    TRes Function(Input$WorkTimeInput) then,
  ) = _CopyWithImpl$Input$WorkTimeInput;

  factory CopyWith$Input$WorkTimeInput.stub(TRes res) =
      _CopyWithStubImpl$Input$WorkTimeInput;

  TRes call({
    DateTime? endTime,
    DateTime? startTime,
  });
}

class _CopyWithImpl$Input$WorkTimeInput<TRes>
    implements CopyWith$Input$WorkTimeInput<TRes> {
  _CopyWithImpl$Input$WorkTimeInput(
    this._instance,
    this._then,
  );

  final Input$WorkTimeInput _instance;

  final TRes Function(Input$WorkTimeInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? endTime = _undefined,
    Object? startTime = _undefined,
  }) =>
      _then(Input$WorkTimeInput._({
        ..._instance._$data,
        if (endTime != _undefined && endTime != null)
          'endTime': (endTime as DateTime),
        if (startTime != _undefined && startTime != null)
          'startTime': (startTime as DateTime),
      }));
}

class _CopyWithStubImpl$Input$WorkTimeInput<TRes>
    implements CopyWith$Input$WorkTimeInput<TRes> {
  _CopyWithStubImpl$Input$WorkTimeInput(this._res);

  TRes _res;

  call({
    DateTime? endTime,
    DateTime? startTime,
  }) =>
      _res;
}

class Input$WorkTimeObjInput {
  factory Input$WorkTimeObjInput({
    required DateTime date,
    int? max,
    int? now,
  }) =>
      Input$WorkTimeObjInput._({
        r'date': date,
        if (max != null) r'max': max,
        if (now != null) r'now': now,
      });

  Input$WorkTimeObjInput._(this._$data);

  factory Input$WorkTimeObjInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$date = data['date'];
    result$data['date'] = dateTimeParseG(l$date);
    if (data.containsKey('max')) {
      final l$max = data['max'];
      result$data['max'] = (l$max as int?);
    }
    if (data.containsKey('now')) {
      final l$now = data['now'];
      result$data['now'] = (l$now as int?);
    }
    return Input$WorkTimeObjInput._(result$data);
  }

  Map<String, dynamic> _$data;

  DateTime get date => (_$data['date'] as DateTime);

  int? get max => (_$data['max'] as int?);

  int? get now => (_$data['now'] as int?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$date = date;
    result$data['date'] = toMapDateTimeParseG(l$date);
    if (_$data.containsKey('max')) {
      final l$max = max;
      result$data['max'] = l$max;
    }
    if (_$data.containsKey('now')) {
      final l$now = now;
      result$data['now'] = l$now;
    }
    return result$data;
  }

  CopyWith$Input$WorkTimeObjInput<Input$WorkTimeObjInput> get copyWith =>
      CopyWith$Input$WorkTimeObjInput(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$WorkTimeObjInput) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$date = date;
    final lOther$date = other.date;
    if (l$date != lOther$date) {
      return false;
    }
    final l$max = max;
    final lOther$max = other.max;
    if (_$data.containsKey('max') != other._$data.containsKey('max')) {
      return false;
    }
    if (l$max != lOther$max) {
      return false;
    }
    final l$now = now;
    final lOther$now = other.now;
    if (_$data.containsKey('now') != other._$data.containsKey('now')) {
      return false;
    }
    if (l$now != lOther$now) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$date = date;
    final l$max = max;
    final l$now = now;
    return Object.hashAll([
      l$date,
      _$data.containsKey('max') ? l$max : const {},
      _$data.containsKey('now') ? l$now : const {},
    ]);
  }
}

abstract class CopyWith$Input$WorkTimeObjInput<TRes> {
  factory CopyWith$Input$WorkTimeObjInput(
    Input$WorkTimeObjInput instance,
    TRes Function(Input$WorkTimeObjInput) then,
  ) = _CopyWithImpl$Input$WorkTimeObjInput;

  factory CopyWith$Input$WorkTimeObjInput.stub(TRes res) =
      _CopyWithStubImpl$Input$WorkTimeObjInput;

  TRes call({
    DateTime? date,
    int? max,
    int? now,
  });
}

class _CopyWithImpl$Input$WorkTimeObjInput<TRes>
    implements CopyWith$Input$WorkTimeObjInput<TRes> {
  _CopyWithImpl$Input$WorkTimeObjInput(
    this._instance,
    this._then,
  );

  final Input$WorkTimeObjInput _instance;

  final TRes Function(Input$WorkTimeObjInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? date = _undefined,
    Object? max = _undefined,
    Object? now = _undefined,
  }) =>
      _then(Input$WorkTimeObjInput._({
        ..._instance._$data,
        if (date != _undefined && date != null) 'date': (date as DateTime),
        if (max != _undefined) 'max': (max as int?),
        if (now != _undefined) 'now': (now as int?),
      }));
}

class _CopyWithStubImpl$Input$WorkTimeObjInput<TRes>
    implements CopyWith$Input$WorkTimeObjInput<TRes> {
  _CopyWithStubImpl$Input$WorkTimeObjInput(this._res);

  TRes _res;

  call({
    DateTime? date,
    int? max,
    int? now,
  }) =>
      _res;
}

enum Enum$NotificationType {
  bought_booking_admin,
  bought_booking_participance,
  bought_booking_trainer,
  bought_service_admin,
  create_booking_admin,
  create_company_admin,
  create_event_admin,
  create_service_admin,
  delete_booking_admin,
  delete_event_admin,
  delete_service_admin,
  invite_participance,
  joining_company_admin,
  joining_participance,
  joining_trainer,
  $unknown
}

String toJson$Enum$NotificationType(Enum$NotificationType e) {
  switch (e) {
    case Enum$NotificationType.bought_booking_admin:
      return r'bought_booking_admin';
    case Enum$NotificationType.bought_booking_participance:
      return r'bought_booking_participance';
    case Enum$NotificationType.bought_booking_trainer:
      return r'bought_booking_trainer';
    case Enum$NotificationType.bought_service_admin:
      return r'bought_service_admin';
    case Enum$NotificationType.create_booking_admin:
      return r'create_booking_admin';
    case Enum$NotificationType.create_company_admin:
      return r'create_company_admin';
    case Enum$NotificationType.create_event_admin:
      return r'create_event_admin';
    case Enum$NotificationType.create_service_admin:
      return r'create_service_admin';
    case Enum$NotificationType.delete_booking_admin:
      return r'delete_booking_admin';
    case Enum$NotificationType.delete_event_admin:
      return r'delete_event_admin';
    case Enum$NotificationType.delete_service_admin:
      return r'delete_service_admin';
    case Enum$NotificationType.invite_participance:
      return r'invite_participance';
    case Enum$NotificationType.joining_company_admin:
      return r'joining_company_admin';
    case Enum$NotificationType.joining_participance:
      return r'joining_participance';
    case Enum$NotificationType.joining_trainer:
      return r'joining_trainer';
    case Enum$NotificationType.$unknown:
      return r'$unknown';
  }
}

Enum$NotificationType fromJson$Enum$NotificationType(String value) {
  switch (value) {
    case r'bought_booking_admin':
      return Enum$NotificationType.bought_booking_admin;
    case r'bought_booking_participance':
      return Enum$NotificationType.bought_booking_participance;
    case r'bought_booking_trainer':
      return Enum$NotificationType.bought_booking_trainer;
    case r'bought_service_admin':
      return Enum$NotificationType.bought_service_admin;
    case r'create_booking_admin':
      return Enum$NotificationType.create_booking_admin;
    case r'create_company_admin':
      return Enum$NotificationType.create_company_admin;
    case r'create_event_admin':
      return Enum$NotificationType.create_event_admin;
    case r'create_service_admin':
      return Enum$NotificationType.create_service_admin;
    case r'delete_booking_admin':
      return Enum$NotificationType.delete_booking_admin;
    case r'delete_event_admin':
      return Enum$NotificationType.delete_event_admin;
    case r'delete_service_admin':
      return Enum$NotificationType.delete_service_admin;
    case r'invite_participance':
      return Enum$NotificationType.invite_participance;
    case r'joining_company_admin':
      return Enum$NotificationType.joining_company_admin;
    case r'joining_participance':
      return Enum$NotificationType.joining_participance;
    case r'joining_trainer':
      return Enum$NotificationType.joining_trainer;
    default:
      return Enum$NotificationType.$unknown;
  }
}

enum Enum$StatusEnumParticipance {
  admin,
  participance,
  trainer,
  waiting,
  $unknown
}

String toJson$Enum$StatusEnumParticipance(Enum$StatusEnumParticipance e) {
  switch (e) {
    case Enum$StatusEnumParticipance.admin:
      return r'admin';
    case Enum$StatusEnumParticipance.participance:
      return r'participance';
    case Enum$StatusEnumParticipance.trainer:
      return r'trainer';
    case Enum$StatusEnumParticipance.waiting:
      return r'waiting';
    case Enum$StatusEnumParticipance.$unknown:
      return r'$unknown';
  }
}

Enum$StatusEnumParticipance fromJson$Enum$StatusEnumParticipance(String value) {
  switch (value) {
    case r'admin':
      return Enum$StatusEnumParticipance.admin;
    case r'participance':
      return Enum$StatusEnumParticipance.participance;
    case r'trainer':
      return Enum$StatusEnumParticipance.trainer;
    case r'waiting':
      return Enum$StatusEnumParticipance.waiting;
    default:
      return Enum$StatusEnumParticipance.$unknown;
  }
}

enum Enum$TypeFinance { Booking, $unknown }

String toJson$Enum$TypeFinance(Enum$TypeFinance e) {
  switch (e) {
    case Enum$TypeFinance.Booking:
      return r'Booking';
    case Enum$TypeFinance.$unknown:
      return r'$unknown';
  }
}

Enum$TypeFinance fromJson$Enum$TypeFinance(String value) {
  switch (value) {
    case r'Booking':
      return Enum$TypeFinance.Booking;
    default:
      return Enum$TypeFinance.$unknown;
  }
}

enum Enum$__TypeKind {
  SCALAR,
  OBJECT,
  INTERFACE,
  UNION,
  ENUM,
  INPUT_OBJECT,
  LIST,
  NON_NULL,
  $unknown
}

String toJson$Enum$__TypeKind(Enum$__TypeKind e) {
  switch (e) {
    case Enum$__TypeKind.SCALAR:
      return r'SCALAR';
    case Enum$__TypeKind.OBJECT:
      return r'OBJECT';
    case Enum$__TypeKind.INTERFACE:
      return r'INTERFACE';
    case Enum$__TypeKind.UNION:
      return r'UNION';
    case Enum$__TypeKind.ENUM:
      return r'ENUM';
    case Enum$__TypeKind.INPUT_OBJECT:
      return r'INPUT_OBJECT';
    case Enum$__TypeKind.LIST:
      return r'LIST';
    case Enum$__TypeKind.NON_NULL:
      return r'NON_NULL';
    case Enum$__TypeKind.$unknown:
      return r'$unknown';
  }
}

Enum$__TypeKind fromJson$Enum$__TypeKind(String value) {
  switch (value) {
    case r'SCALAR':
      return Enum$__TypeKind.SCALAR;
    case r'OBJECT':
      return Enum$__TypeKind.OBJECT;
    case r'INTERFACE':
      return Enum$__TypeKind.INTERFACE;
    case r'UNION':
      return Enum$__TypeKind.UNION;
    case r'ENUM':
      return Enum$__TypeKind.ENUM;
    case r'INPUT_OBJECT':
      return Enum$__TypeKind.INPUT_OBJECT;
    case r'LIST':
      return Enum$__TypeKind.LIST;
    case r'NON_NULL':
      return Enum$__TypeKind.NON_NULL;
    default:
      return Enum$__TypeKind.$unknown;
  }
}

enum Enum$__DirectiveLocation {
  QUERY,
  MUTATION,
  SUBSCRIPTION,
  FIELD,
  FRAGMENT_DEFINITION,
  FRAGMENT_SPREAD,
  INLINE_FRAGMENT,
  VARIABLE_DEFINITION,
  SCHEMA,
  SCALAR,
  OBJECT,
  FIELD_DEFINITION,
  ARGUMENT_DEFINITION,
  INTERFACE,
  UNION,
  ENUM,
  ENUM_VALUE,
  INPUT_OBJECT,
  INPUT_FIELD_DEFINITION,
  $unknown
}

String toJson$Enum$__DirectiveLocation(Enum$__DirectiveLocation e) {
  switch (e) {
    case Enum$__DirectiveLocation.QUERY:
      return r'QUERY';
    case Enum$__DirectiveLocation.MUTATION:
      return r'MUTATION';
    case Enum$__DirectiveLocation.SUBSCRIPTION:
      return r'SUBSCRIPTION';
    case Enum$__DirectiveLocation.FIELD:
      return r'FIELD';
    case Enum$__DirectiveLocation.FRAGMENT_DEFINITION:
      return r'FRAGMENT_DEFINITION';
    case Enum$__DirectiveLocation.FRAGMENT_SPREAD:
      return r'FRAGMENT_SPREAD';
    case Enum$__DirectiveLocation.INLINE_FRAGMENT:
      return r'INLINE_FRAGMENT';
    case Enum$__DirectiveLocation.VARIABLE_DEFINITION:
      return r'VARIABLE_DEFINITION';
    case Enum$__DirectiveLocation.SCHEMA:
      return r'SCHEMA';
    case Enum$__DirectiveLocation.SCALAR:
      return r'SCALAR';
    case Enum$__DirectiveLocation.OBJECT:
      return r'OBJECT';
    case Enum$__DirectiveLocation.FIELD_DEFINITION:
      return r'FIELD_DEFINITION';
    case Enum$__DirectiveLocation.ARGUMENT_DEFINITION:
      return r'ARGUMENT_DEFINITION';
    case Enum$__DirectiveLocation.INTERFACE:
      return r'INTERFACE';
    case Enum$__DirectiveLocation.UNION:
      return r'UNION';
    case Enum$__DirectiveLocation.ENUM:
      return r'ENUM';
    case Enum$__DirectiveLocation.ENUM_VALUE:
      return r'ENUM_VALUE';
    case Enum$__DirectiveLocation.INPUT_OBJECT:
      return r'INPUT_OBJECT';
    case Enum$__DirectiveLocation.INPUT_FIELD_DEFINITION:
      return r'INPUT_FIELD_DEFINITION';
    case Enum$__DirectiveLocation.$unknown:
      return r'$unknown';
  }
}

Enum$__DirectiveLocation fromJson$Enum$__DirectiveLocation(String value) {
  switch (value) {
    case r'QUERY':
      return Enum$__DirectiveLocation.QUERY;
    case r'MUTATION':
      return Enum$__DirectiveLocation.MUTATION;
    case r'SUBSCRIPTION':
      return Enum$__DirectiveLocation.SUBSCRIPTION;
    case r'FIELD':
      return Enum$__DirectiveLocation.FIELD;
    case r'FRAGMENT_DEFINITION':
      return Enum$__DirectiveLocation.FRAGMENT_DEFINITION;
    case r'FRAGMENT_SPREAD':
      return Enum$__DirectiveLocation.FRAGMENT_SPREAD;
    case r'INLINE_FRAGMENT':
      return Enum$__DirectiveLocation.INLINE_FRAGMENT;
    case r'VARIABLE_DEFINITION':
      return Enum$__DirectiveLocation.VARIABLE_DEFINITION;
    case r'SCHEMA':
      return Enum$__DirectiveLocation.SCHEMA;
    case r'SCALAR':
      return Enum$__DirectiveLocation.SCALAR;
    case r'OBJECT':
      return Enum$__DirectiveLocation.OBJECT;
    case r'FIELD_DEFINITION':
      return Enum$__DirectiveLocation.FIELD_DEFINITION;
    case r'ARGUMENT_DEFINITION':
      return Enum$__DirectiveLocation.ARGUMENT_DEFINITION;
    case r'INTERFACE':
      return Enum$__DirectiveLocation.INTERFACE;
    case r'UNION':
      return Enum$__DirectiveLocation.UNION;
    case r'ENUM':
      return Enum$__DirectiveLocation.ENUM;
    case r'ENUM_VALUE':
      return Enum$__DirectiveLocation.ENUM_VALUE;
    case r'INPUT_OBJECT':
      return Enum$__DirectiveLocation.INPUT_OBJECT;
    case r'INPUT_FIELD_DEFINITION':
      return Enum$__DirectiveLocation.INPUT_FIELD_DEFINITION;
    default:
      return Enum$__DirectiveLocation.$unknown;
  }
}

const possibleTypesMap = <String, Set<String>>{
  'ClubParticipanceGraph': {
    'ClientClubParticipanceGraph',
    'TrainerClubParticipanceGraph',
  },
  'NotificationEntity': {
    'NotificationAdminGraph',
    'NotificationParticipanceGraph',
    'NotificationTrainerGraph',
  },
};
