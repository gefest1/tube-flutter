import 'package:flutter/material.dart';
import 'package:graphql/client.dart';
import 'package:tube/logic/blocs/user/user.model.dart';

class AdminClubProvider extends ChangeNotifier {
  GraphQLClient client;

  AdminClubProvider(this.client);

  Map<String, List<User>> workers = {};
}
