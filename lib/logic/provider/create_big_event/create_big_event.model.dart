import 'package:tube/logic/blocs/big_event/entities/create_big_entity.dart';

class CreateBigEventModel {
  final String? name;
  final String? description;
  final AddressData? addressData;
  final DateTime? startDate;
  final Duration? duration;

  const CreateBigEventModel({
    this.name,
    this.description,
    this.addressData,
    this.startDate,
    this.duration,
  });

  CreateBigEventModel copyWith({
    String? name,
    String? description,
    AddressData? addressData,
    DateTime? startDate,
    Duration? duration,
  }) {
    return CreateBigEventModel(
      name: name ?? this.name,
      description: description ?? this.description,
      addressData: addressData ?? this.addressData,
      startDate: startDate ?? this.startDate,
      duration: duration ?? this.duration,
    );
  }
}

class AddressData {
  final String country = "kazakhstan";
  final String city = "Almaty";
  final String? street;
  final String? house;

  const AddressData({
    this.street,
    this.house,
  });
  AddressDataEntity toAddressDataEntity() {
    return AddressDataEntity(
      country: country,
      city: city,
      street: street!,
      house: house!,
    );
  }
}
