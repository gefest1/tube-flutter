import 'package:flutter/material.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/logic/model/sub_booking.model.dart';

class CreateTariffProvider extends ChangeNotifier {
  Booking booking;
  Map<String, List<DateTime>>? dateTimesMap;
  Map<String, User>? mapUsers;
  String? owner;
  DateTime? dateTime;
  SubBooking? subBooking;

  CreateTariffProvider({
    required this.booking,
    this.dateTimesMap,
    this.mapUsers,
    this.owner,
    this.dateTime,
    this.subBooking,
  });

  void update({
    Booking? booking,
    Map<String, List<DateTime>>? dateTimesMap,
    Map<String, User>? mapUsers,
    String? owner,
    DateTime? dateTime,
    SubBooking? subBooking,
  }) {
    this.booking = booking ?? this.booking;
    this.dateTimesMap = dateTimesMap ?? this.dateTimesMap;
    this.mapUsers = mapUsers ?? this.mapUsers;
    this.owner = owner ?? this.owner;
    this.dateTime = dateTime ?? this.dateTime;
    this.subBooking = subBooking ?? this.subBooking;
    notifyListeners();
  }

  void clean({
    // bool booking = false,
    bool dateTimesMap = false,
    bool mapUsers = false,
    bool owner = false,
    bool dateTime = false,
    bool subBooking = false,
  }) {
    // if (booking) this.booking = null;
    if (dateTimesMap) this.dateTimesMap = null;
    if (mapUsers) this.mapUsers = null;
    if (owner) this.owner = null;
    if (dateTime) this.dateTime = null;
    if (subBooking) this.subBooking = null;
    notifyListeners();
  }
}
