import 'package:flutter/material.dart';
import 'package:tube/logic/provider/create_big_service/create_big_service.model.dart';

class CreateBigServiceProvider extends ChangeNotifier {
  CreateBigServiceModel createBigService;

  CreateBigServiceProvider({
    required this.createBigService,
  });

  CreateBigServiceModel setNewInstance(final CreateBigServiceModel newVal) {
    if (createBigService != newVal) {
      createBigService = newVal;
      notifyListeners();
    }
    return createBigService;
  }
}
