// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_header_delegate.dart';
import 'package:tube/flexible/lib/src/widgets/change_insets_detector.dart';

typedef FlexibleDraggableScrollableWidgetBuilder = Widget Function(
  BuildContext context,
  ScrollController scrollController,
  double bottomSheetOffset,
);

typedef FlexibleDraggableScrollableHeaderWidgetBuilder = Widget Function(
  BuildContext context,
  double bottomSheetOffset,
);

typedef FlexibleDraggableScrollableWidgetBodyBuilder = SliverChildDelegate
    Function(
  BuildContext context,
  double bottomSheetOffset,
);

class FlexibleBottomSheet extends StatefulWidget {
  final double minHeight;
  final double initHeight;
  final double maxHeight;
  final FlexibleDraggableScrollableWidgetBuilder? builder;
  final FlexibleDraggableScrollableHeaderWidgetBuilder? headerBuilder;
  final FlexibleDraggableScrollableWidgetBodyBuilder? bodyBuilder;
  final bool isCollapsible;
  final bool isExpand;
  final AnimationController? animationController;
  final List<double>? anchors;
  final double? minHeaderHeight;
  final double? maxHeaderHeight;
  final Decoration? decoration;
  final VoidCallback? onDismiss;

  FlexibleBottomSheet({
    Key? key,
    this.minHeight = 0,
    this.initHeight = 0.5,
    this.maxHeight = 1,
    this.builder,
    this.headerBuilder,
    this.bodyBuilder,
    this.isCollapsible = false,
    this.isExpand = true,
    this.animationController,
    this.anchors,
    this.minHeaderHeight,
    this.maxHeaderHeight,
    this.decoration,
    this.onDismiss,
  })  : assert(minHeight >= 0 && minHeight <= 1),
        assert(maxHeight > 0 && maxHeight <= 1),
        assert(maxHeight > minHeight),
        assert(!isCollapsible || minHeight == 0),
        assert(anchors == null || !anchors.any((anchor) => anchor > maxHeight)),
        assert(anchors == null || !anchors.any((anchor) => anchor < minHeight)),
        super(key: key);

  FlexibleBottomSheet.collapsible({
    Key? key,
    double initHeight = 0.5,
    double maxHeight = 1,
    FlexibleDraggableScrollableWidgetBuilder? builder,
    FlexibleDraggableScrollableHeaderWidgetBuilder? headerBuilder,
    FlexibleDraggableScrollableWidgetBodyBuilder? bodyBuilder,
    bool isExpand = true,
    AnimationController? animationController,
    List<double>? anchors,
    double? minHeaderHeight,
    double? maxHeaderHeight,
    Decoration? decoration,
  }) : this(
          key: key,
          maxHeight: maxHeight,
          builder: builder,
          headerBuilder: headerBuilder,
          bodyBuilder: bodyBuilder,
          minHeight: 0,
          initHeight: initHeight,
          isCollapsible: true,
          isExpand: isExpand,
          animationController: animationController,
          anchors: anchors,
          minHeaderHeight: minHeaderHeight,
          maxHeaderHeight: maxHeaderHeight,
          decoration: decoration,
        );

  @override
  State<FlexibleBottomSheet> createState() => _FlexibleBottomSheetState();
}

class _FlexibleBottomSheetState extends State<FlexibleBottomSheet>
    with SingleTickerProviderStateMixin {
  final _controller = DraggableScrollableController();

  late double initialChildSize = widget.initHeight;
  bool _isClosing = false;

  @override
  Widget build(BuildContext context) {
    return NotificationListener<DraggableScrollableNotification>(
      onNotification: _scrolling,
      child: DraggableScrollableSheet(
        maxChildSize: widget.maxHeight,
        minChildSize: widget.minHeight,
        initialChildSize: initialChildSize,
        snap: widget.anchors != null,
        controller: _controller,
        snapSizes: widget.anchors,
        expand: widget.isExpand,
        builder: (
          context,
          controller,
        ) {
          return ChangeInsetsDetector(
            handler: (delta, inset) {
              if (delta > 0) {
                _animateToFocused(controller);
                _animateToMaxHeigt();
              }

              if (delta == 0 && inset != 0) {
                WidgetsBinding.instance.addPostFrameCallback(
                  (_) {
                    setState(() {
                      initialChildSize = widget.maxHeight;
                    });
                  },
                );
              }
            },
            child: _Content(
              builder: widget.builder,
              decoration: widget.decoration,
              bodyBuilder: widget.bodyBuilder,
              headerBuilder: widget.headerBuilder,
              minHeaderHeight: widget.minHeaderHeight,
              maxHeaderHeight: widget.maxHeaderHeight,
              currentExtent:
                  _controller.isAttached ? _controller.size : widget.initHeight,
              scrollController: controller,
            ),
          );
        },
      ),
    );
  }

  bool _scrolling(DraggableScrollableNotification notification) {
    if (_isClosing) return false;

    initialChildSize = notification.extent;

    _checkNeedCloseBottomSheet(notification.extent);

    return false;
  }

  void _animateToMaxHeigt() {
    final currPosition = _controller.size;
    if (currPosition != widget.maxHeight) {
      _controller.animateTo(
        widget.maxHeight,
        duration: const Duration(milliseconds: 300),
        curve: Curves.ease,
      );
    }
  }

  void _animateToFocused(ScrollController controller) {
    if (FocusManager.instance.primaryFocus == null || _isClosing) return;

    final keyboardHeight = MediaQuery.of(context).viewInsets.bottom;
    final widgetHeight = FocusManager.instance.primaryFocus!.size.height;
    final widgetOffset = FocusManager.instance.primaryFocus!.offset.dy;
    final screenHeight = MediaQuery.of(context).size.height;

    final targetWidgetOffset =
        screenHeight - keyboardHeight - widgetHeight - 20;
    final valueToScroll = widgetOffset - targetWidgetOffset;
    final currentOffset = controller.offset;

    if (valueToScroll > 0) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        controller.animateTo(
          currentOffset + valueToScroll,
          duration: const Duration(milliseconds: 200),
          curve: Curves.ease,
        );
      });
    }
  }

  void _checkNeedCloseBottomSheet(double extent) {
    if (widget.isCollapsible && !_isClosing) {
      if (extent - widget.minHeight <= 0.005) {
        _isClosing = true;
        _dismiss();
      }
    }
  }

  void _dismiss() {
    if (widget.isCollapsible) {
      if (widget.onDismiss != null) widget.onDismiss!();
      Navigator.maybePop(context);
    }
  }
}

class _Content extends StatelessWidget {
  final FlexibleDraggableScrollableWidgetBuilder? builder;
  final Decoration? decoration;
  final FlexibleDraggableScrollableHeaderWidgetBuilder? headerBuilder;
  final FlexibleDraggableScrollableWidgetBodyBuilder? bodyBuilder;
  final double? minHeaderHeight;
  final double? maxHeaderHeight;
  final double currentExtent;
  final ScrollController scrollController;

  const _Content({
    required this.currentExtent,
    required this.scrollController,
    this.builder,
    this.decoration,
    this.headerBuilder,
    this.bodyBuilder,
    this.minHeaderHeight,
    this.maxHeaderHeight,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (builder != null) {
      return builder!(
        context,
        scrollController,
        currentExtent,
      );
    }

    return Material(
      type: MaterialType.transparency,
      child: Container(
        decoration: decoration,
        child: CustomScrollView(
          controller: scrollController,
          slivers: <Widget>[
            if (headerBuilder != null)
              SliverPersistentHeader(
                pinned: true,
                delegate: FlexibleBottomSheetHeaderDelegate(
                  minHeight: minHeaderHeight ?? 0.0,
                  maxHeight: maxHeaderHeight ?? 1.0,
                  child: headerBuilder!(context, currentExtent),
                ),
              ),
            if (bodyBuilder != null)
              SliverList(
                delegate: bodyBuilder!(
                  context,
                  currentExtent,
                ),
              ),
          ],
        ),
      ),
    );
  }
}
