import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/admin/workers/worker_bloc.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/ui/admin/molecules/trainer_tile.dart';
import 'package:tube/ui/admin/page/read_trainer_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/trainer_page/components/pages/trainer_worker_time.dart';
import 'package:tube/utils/load_popup_route.dart';

class TrainerListPage extends StatelessWidget {
  final String clubId;

  const TrainerListPage({
    required this.clubId,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final workBloc =
        Provider.of<AdminBlocMap>(context, listen: false).getWorkerBloc(clubId);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(
            Icons.arrow_back,
            size: 33,
          ),
        ),
        title: const H4Text(
          'Сотрудники',
          color: Colors.white,
        ),
      ),
      body: BlocBuilder<WorkerBloc, WorkerState>(
        bloc: workBloc,
        builder: (ctx, state) {
          if (state is ClubWorkerState && state.mapWorkers.isNotEmpty) {
            final List<User> users = state.mapWorkers;
            return ListView.builder(
              itemBuilder: (ctx, index) {
                final userWorkTime = state.workUserMap[users[index].id];
                // state.

                return TrainerTile(
                  innerPadding: const EdgeInsets.symmetric(horizontal: 20),
                  onTap: () async {
                    if (userWorkTime == null) {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => TrainerWorkerTime(
                            clubId:
                                MainDrawerState.currentParticipance!.clubId!,
                            trainerId: users[index].id,
                          ),
                        ),
                      );
                      final completer = Completer();

                      // ignore: use_build_context_synchronously
                      Navigator.push(
                        context,
                        LoadPopupRoute(completer: completer),
                      );
                      workBloc.add(GetWorkWorkerEvent(completer: completer));
                    } else {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => ReadTrainerPage(
                            workTime: userWorkTime,
                            user: users[index],
                            participance:
                                state.mapParticipance[users[index].id],
                          ),
                        ),
                      );
                    }
                  },
                  photoUrl: users[index].photoUrl?.thumbnail,
                  title: users[index].fullName ?? users[index].email,
                  subTitle: userWorkTime == null
                      ? 'Завершите добавление сотрудника'
                      : null,
                );
              },
              itemCount: users.length,
            );
          }
          return const SizedBox();
        },
      ),
    );
  }
}
