import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/icon/facebook.icon.dart';
import 'package:tube/icon/instagram.icon.dart';
import 'package:tube/icon/telegram.icon.dart';
import 'package:tube/icon/whatsapp.icon.dart';
import 'package:tube/logic/blocs/participance/model/participance.model.dart';
import 'package:tube/logic/blocs/trainer/work_time/work_time_bloc.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/user_worker_time.dart';
import 'package:tube/ui/common/atoms/read_more_widget.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/trainer_page/components/pages/trainer_worker_time.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';

class ReadTrainerPage extends StatefulWidget {
  final UserWorkerTime workTime;
  final Participance? participance;
  final User user;

  const ReadTrainerPage({
    required this.workTime,
    required this.user,
    this.participance,
    Key? key,
  }) : super(key: key);

  @override
  State<ReadTrainerPage> createState() => _ReadTrainerPageState();
}

class _ReadTrainerPageState extends State<ReadTrainerPage> {
  late ControllerWorkTimeBloc workTimeBloc = ControllerWorkTimeBloc(
    clubId: MainDrawerState.clubId!,
    trainerId: widget.user.id,
  );

  bool contactNotEmpty() {
    return widget.participance?.subData?.facebook != null ||
        widget.participance?.subData?.instagram != null ||
        widget.participance?.subData?.telegram != null ||
        widget.participance?.subData?.whatsapp != null;
  }

  Widget widgetWrap({
    required Widget widget,
    required String title,
    required String label,
    VoidCallback? onTap,
  }) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: widget,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  H5Text(
                    title,
                    color: ColorData.colorTextMain,
                  ),
                  const SizedBox(height: 2),
                  P0Text(
                    label,
                    color: ColorData.colorMainLink,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void fillArray(List<Widget> widgetAr) {
    if (widget.participance?.subData?.facebook != null) {
      widgetAr.add(
        widgetWrap(
          onTap: () {
            final facLink = "https://www.facebook.com/${widget.participance!.subData!.facebook!}";

            launchUrlString(facLink, mode: LaunchMode.externalApplication);
          },
          title: 'Facebook',
          widget: const CustomPaint(
            painter: FacebookCustomPainter(
              color: ColorData.colorMainLink,
            ),
            size: Size(24, 24),
          ),
          label: widget.participance!.subData!.facebook!,
        ),
      );
    }
    if (widget.participance?.subData?.instagram != null) {
      widgetAr.add(
        widgetWrap(
          onTap: () {
            final instLink = "https://instagram.com/${widget.participance!.subData!.instagram!}";
            launchUrlString(instLink, mode: LaunchMode.externalApplication);
          },
          title: 'Instagram',
          widget: const CustomPaint(
            painter: InstagramGlyphCustomPainter(
              color: ColorData.colorMainLink,
            ),
            size: Size(24, 24),
          ),
          label: widget.participance!.subData!.instagram!,
        ),
      );
    }
    if (widget.participance?.subData?.telegram != null) {
      widgetAr.add(
        widgetWrap(
          onTap: () {
            final phoneNumber = widget.participance!.subData!.telegram!
                .replaceAll(" ", "")
                .replaceAll("-", "")
                .replaceAll("(", "")
                .replaceAll(")", "")
                .replaceAll("_", "");
            final telegLink = "https://t.me/$phoneNumber";

            launchUrlString(telegLink, mode: LaunchMode.externalApplication);
          },
          title: 'Telegram',
          widget: const CustomPaint(
            painter: TelegramCustomPainter(
              color: ColorData.colorMainLink,
            ),
            size: Size(24, 24),
          ),
          label: widget.participance!.subData!.telegram!,
        ),
      );
    }
    if (widget.participance?.subData?.whatsapp != null) {
      widgetAr.add(
        widgetWrap(
          onTap: () async {
            final whtLink =
                "https://wa.me/${widget.participance!.subData!.whatsapp!.replaceAll(" ", "").replaceAll(
                      "+",
                      "",
                    ).replaceAll(
                      "-",
                      "",
                    ).replaceAll(
                      "(",
                      "",
                    ).replaceAll(
                      ")",
                      "",
                    ).replaceAll(
                      "_",
                      "",
                    )}";

            launchUrl(Uri.parse(whtLink), mode: LaunchMode.externalApplication);
          },
          title: 'What’s App',
          widget: const CustomPaint(
            painter: WhatsappGlyphCustomPainter(
              color: ColorData.colorMainLink,
            ),
            size: Size(24, 24),
          ),
          label: widget.participance!.subData!.whatsapp!,
        ),
      );
    }
  }

  @override
  void dispose() {
    workTimeBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final widgets = <Widget>[];
    fillArray(widgets);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(
            Icons.arrow_back,
            size: 33,
          ),
          color: Colors.white,
        ),
        title: const H4Text(
          'О вас',
          color: Colors.white,
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.only(top: 20, bottom: 100),
        children: [
          if ((widget.user.fullName ?? widget.user.email) != null)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: H2Text(
                widget.user.fullName ?? widget.user.email!,
                color: ColorData.colorElementsActive,
              ),
            ),
          const SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(
                  radius: 52,
                  backgroundColor: ColorData.color3Percent,
                  foregroundImage: widget.user.photoUrl?.xl == null
                      ? null
                      : PCacheImage(
                          widget.user.photoUrl!.xl!,
                          enableInMemory: true,
                        ),
                  child: Stack(
                    children: [
                      const Center(
                        child: Icon(
                          Icons.perm_contact_cal,
                          size: 70,
                          color: ColorData.colorElementsActive,
                        ),
                      ),
                      if (widget.user.photoUrl?.thumbnail != null)
                        ClipOval(
                          child: Image(
                            image: PCacheImage(
                              widget.user.photoUrl!.thumbnail!,
                              enableInMemory: true,
                            ),
                            height: 104,
                            width: 104,
                            fit: BoxFit.cover,
                          ),
                        )
                    ],
                  ),
                ),
                const SizedBox(width: 10),
                Flexible(
                  fit: FlexFit.tight,
                  child: SizeTapAnimation(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => TrainerWorkerTime(
                            clubId: MainDrawerState.currentParticipance!.clubId!,
                            trainerId: widget.user.id,
                          ),
                        ),
                      );
                    },
                    child: Container(
                        padding: const EdgeInsets.all(10),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                          color: Colors.white,
                          boxShadow: blocksMain,
                        ),
                        child: BlocBuilder<ControllerWorkTimeBloc, WorkTimeState>(
                          bloc: workTimeBloc,
                          builder: (ctx, state) {
                            if (state is! SuccessWorkTimeState) {
                              return const SizedBox();
                            }
                            final weekday = (DateTime.now().weekday + 6) % 7;
                            if (state.workTime[weekday] == null) {
                              return const Padding(
                                padding: EdgeInsets.symmetric(vertical: 32),
                                child: Center(
                                  child: P2Text(
                                    'Выходной',
                                    color: ColorData.colorMainLink,
                                  ),
                                ),
                              );
                            }

                            final start = DateFormat("HH:mm").format(
                              state.workTime[weekday]!.startTime!.parseTimeGG,
                            );
                            final end = DateFormat("HH:mm").format(
                              state.workTime[weekday]!.endTime!.parseTimeGG,
                            );
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                H4Text(
                                  "$start — $end",
                                  color: Colors.black,
                                ),
                                const SizedBox(height: 8),
                                const P2Text(
                                  'Рабочее время',
                                  color: ColorData.colorMainLink,
                                ),
                              ],
                            );
                          },
                        )),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          if (widget.participance?.subData?.description != null)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: SizeTapAnimation(
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {},
                  child: Container(
                    decoration: const BoxDecoration(
                      color: ColorData.color3Percent,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    padding: const EdgeInsets.only(
                      left: 15,
                      right: 15,
                      top: 15,
                      bottom: 20,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const H5Text(
                          'Личная информация',
                          color: ColorData.colortTextSecondary,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 15),
                          child: ReadMoreWidget(
                            widget.participance!.subData!.description!,
                            trimLines: 4,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          // BlocBuilder<TariffTrainerBloc, TariffTrainerState>(
          //   bloc: _tariffTrainerBloc,
          //   builder: (ctx, state) {
          //     if (state is! GetTariffTrainerState) {
          //       return const SizedBox();
          //     }
          //     final booking = state.bookingMap.values.toList();
          //     return SizedBox(
          //       height: 158,
          //       child: ListView.separated(
          //         padding: const EdgeInsets.symmetric(horizontal: 20),
          //         clipBehavior: Clip.none,
          //         scrollDirection: Axis.horizontal,
          //         itemBuilder: (context, index) {
          //           return BookingCard(
          //             onTap: () {},
          //             title: booking[index].name,
          //             subTitle: "Без сотрудников",
          //             label: booking[index].price == null
          //                 ? null
          //                 : NumberFormat('###,###,000₸')
          //                     .format(booking[index].price!),
          //             // '${booking[index].price!.toInt()} тг.',
          //             iconWidget: const Icon(
          //               Icons.fiber_smart_record,
          //               color: ColorData.colorMainLink,
          //               size: 32,
          //             ),
          //           );
          //         },
          //         separatorBuilder: (context, index) =>
          //             const SizedBox(width: 10),
          //         itemCount: booking.length,
          //       ),
          //     );
          //   },
          // ),
          const SizedBox(height: 10),
          if (contactNotEmpty())
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Container(
                padding: const EdgeInsets.only(left: 15, right: 15, bottom: 5, top: 15),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: ColorData.color3Percent,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const H5Text(
                      'Контакты',
                      color: ColorData.colortTextSecondary,
                    ),
                    Column(
                      children: List.generate(
                        widgets.length * 2 - 1,
                        (index) => index.isOdd
                            ? const Divider(
                                color: ColorData.color5Percent,
                                height: 0,
                                thickness: 0.5,
                              )
                            : widgets[index ~/ 2],
                      ),
                    )
                    // StreamBuilder(
                    //   stream: MainDrawerState.currentParticipanceValue,
                    //   builder: (ctx, _) {
                    //     if (participanceSubject.value?.subData == null) {
                    //       return const SizedBox(height: 15);
                    //     }
                    //     final subData = participanceSubject.value!.subData!;
                    //     final _widgets = subWidget(subData);

                    //     if (_widgets.isEmpty) return const SizedBox();
                    //     return Column(
                    //       children: List.generate(
                    //         _widgets.length * 2 - 1,
                    //         (index) => index.isOdd
                    //             ? const Divider(
                    //                 color: ColorData.color5Percent,
                    //                 height: 0,
                    //                 thickness: 0.5,
                    //               )
                    //             : _widgets[index ~/ 2],
                    //       ),
                    //     );
                    //   },
                    // ),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }
}
