import 'dart:async';
import 'dart:developer';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/club/club_bloc.dart';
import 'package:tube/logic/blocs/participance/participance_bloc.dart';
import 'package:tube/ui/common/atoms/load_photo_circle.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/custom_toggle_button.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/load_popup_route.dart';
import 'package:url_launcher/url_launcher_string.dart';

class CreateNewCompanyPage extends StatefulWidget {
  const CreateNewCompanyPage({super.key});

  @override
  State<CreateNewCompanyPage> createState() => _CreateNewCompanyPageState();
}

class _CreateNewCompanyPageState extends State<CreateNewCompanyPage> {
  XFile? _file;
  List<bool> _selections = [
    false,
    false,
  ];
  ParticipanceBloc? participanceBloc;

  final ValueNotifier<int> toggleValue = ValueNotifier(0);
  final name = TextEditingController();
  final phoneNumber = TextEditingController();
  final bin = TextEditingController();
  final jurName = TextEditingController();
  final jurAddress = TextEditingController();
  final bankNumber = TextEditingController();
  final bankName = TextEditingController();
  final iikBank = TextEditingController();
  late Listenable listenable;
  bool isValid() {
    return _file != null &&
        name.text.isNotEmpty &&
        phoneNumber.text.isNotEmpty &&
        bin.text.isNotEmpty;
  }

  @override
  void dispose() {
    toggleValue.dispose();
    listenable.removeListener(_listen);
    // listenable = null;
    name.dispose();
    phoneNumber.dispose();
    bin.dispose();
    jurName.dispose();
    jurAddress.dispose();
    bankNumber.dispose();
    bankName.dispose();
    iikBank.dispose();
    super.dispose();
  }

  void _listen() {}

  @override
  void initState() {
    listenable = Listenable.merge([
      name,
      phoneNumber,
      bin,
      jurName,
      jurAddress,
      bankNumber,
      bankName,
      iikBank,
    ])
      ..addListener(_listen);
    super.initState();
  }

  void onSubmit() async {
    final modalRoute = ModalRoute.of(context)!;
    final compl = Completer();
    Navigator.push(
      context,
      LoadPopupRoute(completer: compl),
    );
    context.read<ClubBloc>().add(
          RequestClubEvent(
            name: name.text,
            phoneNumber: phoneNumber.text,
            bin: bin.text,
            jurName: jurName.text,
            jurAddress: jurAddress.text,
            bankNumber: bankNumber.text,
            bankName: bankName.text,
            iikBank: iikBank.text,
            completer: compl,
            file: _file!,
          ),
        );
    await compl.future;

    participanceBloc?.add(const GetDataParticipanceEvent());
    if (!mounted) return;
    Navigator.removeRoute(context, modalRoute);
  }

  @override
  Widget build(BuildContext context) {
    participanceBloc = context.read<ParticipanceBloc>();
    log("message");
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: ColorData.colorMainBlack,
        title: const H4Text(
          'Создание компании',
          color: Colors.white,
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(45),
          child: Center(
            child: CustomToggleButton(
              margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
              values: const ['Физ. лицо', 'Юр. лицо'],
              valueNotifier: toggleValue,
            ),
          ),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.only(left: 20, right: 20),
        children: [
          const SizedBox(
            height: 30,
          ),
          Center(
            child: LoadPhotoCircle(
              file: _file,
              onTap: () async {
                final file = await ImagePicker().pickImage(
                  source: ImageSource.gallery,
                );
                if (file == null) return;

                setState(() {
                  _file = file;
                });
              },
            ),
          ),
          const Center(
            child: Column(
              children: [
                Text(
                  "Вы можете загрузить ",
                  style: TextStyle(
                    color: Color(0xff969696),
                    fontSize: 16,
                  ),
                ),
                Text(
                  "логотип позже",
                  style: TextStyle(
                    color: Color(0xff969696),
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 30),
          CustomTextField(
            title: "Название",
            hintText: 'Как будет называться ваша компания?',
            controller: name,
          ),
          const SizedBox(height: 30),
          CustomTextField(
            icon: const Icon(
              Icons.call,
              color: Colors.black,
            ),
            hintText: "+",
            title: "Робочий номер телефона",
            controller: phoneNumber,
            keyboardType: TextInputType.phone,
          ),
          const SizedBox(height: 30),
          CustomTextField(
            hintText: "XXX XXX XXX XXX",
            title: "БИН",
            controller: bin,
          ),
          const SizedBox(height: 30),
          CustomTextField(
            hintText: "XXX XXX XXX XXX",
            title: "Наименование юр. лица",
            controller: jurName,
          ),
          const SizedBox(height: 30),
          CustomTextField(
            hintText: "Страна, город, улица, дом, квартира",
            title: "Юридический адрес",
            controller: jurAddress,
          ),
          const SizedBox(height: 30),
          CustomTextField(
            hintText: "XXX XXX XXX XXX",
            title: "Номер расчетного счета компании",
            controller: bankNumber,
          ),
          const SizedBox(height: 30),
          CustomTextField(
            hintText: "XXX XXX XXX XXX",
            title: "Банк",
            controller: bankName,
          ),
          const SizedBox(height: 30),
          CustomTextField(
            hintText: "XXX XXX XXX XXX",
            title: "ИИК Банка",
            controller: iikBank,
          ),
          const SizedBox(height: 30),
          ListenableBuilder(
              listenable: listenable!,
              // stream: listenable,
              builder: (context, snapshot) {
                return CustomButton(
                  onTap: onSubmit,
                  title: 'Продолжить',
                  titleColor:
                      isValid() ? Colors.white : ColorData.colortTextSecondary,
                  color: isValid()
                      ? ColorData.colorMainLink
                      : ColorData.colorElementsLightMenu,
                );
              }),
          const SizedBox(height: 10),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: const TextStyle(
                fontSize: P2TextStyle.fontSize,
                height: P2TextStyle.height,
                fontWeight: P2TextStyle.fontWeight,
                color: ColorData.colorElementsSecondary,
              ),
              children: [
                const TextSpan(
                  text: "Продолжая создание компании, вы принимаете наши ",
                ),
                TextSpan(
                  text: "Условия использования для клиентов",
                  style: const TextStyle(
                    color: ColorData.colorMainLink,
                  ),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      launchUrlString(
                        'https://www.checkallnow.net/terms',
                        mode: LaunchMode.externalApplication,
                      );
                    },
                ),
                const TextSpan(
                  text: " и ",
                ),
                TextSpan(
                  text: "Политику конфиденциальности",
                  style: const TextStyle(
                    color: ColorData.colorMainLink,
                  ),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      launchUrlString(
                        'https://www.checkallnow.net/policy',
                        mode: LaunchMode.externalApplication,
                      );
                    },
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 160,
          )
        ],
      ),
    );
  }
}
