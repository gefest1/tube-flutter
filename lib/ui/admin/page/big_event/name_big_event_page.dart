import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/logic/provider/create_big_event/create_big_event.dart';
import 'package:tube/logic/provider/create_big_event/create_big_event.model.dart';
import 'package:tube/ui/admin/page/big_event/address_big_event_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';

import 'package:tube/ui/common/molecules/custom_text_field.dart';

class NameBigEventPage extends StatefulWidget {
  const NameBigEventPage({super.key});

  @override
  State<NameBigEventPage> createState() => _NameBigEventPageState();
}

class _NameBigEventPageState extends State<NameBigEventPage> {
  late final nameController = TextEditingController()..addListener(update);
  late final descriptionController = TextEditingController()
    ..addListener(update);

  void update() {
    activeSubject.value =
        nameController.text.isNotEmpty && descriptionController.text.isNotEmpty;
  }

  final activeSubject = BehaviorSubject<bool>.seeded(false);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);
        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          ),
          centerTitle: true,
          title: const H4Text(
            'Создание мероприятия',
            color: Colors.white,
          ),
        ),
        body: Stack(
          children: [
            CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 40, horizontal: 20),
                    child: Column(
                      children: [
                        CustomTextField(
                          hintText: 'Например: «Соревнования»',
                          title: 'Название',
                          controller: nameController,
                        ),
                        const SizedBox(height: 30),
                        CustomTextField(
                          hintText:
                              'Расскажите о мероприятии, про адрес, и как добраться — в следующем пункте',
                          title: 'Описание',
                          minLines: 2,
                          hintMaxLines: 2,
                          scrollPadding: const EdgeInsets.only(bottom: 160),
                          maxLines: null,
                          controller: descriptionController,
                        ),
                        const SizedBox(height: 200),
                      ],
                    ),
                  ),
                )
              ],
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: StreamBuilder(
                stream: activeSubject,
                builder: (context, _) {
                  return BottomButton(
                    title: 'Продолжить',
                    active: activeSubject.value,
                    onTap: () async {
                      final bigEventProvider =
                          Provider.of<CreateBigEventProvider>(context,
                              listen: false);
                      bigEventProvider.setNewInstance(
                        CreateBigEventModel(
                          name: nameController.text,
                          description: descriptionController.text,
                        ),
                      );
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => const AddressBigEventPage(),
                        ),
                      );
                      bigEventProvider
                          .setNewInstance(const CreateBigEventModel());
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
