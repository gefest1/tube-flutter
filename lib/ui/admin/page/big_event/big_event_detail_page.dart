import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/big_event/big_event_bloc.dart';
import 'package:tube/logic/blocs/big_event/entities/big_event_entity.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/provider/big_user_map.dart';
import 'package:tube/ui/admin/molecules/count_title.dart';
import 'package:tube/ui/admin/page/big_event/edit_big_event_page.dart';
import 'package:tube/ui/admin/page/big_event/user_gmail_list.dart';
import 'package:tube/ui/admin/page/big_event/user_list.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:tube/utils/load_popup_route.dart';

class CancelButton extends StatelessWidget {
  final VoidCallback? onTap;
  final String title;

  const CancelButton({
    this.onTap,
    this.title = 'Отменить мероприятие',
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: SizeTapAnimation(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.all(15),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(5)),
          ),
          child: P1Text(
            title,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}

class BigEventDetailPage extends StatefulWidget {
  final bool isAdmin;
  final BigEventEntity entity;

  const BigEventDetailPage({
    required this.entity,
    this.isAdmin = false,
    super.key,
  });

  @override
  State<BigEventDetailPage> createState() => _BigEventDetailPageState();
}

class _BigEventDetailPageState extends State<BigEventDetailPage> {
  final iconKey = GlobalKey();
  late BigUserMap bigUserMap = Provider.of<BigUserMap>(context, listen: false);
  late AdminBigEventBloc bigEventBloc = Provider.of<AdminBlocMap>(context, listen: false)
      .getAdminBigEventBloc(MainDrawerState.clubId!);

  late BigEventEntity entity = widget.entity;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: [
          if (widget.isAdmin)
            SizeTapAnimation(
              key: iconKey,
              onTap: () {
                if (iconKey.currentContext == null) return;
                final offset = (iconKey.currentContext!.findRenderObject() as RenderBox)
                    .localToGlobal(Offset.zero);

                final route1 = ModalRoute.of<dynamic>(context);

                showDialog(
                  context: context,
                  builder: (contextDialog) {
                    return Stack(
                      children: [
                        Positioned(
                          top: offset.dy + 36,
                          right: MediaQuery.of(contextDialog).size.width - offset.dx - 33,
                          child: CancelButton(
                            onTap: () async {
                              final route2 = ModalRoute.of<dynamic>(contextDialog);
                              final completer = Completer();
                              Navigator.push(
                                contextDialog,
                                LoadPopupRoute(completer: completer),
                              );
                              bigEventBloc.add(
                                RemoveAdminBigEventEvent(
                                  bigEventId: entity.id,
                                  completer: completer,
                                ),
                              );
                              await completer.future;
                              if (!mounted) return;
                              if (route1 != null) {
                                Navigator.removeRoute(context, route1);
                              }
                              if (route2 != null) {
                                Navigator.removeRoute(context, route2);
                              }
                            },
                          ),
                        )
                      ],
                    );
                  },
                );
              },
              child: const Padding(
                padding: EdgeInsets.all(10),
                child: Icon(
                  Icons.more_vert,
                  size: 33,
                ),
              ),
            )
        ],
        centerTitle: true,
        title: const H4Text(
          'Мероприятие',
          color: Colors.white,
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: [
          H2Text(
            entity.name,
            color: ColorData.colorElementsActive,
          ),
          const SizedBox(height: 20),
          IntrinsicHeight(
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: const BoxDecoration(
                      color: ColorData.color3Percent,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 15,
                      vertical: 20,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Icon(
                          Icons.today,
                          size: 24,
                          color: ColorData.colorTextMain,
                        ),
                        const SizedBox(height: 10),
                        H2Text(
                          DateFormat('HH:mm').format(entity.date.parseTimeGG),
                          color: ColorData.colorTextMain,
                        ),
                        const SizedBox(height: 5),
                        // 23.04.2022 — 25.04.2022
                        P1Text(
                          "${DateFormat('dd.MM.yyyy').format(entity.date.parseTimeGG)}"
                          " — "
                          "${DateFormat('dd.MM.yyyy').format(entity.finishDate.parseTimeGG)}",
                          color: ColorData.colorElementsSecondary,
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Container(
                    decoration: const BoxDecoration(
                      color: ColorData.color3Percent,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 15,
                      vertical: 20,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Icon(
                          Icons.pin_drop,
                          size: 24,
                          color: ColorData.colorTextMain,
                        ),
                        const SizedBox(height: 10),
                        H4Text(
                          "${entity.addressData.street}"
                          " "
                          "${entity.addressData.house}",
                          color: ColorData.colorTextMain,
                        ),
                        Expanded(
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: P1Text(
                              entity.addressData.city,
                              color: ColorData.colorElementsSecondary,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          SizeTapAnimation(
            onTap: () async {
              final newObj = await Navigator.push<BigEventEntity>(
                context,
                MaterialPageRoute(
                  builder: (context) => EditBigEventPage(
                    entity: entity,
                  ),
                ),
              );
              if (newObj == null) return;
              setState(() {
                entity = newObj;
              });
            },
            child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: ColorData.color3Percent,
              ),
              padding: const EdgeInsets.only(
                left: 15,
                right: 15,
                top: 15,
                bottom: 20,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      const Expanded(
                        child: H4Text(
                          'О мероприятии',
                          color: ColorData.colorTextMain,
                        ),
                      ),
                      if (widget.isAdmin)
                        const P1Text(
                          'Изменить',
                          color: ColorData.colorMainLink,
                        ),
                    ],
                  ),
                  const SizedBox(height: 15),
                  P2Text(
                    entity.description,
                    color: ColorData.colorElementsActive,
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 10),
          IntrinsicHeight(
            child: Row(
              children: [
                Expanded(
                  child: CountTitle(
                    onTap: () {
                      bigUserMap.getUsers(entity.trainerIds);

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => UserListPage(
                            ids: entity.trainerIds,
                            title: 'Сотрудники',
                          ),
                        ),
                      );
                    },
                    title: 'Сотрудники',
                    label: "${entity.trainerIds.length}"
                        ' чел',
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: CountTitle(
                    title: 'Клиенты',
                    label: "${entity.clientIds.length}"
                        ' чел',
                    onTap: () {
                      bigUserMap.getUsers(entity.clientIds);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => UserListPage(
                            ids: entity.clientIds,
                            title: 'Клиенты',
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          CountTitle(
            title: 'Гости',
            label: "${entity.guestsEmail.length}"
                ' чел',
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => UserGmailList(
                    title: 'Гости',
                    emails: entity.guestsEmail,
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
