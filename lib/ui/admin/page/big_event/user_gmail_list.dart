import 'package:flutter/material.dart';

import 'package:tube/ui/admin/molecules/trainer_tile.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';

class UserGmailList extends StatefulWidget {
  final String title;
  final List<String> emails;

  const UserGmailList({
    required this.title,
    required this.emails,
    super.key,
  });

  @override
  State<UserGmailList> createState() => _UserGmailListState();
}

class _UserGmailListState extends State<UserGmailList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: H4Text(widget.title),
      ),
      backgroundColor: Colors.white,
      body: ListView.builder(
        itemCount: widget.emails.length,
        itemBuilder: (context, index) => TrainerTile(
          innerPadding: const EdgeInsets.symmetric(horizontal: 20),
          title: widget.emails[index],
          // photoUrl: filteredUsers[index]?.photoUrl?.m,
        ),
      ),
    );
  }
}
