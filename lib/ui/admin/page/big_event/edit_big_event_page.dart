import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/big_event/big_event_bloc.dart';
import 'package:tube/logic/blocs/big_event/entities/big_event_entity.dart';
import 'package:tube/logic/general_providers.dart';

import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/load_popup_route.dart';

class EditBigEventPage extends StatefulWidget {
  final BigEventEntity entity;
  const EditBigEventPage({required this.entity, super.key});

  @override
  State<EditBigEventPage> createState() => _EditBigEventPageState();
}

class _EditBigEventPageState extends State<EditBigEventPage> {
  late final controller =
      TextEditingController(text: widget.entity.description);
  late AdminBigEventBloc bigEventBloc =
      Provider.of<AdminBlocMap>(context, listen: false)
          .getAdminBigEventBloc(MainDrawerState.clubId!);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const H5Text(
          'О мероприятии',
          color: Colors.white,
        ),
      ),
      body: Stack(
        children: [
          ListView(
            padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
            children: [
              CustomTextField(
                hintText:
                    'Расскажите о мероприятии, про адрес, и как добраться — в следующем пункте',
                title: 'Описание',
                controller: controller,
                maxLines: null,
                minLines: 3,
                hintMaxLines: 3,
                inputFormatters: [LengthLimitingTextInputFormatter(400)],
              ),
              const SizedBox(height: 100),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: ValueListenableBuilder(
              valueListenable: controller,
              builder: (context, _, __) {
                return BottomButton(
                  title: 'Сохранить',
                  active: controller.text != widget.entity.description,
                  onTap: () async {
                    // final route = ModalRoute.of(context);
                    final completer = Completer<BigEventEntity>();
                    Navigator.push(
                      context,
                      LoadPopupRoute(completer: completer),
                    );
                    bigEventBloc.add(
                      EditAdminBigEventEvent(
                        bigEventId: widget.entity.id,
                        completer: completer,
                        description: controller.text,
                      ),
                    );
                    final newObj = await completer.future;
                    Future(() {
                      Navigator.pop(context, newObj);
                    });
                  },
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
