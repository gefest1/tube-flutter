import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class ChipAddress extends StatelessWidget {
  final String title;

  const ChipAddress({
    required this.title,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: ColorData.color3Percent,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.symmetric(vertical: 13, horizontal: 10),
      child: P1Text(
        title,
        color: ColorData.colorElementsActive,
      ),
    );
  }
}
