import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/logic/provider/create_big_event/create_big_event.dart';
import 'package:tube/logic/provider/create_big_event/create_big_event.model.dart';
import 'package:tube/ui/admin/page/big_event/chip_address.dart';
import 'package:tube/ui/admin/page/big_event/date_big_event_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/utils/colors.dart';

class AddressBigEventPage extends StatefulWidget {
  const AddressBigEventPage({super.key});

  @override
  State<AddressBigEventPage> createState() => _AddressBigEventPageState();
}

class _AddressBigEventPageState extends State<AddressBigEventPage> {
  late final streetController = TextEditingController()..addListener(update);
  late final houseController = TextEditingController()..addListener(update);

  void update() {
    activeSubject.value =
        streetController.text.isNotEmpty && houseController.text.isNotEmpty;
  }

  final activeSubject = BehaviorSubject<bool>.seeded(false);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        final f = FocusScope.of(context);

        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          ),
          centerTitle: true,
          title: const H4Text(
            'Адрес',
            color: Colors.white,
          ),
        ),
        body: Stack(
          children: [
            ListView(
              padding: const EdgeInsets.symmetric(
                vertical: 40,
                horizontal: 20,
              ),
              children: [
                const H5Text(
                  'Страна',
                  color: ColorData.colortTextSecondary,
                ),
                const SizedBox(height: 10),
                const ChipAddress(title: 'Казахстан'),
                const SizedBox(height: 20),
                const H5Text(
                  'Город',
                  color: ColorData.colortTextSecondary,
                ),
                const SizedBox(height: 10),
                const ChipAddress(title: 'Алматы'),
                const SizedBox(height: 30),
                CustomTextField(
                  scrollPadding: const EdgeInsets.only(bottom: 160),
                  hintText: 'Сейфуллина',
                  title: 'Улица',
                  controller: streetController,
                ),
                const SizedBox(height: 30),
                CustomTextField(
                  scrollPadding: const EdgeInsets.only(bottom: 160),
                  hintText: '123',
                  title: 'Дом',
                  controller: houseController,
                ),
                const SizedBox(height: 100),
              ],
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: StreamBuilder(
                stream: activeSubject,
                builder: (context, _) {
                  return BottomButton(
                    title: 'Продолжить',
                    active: activeSubject.value,
                    onTap: () async {
                      final model = Provider.of<CreateBigEventProvider>(context,
                          listen: false);
                      final value = model.createBigEvent;
                      model.setNewInstance(
                        value.copyWith(
                          addressData: AddressData(
                            house: houseController.text,
                            street: streetController.text,
                          ),
                        ),
                      );
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => const DateBigEventPage(),
                        ),
                      );

                      model.setNewInstance(value);
                    },
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
