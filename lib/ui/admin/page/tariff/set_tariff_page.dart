import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/model/admin/create.booking.model.dart';
import 'package:tube/ui/admin/page/tariff/pages/set_tags_booking_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';

class SetTariffPage extends StatefulWidget {
  const SetTariffPage({Key? key}) : super(key: key);

  @override
  State<SetTariffPage> createState() => _SetTariffPageState();
}

class _SetTariffPageState extends State<SetTariffPage> {
  late final TextEditingController nameController = TextEditingController()
    ..addListener(() {
      streamController.add(null);
    });
  late final TextEditingController descController = TextEditingController();

  final StreamController streamController = StreamController();

  @override
  void dispose() {
    nameController.dispose();
    descController.dispose();
    streamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);

        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: const H4Text(
            'Добавление абонемента',
            color: Colors.white,
          ),
        ),
        body: Stack(
          children: [
            ListView(
              padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
              children: [
                CustomTextField(
                  hintText: 'Например: «Детский»',
                  title: 'Название абонемента',
                  controller: nameController,
                ),
                const SizedBox(height: 30),
                CustomTextField(
                  hintText: 'Опишите абонемент',
                  title:
                      'Описание. Поможет вашим клиентам выбрать именно то, что им нужно',
                  controller: descController,
                ),
              ],
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: StreamBuilder(
                stream: streamController.stream,
                builder: (ctx, _) {
                  return BottomButton(
                    active: nameController.text.isNotEmpty,
                    onTap: () async {
                      final f0 = FocusScope.of(context);

                      if (f0.hasFocus || f0.hasPrimaryFocus) {
                        f0.unfocus();
                      }
                      final createBookingModel =
                          Provider.of<CreateBookingModel>(context,
                              listen: false);
                      createBookingModel.setNewInstance(
                        createBookingModel.booking.copyWith(
                          name: nameController.text.trim(),
                          description: descController.text.trim(),
                        ),
                      );

                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const SetTagsBookingPage(),
                        ),
                      );
                    },
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
