import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:tube/logic/blocs/finance/finance_bloc.dart';
import 'package:tube/logic/blocs/finance/model/finance.model.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_adaptive_span.dart';
import 'package:tube/utils/get_time_shift.dart';

class FinanceCard extends StatefulWidget {
  final String title;

  const FinanceCard({
    this.title = 'Баланс',
    Key? key,
  }) : super(key: key);

  @override
  State<FinanceCard> createState() => _FinanceCardState();
}

class _FinanceCardState extends State<FinanceCard> {
  late final FinanceBloc finBloc = getFinanceBloc(context);

  @override
  void initState() {
    final date = DateTime.now();
    finBloc.add(
      GetFinanceEvent(
        startTime: date.subtract(const Duration(days: 1)),
        endTime: date,
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: H5Text(
                        widget.title,
                        color: const Color(0xff959595),
                      ),
                    ),
                    Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        BlocBuilder<FinanceBloc, FinanceState>(
                          bloc: finBloc,
                          builder: (context, state) {
                            List<Finance> data = [];
                            if (state is DataFinanceState) {
                              final startTime = DateTime(
                                DateTime.now().year,
                                DateTime.now().month,
                                DateTime.now().day,
                              );
                              final endTime = startTime.add(const Duration(days: 1));
                              data.addAll(
                                state.data.where((element) {
                                  return element.createdAt!.isAfter(startTime) &&
                                      element.createdAt!.isBefore(endTime) &&
                                      (element.isFull ?? false);
                                }),
                              );
                            }
                            return RichText(
                              text: getAdaptiveTextSpan(
                                NumberFormat('###,###,000₸').format(data.fold<num>(
                                    0,
                                    (previousValue, element) =>
                                        previousValue + (element.value ?? 0))),
                                const TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'InterTight',
                                ),
                                context,
                              ),
                            );
                          },
                        ),
                        const SizedBox(width: 5),
                        P2Text(
                          'На ${DateFormat("dd.MM.yyyy").format(DateTime.now().parseTimeGG)}',
                          color: ColorData.colortTextSecondary,
                        )
                      ],
                    ),
                  ],
                ),
              ),
              const Icon(
                Icons.chevron_right,
                size: 24,
                color: ColorData.colorElementsSecondary,
              )
            ],
          ),
        ),
        const Divider(
          height: 0,
          thickness: 0.5,
          color: Color(0xffA5A5A5),
        ),
      ],
    );
  }
}
