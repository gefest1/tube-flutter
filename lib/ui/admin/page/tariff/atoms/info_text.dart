import 'package:flutter/material.dart';

class InfoText extends StatelessWidget {
  final TextStyle style1;
  final TextStyle style2;

  final String text1;
  final String text2;

  const InfoText({
    required this.text1,
    required this.style1,
    required this.text2,
    required this.style2,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            style: style1,
            text: text1,
          ),
          TextSpan(
            style: style2,
            text: text2,
          ),
        ],
      ),
    );
  }
}
