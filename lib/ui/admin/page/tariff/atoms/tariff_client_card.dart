import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class TariffClientCard extends StatelessWidget {
  final String? count;
  const TariffClientCard({
    required this.count,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 5,
      ),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            blurRadius: 20,
            color: Color(0x0d000000),
            offset: Offset(0, 5),
          ),
          BoxShadow(
            blurRadius: 2,
            color: Color(0x03000000),
            offset: Offset(3, 0),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (count != null)
            H1Text(
              count!,
              color: ColorData.colorTextMain,
            ),
          const SizedBox(height: 15),
          const Row(
            children: [
              Expanded(
                child: P1Text(
                  'Клиентов',
                  color: ColorData.colortTextSecondary,
                ),
              ),
              Icon(
                Icons.navigate_next,
                color: ColorData.colorTextMain,
                size: 24,
              )
            ],
          ),
          const SizedBox(height: 15),
        ],
      ),
    );
  }
}
