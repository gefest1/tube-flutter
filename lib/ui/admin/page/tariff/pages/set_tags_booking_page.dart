import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/model/admin/create.booking.model.dart';
import 'package:tube/ui/admin/page/tariff/add_trainer_page.dart';
import 'package:tube/ui/admin/page/tariff/pages/change_set_tag_page.dart';
import 'package:tube/ui/common/atoms/active_tag.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/shadow.dart';

class SetTagsBookingPage extends StatefulWidget {
  const SetTagsBookingPage({super.key});

  @override
  State<SetTagsBookingPage> createState() => _SetTagsBookingPageState();
}

class _SetTagsBookingPageState extends State<SetTagsBookingPage> {
  Map<String, bool> mapVal = <String, bool>{};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const H4Text(
          'Атрибуты',
          color: Colors.white,
        ),
      ),
      body: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Column(
                  children: [
                    const SizedBox(height: 40),
                    StreamBuilder(
                      stream: MainDrawerState.currentParticipanceValue,
                      builder: (context, snapshot) {
                        final tags = MainDrawerState
                                .currentParticipanceValue.value?.club?.tags ??
                            <String>[];
                        mapVal = Map.fromEntries(
                          tags.map(
                            (e) => MapEntry(e, mapVal[e] ?? false),
                          ),
                        );

                        if (tags.isEmpty) return const SizedBox();
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 40),
                          child: Wrap(
                            alignment: WrapAlignment.center,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            spacing: 10,
                            runSpacing: 10,
                            children: tags
                                .map(
                                  (e) => ActiveTag(
                                      title: e,
                                      active: mapVal[e] ?? false,
                                      onTap: () {
                                        setState(() {
                                          mapVal[e] = !(mapVal[e] ?? false);
                                        });
                                      }),
                                )
                                .toList(),
                          ),
                        );
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40),
                      child: CustomButton(
                        title: 'Настроить атрибуты',
                        titleColor: ColorData.colorMainLink,
                        color: Colors.white,
                        boxShadow: blocksMains,
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const ChangeSetTagPage(),
                              ));
                        },
                      ),
                    ),
                    const SizedBox(height: 100),
                  ],
                ),
              ),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: BottomButton(
              title: 'Продолжить',
              onTap: () async {
                final createBookingModel =
                    Provider.of<CreateBookingModel>(context, listen: false);
                final oldBooking = createBookingModel.booking;

                createBookingModel.setNewInstance(
                  createBookingModel.booking.copyWith(
                      tags: mapVal.entries
                          .where((element) => element.value)
                          .map((e) => e.key)
                          .toList()),
                );

                await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const AddTrainerPage(),
                  ),
                );
                createBookingModel.setNewInstance(oldBooking);
                createBookingModel.setNewWorkTime({});
              },
            ),
          )
        ],
      ),
    );
  }
}
