import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/logic/model/admin/create.booking.model.dart';
import 'package:tube/logic/model/admin/edit_booking.model.dart';
import 'package:tube/logic/model/work_time_graph.dart';

import 'package:tube/ui/admin/page/tariff/molecules/time_active_card.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/fast_app_bar.dart';
import 'package:tube/utils/get_next_week_date.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:tube/utils/week_day.dart';
import 'dart:developer' as dev;

class SetTimeTrainerPage extends StatefulWidget {
  final String trainerId;
  final ScrollController scrollController;
  final int weekday;
  final List<DateTime> dateTimes;
  final WorkTimeGraph timeTable;
  final String trainerTitle;

  const SetTimeTrainerPage({
    required this.scrollController,
    required this.trainerId,
    required this.weekday,
    required this.timeTable,
    required this.dateTimes,
    required this.trainerTitle,
    Key? key,
  }) : super(key: key);

  @override
  State<SetTimeTrainerPage> createState() => _SetTimeTrainerPageState();
}

class _SetTimeTrainerPageState extends State<SetTimeTrainerPage> {
  late final BehaviorSubject<List<DateTime>> dateTimeSubject =
      BehaviorSubject<List<DateTime>>.seeded(widget.dateTimes);

  @override
  void dispose() {
    dateTimeSubject.close();
    super.dispose();
  }

  late num timePerMinute =
      Provider.of<CreateBookingModel>(context, listen: false).booking.timePerMinute ??
          Provider.of<EditBookingModel>(context, listen: false).timePerMinute;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(10),
        ),
      ),
      child: Stack(
        children: [
          CustomScrollView(
            physics: const ClampingScrollPhysics(),
            controller: widget.scrollController,
            slivers: [
              SliverToBoxAdapter(
                child: FastAppBar(
                  title: weeks[widget.weekday],
                  subTitle: '${widget.trainerTitle} расписание',
                ),
              ),
              SetTimeTrainerBody(
                listDateTime: dateTimeSubject,
                currentWorkTime: widget.timeTable,
                duration: Duration(
                  minutes: timePerMinute.toInt(),
                ),
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: StreamBuilder(
              stream: dateTimeSubject.stream,
              builder: (ctx, _) {
                if (listEquals(dateTimeSubject.value, widget.dateTimes)) {
                  return const SizedBox();
                }

                return BottomButton(
                  title: 'Сохранить',
                  onTap: () {
                    Navigator.pop(context, dateTimeSubject.value);
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class SetTimeTrainerBody extends StatefulWidget {
  final WorkTimeGraph currentWorkTime;
  final Duration duration;
  final BehaviorSubject<List<DateTime>>? listDateTime;

  const SetTimeTrainerBody({
    required this.currentWorkTime,
    required this.duration,
    this.listDateTime,
    Key? key,
  }) : super(key: key);

  @override
  State<SetTimeTrainerBody> createState() => _SetTimeTrainerBodyState();
}

class _SetTimeTrainerBodyState extends State<SetTimeTrainerBody> {
  final interval = const Duration(minutes: 15);
  late final BehaviorSubject<List<DateTime>> _curr =
      widget.listDateTime ?? BehaviorSubject<List<DateTime>>.seeded([]);

  final List<DateTime> _dateTime = [];
  final List<bool> _answerTime = [];
  List<bool> listAvailabe = [];
  final Map<int, bool> _actives = {};
  DateTime get startTime => widget.currentWorkTime.startTime!;
  DateTime get endTime => widget.currentWorkTime.endTime!;

  @override
  void dispose() {
    _curr.close();
    super.dispose();
  }

  void _initS() {
    for (DateTime jumpTime = startTime;
        jumpTime.compareTo(endTime) == -1;
        jumpTime = jumpTime.add(interval)) {
      _dateTime.add(jumpTime);
      final exist =
          _curr.value.where((DateTime element) => element.compareTo(jumpTime) == 0).isNotEmpty;
      _answerTime.add(exist);
      if (exist) {
        _actives[_dateTime.length - 1] = true;
      }

      listAvailabe.add(true);
    }
  }

  @override
  void initState() {
    _initS();
    _allUpdate();
    super.initState();
  }

  void _allUpdate() {
    _curr.value = _actives.keys.map((int key) {
      return _dateTime[key];
    }).toList();

    listAvailabe = List.generate(listAvailabe.length, (index) => true);
    for (final entry in _actives.entries) {
      final index = entry.key;
      final startTime = _dateTime[index].add(widget.duration);
      for (int currentIndex = index + 1;
          currentIndex < _dateTime.length && startTime.compareTo(_dateTime[currentIndex]) == 1;
          ++currentIndex) {
        listAvailabe[currentIndex] = !_answerTime[index];
      }

      final endTime = _dateTime[index].subtract(widget.duration);
      for (int currentIndex = index - 1;
          currentIndex >= 0 && endTime.compareTo(_dateTime[currentIndex]) == -1;
          --currentIndex) {
        listAvailabe[currentIndex] = !_answerTime[index];
      }
    }
  }

  void _onTap(int index) {
    setState(() {
      _answerTime[index] = !_answerTime[index];
      if (_answerTime[index]) {
        _actives[index] = true;
      } else {
        _actives.remove(index);
      }
      _allUpdate();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 200),
      sliver: SliverList(
        delegate: SliverChildBuilderDelegate(
          (context, index) {
            if (!index.isEven) return const SizedBox(height: 10);
            final itemIndex = index ~/ 2;

            return TimeActiveCard(
              title: DateFormat('HH:mm').format(_dateTime[itemIndex].parseTimeGG),
              active: _answerTime[itemIndex],
              available: listAvailabe[itemIndex],
              onTap: listAvailabe[itemIndex] ? () => _onTap(itemIndex) : null,
            );
          },
          childCount: _dateTime.isEmpty ? 0 : _dateTime.length * 2 - 1,
        ),
      ),
    );
  }
}
