import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/trainer/tariff_trainer_bloc/tariff_trainer_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/ui/admin/page/tariff/molecules/stack_image_card_wrapper.dart';
import 'package:tube/ui/common/atoms/read_more_widget.dart';
import 'package:tube/ui/common/atoms/short_divide_tile.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'dart:math' as math;

import 'package:tube/utils/get_time_shift.dart';

class TariffNoTrainerPage extends StatefulWidget {
  final List<TariffSingle> list;
  final String uniqueMapKey;
  final Booking booking;
  final DateTime dateTime;

  const TariffNoTrainerPage({
    required this.list,
    required this.booking,
    required this.dateTime,
    required this.uniqueMapKey,
    Key? key,
  }) : super(key: key);
  @override
  State<TariffNoTrainerPage> createState() => _TariffNoTrainerPageState();
}

class _TariffNoTrainerPageState extends State<TariffNoTrainerPage> {
  late Uri uriLink = Uri.parse('can.io').replace(
    queryParameters: {
      "bookingId": widget.booking.id!,
      "date": widget.dateTime.toIso8601String(),
    },
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          log('CHECK');
        },
      ),
      appBar: AppBar(
        title: const H4Text('Абонемент без сотрудников'),
        centerTitle: true,
      ),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: [
          LayoutBuilder(
            builder: (context, constraints) {
              final qrWidth = math.min(constraints.maxWidth / 2, 300.0);
              return Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: QrImageView(
                      data: uriLink.toString(),
                      version: QrVersions.auto,
                      padding: EdgeInsets.zero,
                      size: qrWidth,
                    ),
                  ),
                  Expanded(
                    child: SizeTapAnimation(
                      child: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {},
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                            vertical: 20,
                            horizontal: 15,
                          ),
                          decoration: const BoxDecoration(
                            color: ColorData.color3Percent,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Icon(
                                Icons.today,
                                size: 24,
                                color: ColorData.colorTextMain,
                              ),
                              const SizedBox(height: 46),
                              H2Text(
                                DateFormat('HH:mm').format(widget.dateTime.parseTimeGG),
                                // '14:00',
                                color: ColorData.colorTextMain,
                              ),
                              const SizedBox(height: 5),
                              P1Text(
                                DateFormat('dd.MM.yyyy').format(widget.dateTime.parseTimeGG),
                                // '23.04.2022',
                                color: ColorData.colorElementsSecondary,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  // Expanded(
                  //   child: Column(
                  //     mainAxisAlignment: MainAxisAlignment.start,
                  //     crossAxisAlignment: CrossAxisAlignment.start,
                  //     children: [
                  //       BlocBuilder<TariffTrainerBloc, TariffTrainerState>(
                  //         bloc: Provider.of<TrainerBlocMap>(context)
                  //             .getTariffTrainerBloc(
                  //           MainDrawerState.currentParticipance!.clubId!,
                  //         ),
                  //         builder: (ctx, state) {
                  //           if (state is GetTariffTrainerState) {
                  //             final count = state
                  //                 .uniqueTariffList[widget.uniqueMapKey]!
                  //                 .where((element) =>
                  //                     element.trainTimesSingle.come != true)
                  //                 .length;

                  //             return SizeTapAnimation(
                  //               child: GestureDetector(
                  //                 onTap: () {},
                  //                 child: TariffClientCard(
                  //                   count: count.toString(),
                  //                 ),
                  //               ),
                  //             );
                  //           }
                  //           return const SizedBox();
                  //         },
                  //       ),
                  //       // SizeTapAnimation(
                  //       //   child: GestureDetector(
                  //       //     onTap: () {},
                  //       //     child: TariffClientCard(
                  //       //       count: widget.list.length.toString(),
                  //       //     ),
                  //       //   ),
                  //       // ),
                  //       const SizedBox(height: 10),
                  //       SizeTapAnimation(
                  //         child: GestureDetector(
                  //           onTap: () {},
                  //           child: Container(
                  //             alignment: Alignment.center,
                  //             padding: const EdgeInsets.all(10),
                  //             decoration: const BoxDecoration(
                  //               color: Color(0xffF5F5F5),
                  //               borderRadius:
                  //                   BorderRadius.all(Radius.circular(5)),
                  //             ),
                  //             child: const P1Text(
                  //               'Поделиться QR',
                  //               color: ColorData.colorMainLink,
                  //             ),
                  //           ),
                  //         ),
                  //       )
                  //     ],
                  //   ),
                  // )
                ],
              );
            },
          ),
          const SizedBox(height: 20),
          BlocBuilder<TariffTrainerBloc, TariffTrainerState>(
            bloc: Provider.of<TrainerBlocMap>(context).getTariffTrainerBloc(
              MainDrawerState.currentParticipance!.clubId!,
            ),
            builder: (ctx, state) {
              if (state is GetTariffTrainerState) {
                return StackImageCardWrapper(
                  tariffs: state.uniqueTariffList[widget.uniqueMapKey]!,
                  dateTime: widget.dateTime,
                  uniqueMapKey: widget.uniqueMapKey,
                );
              }
              return const SizedBox();
            },
          ),
          // StackImageCardWrapper(uniqueMapKey: widget.uniqueMapKey),
          Container(
            decoration: BoxDecoration(
              color: ColorData.color3Percent,
              borderRadius: BorderRadius.circular(10),
            ),
            padding: const EdgeInsets.only(
              top: 15,
              bottom: 20,
              left: 15,
              right: 15,
            ),
            child: const Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                H4Text(
                  'Об абонементе',
                  color: ColorData.colorTextMain,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  child: P0Text(
                    'Фитнес',
                    color: ColorData.colorElementsActive,
                  ),
                ),
                Divider(
                  height: 0,
                  color: Color(0xffCBCBCB),
                  thickness: 0.5,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 15),
                ),
                ReadMoreWidget(
                  'Flutter is Google’s mobile UI open source framework to build high-quality native (super fast) interfaces for iOS and Android apps with the unified codebase.',
                  trimLines: 2,
                ),
                // ReadMoreText(
                //   'Flutter is Google’s mobile UI open source framework to build high-quality native (super fast) interfaces for iOS and Android apps with the unified codebase.',
                //   trimLines: 2,
                //   delimiter: '',
                //   colorClickableText: ColorData.colorMainLink,
                //   trimMode: TrimMode.Line,
                //   trimCollapsedText: ' ...показать полностью',
                //   trimExpandedText: ' скрыть',
                //   style: TextStyle(
                //     fontSize: P0TextStyle.fontSize,
                //     fontWeight: P0TextStyle.fontWeight,
                //     height: P0TextStyle.height,
                //     color: ColorData.colorTextMain,
                //   ),
                //   moreStyle: TextStyle(
                //     fontSize: P0TextStyle.fontSize,
                //     fontWeight: P0TextStyle.fontWeight,
                //     height: P0TextStyle.height,
                //     color: ColorData.colorMainLink,
                //   ),
                // ),
              ],
            ),
          ),
          const ShortDivideTile(
            title: 'Cрок действия абонемента',
            label: '5 меc',
          ),
          const ShortDivideTile(
            title: 'Цена абонемента',
            label: '50 000 тенге',
          ),
        ],
      ),
    );
  }
}
