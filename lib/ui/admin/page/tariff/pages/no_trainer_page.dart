import 'package:flutter/material.dart';
import 'package:tube/icon/perm_contact_calendar.icon.dart';
import 'package:tube/ui/admin/page/validity_tariff_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/utils/colors.dart';

// PermContactCalendarCancelCustomPainter
class NoTrainerPage extends StatefulWidget {
  const NoTrainerPage({Key? key}) : super(key: key);

  @override
  State<NoTrainerPage> createState() => _NoTrainerPageState();
}

class _NoTrainerPageState extends State<NoTrainerPage> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
      children: [
        const Align(
          alignment: Alignment.centerLeft,
          child: CustomPaint(
            foregroundPainter: PermContactCalendarCancelCustomPainter(),
            child: Icon(
              Icons.perm_contact_calendar,
              color: ColorData.colorMainElements,
            ),
          ),
        ),
        const SizedBox(height: 10),
        const H3Text(
          'Без сотрудников',
          color: ColorData.colorTextMain,
        ),
        const SizedBox(height: 10),
        const P1Text(
          '— Клиент пользуется абонементом сколько угодно раз в установленный временной промежуток',
          color: ColorData.colorTextMain,
        ),
        const SizedBox(height: 10),
        const P1Text(
          '— Услуги ваших сотрудников покупаются отдельно',
          color: ColorData.colorTextMain,
        ),
        const SizedBox(height: 30),
        const ValidityTariffPage(),
        const SizedBox(height: 40),
        const CustomTextField(
          hintText: '10',
          title: 'Количество занятий в течение срока действия',
        ),
        const SizedBox(height: 20),
        Container(
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            border: Border.all(
              color: const Color(0xffA5A5A5),
              width: 0.5,
            ),
          ),
          padding: const EdgeInsets.all(15),
          child: const Row(
            children: [
              Padding(
                padding: EdgeInsets.only(right: 10),
                child: Icon(
                  Icons.warning_amber,
                  size: 24,
                  color: ColorData.colorElementsActive,
                ),
              ),
              Expanded(
                child: P1Text(
                  'Если клиент не успеет отходить нужное количество занятий за срок действия абонемента —  неиспользованные занятия сгорают',
                  color: ColorData.colorTextMain,
                ),
              )
            ],
          ),
        ),
        const SizedBox(height: 40),
        CustomButton(
          title: 'Продолжить',
          onTap: () {},
          color: ColorData.colorMainLink,
        ),
        const SizedBox(height: 40),
      ],
    );
  }
}
