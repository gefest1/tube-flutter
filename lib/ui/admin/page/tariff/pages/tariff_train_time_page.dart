import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/admin/tariff_admin_bloc/tariff_admin_bloc.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/ui/admin/page/tariff/molecules/dropdown_action_tariff.dart';
import 'package:tube/ui/admin/page/tariff/pages/edit_tariff_admin_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/drop_down_route.dart';
import 'package:tube/utils/get_date_time_object_id.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:tube/utils/load_popup_route.dart';
import 'package:url_launcher/url_launcher.dart';

class TariffTrainTimePage extends StatefulWidget {
  final String title;
  final DateTime dateTime;
  final List<TariffSingle> tariffList;
  final String uniqueMapKey;

  const TariffTrainTimePage({
    required this.title,
    required this.dateTime,
    required this.tariffList,
    required this.uniqueMapKey,
    Key? key,
  }) : super(key: key);

  @override
  State<TariffTrainTimePage> createState() => _TariffTrainTimePageState();
}

class _TariffTrainTimePageState extends State<TariffTrainTimePage> {
  late List<TariffSingle> tariffList = widget.tariffList;
  late final TariffAdminBloc tariffAdminBloc =
      Provider.of<AdminBlocMap>(context, listen: false).getTariffAdminBloc(MainDrawerState.clubId!);

  StreamSubscription? streamSub;

  String countStr(TariffSingle tariff) {
    final trainTimes = (tariff.trainTimes ?? []);
    final notActive = trainTimes.where(
      (element) {
        return DateTime.now().microsecondsSinceEpoch > element.endTime!.microsecondsSinceEpoch ||
            element.come == true;
      },
    );
    final trainCount = tariff.trainCount ?? 10;
    return (trainCount - notActive.length).toString();
  }

  void updateTariff(TariffAdminState newState) {
    if (!mounted) return;
    if (newState is GetTariffAdminState) {
      setState(() {
        tariffList = newState.uniqueTariffList[widget.uniqueMapKey] ?? [];
      });
    }
  }

  @override
  void initState() {
    streamSub = tariffAdminBloc.stream.listen(updateTariff);
    super.initState();
  }

  @override
  void dispose() {
    streamSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          iconSize: 33,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        title: Column(
          children: [
            H4Text(widget.title, color: Colors.white),
            const SizedBox(height: 2),
            P1Text(
              DateFormat("HH:mm dd.MM.yyyy").format(
                widget.dateTime.parseTimeGG,
              ),
              color: Colors.white,
            ),
          ],
        ),
      ),
      body: ListView.builder(
        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 40, top: 25),
        itemCount: tariffList.length,
        itemBuilder: (ctx, index) {
          final tarif = tariffList[index];
          final photoUrl = tarif.user!.photoUrl;

          return Column(
            children: [
              Row(
                children: [
                  CircleAvatar(
                    radius: 20,
                    backgroundColor: ColorData.color3Percent,
                    child: Stack(
                      children: [
                        const Center(
                          child: Icon(
                            Icons.face,
                            color: ColorData.colorElementsActive,
                          ),
                        ),
                        if (photoUrl?.m != null)
                          ClipOval(
                            child: Image(
                              image: PCacheImage(
                                photoUrl!.m!,
                                enableInMemory: true,
                              ),
                              height: 40,
                              width: 40,
                            ),
                          )
                      ],
                    ),
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 15),
                        H4Text(
                          tarif.user!.fullName ?? tarif.user!.email ?? '',
                          color: ColorData.colorTextMain,
                        ),
                        // getDateTimefromObjectId(objectId)
                        RichText(
                          text: TextSpan(
                            style: const TextStyle(fontFamily: 'InterTight'),
                            children: [
                              const TextSpan(
                                // Абонемент куплен: 15.10.2022
                                text: 'Абонемент куплен: ',
                                style: TextStyle(
                                  fontSize: P2TextStyle.fontSize,
                                  fontWeight: P2TextStyle.fontWeight,
                                  height: P2TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                              TextSpan(
                                text: DateFormat("dd.MM.yyyy").format(
                                  getDateTimefromObjectId(tarif.id!).parseTimeGG,
                                ),
                                style: const TextStyle(
                                  fontSize: P1TextStyle.fontSize,
                                  fontWeight: FontWeight.w700,
                                  height: P1TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                            ],
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            style: const TextStyle(fontFamily: 'InterTight'),
                            children: [
                              const TextSpan(
                                text: 'Действует от: ',
                                style: TextStyle(
                                  fontSize: P2TextStyle.fontSize,
                                  fontWeight: P2TextStyle.fontWeight,
                                  height: P2TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                              TextSpan(
                                text: DateFormat("dd.MM.yyyy").format(
                                  (tarif.startDate ?? getDateTimefromObjectId(tarif.id!))
                                      .parseTimeGG,
                                ),
                                style: const TextStyle(
                                  fontSize: P1TextStyle.fontSize,
                                  fontWeight: FontWeight.w700,
                                  height: P1TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                            ],
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            style: const TextStyle(fontFamily: 'InterTight'),
                            children: [
                              const TextSpan(
                                text: 'Действует до: ',
                                style: TextStyle(
                                  fontSize: P2TextStyle.fontSize,
                                  fontWeight: P2TextStyle.fontWeight,
                                  height: P2TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                              TextSpan(
                                text: DateFormat("dd.MM.yyyy").format(
                                  tarif.deadlineDate!.parseTimeGG,
                                ),
                                style: const TextStyle(
                                  fontSize: P1TextStyle.fontSize,
                                  fontWeight: FontWeight.w700,
                                  height: P1TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                            ],
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            style: const TextStyle(fontFamily: 'InterTight'),
                            children: [
                              const TextSpan(
                                text: 'Осталось занятий: ',
                                style: TextStyle(
                                  fontSize: P2TextStyle.fontSize,
                                  fontWeight: P2TextStyle.fontWeight,
                                  height: P2TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                              TextSpan(
                                text: "${countStr(tarif)}/${tarif.trainCount}",
                                style: const TextStyle(
                                  fontSize: P1TextStyle.fontSize,
                                  fontWeight: FontWeight.w700,
                                  height: P1TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                            ],
                          ),
                        ),
                        if (tarif.user?.phoneNumber != null)
                          GestureDetector(
                            onTap: () {
                              launchUrl(
                                Uri.parse('tel:${tarif.user!.phoneNumber!}'),
                              );
                            },
                            behavior: HitTestBehavior.opaque,
                            child: IgnorePointer(
                              child: RichText(
                                text: TextSpan(
                                  style: const TextStyle(fontFamily: 'InterTight'),
                                  children: [
                                    const TextSpan(
                                      text: 'Телефон: ',
                                      style: TextStyle(
                                        fontSize: P2TextStyle.fontSize,
                                        fontWeight: P2TextStyle.fontWeight,
                                        height: P2TextStyle.height,
                                        color: ColorData.colorTextMain,
                                      ),
                                    ),
                                    TextSpan(
                                      text: tarif.user!.phoneNumber!,
                                      style: const TextStyle(
                                        fontSize: P1TextStyle.fontSize,
                                        fontWeight: FontWeight.w700,
                                        height: P1TextStyle.height,
                                        color: ColorData.colorMainLink,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Center(
                      child: Builder(builder: (contextTap) {
                        return SizeTapAnimation(
                          onTap: () {
                            final RenderBox itemBox = contextTap.findRenderObject()! as RenderBox;
                            final NavigatorState navigator = Navigator.of(context);
                            Rect itemRect = itemBox.localToGlobal(
                                  Offset.zero,
                                  ancestor: navigator.context.findRenderObject(),
                                ) &
                                itemBox.size;

                            final direction = Directionality.of(context);
                            final rect = Rect.fromLTRB(
                              direction == TextDirection.rtl
                                  ? itemRect.left + itemRect.size.width
                                  : double.negativeInfinity,
                              itemRect.top - 2,
                              direction == TextDirection.rtl ? double.infinity : itemRect.left,
                              double.infinity,
                            );

                            Navigator.push(
                              context,
                              CustomDropdownRoute(
                                alignment: direction == TextDirection.rtl
                                    ? Alignment.topLeft
                                    : Alignment.topRight,
                                rect: rect,
                                child: DropDownActionTariff(
                                  onTap: (
                                    onRemove: () async {
                                      final completer = Completer();
                                      Navigator.push(
                                        context,
                                        LoadPopupRoute(completer: completer),
                                      );
                                      tariffAdminBloc.add(
                                        EditSingleTariffAdminEvent(
                                          completer: completer,
                                          data: (
                                            id: tarif.id!,
                                            lastCount: 0,
                                            startDate: tarif.startDate!,
                                          ),
                                        ),
                                      );
                                    },
                                    onResume: () {},
                                    onEdit: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => EditTariffAdminPage(
                                            tariffSingle: tarif,
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ),
                            );
                          },
                          child: const Icon(
                            Icons.more_vert,
                            color: ColorData.colorBlackInactive,
                            size: 24,
                          ),
                        );
                      }),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 15),
              const Divider(
                height: 0,
                color: Color(0xffEBEBEB),
                thickness: 0.5,
              )
            ],
          );
        },
        // children: ,
      ),
    );
  }
}
