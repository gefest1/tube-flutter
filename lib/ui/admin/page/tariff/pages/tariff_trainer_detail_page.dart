import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/trainer/tariff_trainer_bloc/tariff_trainer_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/ui/admin/page/tariff/atoms/duration_label.dart';
import 'package:tube/ui/admin/page/tariff/molecules/session_start_card.dart';
import 'package:tube/ui/admin/page/tariff/molecules/stack_image_card_wrapper.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'dart:math' as math;
import 'dart:developer' as dev;

import 'package:tube/utils/get_time_shift.dart';

class TariffTrainerDetailPage extends StatefulWidget {
  final List<TariffSingle> list;
  final String uniqueMapKey;
  final Booking booking;
  final DateTime dateTime;

  const TariffTrainerDetailPage({
    required this.list,
    required this.booking,
    required this.dateTime,
    required this.uniqueMapKey,
    Key? key,
  }) : super(key: key);
  @override
  State<TariffTrainerDetailPage> createState() => _TariffNoTrainerPageState();
}

class _TariffNoTrainerPageState extends State<TariffTrainerDetailPage> {
  late Uri uriLink = Uri.parse('can.io').replace(
    queryParameters: {
      "bookingId": widget.booking.id!,
      "date": widget.dateTime.toIso8601String(),
    },
  );
  late TariffTrainerBloc tariffTrainerBloc = Provider.of<TrainerBlocMap>(context, listen: false)
      .getTariffTrainerBloc(MainDrawerState.currentParticipance!.clubId!);

  @override
  void initState() {
    tariffTrainerBloc.add(const GetSoonTariffTrainerEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const H4Text('Абонемент с сотрудниками'),
        centerTitle: true,
      ),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: H2Text(
              "Занятие «${widget.booking.name!}»",
              color: ColorData.colorElementsActive,
            ),
          ),
          LayoutBuilder(
            builder: (context, constraints) {
              final qrWidth = math.min(constraints.maxWidth / 2, 300.0);
              return Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: QrImageView(
                      data: uriLink.toString(),
                      version: QrVersions.auto,
                      padding: EdgeInsets.zero,
                      size: qrWidth,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: qrWidth,
                      padding: const EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 15,
                      ),
                      decoration: const BoxDecoration(
                        color: ColorData.color3Percent,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Icon(
                            Icons.today,
                            size: 24,
                            color: ColorData.colorTextMain,
                          ),
                          const SizedBox(height: 46),
                          H2Text(
                            DateFormat('HH:mm').format(widget.dateTime.parseTimeGG),
                            // '14:00',
                            color: ColorData.colorTextMain,
                          ),
                          const SizedBox(height: 5),
                          Flexible(
                            child: P1Text(
                              DateFormat('dd.MM.yyyy').format(widget.dateTime.parseTimeGG),
                              color: ColorData.colorElementsSecondary,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  // Expanded(
                  //   child: Column(
                  //     mainAxisAlignment: MainAxisAlignment.start,
                  //     crossAxisAlignment: CrossAxisAlignment.start,
                  //     children: [
                  //       BlocBuilder<TariffTrainerBloc, TariffTrainerState>(
                  //         bloc: Provider.of<TrainerBlocMap>(context)
                  //             .getTariffTrainerBloc(
                  //           MainDrawerState.currentParticipance!.clubId!,
                  //         ),
                  //         builder: (ctx, state) {
                  //           if (state is GetTariffTrainerState) {
                  //             final count = state
                  //                 .uniqueTariffList[widget.uniqueMapKey]!
                  //                 .where((element) =>
                  //                     element.trainTimesSingle.come != true)
                  //                 .length;

                  //             return SizeTapAnimation(
                  //               child: GestureDetector(
                  //                 onTap: () {},
                  //                 child: TariffClientCard(
                  //                   count: count.toString(),
                  //                 ),
                  //               ),
                  //             );
                  //           }
                  //           return const SizedBox();
                  //         },
                  //       ),
                  //       // SizeTapAnimation(
                  //       //   child: GestureDetector(
                  //       //     onTap: () {},
                  //       //     child: TariffClientCard(
                  //       //       count: widget.list.length.toString(),
                  //       //     ),
                  //       //   ),
                  //       // ),
                  //       const SizedBox(height: 10),
                  //       SizeTapAnimation(
                  //         child: GestureDetector(
                  //           onTap: () {},
                  //           child: Container(
                  //             alignment: Alignment.center,
                  //             padding: const EdgeInsets.all(10),
                  //             decoration: const BoxDecoration(
                  //               color: Color(0xffF5F5F5),
                  //               borderRadius:
                  //                   BorderRadius.all(Radius.circular(5)),
                  //             ),
                  //             child: const P1Text(
                  //               'Поделиться QR',
                  //               color: ColorData.colorMainLink,
                  //             ),
                  //           ),
                  //         ),
                  //       )
                  //     ],
                  //   ),
                  // )
                ],
              );
            },
          ),
          const SizedBox(height: 10),
          SessionStartCard(
            dateTime: widget.dateTime,
            endTime: widget.dateTime.add(
              Duration(
                minutes: widget.booking.timePerMinute!.toInt(),
              ),
            ),
          ),
          const SizedBox(height: 20),
          BlocBuilder<TariffTrainerBloc, TariffTrainerState>(
            bloc: Provider.of<TrainerBlocMap>(context).getTariffTrainerBloc(
              MainDrawerState.currentParticipance!.clubId!,
            ),
            builder: (ctx, state) {
              if (state is GetTariffTrainerState) {
                if (state.uniqueTariffList[widget.uniqueMapKey] == null) {
                  dev.log(StackTrace.current.toString().substring(0, 500),
                      error: "SOMETHING WENT WRONG CHECK IT");
                }
                return StackImageCardWrapper(
                  uniqueMapKey: widget.uniqueMapKey,
                  dateTime: widget.dateTime,
                  tariffs: state.uniqueTariffList[widget.uniqueMapKey] ?? [],
                );
              }
              return const SizedBox();
            },
          ),
          const SizedBox(height: 30),
          DurationLabel(
            duration: Duration(
              minutes: widget.booking.timePerMinute!.toInt(),
            ),
          ),
          const SizedBox(height: 30),
          // const H5Text(
          //   'О занятии',
          //   color: ColorData.colorElementsSecondary,
          // ),
          // const SizedBox(height: 5),

          // Padding(
          //   padding: const EdgeInsets.symmetric(vertical: 15),
          //   child: Row(
          //     children: const [
          //       Expanded(
          //         child: P1Text(
          //           'Длительность',
          //           color: ColorData.colorTextMain,
          //         ),
          //       ),
          //       P1Text(
          //         '1ч',
          //         color: ColorData.colorTextMain,
          //       )
          //     ],
          //   ),
          // ),
          // const Divider(
          //   color: ColorData.colorElementsSecondary,
          //   thickness: 0.2,
          //   height: 0,
          // ),

          // Column(
          //   crossAxisAlignment: CrossAxisAlignment.start,
          //   children: [
          //     const SizedBox(height: 15),
          //     if (title != null)
          //       H4Text(
          //         title!,
          //         color: ColorData.colorTextMain,
          //       ),
          //     if (label != null)
          //       Padding(
          //         padding: const EdgeInsets.only(top: 5.0),
          //         child: P1Text(
          //           label!,
          //           color: ColorData.colorTextMain,
          //         ),
          //       ),
          //     const SizedBox(height: 15),
          //     const Divider(
          //       height: 0,
          //       thickness: 0.2,
          //       color: Color(0xff969696),
          //     )
          //   ],
          // )
          // StackImageCardWrapper(uniqueMapKey: widget.uniqueMapKey),
          // const ShortDivideTile(
          //   title: 'Cрок действия абонемента',
          //   label: '5 меc',
          // ),
          // const ShortDivideTile(
          //   title: 'Цена абонемента',
          //   label: '50 000 тенге',
          // ),
        ],
      ),
    );
  }
}
