import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tube/utils/infinite_listview.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tube/logic/blocs/admin/tariff_admin_bloc/tariff_admin_bloc.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/ui/admin/page/tariff/molecules/edit_tariff_header.dart';
import 'package:tube/ui/admin/page/tariff/molecules/sub_data_tariff.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/shedule_table.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/get_date_time_object_id.dart';

import 'package:tube/utils/load_popup_route.dart';

class EditTariffAdminPage extends StatefulWidget {
  final TariffSingle tariffSingle;

  const EditTariffAdminPage({
    required this.tariffSingle,
    super.key,
  });

  @override
  State<EditTariffAdminPage> createState() => _EditTariffAdminPageState();
}

class _EditTariffAdminPageState extends State<EditTariffAdminPage> {
  late Tariff _tariff;
  DateTime get tarifTime =>
      _tariff.startDate ??
      getDateTimefromObjectIdNullable(_tariff.id) ??
      DateTime.now();

  final currentYear = DateTime.now().year;
  final currentMonth = DateTime.now().month;
  late final InfiniteScrollController infiniteScrollController =
      InfiniteScrollController();
  late final lastIndex = BehaviorSubject.seeded(
      (tarifTime.year - currentYear) * 12 + tarifTime.month - currentMonth);

  // final SheduleTableController sheduleTableController =
  //     SheduleTableController();

  late BehaviorSubject<DateTime> dateTimeSubject =
      BehaviorSubject<DateTime>.seeded(tarifTime);

  TextEditingController countController = TextEditingController();

  int get lastCount {
    final trainTimes = _tariff.trainTimes ?? [];
    final notActive = trainTimes.where(
      (element) {
        return DateTime.now().microsecondsSinceEpoch >
                element.endTime!.microsecondsSinceEpoch ||
            element.come == true;
      },
    );
    final trainCount = _tariff.trainCount ?? 10;
    return trainCount - notActive.length;
  }

  StreamSubscription? streamSub;

  void updateTariff(TariffAdminState newState) {
    if (!mounted) return;
    if (newState is GetTariffAdminState) {
      final newTariff = newState.dirtTariffList.firstWhere((element) {
        return _tariff.id == element.id;
      });
      setState(() {
        _tariff = newTariff;
      });
    }
  }

  @override
  void initState() {
    streamSub = tariffAdminBloc.stream.listen(updateTariff);

    _tariff = widget.tariffSingle;
    countController.text = lastCount.toString();
    super.initState();
  }

  late final TariffAdminBloc tariffAdminBloc =
      Provider.of<AdminBlocMap>(context, listen: false)
          .getTariffAdminBloc(MainDrawerState.clubId!);

  @override
  void dispose() {
    streamSub?.cancel();
    infiniteScrollController.dispose();
    countController.dispose();
    dateTimeSubject.close();
    lastIndex.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final focus = FocusScope.of(context);

        if (focus.hasFocus || focus.hasPrimaryFocus) {
          focus.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Column(
            children: [
              const H4Text('Изменение абонемента'),
              P1Text(_tariff.booking?.name ?? ''),
            ],
          ),
        ),
        body: Stack(
          children: [
            CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 10),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 20,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: H5Text(
                                'Срок действия абонемента',
                                color: ColorData.colortTextSecondary,
                              ),
                            ),
                            const SizedBox(height: 10),
                            DecoratedBox(
                              decoration: BoxDecoration(
                                color: ColorData.color3Percent,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Column(
                                children: [
                                  const SizedBox(height: 10),
                                  EditTariffHeader(
                                    controller: infiniteScrollController,
                                    lastIndex: lastIndex,
                                    currentYear: currentYear,
                                    currentMonth: currentMonth,
                                  ),
                                  const SizedBox(height: 10),
                                  SheduleTable(
                                    padding: const EdgeInsets.all(10),
                                    controller: infiniteScrollController,
                                    lastIndexSubject: lastIndex,
                                    dateTimeSubject: dateTimeSubject,
                                    onTap: (dateTime) {
                                      dateTimeSubject.value = dateTime;
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: CustomTextField(
                          keyboardType: TextInputType.number,
                          title: 'Количество оставшихся занятий',
                          hintText: '5',
                          controller: countController,
                          scrollPadding: const EdgeInsets.only(bottom: 150),
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                        ),
                      ),
                      const SizedBox(height: 30),
                      SubDataTariff(
                        tariff: _tariff,
                        padding: const EdgeInsets.symmetric(
                          horizontal: 20,
                        ),
                      ),
                      const SizedBox(height: 240),
                    ],
                  ),
                )
              ],
            ),
            Positioned(
              bottom: 0,
              right: 0,
              left: 0,
              child: BottomButton(
                title: 'Сохранить',
                onTap: () async {
                  final compl = Completer();
                  Navigator.push(context, LoadPopupRoute(completer: compl));

                  tariffAdminBloc.add(
                    EditSingleTariffAdminEvent(
                      completer: compl,
                      data: (
                        id: _tariff.id!,
                        lastCount: int.parse(countController.text),
                        startDate: dateTimeSubject.value,
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

const p2TextStyle = TextStyle(
  fontSize: P2TextStyle.fontSize,
  fontWeight: P2TextStyle.fontWeight,
  height: P2TextStyle.height,
  color: ColorData.colortTextSecondary,
);
const p1TextStyle = TextStyle(
  fontSize: P1TextStyle.fontSize,
  fontWeight: P1TextStyle.fontWeight,
  height: P1TextStyle.height,
  color: ColorData.colortTextSecondary,
);
const p1TextStyleBold = TextStyle(
  fontSize: P1TextStyle.fontSize,
  fontWeight: FontWeight.w700,
  height: P1TextStyle.height,
  color: ColorData.colortTextSecondary,
);
