import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/admin/workers/worker_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/ui/admin/page/tariff/with_trainer_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/const.dart';

class CreateTariffPage extends StatefulWidget {
  const CreateTariffPage({Key? key}) : super(key: key);

  @override
  State<CreateTariffPage> createState() => _CreateTariffPageState();
}

class _CreateTariffPageState extends State<CreateTariffPage>
    with SingleTickerProviderStateMixin {
  late final TabController tabController =
      TabController(vsync: this, length: 1);

  @override
  void initState() {
    super.initState();

    Provider.of<AdminBlocMap>(context, listen: false)
        .getWorkerBloc(MainDrawerState.currentParticipance!.clubId!)
        .add(const GetWorkWorkerEvent());
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final focusScope = FocusScope.of(context);

        if (focusScope.hasFocus || focusScope.hasPrimaryFocus) {
          focusScope.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: IconButton(
            iconSize: 24,
            color: Colors.white,
            icon: const Icon(
              Icons.arrow_back,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          centerTitle: true,
          title: const H4Text(
            'Тип абонемента',
            color: Colors.white,
          ),
          bottom: TabBar(
            controller: tabController,
            labelStyle: const TextStyle(
              fontWeight: P1TextStyle.fontWeight,
              fontSize: P1TextStyle.fontSize,
              height: P1TextStyle.height,
              fontFamily: "InterTight",
            ),
            indicatorColor: Colors.white,
            indicatorWeight: 5,
            labelPadding: const EdgeInsets.only(
              bottom: 10,
              top: 15,
            ),
            tabs: const [
              Text('С сотрудниками'),
              // Text('Без сотрудников'),
            ],
          ),
        ),
        body: TabBarView(
          physics: const NeverScrollableScrollPhysics(),
          controller: tabController,
          children: const [
            WithTrainerPage(),
            // NoTrainerPage(),
          ],
        ),
      ),
    );
  }
}
