import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/blocs/admin/workers/worker_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/admin/create.booking.model.dart';
import 'package:tube/logic/model/user_worker_time.dart';
import 'package:tube/logic/model/work_time_graph.dart';
import 'package:tube/ui/admin/page/service/money_question_page.dart';
import 'package:tube/ui/admin/page/tariff/molecules/trainer_detail_sheet.dart';
import 'package:tube/ui/admin/page/tariff/molecules/trainer_short_tile.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';

class AddTrainerPage extends StatefulWidget {
  const AddTrainerPage({
    Key? key,
  }) : super(key: key);

  @override
  State<AddTrainerPage> createState() => _AddTrainerPageState();
}

class _AddTrainerPageState extends State<AddTrainerPage> {
  late final WorkerBloc workerBlocMap =
      Provider.of<AdminBlocMap>(context, listen: false)
          .getWorkerBloc(MainDrawerState.currentParticipance!.clubId!);

  bool checkisEmpty(Map<int, List<DateTime>> map) {
    for (final list in map.values) {
      if (list.isNotEmpty) {
        return true;
      }
    }
    return false;
  }

  Map<String, Map<int, List<DateTime>>> getInit() {
    final Map<String, Map<int, List<DateTime>>> returnMap = {};
    late final workTime = Provider.of<CreateBookingModel>(
      context,
      listen: false,
    ).workTime;
    for (final entryU in workTime.entries) {
      final weekdayDates = entryU.value.entries;
      for (final weekEntry in weekdayDates) {
        (returnMap[entryU.key] ??= {})[weekEntry.key] = [...weekEntry.value];
      }
    }
    return returnMap;
  }

  List<MapEntry<int, WorkTimeGraph>> parseTime(
    List<WorkTimeGraph>? workTimeList,
  ) {
    final list = (workTimeList ?? []).map((e) {
      return MapEntry((e.startTime!.weekday + 6) % 7, e);
    }).toList();
    list.sort((a, b) => a.key.compareTo(b.key));
    return list;
  }

  @override
  Widget build(BuildContext context) {
    final createBooking = context.watch<CreateBookingModel>();

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const H4Text(
          'Сотрудники',
          color: Colors.white,
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: CustomScrollView(
              slivers: [
                const SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 40),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: H3Text(
                          'Добавьте сотрудников',
                          color: ColorData.colorTextMain,
                        ),
                      ),
                      SizedBox(height: 5),
                    ],
                  ),
                ),
                BlocBuilder<WorkerBloc, WorkerState>(
                  bloc: workerBlocMap,
                  builder: (_, newState) {
                    if (newState is LoadginWorkerState) {
                      return const SliverToBoxAdapter(
                        child: Center(
                          child: CircularProgressIndicator.adaptive(),
                        ),
                      );
                    }
                    final ClubWorkerState state = newState as ClubWorkerState;

                    return SliverPadding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      sliver: SliverList(
                        delegate: SliverChildBuilderDelegate(
                          (ctx, index) {
                            final worker = state.mapWorkers[index];
                            if (state.workUserMap[worker.id]?.workTime ==
                                null) {
                              return const SizedBox();
                            }
                            final UserWorkerTime newUser =
                                state.workUserMap[worker.id]!;

                            return TrainerShortTile(
                              onTap: () async {
                                final Map<String, Map<int, List<DateTime>>>?
                                    newMap = await showFlexibleBottomSheet(
                                  minHeight: 0,
                                  initHeight: 0.95,
                                  maxHeight: 0.95,
                                  context: context,
                                  anchors: [0.95],
                                  builder: (_, scroll, __) => Material(
                                    color: Colors.transparent,
                                    child: TrainerDetailSheet(
                                      scrollController: scroll,
                                      trainer: worker,
                                      workTime: getInit(),
                                      timeTable: parseTime(newUser.workTime),
                                    ),
                                  ),
                                );
                                if (newMap == null) return;

                                createBooking.setNewWorkTime(newMap);
                              },
                              title: worker.fullName ?? worker.email,
                              photoUrl: worker.photoUrl?.m,
                              label: 'Тренер',
                              active: checkisEmpty(
                                createBooking.workTime[worker.id!] ??= {},
                              ),
                            );
                          },
                          childCount: state.mapWorkers.length,
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          BottomButton(
            onTap: () async {
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => const MoneyQuestionPage(),
                ),
              );
              setState(() {});
            },
          )
        ],
      ),
    );
  }
}
