import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/blocs/admin/workers/worker_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/admin/edit_booking.model.dart';
import 'package:tube/logic/model/user_worker_time.dart';
import 'package:tube/logic/model/work_time_graph.dart';
import 'package:tube/ui/admin/page/tariff/molecules/trainer_detail_sheet.dart';
import 'package:tube/ui/admin/page/tariff/molecules/trainer_short_tile.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';

class EditTrainerPage extends StatefulWidget {
  final String appBarTitle;
  final WorkTimeType? workTime;

  const EditTrainerPage({this.workTime, required this.appBarTitle, super.key});

  @override
  State<EditTrainerPage> createState() => _EditTrainerPageState();
}

class _EditTrainerPageState extends State<EditTrainerPage> {
  late WorkTimeType workTime = widget.workTime ?? {};

  late final WorkerBloc workerBlocMap =
      Provider.of<AdminBlocMap>(context, listen: false)
          .getWorkerBloc(MainDrawerState.clubId!);

  Map<String, Map<int, List<DateTime>>> getInit() {
    final Map<String, Map<int, List<DateTime>>> returnMap = {};

    for (final entryU in workTime.entries) {
      final weekdayDates = entryU.value.entries;
      for (final weekEntry in weekdayDates) {
        (returnMap[entryU.key] ??= {})[weekEntry.key] = [...weekEntry.value];
      }
    }
    return returnMap;
  }

  List<MapEntry<int, WorkTimeGraph>> parseTime(
    List<WorkTimeGraph>? workTimeList,
  ) {
    final list = (workTimeList ?? []).map((e) {
      return MapEntry((e.startTime!.weekday + 6) % 7, e);
    }).toList();
    list.sort((a, b) => a.key.compareTo(b.key));
    return list;
  }

  bool checkisEmpty(Map<int, List<DateTime>> map) {
    for (final list in map.values) {
      if (list.isNotEmpty) {
        return true;
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: SizeTapAnimation(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Icon(Icons.arrow_back_ios),
        ),
        centerTitle: true,
        title: Column(
          children: [
            const H4Text(
              'Сотрудники',
              color: Colors.white,
            ),
            const SizedBox(height: 2),
            P1Text(
              widget.appBarTitle,
              color: Colors.white,
            ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Expanded(
            child: CustomScrollView(
              slivers: [
                const SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 40),
                    ],
                  ),
                ),
                BlocBuilder<WorkerBloc, WorkerState>(
                  bloc: workerBlocMap,
                  builder: (_, realState) {
                    if (realState is LoadginWorkerState) {
                      return const SliverToBoxAdapter(
                        child: Center(
                          child: CircularProgressIndicator.adaptive(),
                        ),
                      );
                    }
                    final ClubWorkerState state = realState as ClubWorkerState;

                    return SliverPadding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      sliver: SliverList(
                        delegate: SliverChildBuilderDelegate(
                          (ctx, index) {
                            final worker = state.mapWorkers[index];
                            if (state.workUserMap[worker.id]?.workTime ==
                                null) {
                              return const SizedBox();
                            }
                            final UserWorkerTime newUser =
                                state.workUserMap[worker.id]!;

                            return TrainerShortTile(
                              onTap: () async {
                                final Map<String, Map<int, List<DateTime>>>?
                                    newMap = await showFlexibleBottomSheet(
                                  minHeight: 0,
                                  initHeight: 0.95,
                                  maxHeight: 0.95,
                                  context: context,
                                  anchors: [0.95],
                                  builder: (_, scroll, __) => Material(
                                    color: Colors.transparent,
                                    child: TrainerDetailSheet(
                                      scrollController: scroll,
                                      trainer: worker,
                                      workTime: getInit(),
                                      timeTable: parseTime(newUser.workTime),
                                    ),
                                  ),
                                );
                                if (newMap == null) return;
                                setState(() {
                                  workTime = newMap;
                                });

                                // createBooking.setNewWorkTime(newMap);
                              },
                              title: worker.fullName ?? worker.email,
                              photoUrl: worker.photoUrl?.m,
                              label: 'Тренер',
                              active: checkisEmpty(
                                workTime[worker.id!] ??= {},
                              ),
                            );
                          },
                          childCount: state.mapWorkers.length,
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          BottomButton(
            onTap: () async {
              Navigator.pop(context, workTime);
            },
          )
        ],
      ),
    );
  }
}
