import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tube/logic/model/admin/edit_booking.model.dart';
import 'package:tube/ui/admin/atoms/custom_check_box.dart';
import 'package:tube/ui/admin/page/tariff/edits/edit_trainer_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/duration_expire.dart';
import 'package:tube/ui/common/molecules/expire_days.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class EditInfoDurationPage extends StatefulWidget {
  final int deadlineDays;
  final String name;
  final int count;
  final int timePerMinute;
  final int studentNumber;

  const EditInfoDurationPage({
    required this.deadlineDays,
    required this.name,
    required this.count,
    required this.timePerMinute,
    required this.studentNumber,
    super.key,
  });

  @override
  State<EditInfoDurationPage> createState() => _EditInfoDurationPageState();
}

class _EditInfoDurationPageState extends State<EditInfoDurationPage> {
  int get deadlineDays => widget.deadlineDays;
  String get name => widget.name;
  int get count => widget.count;

  bool oldUsers = false;

  late final yearSubject =
      BehaviorSubject<int>.seeded((widget.deadlineDays / 365).floor())
        ..listen((value) => str.add(null));
  late final monthSubject = BehaviorSubject<int>.seeded(
      ((widget.deadlineDays - 365 * yearSubject.value) / 30).floor())
    ..listen((value) {
      str.add(null);
    });
  late final daySubject = BehaviorSubject<int>.seeded(
      widget.deadlineDays - 365 * yearSubject.value - monthSubject.value * 30)
    ..listen((value) => str.add(null));

  late final hourSubject =
      BehaviorSubject<int>.seeded(widget.timePerMinute ~/ 60)
        ..listen((value) => str.add(null));
  late final minSubject = BehaviorSubject<int>.seeded(widget.timePerMinute % 60)
    ..listen((value) => str.add(null));

  late final countController = TextEditingController(text: "${widget.count}");
  late final studentController =
      TextEditingController(text: "${widget.studentNumber}");

  int get curDeadlineDays =>
      yearSubject.value * 365 + monthSubject.value * 30 + daySubject.value;
  int get curCount =>
      countController.text.isEmpty ? 10 : int.parse(countController.text);
  int get curStudentNumber =>
      studentController.text.isEmpty ? 10 : int.parse(studentController.text);
  int get curDuraton => hourSubject.value * 60 + minSubject.value;

  final str = StreamController();
  bool isActive() {
    return widget.deadlineDays != curDeadlineDays ||
        widget.count != curCount ||
        curDuraton != widget.timePerMinute ||
        curStudentNumber != widget.studentNumber ||
        oldUsers;
  }

  @override
  void dispose() {
    str.close();
    yearSubject.close();
    monthSubject.close();
    daySubject.close();
    hourSubject.close();
    minSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);

        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: SizeTapAnimation(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Icon(Icons.arrow_back_ios),
          ),
          centerTitle: true,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const H4Text(
                'Основное',
                color: Colors.white,
              ),
              P1Text(
                name,
                color: Colors.white,
              )
            ],
          ),
        ),
        body: Stack(
          children: [
            CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Column(
                    children: [
                      const SizedBox(height: 40),
                      SizeTapAnimation(
                        onTap: () {
                          setState(() {
                            oldUsers = !oldUsers;
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Row(
                            children: [
                              CustomCheckBox(
                                isCircle: true,
                                active: oldUsers,
                                margin: const EdgeInsets.only(right: 10),
                              ),
                              const Expanded(
                                child: P1Text(
                                  'Распространять изменения для уже купивших пользователей',
                                  color: ColorData.colorTextMain,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                    ],
                  ),
                ),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        ExpireDays(
                          yearSubject: yearSubject,
                          monthSubject: monthSubject,
                          daySubject: daySubject,
                        ),
                        const SizedBox(height: 30),
                        CustomTextField(
                          controller: countController,
                          hintText: '10',
                          title: 'Количество занятий в течение срока действия',
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          keyboardType: TextInputType.number,
                        ),
                        const SizedBox(height: 20),
                        CustomTextField(
                          controller: studentController,
                          hintText: '10',
                          title: 'Количество людей в группе',
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          keyboardType: TextInputType.number,
                        ),
                        const SizedBox(height: 20),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                            border: Border.all(
                              color: ColorData.color5Percent,
                              width: 1,
                            ),
                          ),
                          padding: const EdgeInsets.all(15),
                          child: const Row(
                            children: [
                              Icon(
                                Icons.warning_amber,
                                color: ColorData.colorElementsActive,
                                size: 24,
                              ),
                              SizedBox(width: 10),
                              Expanded(
                                child: P1Text(
                                  'Если клиент не успеет отходить нужное количество занятий за срок действия абонемента —  неиспользованные занятия сгорают',
                                  color: ColorData.colorTextMain,
                                ),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(height: 20),
                        DurationExpire(
                          hourSubject: hourSubject,
                          minSubject: minSubject,
                        ),
                        const SizedBox(height: 240),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: StreamBuilder(
                  stream: str.stream,
                  builder: (context, _) {
                    return BottomButton(
                      active: isActive(),
                      title: 'Сохранить',
                      onTap: () async {
                        final eModel = context.read<EditBookingModel>();
                        try {
                          eModel.concernsOld = oldUsers;
                          eModel.count = curCount;
                          eModel.timePerMinute = curDuraton;
                          eModel.deadlineDays = curDeadlineDays;
                          eModel.studentNumber = curStudentNumber;
                          if (curDuraton != widget.timePerMinute) {
                            final workTime = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => EditTrainerPage(
                                  appBarTitle: widget.name,
                                ),
                              ),
                            );
                            if (workTime == null) throw "Empty trainer page";

                            eModel.workTime = workTime;
                          }
                          if (!mounted) return;
                          Navigator.pop(context);
                        } catch (e) {
                          eModel.count = null;
                          eModel.timePerMinute = widget.timePerMinute;
                          eModel.deadlineDays = null;
                          eModel.studentNumber = null;
                        }
                      },
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
