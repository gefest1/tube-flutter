import 'package:flutter/material.dart';

import 'package:tube/ui/admin/page/tariff/edits/atoms/title_label.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class EditPricesCard extends StatelessWidget {
  final num price;
  final num trainerPrice;
  final VoidCallback? onTap;

  const EditPricesCard({
    required this.price,
    required this.trainerPrice,
    this.onTap,
    super.key,
  });

  Widget get div =>
      const Divider(height: 0, thickness: 0.5, color: ColorData.color5Percent);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: ColorData.color3Percent,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      padding: const EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              const Expanded(
                child: H4Text(
                  'Цена',
                  color: ColorData.colorTextMain,
                ),
              ),
              SizeTapAnimation(
                onTap: onTap,
                child: const P2Text(
                  'Изменить',
                  color: ColorData.colorMainLink,
                ),
              )
            ],
          ),
          TitleLabel(
            title: 'Цена абонемента',
            label: "$price тенге",
          ),
          div,
          TitleLabel(
            title: 'Процент сотрудника',
            label: "$trainerPrice тг.",
          ),
        ],
      ),
    );
  }
}
