import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tube/logic/blocs/admin/booking_admin/booking_admin_bloc.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/ui/admin/page/select_deadline_page.dart';
import 'package:tube/ui/admin/page/tariff/edits/atoms/active_pause_button.dart';
import 'package:tube/ui/admin/page/tariff/edits/atoms/stopped_pause_button.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/load_popup_route.dart';

class EditPauseCard extends StatefulWidget {
  final Booking booking;

  final BookingAdminBloc bloc;

  const EditPauseCard({
    required this.bloc,
    required this.booking,
    super.key,
  });

  @override
  State<EditPauseCard> createState() => _EditPauseCardState();
}

class _EditPauseCardState extends State<EditPauseCard> {
  final pauseGLobalKey = GlobalKey();

  bool get isActive =>
      widget.booking.stopDate == null ||
      DateTime.now().millisecondsSinceEpoch >
          widget.booking.stopDate!.millisecondsSinceEpoch;

  Future resume() async {
    final comleter = Completer();
    Navigator.push(context, LoadPopupRoute(completer: comleter));

    widget.bloc.add(
      ResumeBookingDetailDataEvent(
        completer: comleter,
        bookingId: widget.booking.id!,
      ),
    );
    await comleter.future;
  }

  Future stop() async {
    final comleter = Completer();
    Navigator.push(context, LoadPopupRoute(completer: comleter));

    widget.bloc.add(
      StopBookingDetailDataEvent(
        completer: comleter,
        bookingId: widget.booking.id!,
        date: null,
      ),
    );
    await comleter.future;
  }

  (
    MediaQueryData mediaQuery,
    Offset offset,
  ) getRenderData() {
    final mediaQuery = MediaQuery.of(context);
    final renderObj =
        pauseGLobalKey.currentContext!.findRenderObject()! as RenderBox;
    final offset = renderObj.localToGlobal(Offset.zero);

    return (
      mediaQuery,
      offset,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      key: pauseGLobalKey,
      onTap: () async {
        final setDateTime = await Navigator.push<({DateTime? date})>(
          context,
          MaterialPageRoute(
            builder: (context) => SelectDeadlinePage(
              title: widget.booking.name ?? '',
            ),
          ),
        );
        if (setDateTime == null) return;

        final comleter = Completer();
        widget.bloc.add(
          StopBookingDetailDataEvent(
            completer: comleter,
            bookingId: widget.booking.id!,
            date: setDateTime.date,
          ),
        );
        if (!mounted) return;
        Navigator.push(context, LoadPopupRoute(completer: comleter));
      },
      child: isActive
          ? const ActivePauseButton()
          : StoppedPauseButton(
              date: widget.booking.stopDate!,
            ),
    );
  }
}

class PauseButton extends StatelessWidget {
  final String title;
  const PauseButton({
    required this.title,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: P1Text(
        title,
        color: Colors.black,
      ),
    );
  }
}
