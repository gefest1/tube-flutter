import 'dart:async';
import 'package:flutter/material.dart';
import 'package:tube/logic/blocs/admin/booking_admin/models/edit_description.model.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';

class NameDescriptionEditPage extends StatefulWidget {
  final String name;
  final String description;

  const NameDescriptionEditPage({
    required this.name,
    required this.description,
    super.key,
  });

  @override
  State<NameDescriptionEditPage> createState() =>
      _NameDescriptionEditPageState();
}

class _NameDescriptionEditPageState extends State<NameDescriptionEditPage> {
  late final nameController = TextEditingController(text: widget.name)
    ..addListener(_updt);
  late final descController = TextEditingController(text: widget.description)
    ..addListener(_updt);
  final strController = StreamController();

  void _updt() => strController.add(null);

  @override
  void dispose() {
    nameController.dispose();
    descController.dispose();
    strController.close();
    super.dispose();
  }

  bool isValid() {
    if (nameController.text.isEmpty) return false;
    return nameController.text != widget.name ||
        descController.text != widget.description;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);

        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: const H4Text(
            'Об абонементе',
            color: Colors.white,
          ),
        ),
        body: Stack(
          children: [
            ListView(
              padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
              children: [
                CustomTextField(
                  hintText: 'Например: «Детский»',
                  title: 'Название абонемента',
                  controller: nameController,
                ),
                const SizedBox(height: 30),
                CustomTextField(
                  hintText: 'Опишите абонемент',
                  title:
                      'Описание. Поможет вашим клиентам выбрать именно то, что им нужно',
                  controller: descController,
                ),
              ],
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: StreamBuilder(
                stream: strController.stream,
                builder: (ctx, _) {
                  return BottomButton(
                    active: isValid(),
                    onTap: () async {
                      Navigator.pop(
                        context,
                        EditDescription(
                          name: nameController.text,
                          description: descController.text,
                        ),
                      );
                    },
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
