import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'dart:math' as math;

import 'package:qr_flutter/qr_flutter.dart';
import 'package:tube/logic/blocs/admin/booking_detail_data_bloc/booking_detail_data_bloc.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class ObserveQRBody extends StatelessWidget {
  final BookingDetailDataBloc bloc;
  const ObserveQRBody({
    required this.bloc,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: LayoutBuilder(
        builder: (context, constraints) {
          final qrWidth = math.min(constraints.maxWidth / 2, 300.0);
          return Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 10),
                child: QrImageView(
                  data: 'uriLink'.toString(),
                  version: QrVersions.auto,
                  padding: EdgeInsets.zero,
                  size: qrWidth,
                ),
              ),
              Expanded(
                child:
                    BlocBuilder<BookingDetailDataBloc, BookingDetailDataState>(
                  bloc: bloc,
                  builder: (context, state) {
                    if (state is! DataBookingDetailDataState) {
                      return const SizedBox();
                    }
                    return Container(
                      height: qrWidth,
                      padding: const EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 15,
                      ),
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Color(0x0d000000),
                            offset: Offset(0, 5),
                            blurRadius: 20,
                          ),
                          BoxShadow(
                            color: Color(0x03000000),
                            offset: Offset(3, 0),
                            blurRadius: 2,
                          ),
                        ],
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          H1Text(
                            state.activeTariffs.length.toString(),
                            color: ColorData.colorTextMain,
                          ),
                          const SizedBox(height: 15),
                          const Row(
                            children: [
                              Expanded(
                                child: P1Text(
                                  'Записей ',
                                  // '23.04.2022',
                                  color: ColorData.colorElementsSecondary,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
