import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tube/logic/blocs/admin/booking_admin/models/edit_price.model.dart';
import 'package:tube/ui/admin/atoms/custom_check_box.dart';
import 'package:tube/ui/admin/page/service/money_question_page.dart';
import 'package:tube/ui/common/atoms/animated_close.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class EditPricePage extends StatefulWidget {
  final num price;
  final num percent;
  final String? trialPrice;

  const EditPricePage({
    required this.price,
    required this.percent,
    this.trialPrice,
    super.key,
  });

  @override
  State<EditPricePage> createState() => _EditPricePageState();
}

class _EditPricePageState extends State<EditPricePage>
    with MoneyQuestionExtension {
  @override
  late final TextEditingController costController =
      TextEditingController(text: widget.price.toString())
        ..addListener(() => update(TypeChange.cost));
  @override
  late final TextEditingController percentController =
      TextEditingController(text: widget.percent.toString())
        ..addListener(() => update(TypeChange.percent));
  @override
  late final TextEditingController percentPriceController =
      TextEditingController(
          text: (widget.price * widget.percent / 100).toString())
        ..addListener(() => update(TypeChange.percentPrice));
  @override
  bool get check =>
      percentController.text.isNotEmpty &&
      costController.text.isNotEmpty &&
      (widget.percent != num.parse(percentController.text) ||
          widget.price != num.parse(costController.text));
  @override
  final streamController = StreamController();

  late bool trial = widget.trialPrice != null;
  late final trialController = TextEditingController(text: widget.trialPrice)
    ..addListener(() {
      streamController.add(null);
    });

  bool get trialActive {
    if (trial) {
      if (trialController.text.isEmpty) return false;
      return trialController.text != widget.trialPrice;
    } else {
      return widget.trialPrice != null;
    }
  }

  @override
  void dispose() {
    costController.dispose();
    percentController.dispose();
    percentPriceController.dispose();
    trialController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);
        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const H4Text(
            'Цена',
            color: Colors.white,
          ),
          centerTitle: true,
        ),
        body: Stack(
          children: [
            CustomScrollView(
              slivers: [
                SliverPadding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  sliver: SliverToBoxAdapter(
                    child: Column(
                      children: [
                        const SizedBox(height: 20),
                        Row(
                          children: [
                            Expanded(
                              child: CustomTextField(
                                hintText: '10',
                                title: 'Стоимость абонемента',
                                controller: costController,
                                keyboardType: TextInputType.number,
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(
                                    RegExp(r'[+-]?([0-9]*[.])?[0-9]+'),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Container(
                                width: 105,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 13),
                                decoration: const BoxDecoration(
                                  color: Color(0xffF7F7F7),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(10),
                                  ),
                                ),
                                child: const P1Text(
                                  'Тенге',
                                  color: ColorData.colorElementsActive,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 20),
                        Container(
                          padding: const EdgeInsets.all(15),
                          decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                            border: Border.all(
                              color: const Color(0xffA5A5A5),
                              width: 0.5,
                            ),
                          ),
                          child: const Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(right: 10),
                                child: Icon(
                                  Icons.warning_amber,
                                  color: ColorData.colorElementsActive,
                                ),
                              ),
                              Flexible(
                                child: P1Text(
                                  'Комиссия Checkallnow. при каждой покупке ваших продуктов = 3.9%',
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 20),
                        SizeTapAnimation(
                          onTap: () {
                            setState(() {
                              trial = !trial;
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                CustomCheckBox(
                                  isCircle: true,
                                  active: trial,
                                ),
                                const SizedBox(width: 14),
                                const P1Text(
                                  'Пробное занятие',
                                  color: ColorData.colorMainElements,
                                ),
                              ],
                            ),
                          ),
                        ),
                        // const SizedBox(height: 10),
                        AnimatedFastClose(
                          active: trial,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 20),
                            child: CustomTextField(
                              hintText: '10 тенге',
                              controller: trialController,
                              title: 'Стоимость пробного занятия',
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly,
                              ],
                            ),
                          ),
                        ),
                        // const SizedBox(height: 10),
                        Row(
                          children: [
                            Expanded(
                              child: TextField(
                                keyboardType: TextInputType.number,
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(
                                    RegExp(r'^[1-9][0-9]?$|^100$'),
                                  ),
                                ],
                                cursorColor: Colors.black,
                                style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: P0TextStyle.fontSize,
                                  height: P0TextStyle.height,
                                  fontWeight: P0TextStyle.fontWeight,
                                ),
                                textAlign: TextAlign.center,
                                controller: percentController,
                                decoration: percentInputDecoration,
                              ),
                            ),
                            const SizedBox(width: 10),
                            Expanded(
                              child: TextField(
                                controller: percentPriceController,
                                keyboardType: TextInputType.number,
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(
                                    RegExp(r'[+-]?([0-9]*[.])?[0-9]+'),
                                  ),
                                ],
                                cursorColor: Colors.black,
                                style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: P0TextStyle.fontSize,
                                  height: P0TextStyle.height,
                                  fontWeight: P0TextStyle.fontWeight,
                                ),
                                textAlign: TextAlign.center,
                                decoration: pricePercentInputDecoration,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 10),
                        const Align(
                          alignment: Alignment.centerLeft,
                          child: P1Text(
                            'Сколько процентов получает сотрудник с абонимента',
                            color: ColorData.colorElementsActive,
                          ),
                        ),
                        const SizedBox(height: 20),
                        Container(
                          padding: const EdgeInsets.all(15),
                          decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                            border: Border.all(
                              color: const Color(0xffA5A5A5),
                              width: 0.5,
                            ),
                          ),
                          child: const Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(right: 10),
                                child: Icon(
                                  Icons.warning_amber,
                                  color: ColorData.colorElementsActive,
                                ),
                              ),
                              Flexible(
                                child: P1Text(
                                  'Сумма зарплаты сотрудника зависит от проведенных им занятий по абонементу',
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: StreamBuilder(
                stream: streamController.stream,
                builder: (context, _) {
                  return BottomButton(
                    title: 'Сохранить',
                    active: check || trialActive,
                    onTap: () {
                      Navigator.pop(
                        context,
                        EditPrice(
                          price: double.parse(costController.text),
                          percent: double.parse(percentController.text),
                          trialPrice:
                              trial ? double.parse(trialController.text) : null,
                        ),
                      );
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
