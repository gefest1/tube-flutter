import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/admin/booking_admin/booking_admin_bloc.dart';
import 'package:tube/logic/blocs/admin/booking_admin/models/edit_schedule.model.dart';
import 'package:tube/logic/model/admin/edit_booking.model.dart';
import 'package:tube/ui/admin/page/tariff/edits/edit_trainer_page.dart';
import 'package:tube/ui/admin/page/tariff/observe_tariff_page.dart';
import 'package:tube/utils/load_popup_route.dart';
import 'package:tube/utils/nested_logic_route.dart';

mixin EditScheduleMixin on ObserveTariffPageStateAbstract {
  @override
  void editSchedule() async {
    final editModel = EditBookingModel(
      timePerMinute: booking.timePerMinute?.toInt() ?? 0,
    );
    final WorkTimeType workTime = {};
    for (var sinWork in booking.workTimes!.entries) {
      for (var workTimeObj in sinWork.value) {
        ((workTime[sinWork.key] ??= {})[(workTimeObj.date!.weekday + 6) % 7] ??=
                [])
            .add(workTimeObj.date!);
      }
    }

    editModel.routes.add(
      MaterialPageRoute(
        builder: (context) {
          return MultiProvider(
            providers: [
              ChangeNotifierProvider(
                create: (context) => editModel,
              )
            ],
            child: NestedLogicRoute(
              child: EditTrainerPage(
                appBarTitle: booking.name ?? '',
                workTime: workTime,
              ),
            ),
          );
        },
      ),
    );

    final val = await Navigator.push(context, editModel.routes.first);
    if (val == null) return;

    final c = Completer();
    if (!mounted) return;
    Navigator.push(context, LoadPopupRoute(completer: c));
    bookingAdminBloc.add(EditBookingEvent(
      bookingId: widget.booking.id!,
      completer: c,
      editSchedule: EditSchedule(workTime: val),
    ));
    await c.future;
  }
}
