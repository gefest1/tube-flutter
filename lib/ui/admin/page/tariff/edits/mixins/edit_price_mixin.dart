import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tube/logic/blocs/admin/booking_admin/booking_admin_bloc.dart';
import 'package:tube/logic/blocs/admin/booking_admin/models/edit_price.model.dart';
import 'package:tube/logic/model/sub_booking.model.dart';
import 'package:tube/ui/admin/page/tariff/edits/edit_price_page.dart';
import 'package:tube/ui/admin/page/tariff/observe_tariff_page.dart';
import 'package:tube/utils/load_popup_route.dart';

mixin EditPriceMixin on ObserveTariffPageStateAbstract {
  @override
  void editPrice() async {
    final val = await Navigator.push<EditPrice>(
      context,
      MaterialPageRoute(
        builder: (context) => EditPricePage(
          percent: booking.percent ?? 0,
          price: booking.price ?? 0,
          trialPrice: booking.subBooking.isEmpty
              ? null
              : booking.subBooking.first.price.toString(),
        ),
      ),
    );
    if (val == null) return;

    final c = Completer();
    if (!mounted) return;
    Navigator.push(
      context,
      LoadPopupRoute(completer: c),
    );
    bookingAdminBloc.add(
      EditBookingEvent(
        bookingId: widget.booking.id!,
        completer: c,
        editPrice: val,
        subBookings: [
          if (val.trialPrice != null)
            SubBooking(
              price: val.trialPrice!,
              count: 1,
              deadlineDays: 30,
            )
        ],
      ),
    );
    await c.future;
    return;
  }
}
