import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tube/logic/blocs/admin/booking_admin/booking_admin_bloc.dart';
import 'package:tube/logic/blocs/admin/booking_admin/models/edit_description.model.dart';
import 'package:tube/ui/admin/page/tariff/edits/name_description_edit_page.dart';
import 'package:tube/ui/admin/page/tariff/observe_tariff_page.dart';
import 'package:tube/utils/load_popup_route.dart';

mixin EditNameMixin on ObserveTariffPageStateAbstract {
  @override
  void editName() async {
    final val = await Navigator.push<EditDescription>(
      context,
      MaterialPageRoute(
        builder: (_) => NameDescriptionEditPage(
          name: booking.name ?? '',
          description: booking.description ?? '',
        ),
      ),
    );
    if (val == null) return;
    final c = Completer();
    if (!mounted) return;
    Navigator.push(context, LoadPopupRoute(completer: c));
    bookingAdminBloc.add(
      EditBookingEvent(
        bookingId: widget.booking.id!,
        completer: c,
        editDescription: val,
      ),
    );
    await c.future;
    return;
  }
}
