import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tube/logic/blocs/admin/booking_admin/models/edit_tags.model.dart';
import 'package:tube/ui/admin/page/tariff/pages/change_set_tag_page.dart';
import 'package:tube/ui/common/atoms/active_tag.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/shadow.dart';

class EditTagsPage extends StatefulWidget {
  final List<String> tags;
  const EditTagsPage({
    required this.tags,
    super.key,
  });

  @override
  State<EditTagsPage> createState() => _EditTagsPageState();
}

class _EditTagsPageState extends State<EditTagsPage> {
  Map<String, bool> mapVal = {};
  Map<String, bool> initialMap = {};

  @override
  void initState() {
    initialMap = Map.fromEntries(
      widget.tags.map(
        (e) => MapEntry(e, true),
      ),
    );
    mapVal = {...initialMap};
    super.initState();
  }

  bool isChanged() {
    return mapEquals(
      initialMap,
      Map.fromEntries(
        mapVal.entries.where((element) => element.value),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const H4Text(
          'Атрибуты',
          color: Colors.white,
        ),
      ),
      body: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Column(
                  children: [
                    const SizedBox(height: 40),
                    StreamBuilder(
                      stream: MainDrawerState.currentParticipanceValue,
                      builder: (context, snapshot) {
                        final tags = MainDrawerState
                                .currentParticipanceValue.value?.club?.tags ??
                            <String>[];
                        mapVal = Map.fromEntries(
                          tags.map(
                            (e) => MapEntry(e, mapVal[e] ?? false),
                          ),
                        );

                        if (tags.isEmpty) return const SizedBox();
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 40),
                          child: Wrap(
                            alignment: WrapAlignment.center,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            spacing: 10,
                            runSpacing: 10,
                            children: tags
                                .map(
                                  (e) => ActiveTag(
                                      title: e,
                                      active: mapVal[e] ?? false,
                                      onTap: () {
                                        setState(() {
                                          mapVal[e] = !(mapVal[e] ?? false);
                                        });
                                      }),
                                )
                                .toList(),
                          ),
                        );
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40),
                      child: CustomButton(
                        title: 'Настроить атрибуты',
                        titleColor: ColorData.colorMainLink,
                        color: Colors.white,
                        boxShadow: blocksMains,
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const ChangeSetTagPage(),
                              ));
                        },
                      ),
                    ),
                    const SizedBox(height: 100),
                  ],
                ),
              ),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: BottomButton(
              title: 'Продолжить',
              active: !isChanged(),
              onTap: () async {
                Navigator.pop(
                  context,
                  EditTags(
                    tags: mapVal.entries
                        .where((element) => element.value)
                        .map((e) => e.key)
                        .toList(),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
