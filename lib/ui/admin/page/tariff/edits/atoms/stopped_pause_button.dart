import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_time_shift.dart';

class StoppedPauseButton extends StatelessWidget {
  final DateTime date;

  const StoppedPauseButton({required this.date, super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: ColorData.color3Percent,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.symmetric(
        vertical: 9,
        horizontal: 20,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(
            Icons.pause_circle_filled,
            size: 24,
            color: ColorData.colorMainNo,
          ),
          const SizedBox(width: 5),
          P1Text(
            'Заморожен ${DateFormat('dd.MM.yyyy').format(date.parseTimeGG)}',
            color: ColorData.colorMainNo,
          )
        ],
      ),
    );
  }
}
