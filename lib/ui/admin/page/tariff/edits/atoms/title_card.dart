import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/shadow.dart';

class TitleCard extends StatelessWidget {
  final String label;
  final String title;
  final VoidCallback? onTap;

  const TitleCard({
    required this.label,
    required this.title,
    this.onTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: blocksMains,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            P1Text(
              label,
              color: ColorData.colorMainBlack,
            ),
            const SizedBox(height: 15),
            Row(
              children: [
                Expanded(
                    child: H3Text(
                  title,
                  color: ColorData.colorTextMain,
                )),
                const SizedBox(width: 10),
                const Icon(
                  Icons.navigate_next,
                  color: ColorData.colorTextMain,
                  size: 32,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
