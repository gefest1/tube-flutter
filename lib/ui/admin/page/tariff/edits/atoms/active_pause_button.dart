import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class ActivePauseButton extends StatelessWidget {
  const ActivePauseButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: ColorData.color3Percent,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.symmetric(
        vertical: 9,
        horizontal: 20,
      ),
      child: const Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.play_circle_filled,
            size: 24,
            color: ColorData.colorMainLink,
          ),
          SizedBox(width: 5),
          P1Text(
            'Активен',
            color: ColorData.colorMainLink,
          )
        ],
      ),
    );
  }
}
