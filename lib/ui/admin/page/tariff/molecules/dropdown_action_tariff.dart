import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/shadow.dart';

class DropDownActionTariff extends StatefulWidget {
  final ({
    void Function()? onEdit,
    void Function()? onRemove,
    void Function()? onResume,
  })? onTap;

  const DropDownActionTariff({this.onTap, super.key});

  @override
  State<DropDownActionTariff> createState() => _DropDownActionTariffState();
}

class _DropDownActionTariffState extends State<DropDownActionTariff> {
  Widget getItem(String txt, void Function()? onTap) {
    return SizedBox(
      width: double.infinity,
      child: SizeTapAnimation(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: P1Text(
            txt,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  Widget get divider => const Divider(
        indent: 15,
        endIndent: 15,
        color: ColorData.color5Percent,
        height: 0,
        thickness: 0.2,
      );

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: const BoxDecoration(boxShadow: blocksMains),
      child: Material(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
        child: IntrinsicWidth(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              getItem(
                'Изменить',
                () {
                  Navigator.pop(context);
                  widget.onTap?.onEdit?.call();
                },
              ),
              divider,
              getItem(
                'Удалить',
                () {
                  Navigator.pop(context);
                  widget.onTap?.onRemove?.call();
                },
              ),
              // divider,
              // getItem(
              //   'Возобновить',
              //   () {
              //     Navigator.pop(context);
              //     widget.onTap?.onResume?.call();
              //   },
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
