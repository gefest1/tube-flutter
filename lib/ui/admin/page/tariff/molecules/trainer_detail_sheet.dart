import 'dart:developer';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/work_time_graph.dart';
import 'package:tube/ui/admin/page/tariff/pages/set_time_trainer_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/main_page/components/atoms/day_card.dart';
import 'package:tube/ui/main_page/components/organisms/active_time_card.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_next_week_date.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:tube/utils/week_day.dart';

class DefaultEqualityTime<E> implements Equality<E> {
  const DefaultEqualityTime();
  @override
  bool equals(Object? e1, Object? e2) {
    if (e1 is DateTime && e2 is DateTime) {
      return e1.isAtSameMomentAs(e2);
    }
    return e1 == e2;
  }

  @override
  int hash(Object? e) => e.hashCode;
  @override
  bool isValidKey(Object? o) => true;
}

class TrainerDetailSheet extends StatefulWidget {
  final ScrollController scrollController;
  final User trainer;
  final Map<String, Map<int, List<DateTime>>> workTime;
  final List<MapEntry<int, WorkTimeGraph>> timeTable;

  const TrainerDetailSheet({
    required this.scrollController,
    required this.timeTable,
    required this.workTime,
    required this.trainer,
    Key? key,
  }) : super(key: key);

  @override
  State<TrainerDetailSheet> createState() => _TrainerDetailSheetState();
}

class _TrainerDetailSheetState extends State<TrainerDetailSheet> {
  String get trainerId => widget.trainer.id!;
  late final timeTableMap = Map.fromEntries(widget.timeTable);
  late final BehaviorSubject<Map<String, Map<int, List<DateTime>>>> workTimeSubject =
      BehaviorSubject.seeded(widget.workTime);
  bool isActive(int weekDay) {
    final list = (workTime[trainerId] ??= {})[weekDay] ??= [];
    return list.isNotEmpty;
  }

  Map<String, Map<int, List<DateTime>>> get workTime => workTimeSubject.value;
  set workTime(Map<String, Map<int, List<DateTime>>> val) {
    workTimeSubject.value = val;
    setState(() {});
  }

  @override
  void dispose() {
    workTimeSubject.close();
    super.dispose();
  }

  void setTime(
    int weekDay,
  ) async {
    final List<DateTime>? result = await showFlexibleBottomSheet(
      minHeight: 0,
      initHeight: 0.95,
      maxHeight: 0.95,
      context: context,
      anchors: [0.95],
      builder: (_, scroll, __) => Material(
        color: Colors.transparent,
        child: SetTimeTrainerPage(
          scrollController: scroll,
          dateTimes: [...((workTime[trainerId] ??= {})[weekDay] ??= [])],
          timeTable: timeTableMap[weekDay]!,
          trainerId: trainerId,
          weekday: weekDay,
          trainerTitle: widget.trainer.fullName ?? widget.trainer.email ?? '',
        ),
      ),
    );
    if (result == null) return;
    final newMap = {...workTime};
    final newUserDetail = {...(newMap[trainerId] ?? {})};
    newMap[trainerId] = newUserDetail;
    newUserDetail[weekDay] = result;

    final isSame = const DeepCollectionEquality(DefaultEqualityTime()).equals(workTime, newMap);
    if (isSame) return;
    log((widget.workTime == workTime).toString());

    workTime = newMap;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(10),
        ),
      ),
      child: Stack(
        children: [
          CustomScrollView(
            controller: widget.scrollController,
            slivers: [
              SliverToBoxAdapter(
                child: DecoratedBox(
                  decoration: const BoxDecoration(color: Color(0xffFCFCFC)),
                  child: Stack(
                    children: [
                      SizeTapAnimation(
                        child: GestureDetector(
                          onTap: () => Navigator.pop(context),
                          behavior: HitTestBehavior.opaque,
                          child: const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 14, vertical: 23),
                            child: Icon(
                              Icons.arrow_back,
                              color: ColorData.colorGrayscaleOffBlack,
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 14),
                          child: Column(
                            children: [
                              if (widget.trainer.fullName != null || widget.trainer.email != null)
                                H4Text(
                                  widget.trainer.fullName ?? widget.trainer.email!,
                                  color: ColorData.colorTextMain,
                                ),
                              const SizedBox(height: 2),
                              const P1Text(
                                'Расписание сотрудника',
                                color: ColorData.colorTextMain,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              if (widget.timeTable.isNotEmpty)
                SliverPadding(
                  padding: const EdgeInsets.all(20),
                  sliver: SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        final itemIndex = index ~/ 2;
                        if (!index.isEven) return const SizedBox(height: 10);
                        final currentTime = widget.timeTable[itemIndex];

                        if (isActive(currentTime.key)) {
                          final txt = List<String>.generate(
                            workTime[trainerId]![currentTime.key]!.length * 2 - 1,
                            (int index) => index.isOdd
                                ? ' | '
                                : DateFormat('HH:mm').format(
                                    dateTimeOnNextWeek(
                                      workTime[trainerId]![currentTime.key]![index ~/ 2],
                                    ).parseTimeGG,
                                  ),
                          ).fold('', (String previousValue, element) => (previousValue + element));
                          return ActiveTimeCard(
                            title: weeks[widget.timeTable[itemIndex].key],
                            onTap: () => setTime(currentTime.key),
                            label: 'Изменить',
                            subLabel: txt,
                          );
                        }
                        return DayCard(
                          title: weeks[widget.timeTable[itemIndex].key],
                          label: 'Выбрать время',
                          onTap: () => setTime(currentTime.key),
                        );

                        // final _indexDay = _list[itemIndex];

                        // final _workTime =
                        //     (createBookingModel.workTime[userId] ??= {})[
                        //         (_list[itemIndex].startTime!.weekday + 6) %
                        //             7] ??= [];
                        // if (newMapToPop != null) {
                        //   final _newMap = (newMapToPop![userId] ??= {})[
                        //       (_list[itemIndex].startTime!.weekday + 6) %
                        //           7] ??= [];
                        //   if (_newMap.isNotEmpty) {
                        //     _workTime.clear();
                        //     _workTime.addAll(_newMap);
                        //   }
                        // }
                        // //

                        // if (_workTime.isNotEmpty) {
                        //   final _txt = List<String>.generate(
                        //     _workTime.length * 2 - 1,
                        //     (int index) => index.isOdd
                        //         ? ' | '
                        //         : DateFormat('HH:mm')
                        //             .format(_workTime[index ~/ 2]),
                        //   ).fold(
                        //       '',
                        //       (String previousValue, element) =>
                        //           (previousValue + element));
                        //   return ActiveTimeCard(
                        //     title:
                        //         weeks[(_indexDay.startTime!.weekday + 6) % 7],
                        //     onTap: () => _openSetTime(
                        //       state.clubId,
                        //       state.userId,
                        //       (_indexDay.startTime!.weekday + 6) % 7,
                        //     ),
                        //     label: 'Изменить',
                        //     subLabel: _txt,
                        //   );
                        // }
                      },
                      childCount: widget.timeTable.length * 2 - 1,
                    ),
                  ),
                ),
            ],
          ),
          if (!const DeepCollectionEquality(DefaultEqualityTime())
              .equals(workTime, widget.workTime))
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: BottomButton(
                title: 'Сохранить',
                active: true,
                onTap: () {
                  Navigator.pop(context, workTime);
                },
              ),
            ),
        ],
      ),
    );
  }
}

 // SliverPadding(
              //   padding: const EdgeInsets.all(20),
              //   sliver: BlocBuilder<WorkerTimeBloc, WorkTimeState>(
              //     bloc: workTimeBloc,
              //     builder: (ctx, _state) {
              //       if (_state is LoadingWorkTimeState) {
              //         return const SliverToBoxAdapter(
              //           child: Center(
              //             child: CircularProgressIndicator(),
              //           ),
              //         );
              //       }
              //       final state = _state as DataWorkTimeState;

              //       final wokrTimeMap = state.workTime;
              //       final List<WorkTimeGraph> _list =
              //           ([...wokrTimeMap.entries]..sort((a, b) {
              //                   return a.key.compareTo(b.key);
              //                 }))
              //               .map<WorkTimeGraph>((e) => e.value)
              //               .toList();
              //       return SliverList(
              //         delegate: SliverChildBuilderDelegate(
              //           (context, index) {
              //             final itemIndex = index ~/ 2;
              //             if (!index.isEven) return const SizedBox(height: 10);
              //             //

              //             final _indexDay = _list[itemIndex];

              //             final _workTime =
              //                 (createBookingModel.workTime[userId] ??= {})[
              //                     (_list[itemIndex].startTime!.weekday + 6) %
              //                         7] ??= [];
              //             if (newMapToPop != null) {
              //               final _newMap = (newMapToPop![userId] ??= {})[
              //                   (_list[itemIndex].startTime!.weekday + 6) %
              //                       7] ??= [];
              //               if (_newMap.isNotEmpty) {
              //                 _workTime.clear();
              //                 _workTime.addAll(_newMap);
              //               }
              //             }
              //             //

              //             if (_workTime.isNotEmpty) {
              //               final _txt = List<String>.generate(
              //                 _workTime.length * 2 - 1,
              //                 (int index) => index.isOdd
              //                     ? ' | '
              //                     : DateFormat('HH:mm')
              //                         .format(_workTime[index ~/ 2]),
              //               ).fold(
              //                   '',
              //                   (String previousValue, element) =>
              //                       (previousValue + element));
              //               return ActiveTimeCard(
              //                 title:
              //                     weeks[(_indexDay.startTime!.weekday + 6) % 7],
              //                 onTap: () => _openSetTime(
              //                   state.clubId,
              //                   state.userId,
              //                   (_indexDay.startTime!.weekday + 6) % 7,
              //                 ),
              //                 label: 'Изменить',
              //                 subLabel: _txt,
              //               );
              //             }

              //             return DayCard(
              //               title:
              //                   weeks[(_indexDay.startTime!.weekday + 6) % 7],
              //               label: 'Выбрать время',
              //               onTap: () => _openSetTime(
              //                 state.clubId,
              //                 state.userId,
              //                 (_indexDay.startTime!.weekday + 6) % 7,
              //               ),
              //             );
              //           },
              //           childCount: _list.length * 2 - 1,
              //         ),
              //       );
              //     },
              //   ),
              // ),
