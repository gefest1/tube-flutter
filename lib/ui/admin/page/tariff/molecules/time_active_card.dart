import 'package:flutter/material.dart';
import 'package:tube/ui/admin/atoms/custom_check_box.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class TimeActiveCard extends StatelessWidget {
  final bool active;
  final VoidCallback? onTap;
  final String title;

  final bool available;

  const TimeActiveCard({
    this.onTap,
    this.active = false,
    this.available = true,
    required this.title,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: AnimatedOpacity(
        opacity: available ? 1 : 0.5,
        duration: const Duration(milliseconds: 300),
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          padding: const EdgeInsets.all(15),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.white,
            boxShadow: available
                ? const [
                    BoxShadow(
                      color: Color(0x03000000),
                      blurRadius: 2,
                      offset: Offset(0, -3),
                    ),
                    BoxShadow(
                      color: Color(0x03000000),
                      blurRadius: 2,
                      offset: Offset(-3, 0),
                    ),
                    BoxShadow(
                      color: Color(0x03000000),
                      blurRadius: 2,
                      offset: Offset(0, 3),
                    ),
                    BoxShadow(
                      color: Color(0x0d000000),
                      blurRadius: 20,
                      offset: Offset(0, 5),
                    ),
                  ]
                : [],
          ),
          child: Row(
            children: [
              Expanded(
                child: H3Text(
                  title,
                  color: ColorData.colorTextMain,
                ),
              ),
              CustomCheckBox(
                margin: const EdgeInsets.only(left: 10),
                active: available && active,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
