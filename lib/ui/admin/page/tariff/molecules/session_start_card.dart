import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class SessionStartCard extends StatefulWidget {
  final DateTime dateTime;
  final DateTime endTime;

  const SessionStartCard({
    required this.dateTime,
    required this.endTime,
    Key? key,
  }) : super(key: key);

  @override
  State<SessionStartCard> createState() => _SessionStartCardState();
}

class _SessionStartCardState extends State<SessionStartCard> {
  Timer? timerPer;

  @override
  void initState() {
    // startTimer();
    timerPer = Timer.periodic(const Duration(seconds: 1), (_) => update());

    super.initState();
  }

  void update() {
    setState(() {});
  }

  @override
  void dispose() {
    timerPer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (DateTime.now().compareTo(widget.endTime) == 1) {
      return Container(
        decoration: const BoxDecoration(
          color: ColorData.color3Percent,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
        child: const Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.alarm,
              size: 18,
              color: ColorData.colorTextMain,
            ),
            SizedBox(width: 5),
            P1Text(
              'Занятие уже закончилось',
              color: ColorData.colorTextMain,
            )
          ],
        ),
      );
    }
    if (DateTime.now().compareTo(widget.dateTime) == -1) {
      return Container(
        decoration: const BoxDecoration(
          color: ColorData.color3Percent,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
        child: const Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.alarm,
              size: 18,
              color: ColorData.colorTextMain,
            ),
            SizedBox(width: 5),
            P1Text(
              'Занятие еще не началось',
              color: ColorData.colorTextMain,
            )
          ],
        ),
      );
    }

    return Container(
      decoration: const BoxDecoration(
        color: ColorData.color3Percent,
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
      child: const Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.alarm,
            size: 18,
            color: Color(0xff12AE3E),
          ),
          SizedBox(width: 5),
          P1Text(
            'Занятие в процессе',
            color: Color(0xff12AE3E),
          )
        ],
      ),
    );
  }
}
