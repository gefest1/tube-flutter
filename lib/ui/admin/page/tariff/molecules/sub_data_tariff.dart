import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/ui/admin/page/tariff/atoms/info_text.dart';
import 'package:tube/ui/admin/page/tariff/pages/edit_tariff_admin_page.dart';

import 'package:tube/utils/get_date_time_object_id.dart';
import 'package:tube/utils/get_time_shift.dart';

class SubDataTariff extends StatelessWidget {
  final Tariff tariff;
  final EdgeInsetsGeometry padding;

  const SubDataTariff({
    required this.tariff,
    this.padding = EdgeInsets.zero,
    super.key,
  });

  int get lastCount {
    final trainTimes = tariff.trainTimes ?? [];
    final notActive = trainTimes.where(
      (element) {
        return DateTime.now().microsecondsSinceEpoch > element.endTime!.microsecondsSinceEpoch ||
            element.come == true;
      },
    );
    final trainCount = tariff.trainCount ?? 10;
    return trainCount - notActive.length;
  }

  @override
  Widget build(BuildContext context) {
    final user = tariff.user;
    return Padding(
      padding: padding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (tariff.id != null)
            InfoText(
              text1: 'Абонемент куплен: ',
              style1: p2TextStyle,
              text2: DateFormat('dd.MM.yyyy').format(
                getDateTimefromObjectId(tariff.id!).parseTimeGG,
              ),
              style2: p1TextStyle,
            ),
          if (tariff.startDate != null) ...[
            const SizedBox(height: 5),
            InfoText(
              text1: 'Начало занятий: ',
              style1: p2TextStyle,
              text2: DateFormat('dd.MM.yyyy').format(
                tariff.startDate!.parseTimeGG,
              ),
              style2: p1TextStyle,
            )
          ],
          if (tariff.deadlineDate != null) ...[
            const SizedBox(height: 5),
            InfoText(
              text1: 'Действует до: ',
              style1: p2TextStyle,
              text2: DateFormat('dd.MM.yyyy').format(
                tariff.deadlineDate!.parseTimeGG,
              ),
              style2: p1TextStyleBold,
            ),
          ],
          const SizedBox(height: 5),
          InfoText(
            text1: 'Владелец: ',
            style1: p2TextStyle,
            text2: user?.fullName ?? user?.email ?? '',
            style2: p1TextStyleBold,
          ),
          const SizedBox(height: 5),
          RichText(
            text: TextSpan(
              children: [
                const TextSpan(
                  style: p2TextStyle,
                  text: 'Осталось занятий: ',
                ),
                TextSpan(
                  style: p1TextStyleBold,
                  text: '$lastCount/${tariff.trainCount}',
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
