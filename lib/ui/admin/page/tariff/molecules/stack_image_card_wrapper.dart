import 'package:flutter/material.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/ui/admin/page/tariff/molecules/stack_image_card.dart';
import 'package:tube/ui/admin/page/tariff/pages/tariff_train_time_page.dart';

class StackImageCardWrapper extends StatelessWidget {
  final List<TariffSingle> tariffs;
  final DateTime dateTime;
  final String uniqueMapKey;

  const StackImageCardWrapper({
    required this.tariffs,
    required this.dateTime,
    required this.uniqueMapKey,
    Key? key,
  }) : super(key: key);

  List<String> getPhotos(List<TariffSingle>? val) {
    List<String> toRet = (val ?? [])
        .map((e) => e.user?.photoUrl?.m)
        .whereType<String>()
        .toList();
    return toRet;
  }

  @override
  Widget build(BuildContext context) {
    final List<TariffSingle> upcoming = tariffs
        .where((element) => element.trainTimesSingle.come != true)
        .toList();
    final List<TariffSingle> here = tariffs
        .where((element) => element.trainTimesSingle.come == true)
        .toList();
    return Column(
      children: [
        StackImageCard(
          title: 'На занятии',
          numberTitle: here.length,
          images: getPhotos(here),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => TariffTrainTimePage(
                  dateTime: dateTime,
                  title: 'На занятии',
                  tariffList: here,
                  uniqueMapKey: uniqueMapKey,
                ),
              ),
            );
          },
        ),
        const SizedBox(height: 10),
        StackImageCard(
          images: getPhotos(upcoming),
          title: 'Ожидаем',
          numberTitle: upcoming.length,
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => TariffTrainTimePage(
                  dateTime: dateTime,
                  title: 'Ожидаем',
                  tariffList: upcoming,
                  uniqueMapKey: uniqueMapKey,
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
