import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class SheduleScroll extends StatefulWidget {
  const SheduleScroll({Key? key}) : super(key: key);

  @override
  State<SheduleScroll> createState() => _SheduleScrollState();
}

class _SheduleScrollState extends State<SheduleScroll> {
  final List<String> _list = [
    "Пон",
    "Вт",
    "Ср",
    "Чт",
    "Пт",
    "Сб",
    "Вс",
  ];
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 330,
      child: PageView.builder(
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: List.generate(
                      _list.length,
                      (index) => SizedBox(
                        width: 31,
                        child: H6Text(
                          _list[index],
                          color: ColorData.colortTextSecondary,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 20),
                  for (double index = 0; index < 5.5; index += 0.5)
                    if (index % 1 != 0)
                      const SizedBox(height: 10)
                    else
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: List.generate(
                          _list.length,
                          (subIndex) => Container(
                            width: 31,
                            height: 41,
                            alignment: Alignment.center,
                            decoration: const BoxDecoration(
                              color: ColorData.colorMainLink,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                            ),
                            padding: const EdgeInsets.all(5),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                H6Text(
                                  ((index * 7) + subIndex).round().toString(),
                                  color: Colors.white,
                                ),
                                const Row(),
                              ],
                            ),
                          ),
                        ),
                      )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
