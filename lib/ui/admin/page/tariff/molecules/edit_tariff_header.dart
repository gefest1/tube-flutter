import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/shedule_table.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/infinite_listview.dart';

class EditTariffHeader extends StatelessWidget {
  final InfiniteScrollController controller;
  // final SheduleTableController sheduleTableController;
  final BehaviorSubject<int> lastIndex;
  final int currentYear;
  final int currentMonth;

  const EditTariffHeader({
    required this.controller,
    required this.lastIndex,
    required this.currentYear,
    required this.currentMonth,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        IconButton(
          onPressed: () {
            controller.animateToPage(
              controller.page - 1,
              duration: const Duration(milliseconds: 300),
              curve: Curves.easeIn,
            );
          },
          icon: const Icon(
            Icons.arrow_back_ios,
            color: ColorData.colorButtonMain,
          ),
          iconSize: 33,
        ),
        Center(
          child: StreamBuilder(
            stream: lastIndex,
            builder: (context, snapshot) {
              final currentDateTime =
                  DateTime(currentYear, currentMonth + lastIndex.value);
              final month = currentDateTime.month - 1;
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  H4Text(
                    monthName[month],
                    color: ColorData.colorTextMain,
                  ),
                  P2Text(
                    currentDateTime.year.toString(),
                    color: ColorData.colortTextSecondary,
                  )
                ],
              );
            },
          ),
        ),
        IconButton(
          onPressed: () {
            controller.animateToPage(
              controller.page + 1,
              duration: const Duration(milliseconds: 300),
              curve: Curves.easeIn,
            );
          },
          icon: const Icon(
            Icons.arrow_forward_ios,
            color: ColorData.colorButtonMain,
          ),
          iconSize: 33,
        ),
      ],
    );
  }
}
