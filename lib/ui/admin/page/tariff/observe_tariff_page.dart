import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/admin/booking_admin/booking_admin_bloc.dart';
import 'package:tube/logic/blocs/admin/booking_detail_data_bloc/booking_detail_data_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/ui/admin/page/big_event/big_event_detail_page.dart';
import 'package:tube/ui/admin/page/tariff/edits/mixins/edit_duration_mixin.dart';
import 'package:tube/ui/admin/page/tariff/edits/mixins/edit_name_mixin.dart';
import 'package:tube/ui/admin/page/tariff/edits/mixins/edit_price_mixin.dart';
import 'package:tube/ui/admin/page/tariff/edits/mixins/edit_schedule_mixin.dart';
import 'package:tube/ui/admin/page/tariff/edits/mixins/edit_tags_mixin.dart';
import 'package:tube/ui/admin/page/tariff/edits/molecules/edit_name_card.dart';
import 'package:tube/ui/admin/page/tariff/edits/molecules/edit_pause_card.dart';
import 'package:tube/ui/admin/page/tariff/edits/molecules/edit_prices_card.dart';
import 'package:tube/ui/admin/page/tariff/edits/molecules/edit_schedule_tags_row.dart';
import 'package:tube/ui/admin/page/tariff/edits/molecules/expire_card.dart';
import 'package:tube/ui/admin/page/tariff/edits/observe_qr_body.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';

import 'package:tube/utils/colors.dart';
import 'package:tube/utils/load_popup_route.dart';

class ObserveTariffPage extends StatefulWidget {
  final Booking booking;
  final String clubId;

  const ObserveTariffPage({
    required this.booking,
    required this.clubId,
    Key? key,
  }) : super(key: key);

  @override
  State<ObserveTariffPage> createState() => _ObserveTariffPageState();
}

class _ObserveTariffPageState extends ObserveTariffPageStateAbstract
    with
        EditNameMixin,
        EditTagsMixin,
        EditScheduleMixin,
        EditDurationMixin,
        EditPriceMixin {}

abstract class ObserveTariffPageStateAbstract extends State<ObserveTariffPage> {
  late final BookingDetailDataBloc detailBookingBloc =
      Provider.of<AdminBlocMap>(
    context,
    listen: false,
  ).getBookingDetailDataBloc(widget.booking.id!);
  StreamSubscription? sub;

  BookingAdminBloc get bookingAdminBloc =>
      Provider.of<AdminBlocMap>(context, listen: false)
          .getBookingAdminBloc(widget.clubId);

  @override
  void initState() {
    super.initState();
    Future(() {
      detailBookingBloc.add(const GetBookingDetailDataEvent());
      // BlocBuilder

      sub = bookingAdminBloc.stream.listen((event) {
        if (!mounted) return;
        setState(() {});
      });
    });
  }

  Booking get booking =>
      bookingAdminBloc.getBooking(widget.booking.id!) ?? widget.booking;
  Widget get div =>
      const Divider(height: 0, thickness: 0.5, color: ColorData.color5Percent);

  void editName();
  void editTags();
  void editSchedule();
  void editInfoDuration();
  void editPrice();
  final iconKey = GlobalKey();

  @override
  void dispose() {
    sub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const H4Text(
          'Абонемент с сотрудниками',
          color: Colors.white,
        ),
        centerTitle: true,
        actions: [
          IconButton(
            key: iconKey,
            onPressed: () {
              if (iconKey.currentContext == null) return;
              final offset =
                  (iconKey.currentContext!.findRenderObject() as RenderBox)
                      .localToGlobal(Offset.zero);

              final route1 = ModalRoute.of<dynamic>(context);

              showDialog(
                context: context,
                builder: (contextDialog) {
                  return Stack(
                    children: [
                      Positioned(
                        top: offset.dy + 36,
                        right: MediaQuery.of(contextDialog).size.width -
                            offset.dx -
                            33,
                        child: CancelButton(
                          title: 'Удалить абонимент',
                          onTap: () async {
                            final route2 =
                                ModalRoute.of<dynamic>(contextDialog);
                            final completer = Completer();
                            Navigator.push(
                              contextDialog,
                              LoadPopupRoute(completer: completer),
                            );
                            bookingAdminBloc.add(
                              RemoveBookingAdminEvent(
                                bookingId: widget.booking.id!,
                                completer: completer,
                              ),
                            );
                            // bigEventBloc.add(
                            //   RemoveAdminBigEventEvent(
                            //     bigEventId: entity.id,
                            //     completer: completer,
                            //   ),
                            // );
                            await completer.future;
                            if (!mounted) return;
                            if (route1 != null) {
                              Navigator.removeRoute(context, route1);
                            }
                            if (route2 != null) {
                              Navigator.removeRoute(context, route2);
                            }
                          },
                        ),
                      )
                    ],
                  );
                },
              );
            },
            icon: const Icon(Icons.more_vert),
          ),
        ],
      ),
      body: BlocListener(
        listener: (context, state) {
          setState(() {});
        },
        bloc: bookingAdminBloc,
        child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          children: [
            const SizedBox(height: 20),
            if (booking.name != null)
              H2Text(
                booking.name!,
                color: ColorData.colorElementsActive,
              ),
            ObserveQRBody(bloc: detailBookingBloc),
            const SizedBox(height: 20),
            EditNameCard(
              description: booking.description ?? '',
              onTap: editName,
            ),
            const SizedBox(height: 10),
            EditScheduleTagsRow(
              bloc: detailBookingBloc,
              tagsLength: booking.tags.length,
              scheduleTap: editSchedule,
              tagsTap: editTags,
            ),
            const SizedBox(height: 10),
            ExpireCard(
              deadlineDays: booking.deadlineDays ?? 0,
              count: booking.count ?? 0,
              timePerMinute: booking.timePerMinute ?? 0,
              onTap: editInfoDuration,
            ),
            const SizedBox(height: 10),
            EditPricesCard(
                price: booking.price ?? 0,
                trainerPrice:
                    (booking.price ?? 0) * (booking.percent ?? 0) / 100,
                onTap: editPrice),
            const SizedBox(height: 10),
            EditPauseCard(
              bloc: bookingAdminBloc,
              booking: booking,
            ),
            const SizedBox(height: 200),
          ],
        ),
      ),
    );
  }
}
