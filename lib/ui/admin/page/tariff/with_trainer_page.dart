import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/logic/blocs/admin/workers/worker_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/admin/create.booking.model.dart';
import 'package:tube/ui/admin/page/tariff/set_tariff_page.dart';
import 'package:tube/ui/admin/page/validity_tariff_page.dart';
import 'package:tube/ui/common/atoms/dangerous_title.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:async/async.dart' show StreamGroup;

class WithTrainerPage extends StatefulWidget {
  const WithTrainerPage({Key? key}) : super(key: key);

  @override
  State<WithTrainerPage> createState() => _WithTrainerPageState();
}

class _WithTrainerPageState extends State<WithTrainerPage> {
  late final createBookingModel =
      Provider.of<CreateBookingModel>(context, listen: false);

  late final TextEditingController numberController = TextEditingController();
  late final TextEditingController countController = TextEditingController();
  late final BehaviorSubject<int> yearSubject = BehaviorSubject<int>.seeded(0);
  late final BehaviorSubject<int> monthSubject = BehaviorSubject<int>.seeded(0);
  late final BehaviorSubject<int> daySubject = BehaviorSubject<int>.seeded(0);
  late final BehaviorSubject<int> hourSubject = BehaviorSubject<int>.seeded(0);
  late final BehaviorSubject<int> minSubject = BehaviorSubject<int>.seeded(0);
  late final Stream _streamValueListen = StreamGroup.mergeBroadcast([
    yearSubject.stream,
    monthSubject.stream,
    daySubject.stream,
    hourSubject.stream,
    minSubject.stream,
  ]);

  @override
  void dispose() {
    numberController.dispose();
    countController.dispose();
    yearSubject.close();
    monthSubject.close();
    daySubject.close();
    hourSubject.close();
    minSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
      children: [
        const Align(
          alignment: Alignment.centerLeft,
          child: Icon(
            Icons.perm_contact_calendar,
            color: ColorData.colorMainElements,
          ),
        ),
        const SizedBox(height: 10),
        const H3Text(
          'С сотрудниками',
          color: ColorData.colorTextMain,
        ),
        const SizedBox(height: 10),
        const P1Text(
          '— Клиент занимается установленное количество раз в установленный временной промежуток',
          color: ColorData.colorTextMain,
        ),
        const SizedBox(height: 10),
        const P1Text(
          '— Занятия проводятся сотрудниками вашей компании',
          color: ColorData.colorTextMain,
        ),
        BlocBuilder<WorkerBloc, WorkerState>(
          bloc: Provider.of<AdminBlocMap>(context, listen: false)
              .getWorkerBloc(MainDrawerState.currentParticipance!.clubId!),
          builder: (_, state) {
            if (state is ClubWorkerState &&
                state.mapWorkers.where((element) {
                  final userWorkTime = state.workUserMap[element.id];
                  return userWorkTime != null;
                }).isNotEmpty) {
              return Column(
                children: [
                  const SizedBox(height: 20),
                  CustomTextField(
                    controller: numberController,
                    hintText: '10',
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                    ],
                    title: 'Количество занятий в течение срока действия',
                  ),
                  const SizedBox(height: 20),
                  CustomTextField(
                    controller: countController,
                    hintText: '10',
                    scrollPadding: const EdgeInsets.only(bottom: 200),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.deny(RegExp(r'^0')),
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                    title: 'Максимальное количество людей в группе',
                  ),
                  const SizedBox(height: 30),
                  ValidityTariffPage(
                    yearSubject: yearSubject,
                    monthSubject: monthSubject,
                    daySubject: daySubject,
                    hourSubject: hourSubject,
                    minSubject: minSubject,
                  ),
                ],
              );
            }
            return const SizedBox();
          },
        ),
        const SizedBox(height: 40),
        BlocBuilder<WorkerBloc, WorkerState>(
          bloc: Provider.of<AdminBlocMap>(context, listen: false)
              .getWorkerBloc(MainDrawerState.currentParticipance!.clubId!),
          builder: (_, state) {
            if (state is ClubWorkerState &&
                state.mapWorkers.where((element) {
                  final userWorkTime = state.workUserMap[element.id];
                  return userWorkTime != null;
                }).isNotEmpty) {
              return const DangerousTitle(
                title:
                    'Если клиент не успеет отходить нужное количество занятий за срок действия абонемента —  неиспользованные занятия сгорают',
              );
            }
            return const DangerousTitle(
              title:
                  'Добавьте сотрудников чтобы создать абонемент или заполните информацию о сотрудниках',
            );
          },
        ),
        const SizedBox(height: 20),
        BlocBuilder<WorkerBloc, WorkerState>(
          bloc: Provider.of<AdminBlocMap>(context, listen: false)
              .getWorkerBloc(MainDrawerState.currentParticipance!.clubId!),
          builder: (_, state) {
            if (state is ClubWorkerState &&
                state.mapWorkers.where((element) {
                  final userWorkTime = state.workUserMap[element.id];
                  return userWorkTime != null;
                }).isNotEmpty) {
              return StreamBuilder(
                  stream: _streamValueListen,
                  builder: (context, snapshot) {
                    if (hourSubject.value == 0 && minSubject.value == 0 ||
                        (yearSubject.value == 0 &&
                            monthSubject.value == 0 &&
                            daySubject.value == 0)) {
                      return const SizedBox();
                    }
                    return CustomButton(
                      title: 'Продолжить',
                      onTap: () async {
                        final int value = numberController.text.isEmpty
                            ? 10
                            : int.parse(numberController.text);
                        final int studentNumber = countController.text.isEmpty
                            ? 10
                            : int.parse(countController.text);

                        createBookingModel.setNewInstance(
                          createBookingModel.booking.copyWith(
                            count: value,
                            studentNumber: studentNumber,
                            deadlineDays: yearSubject.value * 365 +
                                monthSubject.value * 30 +
                                daySubject.value,
                            timePerMinute: Duration(
                              hours: hourSubject.value,
                              minutes: minSubject.value,
                            ).inMinutes,
                          ),
                        );

                        await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => const SetTariffPage(),
                          ),
                        );
                        if (!mounted) return;
                        final createBooking =
                            context.read<CreateBookingModel>();
                        createBooking.setNewWorkTime({});
                      },
                      color: ColorData.colorMainLink,
                    );
                  });
            }
            return const SizedBox();
          },
        ),
        const SizedBox(height: 40),
      ],
    );
  }
}

List<Widget> noTrain = [
  const SizedBox(height: 40),
  Container(
    decoration: BoxDecoration(
      borderRadius: const BorderRadius.all(Radius.circular(10)),
      border: Border.all(
        color: const Color(0xffA5A5A5),
        width: 0.5,
      ),
    ),
    padding: const EdgeInsets.all(15),
    child: const Row(
      children: [
        Padding(
          padding: EdgeInsets.only(right: 10),
          child: Icon(
            Icons.warning_amber,
            size: 24,
            color: ColorData.colorElementsActive,
          ),
        ),
        Expanded(
          child: P1Text(
            'У вас пока нет сотрудников',
            color: ColorData.colorTextMain,
          ),
        )
      ],
    ),
  ),
  const SizedBox(height: 20),
  CustomButton(
    title: 'Добавить сотрудников',
    color: ColorData.colorMainLink,
    onTap: () {},
  )
];
