import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:tube/utils/infinite_listview.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/logic/blocs/admin/tariff_admin_bloc/tariff_admin_bloc.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/tariff/model/train_time.model.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/ui/admin/page/tariff/observe_tariff_page.dart';
import 'package:tube/ui/admin/tariff_admin_detial_page.dart';

import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/shedule_table.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/trainer_page/components/molecules/shedule_card.dart';
import 'package:tube/utils/colors.dart';

import 'package:tube/utils/is_same_day.dart';

class SheduleTableAdminPage extends StatefulWidget {
  const SheduleTableAdminPage({Key? key}) : super(key: key);

  @override
  State<SheduleTableAdminPage> createState() => _SheduleTableAdminPageState();
}

class _SheduleTableAdminPageState extends State<SheduleTableAdminPage> {
  final currentYear = DateTime.now().year;
  final currentMonth = DateTime.now().month;

  final lastIndex = BehaviorSubject.seeded(0);

  final BehaviorSubject<DateTime> dateTimeSubject =
      BehaviorSubject<DateTime>.seeded(DateTime.now());
  late final InfiniteScrollController infiniteScrollController = InfiniteScrollController();

  late TariffAdminBloc tariffAdminBloc = Provider.of<AdminBlocMap>(context, listen: false)
      .getTariffAdminBloc(MainDrawerState.currentParticipance!.clubId!);
  StreamSubscription? streamSub;

  @override
  void initState() {
    super.initState();
    tariffAdminBloc.add(const GetSoonTariffAdminEvent());
    streamSub = lastIndex.listen((value) {
      tariffAdminBloc.add(
        GetSoonTariffAdminEvent(
          startDate: DateTime(
            currentYear,
            currentMonth + lastIndex.value,
          ),
          endDate: DateTime(
            currentYear,
            currentMonth + lastIndex.value + 1,
          ),
        ),
      );
    });
  }

  @override
  void dispose() {
    infiniteScrollController.dispose();
    streamSub?.cancel();
    lastIndex.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: SizedBox(
                  height: MediaQuery.of(context).padding.top + 60,
                ),
              ),
              SliverToBoxAdapter(
                child: Column(
                  children: [
                    const SizedBox(height: 20),
                    BlocBuilder<TariffAdminBloc, TariffAdminState>(
                      bloc: Provider.of<AdminBlocMap>(context, listen: false)
                          .getTariffAdminBloc(MainDrawerState.currentParticipance!.clubId!),
                      builder: (ctx, state) {
                        final Map<String, Set<String>> dateTimeMapWidget = {};
                        if (state is GetTariffAdminState) {
                          for (final single in state.dirtTariffList) {
                            for (TrainTime singleTrainTime in single.trainTimes ?? []) {
                              if (singleTrainTime.date == null) continue;
                              final key = dayOnlyTime(singleTrainTime.date!).toIso8601String();
                              (dateTimeMapWidget[key] ??= {}).add('booking');
                            }
                          }
                        }
                        return SheduleTable(
                          controller: infiniteScrollController,
                          lastIndexSubject: lastIndex,
                          horizontalPadding: const EdgeInsets.symmetric(horizontal: 20),
                          onTap: (DateTime dateTime) {
                            dateTimeSubject.value = dateTime;
                          },
                          dateTimeMapWidget: dateTimeMapWidget,
                          dateTimeSubject: dateTimeSubject,
                        );
                      },
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
              SliverFillRemaining(
                hasScrollBody: false,
                child: DecoratedBox(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 15,
                        offset: Offset(0, -4),
                        color: Color(0x0d000000),
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 200),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 20,
                        vertical: 15,
                      ),
                      child: BlocBuilder<TariffAdminBloc, TariffAdminState>(
                        bloc: tariffAdminBloc,
                        builder: (ctx, state) {
                          final Map<String, Map<String, List<TariffSingle>>> tariffDayMap = {};
                          final Map<String, Booking> bookingMap = {};

                          if (state is GetTariffAdminState) {
                            for (final single in state.dirtTariffList) {
                              for (TrainTime singleTrainTime in single.trainTimes ?? []) {
                                final currentDate = singleTrainTime.date;
                                if (currentDate == null) continue;
                                if (single.bookingId != null && single.booking != null) {
                                  bookingMap[single.bookingId!] = single.booking!;
                                }

                                ((tariffDayMap[dayOnlyTime(currentDate).toIso8601String()] ??=
                                        {})[single.bookingId!] ??= [])
                                    .add(
                                  TariffSingle.fromMapTariff(
                                    single,
                                    singleTrainTime,
                                  ),
                                );
                              }
                            }
                          }
                          return StreamBuilder(
                            stream: dateTimeSubject,
                            builder: (ctx, _) {
                              final Map<String, List<TariffSingle>> tarifMap = tariffDayMap[
                                      dayOnlyTime(dateTimeSubject.value).toIso8601String()] ??
                                  {};
                              final entries = tarifMap.entries.toList();
                              if (entries.isEmpty) {
                                return const Align(
                                  alignment: Alignment.topCenter,
                                  child: H4Text(
                                    'В этот день событий нет',
                                    color: ColorData.colortTextSecondary,
                                  ),
                                );
                              }
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  H4Text(
                                    DateFormat("d MMMM yyyy").format(
                                      dateTimeSubject.value.parseTimeGG,
                                    ),
                                    color: ColorData.colortTextSecondary,
                                  ),
                                  ...List.generate(entries.length * 2 - 1, (ind) {
                                    if (ind.isOdd) {
                                      return const Divider(
                                        color: Color(0xffEBEBEB),
                                        height: 0,
                                        thickness: 0.5,
                                      );
                                    }
                                    final index = ind ~/ 2;
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 20),
                                      child: SheduleCard(
                                        title: bookingMap[entries[index].key]?.name,
                                        trainTimeList: entries[index].value,
                                        bookingTap: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (_) => ObserveTariffPage(
                                                booking: bookingMap[entries[index].key]!,
                                                clubId: MainDrawerState.clubId!,
                                              ),
                                            ),
                                          );
                                        },
                                        tariffTap: ({
                                          required String mapKeyUnique,
                                          required List<TariffSingle> list,
                                          required DateTime dateTime,
                                        }) {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (_) {
                                                return TariffAdminDetailPage(
                                                  list: list,
                                                  booking: bookingMap[entries[index].key]!,
                                                  dateTime: dateTime,
                                                  uniqueMapKey:
                                                      "${entries[index].key}_$mapKeyUnique",
                                                );
                                              },
                                            ),
                                          );
                                        },
                                      ),
                                    );
                                  }),
                                ],
                              );
                            },
                            // builder: dateTime,
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Material(
            color: ColorData.colorMainElements,
            child: Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              child: SizedBox(
                height: 60,
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        onPressed: () {
                          infiniteScrollController.animateToPage(
                            infiniteScrollController.page - 1,
                            duration: const Duration(milliseconds: 300),
                            curve: Curves.easeIn,
                          );
                        },
                        icon: const Icon(Icons.arrow_back_ios),
                        iconSize: 33,
                      ),
                      Center(
                        child: StreamBuilder(
                          stream: lastIndex,
                          builder: (context, snapshot) {
                            final currentDateTime =
                                DateTime(currentYear, currentMonth + lastIndex.value);
                            final month = currentDateTime.month - 1;
                            return Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                H4Text(
                                  monthName[month],
                                  color: Colors.white,
                                ),
                                P2Text(
                                  currentDateTime.year.toString(),
                                  color: ColorData.color3Percent,
                                )
                              ],
                            );
                          },
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          infiniteScrollController.animateToPage(
                            infiniteScrollController.page + 1,
                            duration: const Duration(milliseconds: 300),
                            curve: Curves.easeIn,
                          );
                        },
                        icon: const Icon(Icons.arrow_forward_ios),
                        iconSize: 33,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
