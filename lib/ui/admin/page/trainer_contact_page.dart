import 'package:flutter/material.dart';
import 'package:tube/icon/facebook.icon.dart';
import 'package:tube/icon/telegram.icon.dart';
import 'package:tube/icon/whatsapp.icon.dart';
import 'package:tube/ui/admin/page/choose_trainer_tag.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';

import 'package:tube/utils/colors.dart';
import 'package:tube/icon/instagram.icon.dart';

class TrainerContactPage extends StatefulWidget {
  const TrainerContactPage({Key? key}) : super(key: key);

  @override
  State<TrainerContactPage> createState() => _TrainerContactPageState();
}

class _TrainerContactPageState extends State<TrainerContactPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const Column(
          children: [
            H4Text(
              'jeanwwd@gmail.com',
              color: Colors.white,
            ),
            Padding(
              padding: EdgeInsets.only(top: 2),
              child: P1Text(
                'Иван И.И.',
                color: Colors.white,
              ),
            )
          ],
        ),
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(
            Icons.arrow_back,
            size: 33,
          ),
          color: Colors.white,
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              padding: const EdgeInsets.only(top: 40, left: 20, right: 20),
              children: const [
                CustomTextField(
                  hintText: '+7 707 777 77 77',
                  icon: Icon(
                    Icons.call,
                    color: ColorData.colorElementsActive,
                  ),
                  title: 'Номер телефона для быстрой связи',
                ),
                SizedBox(height: 30),
                CustomTextField(
                  icon: CustomPaint(
                    painter: InstagramGlyphCustomPainter(),
                    size: Size(24, 24),
                  ),
                  hintText: '@example',
                  title: 'Ник компании в Instagram',
                ),
                SizedBox(height: 30),
                CustomTextField(
                  icon: CustomPaint(
                    painter: FacebookCustomPainter(),
                    size: Size(24, 24),
                  ),
                  hintText: '@company',
                  title: 'Ник компании в Facebook',
                ),
                SizedBox(height: 30),
                CustomTextField(
                  icon: CustomPaint(
                    painter: WhatsappGlyphCustomPainter(),
                    size: Size(24, 24),
                  ),
                  hintText: '+7 787 778 72 72',
                  title: 'Ник компании в Instagram',
                ),
                SizedBox(height: 30),
                CustomTextField(
                  icon: CustomPaint(
                    painter: TelegramCustomPainter(),
                    size: Size(24, 24),
                  ),
                  hintText: '@example',
                  title: 'Номер телефона Telegram',
                ),
                SizedBox(height: 30),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            decoration: const BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Color(0x1a000000),
                  offset: Offset(0, -0.5),
                ),
              ],
            ),
            child: CustomButton(
              title: 'Пропустить',
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => const ChooseTrainerTag(),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
