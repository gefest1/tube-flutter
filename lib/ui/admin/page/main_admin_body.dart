// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/admin/booking_admin/booking_admin_bloc.dart';
import 'package:tube/logic/blocs/big_event/big_event_bloc.dart';
import 'package:tube/logic/blocs/participance/participance_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/ui/admin/atoms/finance_button.dart';
import 'package:tube/ui/admin/atoms/head_titile.dart';
import 'package:tube/ui/admin/molecules/add_dialog.dart';
import 'package:tube/ui/admin/molecules/booking_card.dart';
import 'package:tube/ui/admin/molecules/client_list.dart';
import 'package:tube/ui/admin/molecules/trainer_list.dart';
import 'package:tube/ui/admin/page/big_event/big_event_detail_page.dart';
import 'package:tube/ui/admin/page/tariff/create_tariff_page.dart';
import 'package:tube/ui/admin/page/tariff/molecules/short_fast_button.dart';
import 'package:tube/ui/admin/page/tariff/observe_tariff_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/big_event_card.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_date_time_object_id.dart';
import 'package:tube/utils/get_time_shift.dart';

class MainAdminBody extends StatelessWidget {
  final String clubId;

  const MainAdminBody({
    required this.clubId,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // log(getDateTimefromObjectId('64f59fe92de9316325c33d3f').toIso8601String());

    final adminBlocMap = Provider.of<AdminBlocMap>(context, listen: false);
    return Stack(
      children: [
        ListView(
          padding: EdgeInsets.only(
            top: 30 + 56 + MediaQuery.of(context).padding.top,
          ),
          children: [
            const FinanceButton(),
            const SizedBox(height: 20),
            BlocBuilder<AdminBigEventBloc, AdminBigEventState>(
              builder: (context, state) {
                if (state is DataAdminBigEventState && state.bigEvents.isNotEmpty) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: H5Text(
                          'Мероприятия',
                          color: ColorData.colortTextSecondary,
                        ),
                      ),
                      ...List.generate(state.bigEvents.length * 2 - 1, (index) {
                        if (index.isOdd) {
                          return const Divider(
                            endIndent: 15,
                            indent: 15,
                            height: 0,
                            thickness: 0.2,
                            color: ColorData.colortTextSecondary,
                          );
                        }
                        final bigEvent = state.bigEvents[index ~/ 2];
                        return BigEventCard(
                          title: bigEvent.name,
                          subTitle: DateFormat('dd.MM.yyyy HH:mm').format(
                            bigEvent.date.parseTimeGG,
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => BigEventDetailPage(
                                  entity: bigEvent,
                                  isAdmin: true,
                                ),
                              ),
                            );
                          },
                        );
                      }),
                    ],
                  );
                }
                return const SizedBox();
              },
              bloc: adminBlocMap.getAdminBigEventBloc(clubId),
            ),
            const SizedBox(height: 20),
            // const Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 20),
            //   child: H5Text(
            //     'Усулги',
            //     color: ColorData.colorElementsSecondary,
            //   ),
            // ),
            // const SizedBox(height: 10),
            // BlocBuilder<AdminBigServiceBloc, AdminBigServiceState>(
            //   bloc: adminBlocMap.getAdminBigServiceBloc(clubId),
            //   builder: (context, state) {
            //     final List<BigServiceEntity> bigServices = [];
            //     if (state is DataAdminBigServiceState) {
            //       bigServices.addAll(state.data);
            //     }
            //     if (bigServices.isNotEmpty) {
            //       return Column(
            //         children: [
            //           SizedBox(
            //             height: 158,
            //             child: ListView.separated(
            //               padding: const EdgeInsets.symmetric(horizontal: 20),
            //               clipBehavior: Clip.none,
            //               scrollDirection: Axis.horizontal,
            //               itemBuilder: (context, index) {
            //                 return ServiceCard(
            //                   onTap: () {
            //                     Navigator.push(context, MaterialPageRoute(
            //                       builder: (context) {
            //                         return ObserveBigServicePage(
            //                           bigService: bigServices[index],
            //                         );
            //                       },
            //                     ));
            //                   },
            //                   title: bigServices[index].name,
            //                   label: NumberFormat('###,###,000₸')
            //                       .format(bigServices[index].price),
            //                   iconWidget: const CustomPaint(
            //                     painter: ServiceIconCustomPainter(
            //                       color: ColorData.colorMainLink,
            //                     ),
            //                     size: Size(32, 32),
            //                   ),
            //                 );
            //               },
            //               separatorBuilder: (context, index) =>
            //                   const SizedBox(width: 10),
            //               itemCount: bigServices.length,
            //             ),
            //           ),
            //         ],
            //       );
            //     }
            //     return Padding(
            //       padding: const EdgeInsets.symmetric(horizontal: 20),
            //       child: ShortFastButton(
            //         onTap: () {
            //           Navigator.push(
            //             context,
            //             MaterialPageRoute(
            //               builder: (_) => const CreateTariffPage(),
            //             ),
            //           );
            //         },
            //         child: Row(
            //           mainAxisAlignment: MainAxisAlignment.center,
            //           children: const [
            //             Icon(
            //               Icons.control_point_duplicate,
            //               color: ColorData.colorElementsActive,
            //             ),
            //             SizedBox(width: 5),
            //             P1Text(
            //               'Добавить услугу',
            //               color: ColorData.colorElementsActive,
            //             )
            //           ],
            //         ),
            //       ),
            //     );
            //   },
            // ),

            const SizedBox(height: 10),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: H5Text(
                'Абонементы',
                color: ColorData.colorElementsSecondary,
              ),
            ),
            const SizedBox(height: 10),
            BlocBuilder<BookingAdminBloc, BookingAdminState>(
              bloc: adminBlocMap.getBookingAdminBloc(clubId),
              builder: (ctx, state) {
                final List<Booking> booking = [];
                if (state is DataBookingAdminState) {
                  booking.addAll(state.bookings);
                }
                if (booking.isNotEmpty) {
                  return Column(
                    children: [
                      SizedBox(
                        height: 158,
                        child: ListView.separated(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          clipBehavior: Clip.none,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return BookingCard(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) => ObserveTariffPage(
                                      booking: booking[index],
                                      clubId: MainDrawerState.clubId!,
                                    ),
                                  ),
                                );
                              },
                              title: booking[index].name,
                              subTitle: "C сотрудниками",
                              label: booking[index].price == null
                                  ? null
                                  : NumberFormat('###,###,000₸').format(booking[index].price!),
                              // '${booking[index].price!.toInt()} тг.',
                              iconWidget: const Icon(
                                Icons.fiber_smart_record,
                                color: ColorData.colorMainLink,
                                size: 32,
                              ),
                            );
                          },
                          separatorBuilder: (context, index) => const SizedBox(width: 10),
                          itemCount: booking.length,
                        ),
                      ),
                    ],
                  );
                }
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: ShortFastButton(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => const CreateTariffPage(),
                        ),
                      );
                    },
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.control_point_duplicate,
                          color: ColorData.colorElementsActive,
                        ),
                        SizedBox(width: 5),
                        P1Text(
                          'Добавить абонемент',
                          color: ColorData.colorElementsActive,
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
            const SizedBox(height: 30),
            const TrainerList(),
            const SizedBox(height: 30),
            const ClientList(),
            const SizedBox(height: 300),
          ],
        ),
        Positioned(
          left: 0,
          right: 0,
          top: 0,
          child: AppBar(
            automaticallyImplyLeading: false,
            leading: SizedBox(
              height: 56,
              width: 56,
              child: Stack(
                children: [
                  IconButton(
                    onPressed: () => Scaffold.of(context).openDrawer(),
                    icon: const Icon(
                      Icons.menu,
                      size: 33,
                    ),
                    iconSize: 33,
                    color: Colors.white,
                  ),
                  BlocBuilder<ParticipanceBloc, ParticipanceState>(
                    builder: (_, state) {
                      if (state is! DataParticipanceState ||
                          state.participances
                              .where((element) => element.accepted == false)
                              .isEmpty) {
                        return const SizedBox();
                      }
                      return const Positioned(
                        right: 16,
                        top: 8,
                        child: IgnorePointer(
                          child: CircleAvatar(
                            radius: 5,
                            backgroundColor: ColorData.colorMainNo,
                          ),
                        ),
                      );
                    },
                  )
                ],
              ),
            ),
            actions: [
              SizeTapAnimation(
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (ctx) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: AddDialog(
                            clubId: clubId,
                          ),
                        );
                      },
                    );
                  },
                  child: const Padding(
                    padding: EdgeInsets.only(right: 16),
                    child: Icon(
                      Icons.add_circle,
                      size: 33,
                    ),
                  ),
                ),
              ),
            ],
            title: const Center(
              child: Column(
                children: [
                  HeadTitle(),
                  P1Text(
                    'Администратор',
                    color: ColorData.colorMainLink,
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
