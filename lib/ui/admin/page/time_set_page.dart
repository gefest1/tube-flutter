import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:tube/utils/parse_time.dart';

class TimeSet extends StatefulWidget {
  final String title;
  final BehaviorSubject<DateTime>? value;
  final DateTime? startTime;
  final DateTime? endTime;
  final Duration? interval;
  final EdgeInsets padding;

  const TimeSet({
    required this.title,
    this.value,
    this.startTime,
    this.endTime,
    this.interval,
    this.padding = const EdgeInsets.symmetric(horizontal: 20),
    Key? key,
  }) : super(key: key);

  @override
  State<TimeSet> createState() => _TimeSetState();
}

class _TimeSetState extends State<TimeSet> {
  late final _timeInterval = widget.interval ?? const Duration(minutes: 15);

  late final BehaviorSubject<DateTime> _curDurations =
      widget.value ?? BehaviorSubject.seeded(_durations.first);

  late final FixedExtentScrollController scrollController =
      FixedExtentScrollController(initialItem: _lastHapticIndex);

  @override
  void dispose() {
    _curDurations.close();
    scrollController.dispose();
    super.dispose();
  }

  late final List<DateTime> _durations = () {
    List<DateTime> list = [];
    for (int index = 0; true; index++) {
      final startTime = widget.startTime ?? getStratTime();
      final generated = startTime.add(_timeInterval * index);
      final endTime = widget.endTime ?? getStratTime().add(const Duration(days: 1));

      if (generated.compareTo(endTime) != -1) {
        break;
      }
      list.add(generated);
    }

    return list;
  }();

  late int _lastHapticIndex =
      (_curDurations.value.difference(widget.startTime ?? getStratTime()).inSeconds /
              _timeInterval.inSeconds)
          .round();

  void _handleSelectedItemChanged(int indexDirt) {
    _curDurations.value = _durations[indexDirt % _durations.length];
    final index = indexDirt % _durations.length;
    if (index != _lastHapticIndex) {
      _lastHapticIndex = index;
      HapticFeedback.selectionClick();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: widget.padding,
              child: Container(
                height: 146,
                decoration: BoxDecoration(
                  color: const Color(0xffF7F7F7),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: ClipRect(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: Row(
                          children: [
                            Expanded(
                              child: H5Text(
                                widget.title,
                                color: ColorData.colorTextMain,
                              ),
                            ),
                            StreamBuilder(
                              stream: _curDurations.stream,
                              builder: (ctx, AsyncSnapshot<DateTime> asyncSnapshot) {
                                final value = asyncSnapshot.data ?? _durations.first;

                                return H5Text(
                                  DateFormat("HH:mm").format(
                                    value.parseTimeGG,
                                  ),
                                  color: ColorData.colorTextMain,
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10),
                          child: Stack(
                            children: [
                              Center(
                                child: Container(
                                  margin: const EdgeInsets.symmetric(horizontal: 10),
                                  height: 41,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                ),
                              ),
                              ListWheelScrollView.useDelegate(
                                itemExtent: 41,
                                clipBehavior: Clip.none,
                                overAndUnderCenterOpacity: 0.3,
                                diameterRatio: 1.2,
                                controller: scrollController,
                                onSelectedItemChanged: _handleSelectedItemChanged,
                                squeeze: 1.4,
                                physics: const FixedExtentScrollPhysics(),
                                childDelegate: ListWheelChildBuilderDelegate(builder: (ctx, index) {
                                  final cur = _durations[index % _durations.length];
                                  return SizedBox(
                                    height: 41,
                                    child: Center(
                                      child: H3Text(
                                        DateFormat("HH mm").format(
                                          cur.parseTimeGG,
                                        ),
                                        color: ColorData.colorElementsActive,
                                      ),
                                    ),
                                  );
                                }),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
