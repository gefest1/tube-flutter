import 'dart:math';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:fuzzywuzzy/fuzzywuzzy.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/ui/admin/molecules/client_card.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/search_text_field.dart';

class ClientListPage extends StatefulWidget {
  final String clubId;
  final List<User> users;

  const ClientListPage({
    required this.clubId,
    this.users = const [],
    super.key,
  });

  @override
  State<ClientListPage> createState() => _ClientListPageState();
}

class _ClientListPageState extends State<ClientListPage> {
  final controller = TextEditingController();
  late Map<String, User> globalMapUsers = Map.fromEntries(
    widget.users.map(
      (e) => MapEntry(e.id!, e),
    ),
  );

  List<User> filter() {
    if (controller.text.isEmpty) return widget.users;
    final query = controller.text.replaceAll(" ", "").toLowerCase();
    final queryMap = Map.fromEntries(
      extractAllSorted<User>(
        query: query,
        choices: widget.users,
        getter: (obj) => (obj.fullName ?? '').replaceAll(" ", "").toLowerCase(),
      ).map(
        (e) => MapEntry(e.choice.id, e.score),
      ),
    );
    extractAllSorted<User>(
      query: query,
      choices: widget.users,
      getter: (obj) => (obj.email ?? '').replaceAll(" ", "").toLowerCase(),
    ).forEach(
      (element) {
        queryMap[element.choice.id] = max(
          queryMap[element.choice.id]!,
          element.score,
        );
      },
    );
    final users = queryMap.entries
        .sorted((a, b) => b.value.compareTo(a.value))
        .map((e) => globalMapUsers[e.key]!)
        .toList();
    return users;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final focus = FocusScope.of(context);
        if (focus.hasFocus || focus.hasPrimaryFocus) {
          focus.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          title: const H4Text(
            'Клиенты',
            color: Colors.white,
          ),
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(46),
            child: Padding(
              padding: const EdgeInsets.only(bottom: 10, left: 20, right: 20),
              child: Align(
                alignment: Alignment.topCenter,
                child: SearchTextField(
                  controller: controller,
                ),
              ),
            ),
          ),
        ),
        body: ValueListenableBuilder(
          valueListenable: controller,
          builder: (context, _, child) {
            final users = filter();
            return ListView.builder(
              itemCount: users.length,
              padding: const EdgeInsets.symmetric(horizontal: 20),
              itemBuilder: (context, index) => ClientCard(
                email: users[index].email,
                fullName: users[index].fullName,
                tariffCount: 0,
                serviceCount: 0,
                photoUrl: users[index].photoUrl,
                phoneNumber: users[index].phoneNumber,
              ),
            );
          },
        ),
      ),
    );
  }
}
