import 'package:flutter/material.dart';
import 'package:tube/ui/admin/molecules/trainer_tile.dart';
import 'package:tube/ui/admin/page/register_trainer_page.dart';

import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class TrainerDetailPage extends StatefulWidget {
  const TrainerDetailPage({Key? key}) : super(key: key);

  @override
  State<TrainerDetailPage> createState() => _TrainerDetailPageState();
}

class _TrainerDetailPageState extends State<TrainerDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        children: [
          const SizedBox(height: 40),
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(bottom: 8.0),
                        child: H5Text(
                          'Баланс',
                          color: Color(0xff959595),
                        ),
                      ),
                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          RichText(
                            text: const TextSpan(
                              text: '0',
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'InterTight',
                              ),
                              children: [
                                TextSpan(
                                  text: '₸',
                                  style: TextStyle(
                                    fontFamily: '',
                                  ),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(width: 5),
                          const P2Text(
                            'На 02.12.2021',
                            color: ColorData.colortTextSecondary,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                const Icon(
                  Icons.chevron_right,
                  size: 24,
                  color: ColorData.colorElementsSecondary,
                )
              ],
            ),
          ),
          const Divider(
            height: 0,
            thickness: 0.5,
            color: Color(0xffA5A5A5),
          ),
          const SizedBox(height: 36),
          const Padding(
            padding: EdgeInsets.only(bottom: 10.0),
            child: H5Text(
              'Абонементы',
              color: Color(0xff959595),
            ),
          ),
          const SizedBox(height: 10),
          SizeTapAnimation(
            child: GestureDetector(
              onTap: () {},
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: const Color(0xffF5F5F5),
                ),
                padding: const EdgeInsets.all(13),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.control_point_duplicate,
                      color: ColorData.colorMainLink,
                    ),
                    SizedBox(width: 5),
                    P1Text(
                      'Добавить абонемент',
                      color: ColorData.colorMainLink,
                    )
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(height: 20),
          const Padding(
            padding: EdgeInsets.only(bottom: 10.0),
            child: H5Text(
              'Услуги',
              color: Color(0xff959595),
            ),
          ),
          const SizedBox(height: 10),
          SizeTapAnimation(
            child: GestureDetector(
              onTap: () {},
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: const Color(0xffF5F5F5),
                ),
                padding: const EdgeInsets.all(13),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.add_to_photos,
                      color: ColorData.colorMainLink,
                    ),
                    SizedBox(width: 5),
                    P1Text(
                      'Добавить услугу',
                      color: ColorData.colorMainLink,
                    )
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(height: 20),
          const Padding(
            padding: EdgeInsets.only(bottom: 10.0),
            child: H5Text(
              'Сотрудники',
              color: Color(0xff959595),
            ),
          ),
          const SizedBox(height: 5),
          TrainerTile(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => const RegisterTrainerPage(),
                ),
              );
            },
            photoUrl:
                'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg',
            title: 'jeanwwd@gmail.com',
            subTitle: 'Завершите добавление сотрудника',
          ),
        ],
      ),
    );
  }
}
