import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tube/ui/admin/page/tariff/molecules/edit_tariff_header.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/shedule_table.dart';
import 'package:tube/ui/trainer_page/components/molecules/shedule_big_card.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/infinite_listview.dart';

class SelectDeadlinePage extends StatefulWidget {
  final String title;

  const SelectDeadlinePage({
    required this.title,
    super.key,
  });

  @override
  State<SelectDeadlinePage> createState() => _SelectDeadlinePageState();
}

class _SelectDeadlinePageState extends State<SelectDeadlinePage> {
  late final InfiniteScrollController infiniteScrollController =
      InfiniteScrollController();
  final lastIndex = BehaviorSubject.seeded(0);
  late final _sd = DateTime.now();
  late final _startTime = DateTime(
    _sd.year,
    _sd.month,
    _sd.day,
  );
  late final BehaviorSubject<DateTime?> dateTimeSubject =
      BehaviorSubject<DateTime?>.seeded(_startTime);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Column(
          children: [
            const H4Text(
              'Остановка абонемента',
            ),
            P1Text(widget.title)
          ],
        ),
      ),
      body: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Column(
                  children: [
                    const SizedBox(height: 20),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(
                        color: ColorData.color3Percent,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        children: [
                          const SizedBox(height: 10),
                          const H5Text(
                            'Дата возобновления занятий',
                            color: ColorData.colorTextMain,
                          ),
                          const SizedBox(height: 10),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: EditTariffHeader(
                              controller: infiniteScrollController,
                              lastIndex: lastIndex,
                              currentYear: _startTime.year,
                              currentMonth: _startTime.month,
                            ),
                          ),
                          const SizedBox(height: 10),
                          SheduleTable(
                            controller: infiniteScrollController,
                            lastIndexSubject: lastIndex,
                            dateTimeSubject: dateTimeSubject,
                            onTap: (_) {
                              if (dateTimeSubject.value != null &&
                                  _.isSameDate(dateTimeSubject.value!)) {
                                dateTimeSubject.value = null;
                              } else {
                                dateTimeSubject.value = _;
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 20),
                    Container(
                      decoration: BoxDecoration(
                        color: ColorData.color3Percent,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 10),
                      child: const Row(
                        children: [
                          Icon(
                            Icons.error_outline,
                            color: ColorData.colorStrokeIncorrect,
                            size: 24,
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: P1Text(
                              'Пока абонемент на паузе, дата его окончания у всех клиентов продлевается. Расписание клиентов остается не тронутым',
                              color: ColorData.colorTextMain,
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(height: 10),
                    Container(
                      decoration: BoxDecoration(
                        color: ColorData.color3Percent,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 10),
                      child: const Row(
                        children: [
                          Icon(
                            Icons.error_outline,
                            color: ColorData.colorStrokeIncorrect,
                            size: 24,
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: P1Text(
                              'При покупке абонемента, датой начала абонемента будет конец паузы',
                              color: ColorData.colorTextMain,
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(height: 220),
                  ],
                ),
              )
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: BottomButton(
              title: 'Сохранить',
              onTap: () {
                Navigator.pop<({DateTime? date})>(
                  context,
                  (date: dateTimeSubject.value),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
