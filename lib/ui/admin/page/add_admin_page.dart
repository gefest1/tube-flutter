import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/ui/add_compnay/pages/share_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/cards_center_share.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/uri_parse.dart';

class AddAdminPage extends StatefulWidget {
  const AddAdminPage({Key? key}) : super(key: key);

  @override
  State<AddAdminPage> createState() => _AddAdminPageState();
}

class _AddAdminPageState extends State<AddAdminPage> {
  void shareGmail() async {
    final newUsers = await showFlexibleBottomSheet<List<String>>(
      minHeight: 0,
      initHeight: 0.95,
      maxHeight: 0.95,
      context: context,
      anchors: [0.95],
      builder: (ctx, controller, _) {
        return Material(
          color: Colors.transparent,
          child: ShareSheetPage(
            controller: controller,
          ),
        );
      },
    );
    if (newUsers == null) return;
    if (!mounted) return;
    final mutationRes = await graphqlClient.mutate$inviteManyUsers(
      Options$Mutation$inviteManyUsers(
        variables: Variables$Mutation$inviteManyUsers(
          clubId: MainDrawerState.currentParticipance!.clubId!,
          emails: newUsers,
          status: TypeInviteEnum.InviteTrainer.getEnum,
        ),
      ),
    );

    if (mutationRes.hasException) {
      log(mutationRes.exception.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const H4Text(
          'Добавление сотрудников',
          color: Colors.white,
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.only(top: 40, left: 20, right: 20),
        children: [
          const P0Text(
            'Пригласите ваших работников в Checkallnow. Вы сможете назначить роли позже',
            color: ColorData.colorElementsSecondary,
          ),
          const SizedBox(height: 20),
          CardsCenterShare(
            shareLinkTap: () async {
              Share.share(uriParse(
                {
                  "clubId": MainDrawerState.currentParticipance!.clubId!,
                  "type": TypeInviteEnum.InviteTrainer.toString(),
                  // TypeInviteEnum.InviteTrainer.toString(),
                },
              ));
              // await Clipboard.setData(
              //   ClipboardData(
              //     text: uriParse(
              //       {
              //         "clubId": MainDrawerState.currentParticipance!.clubId!,
              //         "type": TypeInviteEnum.InviteTrainer.toString(),
              //         // TypeInviteEnum.InviteTrainer.toString(),
              //       },
              //     ),
              //   ),
              // );
            },
            emailTap: shareGmail,

            //  () {
            //   shareGmail()
            //   Navigator.push(
            //     context,
            //     MaterialPageRoute(
            //       builder: (_) => const AddFieldByMail(),
            //     ),
            //   );
            // },
          ),
        ],
      ),
    );
  }
}
