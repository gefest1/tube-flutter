import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/ui/admin/page/time_set_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/fast_app_bar.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/parse_time.dart';

class SetWorktimeSheet extends StatefulWidget {
  final ScrollController controller;
  final DateTime? startTime;
  final DateTime? endTime;
  final String title;

  const SetWorktimeSheet({
    required this.controller,
    this.startTime,
    this.endTime,
    this.title = 'Понедельник',
    Key? key,
  }) : super(key: key);

  @override
  State<SetWorktimeSheet> createState() => _SetWorktimeSheetState();
}

class _SetWorktimeSheetState extends State<SetWorktimeSheet> {
  late final BehaviorSubject<DateTime> firstValue =
      BehaviorSubject<DateTime>.seeded(widget.startTime ?? getStratTime());

  late final BehaviorSubject<DateTime> secondValue =
      BehaviorSubject<DateTime>.seeded(widget.endTime ?? getStratTime());

  @override
  void dispose() {
    firstValue.close();
    secondValue.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return GestureDetector(
      onTap: () {
        FocusScopeNode focusScope = FocusScope.of(context);

        if (focusScope.hasFocus || focusScope.hasPrimaryFocus) {
          focusScope.unfocus();
        }
      },
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(10),
          ),
        ),
        child: Stack(
          children: [
            CustomScrollView(
              controller: widget.controller,
              slivers: [
                SliverToBoxAdapter(
                  child: FastAppBar(
                    title: widget.title,
                    suffIcon: Icons.close,
                    suffOnTap: () => Navigator.pop(context),
                    suffTextOnTap: () => Navigator.pop(context),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 20),
                      TimeSet(
                        title: 'Начало работы',
                        value: firstValue,
                      ),
                      const SizedBox(height: 20),
                      TimeSet(
                        title: 'Конец работы',
                        value: secondValue,
                      ),
                      const SizedBox(height: 40),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () {
                              // final List<DateTime> _ = <DateTime>[];
                              Navigator.pop(context, <DateTime>[]);
                            },
                            child: const IgnorePointer(
                              child: P0Text(
                                'Сделать выходным',
                                color: ColorData.colorMainNo,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SliverToBoxAdapter(
                  child: SizedBox(
                    height: mediaQuery.viewInsets.bottom,
                  ),
                ),
              ],
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: Container(
                padding: EdgeInsets.only(
                  // vertical: 10,
                  left: 20,
                  right: 20,
                  top: 10,
                  bottom: mediaQuery.padding.bottom == 0
                      ? 10
                      : mediaQuery.padding.bottom,
                ),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0x1a000000),
                      offset: Offset(0, -0.5),
                    ),
                  ],
                ),
                child: CustomButton(
                  title: 'Сохранить',
                  onTap: () async {
                    final endValue =
                        secondValue.value.compareTo(firstValue.value) == 1
                            ? secondValue.value
                            : secondValue.value.add(const Duration(days: 1));
                    Navigator.pop(
                      context,
                      [firstValue.value, endValue],
                    );
                  },
                  color: ColorData.colorMainLink,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
