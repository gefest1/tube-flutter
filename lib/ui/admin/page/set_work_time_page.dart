import 'package:flutter/material.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/ui/admin/page/set_worktime_sheet.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/main_page/components/atoms/day_card.dart';
import 'package:tube/ui/main_page/components/organisms/active_time_card.dart';

class SetWorkTimePage extends StatefulWidget {
  const SetWorkTimePage({Key? key}) : super(key: key);

  @override
  State<SetWorkTimePage> createState() => _SetWorkTimePageState();
}

class _SetWorkTimePageState extends State<SetWorkTimePage> {
  void _openSheet() async {
    await showFlexibleBottomSheet(
      minHeight: 0,
      initHeight: 0.95,
      maxHeight: 0.95,
      context: context,
      anchors: [0.95],
      builder: (ctx, controller, _) {
        return Material(
          color: Colors.transparent,
          child: SetWorktimeSheet(
            controller: controller,
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const Column(
          children: [
            H4Text(
              'jeanwwd@gmail.com',
              color: Colors.white,
            ),
            Padding(
              padding: EdgeInsets.only(top: 2),
              child: P1Text(
                'Иван И.И.',
                color: Colors.white,
              ),
            )
          ],
        ),
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(
            Icons.arrow_back,
            size: 33,
          ),
          color: Colors.white,
        ),
      ),
      body: ListView.separated(
        itemCount: 7,
        padding: const EdgeInsets.only(
          top: 40,
          left: 20,
          right: 20,
          bottom: 40,
        ),
        itemBuilder: (ctx, index) => index % 2 == 1
            ? ActiveTimeCard(
                onTap: _openSheet,
                label: 'Изменить',
                title: 'Понедельник',
                subLabel: '10:00 — 21:00',
              )
            : DayCard(
                title: 'Понедельник',
                label: 'Выбрать время',
                onTap: _openSheet,
              ),
        separatorBuilder: (ctx, index) => const SizedBox(height: 10),
        // children: [],
      ),
    );
  }
}
