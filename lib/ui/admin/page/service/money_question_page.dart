import 'dart:async';
import 'dart:developer';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/admin/booking_admin/booking_admin_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/admin/create.booking.model.dart';
import 'package:tube/logic/model/sub_booking.model.dart';
import 'package:tube/ui/admin/atoms/custom_check_box.dart';
import 'package:tube/ui/common/atoms/animated_close.dart';

import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/load_popup_route.dart';
import 'package:tube/utils/terms_tap.dart';

const percentInputDecoration = InputDecoration(
  hintText: '10%',
  suffixStyle: TextStyle(
    color: Colors.black,
    fontSize: P0TextStyle.fontSize,
    height: P0TextStyle.height,
    fontWeight: P0TextStyle.fontWeight,
  ),
  prefixIconConstraints: BoxConstraints(
    maxHeight: 24,
    maxWidth: 29,
    minHeight: 24,
    minWidth: 29,
  ),
  hintStyle: TextStyle(
    color: Color(0xff969696),
    fontSize: P0TextStyle.fontSize,
    height: P0TextStyle.height,
    fontWeight: P0TextStyle.fontWeight,
  ),
  enabledBorder: UnderlineInputBorder(
    borderSide: BorderSide(
      color: Colors.black,
      width: 0.5,
    ),
  ),
  focusedBorder: UnderlineInputBorder(
    borderSide: BorderSide(
      color: Colors.black,
      width: 0.5,
    ),
  ),
);

const pricePercentInputDecoration = InputDecoration(
  hintText: '1 тенге',
  suffixStyle: TextStyle(
    color: Colors.black,
    fontSize: P0TextStyle.fontSize,
    height: P0TextStyle.height,
    fontWeight: P0TextStyle.fontWeight,
  ),
  prefixIconConstraints: BoxConstraints(
    maxHeight: 24,
    maxWidth: 29,
    minHeight: 24,
    minWidth: 29,
  ),
  hintStyle: TextStyle(
    color: Color(0xff969696),
    fontSize: P0TextStyle.fontSize,
    height: P0TextStyle.height,
    fontWeight: P0TextStyle.fontWeight,
  ),
  enabledBorder: UnderlineInputBorder(
    borderSide: BorderSide(
      color: Colors.black,
      width: 0.5,
    ),
  ),
  focusedBorder: UnderlineInputBorder(
    borderSide: BorderSide(
      color: Colors.black,
      width: 0.5,
    ),
  ),
);

enum TypeChange {
  cost,
  percent,
  percentPrice,
}

class MoneyQuestionPage extends StatefulWidget {
  const MoneyQuestionPage({Key? key}) : super(key: key);

  @override
  State<MoneyQuestionPage> createState() => _MoneyQuestionPageState();
}

class _MoneyQuestionPageState extends State<MoneyQuestionPage>
    with MoneyQuestionExtension {
  bool trial = false;
  final trialController = TextEditingController();

  @override
  late final TextEditingController costController = TextEditingController()
    ..addListener(() => update(TypeChange.cost));
  @override
  late final TextEditingController percentController = TextEditingController()
    ..addListener(() => update(TypeChange.percent));
  @override
  late final TextEditingController percentPriceController =
      TextEditingController()
        ..addListener(() => update(TypeChange.percentPrice));

  @override
  bool get check => (percentLastValue == percentController.text &&
      percentPriceController.text == percentPriceLastValue);
  @override
  final StreamController streamController = StreamController();

  void _submit() async {
    try {
      final completer = Completer();
      Navigator.push(
        context,
        LoadPopupRoute(completer: completer),
      );
      // final SubBooking? s;
      // TODO REMAKE
      final creatBookingModel =
          Provider.of<CreateBookingModel>(context, listen: false);
      creatBookingModel.setNewInstance(
        creatBookingModel.booking.copyWith(
          price: num.parse(costController.text),
          percent: num.parse(percentController.text),
          subBooking: [
            if (trial)
              SubBooking(
                deadlineDays: 30,
                count: 1,
                price: trialController.text.isEmpty
                    ? 10.0
                    : double.parse(trialController.text),
              ),
          ],
        ),
      );

      final BookingAdminBloc bookingAdminBloc = context
          .read<AdminBlocMap>()
          .getBookingAdminBloc(MainDrawerState.currentParticipance!.clubId!);
      bookingAdminBloc.add(
        CreateBookingAdminEvent(
          booking: creatBookingModel.booking,
          workTime: creatBookingModel.workTime,
          completer: completer,
        ),
      );
      await completer.future;
      if (!mounted) return;
      Navigator.popUntil(context, (route) => route.isFirst);
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final focus = FocusScope.of(context);

        if (focus.hasFocus || focus.hasPrimaryFocus) {
          focus.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: const H4Text(
            'Деньги',
            color: Colors.white,
          ),
          leading: IconButton(
            onPressed: () => Navigator.pop(context),
            icon: const Icon(
              Icons.arrow_back,
              size: 33,
            ),
          ),
        ),
        body: CustomScrollView(
          slivers: [
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              sliver: SliverToBoxAdapter(
                child: Column(
                  children: [
                    const SizedBox(height: 20),
                    Row(
                      children: [
                        Expanded(
                          child: CustomTextField(
                            hintText: '10',
                            title: 'Стоимость абонемента',
                            controller: costController,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                RegExp(r'[+-]?([0-9]*[.])?[0-9]+'),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Container(
                            width: 105,
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 13),
                            decoration: const BoxDecoration(
                              color: Color(0xffF7F7F7),
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                            ),
                            child: const P1Text(
                              'Тенге',
                              color: ColorData.colorElementsActive,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Container(
                      padding: const EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                        border: Border.all(
                          color: const Color(0xffA5A5A5),
                          width: 0.5,
                        ),
                      ),
                      child: const Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: Icon(
                              Icons.warning_amber,
                              color: ColorData.colorElementsActive,
                            ),
                          ),
                          Flexible(
                            child: P1Text(
                              'Комиссия Checkallnow. при каждой покупке ваших продуктов = 3.9%',
                              color: ColorData.colorTextMain,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 10),
                    SizeTapAnimation(
                      onTap: () {
                        setState(() {
                          trial = !trial;
                        });
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            CustomCheckBox(
                              isCircle: true,
                              active: trial,
                            ),
                            const SizedBox(width: 14),
                            const P1Text(
                              'Пробное занятие',
                              color: ColorData.colorMainElements,
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    AnimatedFastClose(
                      active: trial,
                      child: CustomTextField(
                        hintText: '10 тенге',
                        controller: trialController,
                        title: 'Стоимость пробного занятия',
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly,
                        ],
                      ),
                    ),
                    const SizedBox(height: 10),
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                RegExp(r'^[1-9][0-9]?$|^100$'),
                              ),
                            ],
                            cursorColor: Colors.black,
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: P0TextStyle.fontSize,
                              height: P0TextStyle.height,
                              fontWeight: P0TextStyle.fontWeight,
                            ),
                            textAlign: TextAlign.center,
                            controller: percentController,
                            decoration: percentInputDecoration,
                          ),
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: TextField(
                            controller: percentPriceController,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                RegExp(r'[+-]?([0-9]*[.])?[0-9]+'),
                              ),
                            ],
                            cursorColor: Colors.black,
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: P0TextStyle.fontSize,
                              height: P0TextStyle.height,
                              fontWeight: P0TextStyle.fontWeight,
                            ),
                            textAlign: TextAlign.center,
                            decoration: pricePercentInputDecoration,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    const Align(
                      alignment: Alignment.centerLeft,
                      child: P1Text(
                        'Сколько процентов получает сотрудник с абонимента',
                        color: ColorData.colorElementsActive,
                      ),
                    ),
                    const SizedBox(height: 20),
                    Container(
                      padding: const EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                        border: Border.all(
                          color: const Color(0xffA5A5A5),
                          width: 0.5,
                        ),
                      ),
                      child: const Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: Icon(
                              Icons.warning_amber,
                              color: ColorData.colorElementsActive,
                            ),
                          ),
                          Flexible(
                            child: P1Text(
                              'Сумма зарплаты сотрудника зависит от проведенных им занятий по абонементу',
                              color: ColorData.colorTextMain,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    StreamBuilder(
                      stream: streamController.stream,
                      builder: (context, snap) {
                        final active = costController.text.isNotEmpty &&
                            percentController.text.isNotEmpty &&
                            percentPriceController.text.isNotEmpty;
                        return CustomButton(
                          onTap: active ? _submit : null,
                          title: 'Создать Абонимент',
                          titleColor: active
                              ? Colors.white
                              : ColorData.colortTextSecondary,
                          color: active
                              ? ColorData.colorMainLink
                              : ColorData.colorElementsLightMenu,
                        );
                      },
                    ),
                    const SizedBox(height: 10),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        style: const TextStyle(
                          fontFamily: 'InterTight',
                          fontSize: P2TextStyle.fontSize,
                          fontWeight: P2TextStyle.fontWeight,
                          height: P2TextStyle.height,
                          color: ColorData.colorTextMain,
                        ),
                        children: [
                          const TextSpan(
                              text: 'Создавая услугу вы соглашаетесь '),
                          TextSpan(
                            text: 'с условием использования приложения',
                            style: const TextStyle(
                              color: ColorData.colorMainLink,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = termsTap,
                          ),
                          const TextSpan(text: ' и '),
                          TextSpan(
                            text: 'политикой конфиденциально',
                            style:
                                const TextStyle(color: ColorData.colorMainLink),
                            recognizer: TapGestureRecognizer()
                              ..onTap = politicTap,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

mixin MoneyQuestionExtension {
  abstract final TextEditingController costController;
  abstract final TextEditingController percentController;
  abstract final TextEditingController percentPriceController;

  String? percentLastValue;
  String? percentPriceLastValue;

  bool get check => (percentLastValue == percentController.text &&
      percentPriceController.text == percentPriceLastValue);

  StreamController get streamController;

  void update(TypeChange type) {
    streamController.add(null);
    if (type == TypeChange.cost) {
      if (percentController.text.isEmpty || costController.text.isEmpty) return;
      percentLastValue = percentController.text;

      percentPriceLastValue = (double.parse(costController.text) *
              double.parse(percentController.text) /
              100)
          .toString();

      percentPriceController.text = percentPriceLastValue!;
    }

    if (type == TypeChange.percent) {
      if (costController.text.isEmpty ||
          check ||
          percentController.text.isEmpty) return;
      percentLastValue = percentController.text;

      percentPriceLastValue = (double.parse(costController.text) *
              double.parse(percentController.text) /
              100)
          .toString();

      percentPriceController.text = percentPriceLastValue!;
    }

    if (type == TypeChange.percentPrice) {
      if (costController.text.isEmpty ||
          check ||
          percentPriceController.text.isEmpty) return;
      if (double.parse(percentPriceController.text) >
          double.parse(costController.text)) {
        percentPriceController.clear();
        percentController.clear();
        return;
      }

      percentPriceLastValue = percentPriceController.text;
      percentLastValue = (double.parse(percentPriceController.text) /
              double.parse(costController.text) *
              100)
          .toString();

      percentController.text = percentLastValue!;
    }
  }
}
