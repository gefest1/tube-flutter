import 'package:flutter/material.dart';
import 'package:tube/ui/admin/molecules/add_trainer_tile.dart';
import 'package:tube/ui/admin/page/service/money_question_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/utils/colors.dart';

class SetTrainerServicePage extends StatefulWidget {
  const SetTrainerServicePage({Key? key}) : super(key: key);

  @override
  State<SetTrainerServicePage> createState() => _SetTrainerServicePageState();
}

class _SetTrainerServicePageState extends State<SetTrainerServicePage> {
  final List<bool> _activeIndexes = List.generate(5, (index) => false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            size: 33,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: CustomScrollView(
              clipBehavior: Clip.none,
              slivers: [
                const SliverPadding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  sliver: SliverToBoxAdapter(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 40),
                        H3Text(
                          'Добавьте сотрудников',
                          color: ColorData.colorTextMain,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 5.0),
                          child: P1Text(
                            'Время записи на услугу будет зависеть от свободного времени сорудника',
                            color: ColorData.colortTextSecondary,
                          ),
                        ),
                        SizedBox(height: 20),
                      ],
                    ),
                  ),
                ),
                SliverPadding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  sliver: SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (ctx, index) {
                        final int itemIndex = index ~/ 2;

                        if (index.isEven) {
                          return AddTrainerTile(
                            onTap: () {
                              setState(() {
                                _activeIndexes[itemIndex] =
                                    !_activeIndexes[itemIndex];
                              });
                            },
                            active: _activeIndexes[itemIndex],
                          );
                        }
                        return const SizedBox(height: 10);
                      },
                      childCount: _activeIndexes.length * 2 - 1,
                    ),
                  ),
                ),
                const SliverToBoxAdapter(
                  child: SizedBox(height: 40),
                ),
              ],
            ),
          ),
          BottomButton(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => const MoneyQuestionPage(),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
