// ignore: unused_import
import 'dart:async';
import 'dart:developer';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/admin/workers/worker_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/ui/admin/molecules/bottom_admin_bar.dart';
import 'package:tube/ui/admin/page/main_admin_body.dart';
import 'package:tube/ui/admin/page/shedule_table_admin.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/main_page/notfication_page.dart';
import 'package:tube/ui/main_page/profile_page.dart';

class MainAdminPage extends StatefulWidget {
  final String clubId;

  const MainAdminPage({
    required this.clubId,
    Key? key,
  }) : super(key: key);

  @override
  State<MainAdminPage> createState() => _MainAdminPageState();
}

class _MainAdminPageState extends State<MainAdminPage>
    with SingleTickerProviderStateMixin {
  late final TabController controller = TabController(
    vsync: this,
    length: 4,
  );
  @override
  void initState() {
    super.initState();
    Provider.of<AdminBlocMap>(context, listen: false)
        .getWorkerBloc(widget.clubId)
        .add(const GetWorkWorkerEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: const MainDrawer(),
      body: Stack(
        children: [
          TabBarView(
            controller: controller,
            children: [
              MainAdminBody(clubId: widget.clubId),
              const SheduleTableAdminPage(),
              const NotificationPage(),
              const ProfilePage(),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: MainAdminBottom(
              tabController: controller,
            ),
          ),
        ],
      ),
    );
  }
}
