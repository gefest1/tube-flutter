import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/logic/provider/create_big_service/create_big_service.dart';
import 'package:tube/ui/admin/page/big_service/big_service_trainers.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/duration_wheel.dart';

class CreateBigServicePage extends StatefulWidget {
  const CreateBigServicePage({super.key});

  @override
  State<CreateBigServicePage> createState() => _CreateBigServicePageState();
}

class _CreateBigServicePageState extends State<CreateBigServicePage> {
  late final nameController = TextEditingController()..addListener(update);
  late final descriptionController = TextEditingController()
    ..addListener(update);
  late final durationSub = BehaviorSubject<Duration>.seeded(const Duration())
    ..listen((value) => update());
  final activeSub = BehaviorSubject.seeded(false);

  void update() {
    activeSub.value = nameController.text.isNotEmpty &&
        descriptionController.text.isNotEmpty &&
        durationSub.value != const Duration();
  }

  @override
  void dispose() {
    nameController.dispose();
    descriptionController.dispose();
    durationSub.close();
    super.dispose();
  }

  CreateBigServiceProvider get createProvider =>
      Provider.of<CreateBigServiceProvider>(context, listen: false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const H4Text(
          'Добавление услуги',
          color: Colors.white,
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
      ),
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 40),
                      // Icon(
                      //   Icons.perm_contact_calendar,
                      //   color: Color(0xff404040),
                      //   size: 36,
                      // ),
                      // SizedBox(height: 10),
                      // H3Text('Для добавления услуги нужны сотрудники'),
                      CustomTextField(
                        title: 'Название услуги',
                        hintText: 'Например: «Детский массаж»',
                        controller: nameController,
                      ),
                      const SizedBox(height: 30),
                      CustomTextField(
                        hintText: 'Опишите услугу',
                        title:
                            'Описание. Поможет вашим клиентам выбрать именно то, что им нужно',
                        controller: descriptionController,
                      ),
                      const SizedBox(height: 30),
                      DurationTitleWheel(
                        durationSubject: durationSub,
                      ),
                      const SizedBox(height: 160),
                    ],
                  ),
                ),
              )
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: StreamBuilder(
                stream: activeSub,
                builder: (context, _) {
                  return BottomButton(
                    active: activeSub.value,
                    title: 'Продолжить',
                    onTap: () async {
                      final value = createProvider.createBigService;

                      createProvider.setNewInstance(
                        value.copyWith(
                          name: nameController.text,
                          description: descriptionController.text,
                          duration: durationSub.value,
                        ),
                      );
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => const BigServiceTrainerPage(),
                        ),
                      );
                      createProvider.setNewInstance(value);
                    },
                  );
                }),
          )
        ],
      ),
    );
  }
}
