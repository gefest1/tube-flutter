import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';

import 'package:tube/logic/blocs/admin/workers/worker_bloc.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/provider/create_big_service/create_big_service.dart';
import 'package:tube/ui/admin/atoms/custom_check_box.dart';
import 'package:tube/ui/admin/page/big_service/pay_big_service_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/shadow.dart';

class BigServiceTrainerPage extends StatefulWidget {
  const BigServiceTrainerPage({super.key});

  @override
  State<BigServiceTrainerPage> createState() => _BigServiceTrainerPageState();
}

class _BigServiceTrainerPageState extends State<BigServiceTrainerPage> {
  late final workerBloc = Provider.of<AdminBlocMap>(context, listen: false)
      .getWorkerBloc(MainDrawerState.currentParticipance!.clubId!);
  final activeSub = BehaviorSubject.seeded(false);
  final Map<String, bool> usersMap = {};
  void update() {
    usersMap.removeWhere((key, value) {
      return value == false;
    });
    activeSub.value = usersMap.isNotEmpty;
  }

  CreateBigServiceProvider get createProvider =>
      Provider.of<CreateBigServiceProvider>(context, listen: false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const H4Text(
          'Сотрудники',
          color: Colors.white,
        ),
      ),
      body: Stack(
        children: [
          CustomScrollView(
            slivers: [
              const SliverToBoxAdapter(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 40),
                      H3Text(
                        'Добавьте сотрудников',
                        color: ColorData.colorTextMain,
                      ),
                      SizedBox(height: 5),
                      P1Text(
                        'Время записи на услугу будет зависеть от свободного времени сотрудника',
                        color: ColorData.colortTextSecondary,
                      ),
                      SizedBox(height: 20),
                    ],
                  ),
                ),
              ),
              BlocBuilder<WorkerBloc, WorkerState>(
                builder: (ctx, state) {
                  if (state is! ClubWorkerState) {
                    return const SliverToBoxAdapter();
                  }
                  List<User> users = state.mapWorkers;

                  return SliverPadding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    sliver: SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (ctx, index) {
                          if (index.isOdd) return const SizedBox(height: 16);
                          return SizeTapAnimation(
                            onTap: () {
                              setState(() {
                                usersMap[users[index ~/ 2].id!] =
                                    !(usersMap[users[index ~/ 2].id] ?? false);
                                update();
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15),
                                boxShadow: blocksMains,
                              ),
                              padding: const EdgeInsets.all(15),
                              child: Row(
                                children: [
                                  const CircleAvatar(),
                                  const SizedBox(width: 10),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        H4Text(
                                          users[index ~/ 2].fullName ??
                                              users[index ~/ 2].email ??
                                              "",
                                          color: ColorData.colorTextMain,
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(width: 10),
                                  CustomCheckBox(
                                    active: usersMap[users[index ~/ 2].id!] ??
                                        false,
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                        childCount: users.length,
                      ),
                    ),
                  );
                },
                bloc: workerBloc,
              ),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: StreamBuilder(
                stream: activeSub,
                builder: (context, _) {
                  return BottomButton(
                    title: 'Сохранить',
                    active: activeSub.value,
                    onTap: () async {
                      final value = createProvider.createBigService;
                      createProvider.setNewInstance(
                        value.copyWith(
                          trainerIds: usersMap.keys.toList(),
                        ),
                      );
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => const PayBigServicePage(),
                        ),
                      );
                      createProvider.setNewInstance(value);
                    },
                  );
                }),
          )
        ],
      ),
    );
  }
}
