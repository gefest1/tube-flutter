import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/admin/big_service/big_service_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/provider/create_big_service/create_big_service.dart';
import 'package:tube/ui/admin/page/service/money_question_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/load_popup_route.dart';
import 'package:tube/utils/terms_tap.dart';

class PayBigServicePage extends StatefulWidget {
  const PayBigServicePage({super.key});

  @override
  State<PayBigServicePage> createState() => _PayBigServicePageState();
}

class _PayBigServicePageState extends State<PayBigServicePage>
    with MoneyQuestionExtension {
  late final adminBlocMap = Provider.of<AdminBlocMap>(context, listen: false);

  AdminBigServiceBloc get serviceBloc =>
      adminBlocMap.getAdminBigServiceBloc(MainDrawerState.clubId!);

  @override
  late final TextEditingController costController = TextEditingController()
    ..addListener(() => update(TypeChange.cost));
  @override
  late final TextEditingController percentController = TextEditingController()
    ..addListener(() => update(TypeChange.percent));
  @override
  late final TextEditingController percentPriceController =
      TextEditingController()
        ..addListener(() => update(TypeChange.percentPrice));
  @override
  final StreamController streamController = StreamController();

  CreateBigServiceProvider get createProvider =>
      Provider.of<CreateBigServiceProvider>(context, listen: false);

  void sendData() async {
    final value = createProvider.createBigService;
    final newVal = value.copyWith(
      price: double.parse(costController.text),
      percent: double.parse(percentController.text),
    );
    createProvider.setNewInstance(newVal);
    final completer = Completer();
    Navigator.push(context, LoadPopupRoute(completer: completer));
    serviceBloc.add(
      AddAdminBigServiceEvent(
        completer: completer,
        entity: CreateBigServiceEntity(
          name: newVal.name!,
          description: newVal.description!,
          durationMin: newVal.duration!.inMinutes,
          trainerIds: newVal.trainerIds!,
          price: newVal.price!,
          percent: newVal.percent!,
          clubId: serviceBloc.clubId,
        ),
      ),
    );
    await completer.future;
    if (!mounted) return;
    Navigator.of(context, rootNavigator: true)
        .popUntil((route) => route.isFirst);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        backgroundColor: Colors.white,
        body: CustomScrollView(
          slivers: [
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              sliver: SliverToBoxAdapter(
                child: Column(
                  children: [
                    const SizedBox(height: 20),
                    Row(
                      children: [
                        Expanded(
                          child: CustomTextField(
                            hintText: '10',
                            title: 'Стоимость услуги',
                            controller: costController,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                RegExp(r'[+-]?([0-9]*[.])?[0-9]+'),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Container(
                            width: 105,
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 13),
                            decoration: const BoxDecoration(
                              color: Color(0xffF7F7F7),
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                            ),
                            child: const P1Text(
                              'Тенге',
                              color: ColorData.colorElementsActive,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Container(
                      padding: const EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                        border: Border.all(
                          color: const Color(0xffA5A5A5),
                          width: 0.5,
                        ),
                      ),
                      child: const Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: Icon(
                              Icons.warning_amber,
                              color: ColorData.colorElementsActive,
                            ),
                          ),
                          Flexible(
                            child: P1Text(
                              'Комиссия Checkallnow. при каждой покупке ваших продуктов = 3.9%',
                              color: ColorData.colorTextMain,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 40),
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                RegExp(r'^[1-9][0-9]?$|^100$'),
                              ),
                            ],
                            cursorColor: Colors.black,
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: P0TextStyle.fontSize,
                              height: P0TextStyle.height,
                              fontWeight: P0TextStyle.fontWeight,
                            ),
                            textAlign: TextAlign.center,
                            controller: percentController,
                            decoration: percentInputDecoration,
                          ),
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: TextField(
                            controller: percentPriceController,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                RegExp(r'[+-]?([0-9]*[.])?[0-9]+'),
                              ),
                            ],
                            cursorColor: Colors.black,
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: P0TextStyle.fontSize,
                              height: P0TextStyle.height,
                              fontWeight: P0TextStyle.fontWeight,
                            ),
                            textAlign: TextAlign.center,
                            decoration: pricePercentInputDecoration,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    const Align(
                      alignment: Alignment.centerLeft,
                      child: P1Text(
                        'Сколько процентов получает сотрудник с услуги',
                        color: ColorData.colorElementsActive,
                      ),
                    ),
                    const SizedBox(height: 20),
                    Container(
                      padding: const EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                        border: Border.all(
                          color: const Color(0xffA5A5A5),
                          width: 0.5,
                        ),
                      ),
                      child: const Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: Icon(
                              Icons.warning_amber,
                              color: ColorData.colorElementsActive,
                            ),
                          ),
                          Flexible(
                            child: P1Text(
                              'Сумма зарплаты сотрудника зависит от проведенных им занятий по услуге',
                              color: ColorData.colorTextMain,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    StreamBuilder(
                        stream: streamController.stream,
                        builder: (context, snap) {
                          final active = costController.text.isNotEmpty &&
                              percentController.text.isNotEmpty &&
                              percentPriceController.text.isNotEmpty;
                          return CustomButton(
                            onTap: active ? sendData : null,
                            title: 'Создать Услугу',
                            titleColor: active
                                ? Colors.white
                                : ColorData.colortTextSecondary,
                            color: active
                                ? ColorData.colorMainLink
                                : ColorData.colorElementsLightMenu,
                          );
                        }),
                    const SizedBox(height: 10),
                    RichText(
                      text: TextSpan(
                        style: const TextStyle(
                          fontFamily: 'InterTight',
                          fontSize: P2TextStyle.fontSize,
                          fontWeight: P2TextStyle.fontWeight,
                          height: P2TextStyle.height,
                          color: ColorData.colorTextMain,
                        ),
                        children: [
                          const TextSpan(
                              text: 'Создавая услугу вы соглашаетесь '),
                          TextSpan(
                            text: 'с условием использования приложения',
                            style: const TextStyle(
                              color: ColorData.colorMainLink,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = termsTap,
                          ),
                          const TextSpan(text: ' и '),
                          TextSpan(
                            text: 'политикой конфиденциально',
                            style:
                                const TextStyle(color: ColorData.colorMainLink),
                            recognizer: TapGestureRecognizer()
                              ..onTap = politicTap,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
