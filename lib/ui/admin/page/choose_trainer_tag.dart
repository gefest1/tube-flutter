import 'package:flutter/material.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/ui/admin/atoms/status_tag.dart';
import 'package:tube/ui/admin/page/new_tag_sheet.dart';
import 'package:tube/ui/admin/page/set_work_time_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/utils/colors.dart';

class ChooseTrainerTag extends StatelessWidget {
  const ChooseTrainerTag({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const Column(
          children: [
            H4Text(
              'jeanwwd@gmail.com',
              color: Colors.white,
            ),
            Padding(
              padding: EdgeInsets.only(top: 2),
              child: P1Text(
                'Иван И.И.',
                color: Colors.white,
              ),
            )
          ],
        ),
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(
            Icons.arrow_back,
            size: 33,
          ),
          color: Colors.white,
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              padding: const EdgeInsets.only(top: 40, left: 20, right: 20),
              children: [
                Wrap(
                  alignment: WrapAlignment.center,
                  runAlignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  runSpacing: 10,
                  spacing: 10,
                  children: List.generate(
                    17,
                    (index) => const StatusTag(
                      active: true,
                    ),
                  ),
                ),
                const SizedBox(height: 30),
                CustomButton(
                  title: 'Добавить новую должность',
                  color: Colors.white,
                  titleColor: ColorData.colorMainLink,
                  onTap: () async {
                    await showFlexibleBottomSheet(
                      context: context,
                      minHeight: 0,
                      initHeight: 0.95,
                      maxHeight: 0.95,
                      builder: (ctx, scoll, _) => Material(
                        color: Colors.transparent,
                        child: NewTagSheet(
                          controller: scoll,
                        ),
                      ),
                      anchors: [0.95],
                    );
                  },
                ),
                const SizedBox(height: 30),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            decoration: const BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Color(0x1a000000),
                  offset: Offset(0, -0.5),
                ),
              ],
            ),
            child: CustomButton(
              title: 'Выбрать должности',
              onTap: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => const SetWorkTimePage(),
                  ),
                );
              },
              color: ColorData.colorMainLink,
            ),
          ),
        ],
      ),
    );
  }
}
