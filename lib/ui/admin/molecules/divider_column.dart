import 'package:flutter/material.dart';

class DividerColumn extends StatelessWidget {
  final List<Widget> children;
  final Widget divider;
  final CrossAxisAlignment crossAxisAlignment;

  const DividerColumn({
    this.crossAxisAlignment = CrossAxisAlignment.center,
    this.children = const [],
    this.divider = const SizedBox(),
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    if (children.isEmpty) return const SizedBox();
    return Column(
      crossAxisAlignment: crossAxisAlignment,
      children: List.generate(
        children.length * 2 - 1,
        (index) {
          if (index.isOdd) return divider;
          return children[index ~/ 2];
        },
      ),
    );
  }
}
