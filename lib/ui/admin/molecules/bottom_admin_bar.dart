import 'package:flutter/material.dart';
import 'dart:ui';

class MainAdminBottom extends StatefulWidget {
  final TabController tabController;

  const MainAdminBottom({
    Key? key,
    required this.tabController,
  }) : super(key: key);

  @override
  State<MainAdminBottom> createState() => _MainAdminBottomState();
}

class _MainAdminBottomState extends State<MainAdminBottom> {
  @override
  void initState() {
    super.initState();
    widget.tabController.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final bottomPadding = MediaQuery.of(context).padding.bottom;
    return DecoratedBox(
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
            blurRadius: 0,
            offset: Offset(0, -0.5),
            color: Color(0x0d000000),
          ),
        ],
      ),
      child: ClipRRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
          child: Container(
            height: 60 + bottomPadding,
            padding: EdgeInsets.only(bottom: bottomPadding),
            decoration: const BoxDecoration(
              color: Color.fromARGB(255 ~/ (1 / 0.6), 33, 33, 33),
            ),
            child: TabBar(
              controller: widget.tabController,
              labelColor: Colors.white,
              unselectedLabelColor: const Color(0xffB7B7B7),
              dividerColor: Colors.transparent,
              indicatorColor: const Color(0x00ffffff),
              tabs: [
                Tab(
                  icon: Icon(
                    Icons.work,
                    color: widget.tabController.index == 0
                        ? Colors.white
                        : const Color(0xffB7B7B7),
                  ),
                  iconMargin: const EdgeInsets.only(top: 10),
                  text: 'Главная',
                ),
                Tab(
                  icon: Icon(
                    Icons.ballot,
                    color: widget.tabController.index == 1
                        ? Colors.white
                        : const Color(0xffB7B7B7),
                  ),
                  iconMargin: const EdgeInsets.only(top: 10),
                  text: 'Расписание',
                ),
                Tab(
                  icon: Icon(
                    Icons.notifications,
                    color: widget.tabController.index == 2
                        ? Colors.white
                        : const Color(0xffB7B7B7),
                  ),
                  iconMargin: const EdgeInsets.only(top: 10),
                  text: 'Уведомления',
                ),
                Tab(
                  icon: Icon(
                    Icons.supervised_user_circle,
                    color: widget.tabController.index == 3
                        ? Colors.white
                        : const Color(0xffB7B7B7),
                  ),
                  iconMargin: const EdgeInsets.only(top: 10),
                  text: 'Профиль',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
