import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tube/ui/admin/atoms/wheel_label.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class ServiceWheel extends StatefulWidget {
  const ServiceWheel({Key? key}) : super(key: key);

  @override
  State<ServiceWheel> createState() => _ServiceWheelState();
}

String getDoubleStr(String str) {
  final toReturn = "00$str";
  return toReturn.substring(toReturn.length - 2, toReturn.length);
}

class _ServiceWheelState extends State<ServiceWheel> {
  int _lastHapticIndex = 0;

  void _handleSelectedItemChanged(int indexDirt) {
    final index = indexDirt % 10;
    if (index != _lastHapticIndex) {
      _lastHapticIndex = index;
      HapticFeedback.selectionClick();
    }
  }

  Widget _wheel(int inter) {
    return ListWheelScrollView.useDelegate(
      itemExtent: 41,
      clipBehavior: Clip.none,
      overAndUnderCenterOpacity: 0.3,
      diameterRatio: 1.2,
      onSelectedItemChanged: _handleSelectedItemChanged,
      squeeze: 1.4,
      physics: const FixedExtentScrollPhysics(),
      childDelegate: ListWheelChildBuilderDelegate(builder: (ctx, index) {
        return SizedBox(
          height: 41,
          child: Center(
            child: H3Text(
              (index % inter).toString(),
              color: ColorData.colorElementsActive,
            ),
          ),
        );
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 146,
      decoration: BoxDecoration(
        color: const Color(0xffF7F7F7),
        borderRadius: BorderRadius.circular(10),
      ),
      child: ClipRect(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.all(10),
              child: H5Text(
                'Срок действия абонемента',
                color: ColorData.colorTextMain,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Stack(
                  children: [
                    Center(
                      child: Container(
                        height: 41,
                        margin: const EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Stack(
                            children: [
                              _wheel(24),
                              const WheelLabel(data: 'ч'),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Stack(
                            children: [
                              _wheel(60),
                              const WheelLabel(data: 'мин')
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
