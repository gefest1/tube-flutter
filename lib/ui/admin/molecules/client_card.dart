import 'package:flutter/material.dart';
import 'package:tube/logic/model/photourl.dart';
import 'package:tube/ui/admin/molecules/divider_column.dart';
import 'package:tube/ui/common/atoms/circle_photo.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';

import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class ClientCard extends StatelessWidget {
  final String? fullName;
  final String? phoneNumber;
  final int tariffCount;
  final int serviceCount;
  final String? email;
  final PhotoUrl? photoUrl;

  const ClientCard({
    this.fullName,
    this.tariffCount = 0,
    this.serviceCount = 0,
    this.phoneNumber,
    this.email,
    this.photoUrl,
    super.key,
  });

  Widget get textDivider => const SizedBox(height: 2);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 15),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CirclePhoto(
              size: 48,
              photoUrl: photoUrl,
            ),
            const SizedBox(width: 10),
            Expanded(
              child: DividerColumn(
                divider: const SizedBox(height: 2),
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (fullName != null)
                    H4Text(
                      fullName!,
                      color: ColorData.colorTextMain,
                    ),
                  P1Text(
                    'Абонементов: $tariffCount',
                    color: ColorData.colorTextMain,
                  ),
                  P1Text(
                    'Услуг: $serviceCount',
                    color: ColorData.colorTextMain,
                  ),
                  if (phoneNumber != null)
                    RichText(
                      text: TextSpan(
                        style: const TextStyle(
                          color: ColorData.colorTextMain,
                          fontSize: P1TextStyle.fontSize,
                          height: P1TextStyle.height,
                          fontWeight: P1TextStyle.fontWeight,
                        ),
                        children: [
                          const TextSpan(text: 'Телефон: '),
                          TextSpan(
                            text: phoneNumber!,
                            style: const TextStyle(
                              color: ColorData.colorMainLink,
                            ),
                          ),
                        ],
                      ),
                    ),
                  if (email != null)
                    RichText(
                      text: TextSpan(
                        style: const TextStyle(
                          color: ColorData.colorTextMain,
                          fontSize: P1TextStyle.fontSize,
                          height: P1TextStyle.height,
                          fontWeight: P1TextStyle.fontWeight,
                        ),
                        children: [
                          const TextSpan(text: 'Почта: '),
                          TextSpan(
                            text: email,
                            style: const TextStyle(
                              color: ColorData.colorTextMain,
                              fontSize: P1TextStyle.fontSize,
                              height: P1TextStyle.height,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ],
                      ),
                    ),
                ],
              ),
            )
          ],
        ),
        const SizedBox(height: 15),
        const Divider(
          height: 0,
          thickness: 0.5,
          color: ColorData.color5Percent,
        ),
      ],
    );
  }
}
