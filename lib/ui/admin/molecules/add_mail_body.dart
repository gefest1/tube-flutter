import 'package:flutter/material.dart';
import 'package:tube/ui/add_compnay/molecults/space_controllers.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';

class AddMailBody extends StatefulWidget {
  final TextEditingController? controller;
  final List<String>? list;
  final VoidCallback? callBack;
  final String hintText;
  final String title;
  final TextInputType? keyboardType;

  const AddMailBody({
    this.controller,
    this.list,
    this.callBack,
    this.keyboardType = TextInputType.emailAddress,
    this.hintText = 'proverka@primer.ru',
    this.title = 'Введите почтовый адрес',
    Key? key,
  }) : super(key: key);

  @override
  State<AddMailBody> createState() => _AddMailBodyState();
}

class _AddMailBodyState extends State<AddMailBody> {
  late TextEditingController controller =
      (widget.controller ?? TextEditingController())..addListener(_listen);
  late List<String> _list = widget.list ?? [];

  @override
  void didUpdateWidget(covariant AddMailBody oldWidget) {
    controller = widget.controller ?? TextEditingController();
    _list = widget.list ?? [];
    super.didUpdateWidget(oldWidget);
  }

  void _scroll() {
    final scroll = PrimaryScrollController.of(context);
    if (scroll.positions.isEmpty) return;
    scroll.animateTo(
      scroll.offset + 80,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeIn,
    );
  }

  void _listen() {
    if (controller.text.contains(' ')) {
      final newElements = controller.text.split(' ');
      controller.clear();
      newElements.removeWhere((element) => element == ' ');
      newElements.removeWhere((element) => element == '');

      if (newElements.isNotEmpty) {
        _scroll();
        setState(() {
          _list.addAll(newElements);
          widget.callBack?.call();
        });
      }
    }
  }

  @override
  void dispose() {
    controller.removeListener(_listen);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (_list.isNotEmpty)
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: SpaceControllers(
              values: _list,
              removeAt: (_) {
                setState(() {
                  _list.removeAt(_);
                });
              },
            ),
          ),
        CustomTextField(
          hintText: widget.hintText,
          title: widget.title,
          controller: controller,
          keyboardType: widget.keyboardType,
          scrollPadding: const EdgeInsets.only(bottom: 200),
          onSubmitted: (value) {
            setState(() {
              _list.add(value);
              widget.callBack?.call();
            });
            controller.clear();
          },
        ),
      ],
    );
  }
}
