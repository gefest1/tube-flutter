import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/admin/workers/worker_bloc.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/ui/admin/page/add_admin_page.dart';
import 'package:tube/ui/admin/page/tariff/molecules/short_fast_button.dart';
import 'package:tube/ui/admin/page/tariff/molecules/stack_image_card.dart';
import 'package:tube/ui/admin/page/trainer_list_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';

class TrainerList extends StatelessWidget {
  final EdgeInsets padding;
  final VoidCallback? onTap;
  final bool active;

  const TrainerList({
    this.padding = const EdgeInsets.symmetric(horizontal: 20),
    this.onTap,
    this.active = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WorkerBloc, WorkerState>(
      bloc: Provider.of<AdminBlocMap>(context, listen: false)
          .getWorkerBloc(MainDrawerState.currentParticipance!.clubId!),
      builder: (_, state) {
        if (state is ClubWorkerState && state.mapWorkers.isNotEmpty) {
          final List<User> users = state.mapWorkers;
          return Padding(
            padding: padding,
            child: StackImageCard(
              active: active,
              images: users
                  .map((e) => e.photoUrl?.thumbnail)
                  .whereType<String>()
                  .toList(),
              numberTitle: users.length,
              onTap: onTap ??
                  () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => TrainerListPage(
                          clubId: MainDrawerState.currentParticipance!.clubId!,
                        ),
                      ),
                    );
                  },
              title: "Сотрудники",
            ),
          );
        }
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: ShortFastButton(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => const AddAdminPage(),
                ),
              );
            },
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.perm_contact_cal,
                  color: ColorData.colorElementsActive,
                ),
                SizedBox(width: 5),
                P1Text(
                  'Добавить сотрудников',
                  color: ColorData.colorElementsActive,
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
