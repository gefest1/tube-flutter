import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/admin/clients/client_bloc.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/ui/admin/page/client_list_page.dart';
import 'package:tube/ui/admin/page/tariff/molecules/stack_image_card.dart';

import 'package:tube/ui/common/templates/main_drawer.dart';

class ClientList extends StatefulWidget {
  final EdgeInsets padding;
  final VoidCallback? onTap;
  final bool active;

  const ClientList({
    this.padding = const EdgeInsets.symmetric(horizontal: 20),
    this.onTap,
    this.active = false,
    super.key,
  });

  @override
  State<ClientList> createState() => _ClientListState();
}

class _ClientListState extends State<ClientList> {
  @override
  Widget build(BuildContext context) {
    // final theWorld = extractAllSorted< User>(
    //   query: 'goolge',
    //   choices: [
    //     'google',
    //     'bing',
    //     'facebook',
    //     'linkedin',
    //     'twitter',
    //     'googleplus',
    //     'bingnews',
    //     'plexoogl'
    //   ],
    //   cutoff: 0,
    //   getter:
    // );
    // log(theWorld.toString());

    return BlocBuilder<AdminClientBloc, AdminClientState>(
      bloc: Provider.of<AdminBlocMap>(context, listen: false)
          .getByType(MainDrawerState.currentParticipance!.clubId!),
      builder: (context, state) {
        final List<User> users = [];
        if (state is DataAdminClientState) {
          users.addAll(state.users);
        }
        return Padding(
          padding: widget.padding,
          child: StackImageCard(
            active: widget.active,
            images:
                users.map((e) => e.photoUrl?.m).whereType<String>().toList(),
            numberTitle: users.length,
            onTap: widget.onTap ??
                () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ClientListPage(
                        clubId: MainDrawerState.currentParticipance!.clubId!,
                        users: users,
                      ),
                    ),
                  );
                },
            title: "Клиенты",
          ),
        );
      },
    );
  }
}
