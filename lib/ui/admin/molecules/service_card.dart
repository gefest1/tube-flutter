import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/get_adaptive_span.dart';

class ServiceCard extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final String? label;
  final Widget? iconWidget;
  final VoidCallback? onTap;

  const ServiceCard({
    this.title,
    this.subTitle,
    this.label,
    this.iconWidget,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        width: 164,
        height: 158,
        padding: const EdgeInsets.all(15),
        decoration: const BoxDecoration(
          color: Color(0xccffffff),
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -3),
              blurRadius: 2,
              color: Color(0x03000000),
            ),
            BoxShadow(
              offset: Offset(-3, 0),
              blurRadius: 2,
              color: Color(0x03000000),
            ),
            BoxShadow(
              offset: Offset(3, 0),
              blurRadius: 2,
              color: Color(0x03000000),
            ),
            BoxShadow(
              offset: Offset(0, 5),
              blurRadius: 20,
              color: Color(0x0d000000),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (title != null)
                    H4Text(
                      title!,
                      color: ColorData.colorMainBlack,
                      maxLines: 3,
                    ),
                  if (subTitle != null)
                    P2Text(
                      subTitle!,
                      color: ColorData.colortTextSecondary,
                    )
                ],
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                if (iconWidget != null) iconWidget!,
                if (label != null)
                  Expanded(
                    child: RichText(
                      textAlign: TextAlign.end,
                      text: getAdaptiveTextSpan(
                        label!,
                        const TextStyle(
                          fontSize: P1TextStyle.fontSize,
                          height: P1TextStyle.height,
                          fontWeight: P1TextStyle.fontWeight,
                          color: ColorData.colorTextMain,
                        ),
                        context,
                      ),
                    ),
                  )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
