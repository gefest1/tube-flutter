import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/animated_switch.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class AddTrainerTile extends StatelessWidget {
  final bool active;
  final VoidCallback? onTap;

  const AddTrainerTile({
    this.active = false,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -3),
              blurRadius: 2,
              color: Color(0x03000000),
            ),
            BoxShadow(
              offset: Offset(-3, 0),
              blurRadius: 2,
              color: Color(0x03000000),
            ),
            BoxShadow(
              offset: Offset(3, 0),
              blurRadius: 2,
              color: Color(0x03000000),
            ),
            BoxShadow(
              offset: Offset(0, 5),
              blurRadius: 20,
              color: Color(0x0d000000),
            ),
          ],
        ),
        child: Row(
          children: [
            const Padding(
              padding: EdgeInsets.only(right: 10),
              child: CircleAvatar(
                radius: 18,
                backgroundColor: Colors.black26,
              ),
            ),
            const Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  H3Text(
                    'Артурия Ф.О.',
                    color: ColorData.colorTextMain,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 2),
                    child: P2Text(
                      'Тренер, Мойщик',
                      color: ColorData.colortTextSecondary,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Container(
                height: 31,
                width: 31,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(
                    color: ColorData.colorMainLink,
                    width: 1,
                  ),
                  color: Colors.black.withOpacity(0.03),
                ),
                alignment: Alignment.center,
                child: AnimatedSwitch(
                  active: active,
                  child: Container(
                    height: 25,
                    width: 25,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: ColorData.colorMainLink,
                    ),
                    alignment: Alignment.center,
                    child: const Icon(
                      Icons.done,
                      color: Colors.white,
                      size: 14,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
