// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:tube/logic/blocs/finance/finance_bloc.dart';
import 'package:tube/logic/blocs/finance/model/finance.model.dart';
import 'package:tube/ui/admin/finances/mol/finance_card.dart';

import 'package:tube/utils/colors.dart';
import 'dart:math' as math;

import 'package:tube/utils/get_time_shift.dart';

class FinanceBody extends StatelessWidget {
  final bool Function(Finance element) filter;

  const FinanceBody({
    required this.filter,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final finBloc = getFinanceBloc(context);
    return BlocBuilder<FinanceBloc, FinanceState>(
      bloc: finBloc,
      builder: (context, state) {
        List<Finance> data = [];
        if (state is DataFinanceState) {
          data.addAll(
            state.data.where((element) => filter(element)),
          );
        }
        return SliverList(
          delegate: SliverChildBuilderDelegate(
            (ctx, i) {
              if (i.isOdd) {
                return const Divider(
                  color: ColorData.color5Percent,
                  height: 0,
                  indent: 20,
                  endIndent: 20,
                  thickness: 0.5,
                );
              }
              final index = i ~/ 2;

              return FinanceBigCard(
                title: data[index].displayValue,
                subTitle: data[index].value == null
                    ? null
                    : NumberFormat('+###,###,000₸').format(data[index].value),
                dateTitle: DateFormat("d MMMM yyyy, HH:mm").format(
                  data[index].createdAt!.parseTimeGG,
                ),
                trainerTitle: data[index].trainer?.fullName ?? data[index].trainer?.email,
                userTitle: data[index].user?.fullName ?? data[index].user?.email,
              );
            },
            childCount: math.max(0, data.length * 2 - 1),
          ),
        );
      },
    );
  }
}
