import 'dart:async';
// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/blocs/finance/finance_bloc.dart';
import 'package:tube/logic/blocs/finance/model/finance.model.dart';
import 'package:tube/ui/admin/finances/finance_body.dart';
import 'package:tube/ui/admin/finances/mol/finance_tabs.dart';
import 'package:tube/ui/admin/finances/organism/show_interval.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/get_adaptive_span.dart';
import 'package:tube/utils/get_time_shift.dart';

class FinanceTrainerPage extends StatefulWidget {
  const FinanceTrainerPage({
    Key? key,
  }) : super(key: key);

  @override
  State<FinanceTrainerPage> createState() => _FinanceTrainerPageState();
}

class _FinanceTrainerPageState extends State<FinanceTrainerPage> {
  final BehaviorSubject<TimeIntervalEnum> timeIntervalSubject =
      BehaviorSubject.seeded(TimeIntervalEnum.day);
  StreamSubscription? _str;
  late final FinanceBloc _finBloc = getFinanceBloc(context);

  Duration getDuration() {
    return timeIntervalSubject.value.duration();
  }

  void _listen(_) {
    _finBloc.add(
      GetFinanceEvent(
        startTime: timeIntervalSubject.value.startTime,
        endTime: timeIntervalSubject.value.endTime,
      ),
    );
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _str = timeIntervalSubject.listen(_listen);
  }

  @override
  void dispose() {
    timeIntervalSubject.close();
    _str?.cancel();
    super.dispose();
  }

  bool filterFinances(Finance element) {
    return element.createdAt!.isAfter(timeIntervalSubject.value.startTime) &&
        element.createdAt!.isBefore(timeIntervalSubject.value.endTime);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const H4Text(
          'Финансы',
          color: Colors.white,
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 20),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: H5Text(
                    'История операций',
                    color: ColorData.colortTextSecondary,
                  ),
                ),
                const SizedBox(height: 10),
                FinanceTabs(
                  subject: timeIntervalSubject,
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  onTap: (TimeIntervalEnum tEnum) async {
                    if (TimeIntervalEnum.interval == tEnum) {
                      DateTime? startTime;
                      DateTime? endTime;
                      if (timeIntervalSubject.value == TimeIntervalEnum.interval) {
                        startTime = timeIntervalSubject.value.startTime;
                        endTime = timeIntervalSubject.value.endTime;
                      }
                      final res = await showFlexibleBottomSheet<List<DateTime>>(
                        minHeight: 0,
                        initHeight: 0.95,
                        maxHeight: 0.95,
                        context: context,
                        anchors: [0.95],
                        builder: (ctx, controller, _) {
                          return Material(
                            color: Colors.transparent,
                            child: ShowIntervalSheet(
                              controller: controller,
                              startTime: startTime,
                              endTime: endTime,
                            ),
                          );
                        },
                      );
                      if (res == null) return;
                      timeIntervalSubject.value = TimeIntervalEnum.intervalFactory(
                        res.first,
                        res.last,
                      );
                      return;
                    }
                    timeIntervalSubject.value = tEnum;
                  },
                ),
                const SizedBox(height: 10),
                Container(
                  decoration: BoxDecoration(
                    color: ColorData.color3Percent,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: StreamBuilder(
                          stream: timeIntervalSubject.stream,
                          builder: (context, _) {
                            DateTime endTime = timeIntervalSubject.value.endTime;
                            DateTime startTime = timeIntervalSubject.value.startTime;

                            final str = '${DateFormat('dd.MM.yyyy').format(startTime.parseTimeGG)}'
                                " — "
                                '${DateFormat('dd.MM.yyyy').format(endTime.parseTimeGG)}';

                            return P1Text(
                              str,
                              color: ColorData.colorTextMain,
                            );
                          },
                        ),
                      ),
                      Flexible(
                        child: StreamBuilder(
                          stream: timeIntervalSubject.stream,
                          builder: (ctx, _) {
                            return BlocBuilder<FinanceBloc, FinanceState>(
                              bloc: _finBloc,
                              builder: (context, state) {
                                List<Finance> data = [];
                                if (state is DataFinanceState) {
                                  data = [
                                    ...state.data.where(filterFinances),
                                  ];
                                }
                                final span = getAdaptiveTextSpan(
                                  NumberFormat('+###,###,000₸').format(
                                    data.fold<num>(
                                      0,
                                      (pr, e) => pr + e.value!,
                                    ),
                                  ),
                                  const TextStyle(
                                    fontSize: H4TextStyle.fontSize,
                                    fontWeight: H4TextStyle.fontWeight,
                                    height: H4TextStyle.height,
                                    color: Color(0xff00C738),
                                  ),
                                  context,
                                );
                                return RichText(text: span);
                              },
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 5),
              ],
            ),
          ),
          FinanceBody(
            filter: filterFinances,
          ),
          const SliverToBoxAdapter(child: SizedBox(height: 200)),
        ],
      ),
    );
  }
}
