// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tube/logic/blocs/finance/finance_bloc.dart';
import 'package:tube/logic/blocs/finance/model/finance.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/ui/admin/finances/mol/finance_service_card.dart';
import 'package:tube/utils/colors.dart';
import 'dart:math' as math;

class FinanceServicePage extends StatelessWidget {
  final bool Function(Finance element) filter;

  const FinanceServicePage({
    required this.filter,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final finBloc = getFinanceBloc(context);
    return BlocBuilder<FinanceBloc, FinanceState>(
      bloc: finBloc,
      builder: (context, state) {
        List<Finance> data = [];
        Map<String, Booking> bookingMap = {};
        Map<String, List<Finance>> financeMap = {};

        if (state is DataFinanceState) {
          data.addAll(
            state.data.where((element) => filter(element)),
          );
          for (final singleFinance in data) {
            bookingMap[singleFinance.bookingId!] = Booking(
              name: singleFinance.displayValue,
            );
            (financeMap[singleFinance.bookingId!] ??= []).add(singleFinance);
          }
        }
        if (data.isEmpty) return const SliverToBoxAdapter();
        return _FinanceServiceBody(
          bookingMap: bookingMap,
          financeMap: financeMap,
        );
      },
    );
  }
}

class _FinanceServiceBody extends StatelessWidget {
  final Map<String, Booking> bookingMap;
  final Map<String, List<Finance>> financeMap;

  const _FinanceServiceBody({
    required this.bookingMap,
    required this.financeMap,
  });

  @override
  Widget build(BuildContext context) {
    final financeMapList = financeMap.entries.toList();
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (ctx, i) {
          if (i.isOdd) {
            return const Divider(
              color: ColorData.color5Percent,
              height: 0,
              indent: 20,
              endIndent: 20,
              thickness: 0.5,
            );
          }
          final index = i ~/ 2;
          final indexEntry = financeMapList[index];
          return FinanceServiceAnimatedCard(
            booking: bookingMap[indexEntry.key]!,
            finances: indexEntry.value,
          );
        },
        childCount: math.max(0, financeMap.length * 2 - 1),
      ),
    );
  }
}
