import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:tube/utils/infinite_listview.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/ui/admin/finances/mol/shedule_select.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'dart:math' as math;

// import 'package:tube/utils/infinite_listview.dart';
import 'package:async/async.dart' show StreamGroup;

class ShowIntervalSheet extends StatefulWidget {
  final ScrollController controller;
  final DateTime? startTime;
  final DateTime? endTime;

  const ShowIntervalSheet({
    required this.controller,
    this.startTime,
    this.endTime,
    Key? key,
  }) : super(key: key);

  @override
  State<ShowIntervalSheet> createState() => _ShowIntervalSheetState();
}

class _ShowIntervalSheetState extends State<ShowIntervalSheet> {
  final BehaviorSubject<DateTime> dateTimeSubject =
      BehaviorSubject<DateTime>.seeded(DateTime.now());
  late final InfiniteScrollController infiniteScrollController = InfiniteScrollController();

  late final BehaviorSubject<DateTime?> startTime =
      BehaviorSubject<DateTime?>.seeded(widget.startTime);
  late final BehaviorSubject<DateTime?> endTime = BehaviorSubject<DateTime?>.seeded(widget.endTime);
  late final Stream _stream = StreamGroup.merge([startTime, endTime]).asBroadcastStream();

  @override
  void dispose() {
    infiniteScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(5),
        ),
      ),
      child: Stack(
        children: [
          ListView(
            controller: widget.controller,
            children: [
              SizedBox(
                height: 52,
                child: Stack(
                  children: [
                    const Center(
                      child: H4Text(
                        'Выбор периода времени',
                        color: ColorData.colorTextMain,
                      ),
                    ),
                    Positioned(
                      right: 0,
                      top: 0,
                      height: 52,
                      width: 52,
                      child: SizeTapAnimation(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const Center(
                            child: Icon(
                              Icons.close,
                              color: ColorData.colorMainElements,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(height: 40),
              SheduleSelect(
                startTime: startTime,
                endTime: endTime,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: P1Text(
                    'Выберите нужный промежуток времени, выбрав две даты',
                    color: ColorData.colortTextSecondary,
                  ),
                ),
              ),
              const SizedBox(height: 160),
            ],
          ),
          StreamBuilder(
            stream: _stream,
            builder: (context, _) {
              if (startTime.value == null || endTime.value == null) {
                return const SizedBox();
              }
              return Positioned(
                left: 0,
                right: 0,
                bottom: 0,
                child: DecoratedBox(
                  decoration: const BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 0,
                        offset: Offset(0, -0.5),
                        color: Color(0x0d000000),
                      ),
                    ],
                  ),
                  child: ClipRRect(
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                      child: Container(
                        padding: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          top: 10,
                          bottom: math.max(MediaQuery.of(context).padding.bottom, 20),
                        ),
                        child: Column(
                          children: [
                            SizeTapAnimation(
                              child: GestureDetector(
                                onTap: () => Navigator.pop(
                                  context,
                                  [startTime.value!, endTime.value!.add(const Duration(days: 1))],
                                ),
                                behavior: HitTestBehavior.opaque,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: ColorData.colorElementsActive,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  alignment: Alignment.center,
                                  height: 61,
                                  child: const P0Text(
                                    'Применить',
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 10),
                            P2Text(
                              "${DateFormat("dd.MM.yyyy").format(startTime.value!.parseTimeGG)}"
                              " - "
                              "${DateFormat("dd.MM.yyyy").format(endTime.value!.parseTimeGG)}",
                              color: ColorData.colorTextMain,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
