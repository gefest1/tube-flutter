import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tube/logic/blocs/finance/model/finance.model.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/ui/admin/finances/mol/finance_card.dart';
import 'package:tube/ui/admin/page/tariff/molecules/custom_avatar.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/get_adaptive_span.dart';
import 'package:tube/utils/get_time_shift.dart';

class FinanceTrainerAnimatedCard extends StatefulWidget {
  final List<Finance> finances;

  final User user;

  const FinanceTrainerAnimatedCard({
    required this.finances,
    required this.user,
    super.key,
  });

  @override
  State<FinanceTrainerAnimatedCard> createState() => _FinanceTrainerAnimatedCardState();
}

class _FinanceTrainerAnimatedCardState extends State<FinanceTrainerAnimatedCard>
    with SingleTickerProviderStateMixin {
  List<Finance> get finances => widget.finances;
  User get user => widget.user;

  AnimationController? animationController;
  bool open = false;
  @override
  void initState() {
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );
    super.initState();
  }

  @override
  void dispose() {
    animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final span = getAdaptiveTextSpan(
      NumberFormat('###,###,000₸').format(
        widget.finances.fold<num>(
          0,
          (pr, e) => pr + e.value!,
        ),
      ),
      const TextStyle(
        fontSize: H4TextStyle.fontSize,
        fontWeight: H4TextStyle.fontWeight,
        height: H4TextStyle.height,
        color: ColorData.colorTextMain,
      ),
      context,
    );
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizeTapAnimation(
          onTap: () {
            if (open) {
              animationController?.reverse();
            } else {
              animationController?.forward();
            }
            open = !open;
          },
          child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 15),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomAvatar(photoUrl: user.photoUrl),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: H4Text(
                              user.fullName ?? user.email!,
                              color: ColorData.colorTextMain,
                            ),
                          ),
                          RichText(text: span),
                        ],
                      ),
                      const SizedBox(height: 5),
                      const Row(
                        children: [
                          Expanded(child: P1Text('Сотрудник', color: ColorData.colorTextMain)),
                          Icon(
                            Icons.expand_more,
                            color: ColorData.colorBlackInactive,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        FadeTransition(
          opacity: animationController!,
          child: SizeTransition(
            sizeFactor: CurvedAnimation(
              curve: Curves.fastOutSlowIn,
              parent: animationController!,
            ),
            child: Column(
              children: List.generate(
                finances.length * 2 - 1,
                (i) {
                  if (i.isOdd) {
                    return const Divider(
                      color: ColorData.color5Percent,
                      height: 0,
                      indent: 20,
                      endIndent: 20,
                      thickness: 0.5,
                    );
                  }
                  final index = i ~/ 2;
                  return FinanceBigCard(
                    title: finances[index].displayValue,
                    subTitle: finances[index].value == null
                        ? null
                        : NumberFormat('+###,###,000₸').format(finances[index].value),
                    dateTitle: DateFormat("d MMMM yyyy, HH:mm")
                        .format(finances[index].createdAt!.parseTimeGG),
                    trainerTitle:
                        finances[index].trainer?.fullName ?? finances[index].trainer?.email,
                    userTitle: finances[index].user?.fullName ?? finances[index].user?.email,
                  );
                },
              ),
            ),
          ),
        ),
        const SizedBox(height: 15),
        const Divider(
          color: ColorData.color5Percent,
          height: 0,
          indent: 20,
          endIndent: 20,
          thickness: 0.5,
        ),
      ],
    );
  }
}
