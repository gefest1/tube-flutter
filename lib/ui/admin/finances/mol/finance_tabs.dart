// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_time_shift.dart';

class TimeIntervalEnum {
  final String _value;
  final int index;
  TimeIntervalEnum._internal(this._value, this.index);

  @override
  String toString() => _value;
  String toMap() => _value;

  DateTime? _startTime;
  DateTime? _endTime;

  DateTime get startTime => _getValueStart();
  DateTime get endTime => _getValueEnd();

  String get startTimeStr => DateFormat('dd.MM.yyyy').format(startTime.parseTimeGG);
  String get endTimeStr =>
      DateFormat('dd.MM.yyyy').format(endTime.subtract(const Duration(days: 1)).parseTimeGG);

  DateTime _getValueStart() {
    final date = DateTime.now();
    if (_value == day._value) {
      return DateTime(date.year, date.month, date.day);
    }
    if (_value == month._value) {
      return DateTime(date.year, date.month);
    }
    if (_value == year._value) {
      return DateTime(date.year);
    }
    return _startTime!;
  }

  DateTime _getValueEnd() {
    final date = DateTime.now();
    if (_value == day._value) {
      return DateTime(date.year, date.month, date.day + 1);
    }
    if (_value == month._value) {
      return DateTime(date.year, date.month + 1);
    }
    if (_value == year._value) {
      return DateTime(date.year + 1);
    }
    return _endTime!;
  }

  static TimeIntervalEnum day = TimeIntervalEnum._internal('day', 0);
  static TimeIntervalEnum month = TimeIntervalEnum._internal('month', 1);
  static TimeIntervalEnum year = TimeIntervalEnum._internal('year', 2);
  static TimeIntervalEnum interval = TimeIntervalEnum._internal('interval', 3);

  factory TimeIntervalEnum.intervalFactory(
    DateTime startTime,
    DateTime endTime,
  ) {
    final timeInterval = TimeIntervalEnum._internal('interval', 3);
    timeInterval._startTime = startTime;
    timeInterval._endTime = endTime;
    return timeInterval;
  }

  @override
  // ignore: hash_and_equals
  bool operator ==(other) {
    if (other is TimeIntervalEnum) {
      return _value == other._value;
    }
    return this == other;
  }

  static List<TimeIntervalEnum> get values {
    return [
      day,
      month,
      year,
      interval,
    ];
  }

  String localString() {
    return ['День', 'Месяц', 'Год', 'Период'][index];
  }

  Duration duration() {
    if (index == 3) return _endTime!.difference(_startTime!);

    return [
      const Duration(days: 1),
      DateTime(DateTime.now().year, DateTime.now().month).difference(
        DateTime(DateTime.now().year, DateTime.now().month - 1),
      ),
      DateTime(DateTime.now().year).difference(
        DateTime(DateTime.now().year - 1),
      ),
    ][index];
  }
}

class FinanceTabs extends StatelessWidget {
  final BehaviorSubject<TimeIntervalEnum> subject;
  final EdgeInsets? padding;
  final void Function(TimeIntervalEnum)? onTap;

  const FinanceTabs({
    required this.subject,
    this.padding,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 39,
      child: StreamBuilder(
        stream: subject.stream,
        builder: (context, _) {
          return Center(
            child: ListView.separated(
              clipBehavior: Clip.none,
              padding: padding,
              itemCount: TimeIntervalEnum.values.length,
              itemBuilder: (ctx, index) {
                final isCurrent = TimeIntervalEnum.values[index] == subject.value;
                return SizeTapAnimation(
                  onTap: onTap == null ? null : () => onTap?.call(TimeIntervalEnum.values[index]),
                  behavior: HitTestBehavior.opaque,
                  child: Container(
                    height: 39,
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    alignment: Alignment.center,
                    decoration: ShapeDecoration(
                      shape: const StadiumBorder(
                        side: BorderSide(
                          color: ColorData.colorElementsActive,
                          width: 0.5,
                        ),
                      ),
                      color: isCurrent ? ColorData.colorElementsActive : Colors.transparent,
                    ),
                    child: P1Text(
                      TimeIntervalEnum.values[index].localString(),
                      color: isCurrent ? Colors.white : ColorData.colorElementsActive,
                    ),
                  ),
                );
              },
              separatorBuilder: (_, __) => const SizedBox(width: 5),
              scrollDirection: Axis.horizontal,
            ),
          );
        },
      ),
    );
  }
}
