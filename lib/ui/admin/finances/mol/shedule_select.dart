import 'package:flutter/material.dart';
import 'package:tube/utils/infinite_listview.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/ui/admin/finances/mol/shedule_body.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/shedule_table.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
// import 'package:tube/utils/infinite_listview.dart';

final List<String> _list = [
  "Пон",
  "Вт",
  "Ср",
  "Чт",
  "Пт",
  "Сб",
  "Вс",
];

class SheduleSelect extends StatefulWidget {
  final BehaviorSubject<DateTime?>? startTime;
  final BehaviorSubject<DateTime?>? endTime;

  final BehaviorSubject<DateTime?>? dateTime;

  const SheduleSelect({
    this.startTime,
    this.endTime,
    this.dateTime,
    Key? key,
  }) : super(key: key);

  @override
  State<SheduleSelect> createState() => _SheduleSelectState();
}

class _SheduleSelectState extends State<SheduleSelect> {
  final currentYear = DateTime.now().year;
  final currentMonth = DateTime.now().month;
  final BehaviorSubject<double> maxWidth = BehaviorSubject<double>.seeded(1);
  final BehaviorSubject<int> lastIndexSubject = BehaviorSubject.seeded(0);
  late final InfiniteScrollController infiniteScrollController =
      InfiniteScrollController();
  final BehaviorSubject<DateTime> dateTimeSubject =
      BehaviorSubject<DateTime>.seeded(DateTime.now());

  // final BehaviorSubject<DateTime?> startTime =
  //     BehaviorSubject<DateTime?>.seeded(null);
  // final BehaviorSubject<DateTime?> endTime =
  //     BehaviorSubject<DateTime?>.seeded(null);

  // final lastIndex = BehaviorSubject.seeded(0);

  void _listen() {
    final currentInd =
        (infiniteScrollController.position.pixels / maxWidth.value).round();
    if (lastIndexSubject.value != currentInd) {
      lastIndexSubject.value = currentInd;
    }
  }

  void prevPage() {
    dateTimeSubject.value = DateTime(
      dateTimeSubject.value.year,
      dateTimeSubject.value.month - 1,
    );
    infiniteScrollController.animateTo(
      infiniteScrollController.position.pixels - maxWidth.value,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeIn,
    );
  }

  void nextPage() {
    dateTimeSubject.value = DateTime(
      dateTimeSubject.value.year,
      dateTimeSubject.value.month + 1,
    );
    infiniteScrollController.animateTo(
      infiniteScrollController.position.pixels + maxWidth.value,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeIn,
    );
  }

  @override
  void initState() {
    super.initState();
    infiniteScrollController.addListener(_listen);
  }

  @override
  void dispose() {
    maxWidth.close();
    lastIndexSubject.close();
    infiniteScrollController.dispose();
    dateTimeSubject.close();

    lastIndexSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 38,
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Stack(
              children: [
                Center(
                  child: StreamBuilder(
                    stream: lastIndexSubject,
                    builder: (context, snapshot) {
                      final currentDateTime = DateTime(
                        currentYear,
                        currentMonth + lastIndexSubject.value,
                      );

                      return Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          H4Text(
                            monthName[currentDateTime.month - 1],
                            color: ColorData.colorTextMain,
                          ),
                          P2Text(
                            currentDateTime.year.toString(),
                            color: ColorData.colortTextSecondary,
                          )
                        ],
                      );
                    },
                  ),
                ),
                Positioned(
                  left: 0,
                  top: 0,
                  bottom: 0,
                  child: Center(
                    child: SizeTapAnimation(
                      child: GestureDetector(
                        onTap: prevPage,
                        child: const SizedBox(
                          height: 33,
                          width: 33,
                          child: Icon(
                            Icons.arrow_back_ios,
                            color: ColorData.colorButtonMain,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  right: 0,
                  top: 0,
                  bottom: 0,
                  child: Center(
                    child: SizeTapAnimation(
                      child: GestureDetector(
                        onTap: nextPage,
                        child: const SizedBox(
                          height: 33,
                          width: 33,
                          child: Icon(
                            Icons.arrow_forward_ios,
                            color: ColorData.colorButtonMain,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(
              _list.length,
              (index) => Expanded(
                child: Center(
                  child: H6Text(
                    _list[index],
                    color: ColorData.colortTextSecondary,
                  ),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(height: 15),
        SheduleBody(
          maxWidth: maxWidth,
          lastIndexSubject: lastIndexSubject,
          infiniteScrollController: infiniteScrollController,
          currentYear: currentYear,
          currentMonth: currentMonth,
          startTime: widget.startTime,
          endTime: widget.endTime,
          dateTime: widget.dateTime,
        ),
        const SizedBox(height: 5),
      ],
    );
  }
}
