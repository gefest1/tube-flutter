import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tube/utils/infinite_listview.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class SheduleBody extends StatefulWidget {
  final BehaviorSubject<double> maxWidth;
  final BehaviorSubject<int> lastIndexSubject;
  final InfiniteScrollController infiniteScrollController;
  final int currentYear; // = DateTime.now().year;
  final int currentMonth; // = DateTime.now().month;

  final BehaviorSubject<DateTime?>? startTime;
  final BehaviorSubject<DateTime?>? endTime;

  final BehaviorSubject<DateTime?>? dateTime;

  const SheduleBody({
    required this.maxWidth,
    required this.lastIndexSubject,
    required this.infiniteScrollController,
    required this.currentYear,
    required this.currentMonth,
    this.startTime,
    this.endTime,
    //
    this.dateTime,
    Key? key,
  }) : super(key: key);

  @override
  State<SheduleBody> createState() => _SheduleBodyState();
}

class _SheduleBodyState extends State<SheduleBody> {
  late StreamSubscription<DateTime?>? str1;
  late StreamSubscription<DateTime?>? str2;

  late StreamSubscription<DateTime?>? str3;

  void listen(_) => setState(() {});

  BehaviorSubject<DateTime?>? get startTime => widget.startTime;
  BehaviorSubject<DateTime?>? get endTime => widget.endTime;

  BehaviorSubject<DateTime?>? get dateTime => widget.dateTime;

  @override
  void initState() {
    str1 = startTime?.listen(listen);
    str2 = endTime?.listen(listen);
    str3 = dateTime?.listen(listen);
    super.initState();
  }

  @override
  void dispose() {
    str1?.cancel();
    str2?.cancel();
    str3?.cancel();
    super.dispose();
  }

  void onTapInterval(DateTime dateTime) {
    if (endTime == null && startTime == null) return;
    if ((startTime?.value == null && endTime?.value == null) ||
        (startTime?.value != null && endTime?.value != null)) {
      startTime?.value = dateTime;
      endTime?.value = null;
      return;
    }
    if (startTime?.value != null && endTime?.value == null) {
      if (dateTime.isAfter(startTime!.value!)) {
        endTime!.value = dateTime;
      } else {
        endTime!.value = startTime!.value;
        startTime!.value = dateTime;
      }
      return;
    }
  }

  void onTapDate(DateTime dateTimeValue) {
    dateTime?.value = dateTimeValue;
  }

  BoxDecoration isColored(DateTime dateTime) {
    if (this.dateTime?.value != null) {
      if (this.dateTime!.value!.isAtSameMomentAs(dateTime)) {
        return BoxDecoration(
          color: ColorData.colorMainLink,
          borderRadius: BorderRadius.circular(5),
        );
      }
      return const BoxDecoration();
    }
    if (endTime == null && startTime == null) return const BoxDecoration();
    if (startTime?.value != null && endTime?.value != null) {
      if (startTime!.value!.isAtSameMomentAs(dateTime) ||
          endTime!.value!.isAtSameMomentAs(dateTime)) {
        return BoxDecoration(
          color: ColorData.colorMainLink,
          borderRadius: BorderRadius.horizontal(
            left: startTime!.value!.isAtSameMomentAs(dateTime)
                ? const Radius.circular(5)
                : Radius.zero,
            right: endTime!.value!.isAtSameMomentAs(dateTime)
                ? const Radius.circular(5)
                : Radius.zero,
          ),
        );
      }
      if (dateTime.compareTo(startTime!.value!) >= 0 &&
          dateTime.compareTo(endTime!.value!) <= 0) {
        return const BoxDecoration(color: Color(0x662e9bff));
      }
    }
    if (startTime!.value != null &&
        startTime!.value!.isAtSameMomentAs(dateTime)) {
      return const BoxDecoration(
        color: ColorData.colorMainLink,
        borderRadius: BorderRadius.horizontal(
          left: Radius.circular(5),
        ),
      );
    }
    return const BoxDecoration();
  }

  Color colorTextStyle(DateTime dateTime) {
    if (this.dateTime?.value != null) {
      if (this.dateTime!.value!.isAtSameMomentAs(dateTime)) {
        return Colors.white;
      }
      return ColorData.colorTextMain;
    }
    if (startTime?.value != null && endTime?.value != null) {
      if (startTime!.value!.isAtSameMomentAs(dateTime) ||
          endTime!.value!.isAtSameMomentAs(dateTime)) {
        return Colors.white;
      }
      if (dateTime.compareTo(startTime!.value!) >= 0 &&
          dateTime.compareTo(endTime!.value!) <= 0) {
        return ColorData.colorTextMain;
      }
    }
    if (startTime?.value != null &&
        startTime!.value!.isAtSameMomentAs(dateTime)) {
      return Colors.white;
    }
    return ColorData.colorTextMain;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 306,
      child: LayoutBuilder(
        builder: (context, box) {
          widget.maxWidth.value = box.maxWidth;
          return InfiniteListView.builder(
            controller: widget.infiniteScrollController,
            physics: const PageScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              final currentStartTime =
                  DateTime(widget.currentYear, widget.currentMonth + index);
              final currentEndTime =
                  DateTime(widget.currentYear, widget.currentMonth + index + 1);
              return SizedBox(
                width: widget.maxWidth.value,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: List.generate(
                      6,
                      (index) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: List.generate(
                            7,
                            (subIndex) {
                              final currentDay = 1 +
                                  index * 7 +
                                  subIndex -
                                  ((currentStartTime.weekday + 6) % 7);
                              final lastDay = currentEndTime
                                  .difference(currentStartTime)
                                  .inDays;
                              if (!(currentDay > 0 && currentDay <= lastDay)) {
                                return const Expanded(
                                  child: SizedBox(),
                                );
                              }
                              final currDayTime = DateTime(
                                currentStartTime.year,
                                currentStartTime.month,
                                currentDay,
                              );

                              return Expanded(
                                child: GestureDetector(
                                  onTap: () => widget.dateTime != null
                                      ? onTapDate(currDayTime)
                                      : onTapInterval(currDayTime),
                                  behavior: HitTestBehavior.opaque,
                                  child: IgnorePointer(
                                    child: Container(
                                      margin: const EdgeInsets.symmetric(
                                        vertical: 5,
                                      ),
                                      height: 41,
                                      alignment: Alignment.center,
                                      decoration: isColored(currDayTime),
                                      child: Column(
                                        children: [
                                          const SizedBox(height: 5),
                                          P0Text(
                                            currentDay.toString(),
                                            color: colorTextStyle(currDayTime),
                                            // ColorData.colorTextMain,
                                          ),
                                          const SizedBox(height: 2),
                                          // if (subWidget != null)
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        );
                      },
                    ),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
