import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/blocs/finance/finance_bloc.dart';
import 'package:tube/logic/blocs/finance/model/finance.model.dart';
import 'package:tube/ui/admin/finances/finance_body.dart';
import 'package:tube/ui/admin/finances/finance_service_page.dart';
import 'package:tube/ui/admin/finances/finance_trainer_body.dart';
import 'package:tube/ui/admin/finances/mol/finance_tabs.dart';
import 'package:tube/ui/admin/finances/organism/show_interval.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/get_adaptive_span.dart';

class FinanceAdminPage extends StatefulWidget {
  const FinanceAdminPage({super.key});

  @override
  State<FinanceAdminPage> createState() => _FinanceAdminPageState();
}

class _FinanceAdminPageState extends State<FinanceAdminPage>
    with SingleTickerProviderStateMixin {
  late TabController controller = TabController(length: 3, vsync: this)
    ..addListener(() {
      if (controller.indexIsChanging == false) {
        setState(() {});
      }
    });
  final BehaviorSubject<TimeIntervalEnum> timeIntervalSubject =
      BehaviorSubject.seeded(TimeIntervalEnum.day);
  StreamSubscription? _str;
  late final FinanceBloc _finBloc = getFinanceBloc(context);

  Duration getDuration() {
    return timeIntervalSubject.value.duration();
  }

  void _listen(_) {
    _finBloc.add(
      GetFinanceEvent(
        startTime: timeIntervalSubject.value.startTime,
        endTime: timeIntervalSubject.value.endTime,
      ),
    );
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _str = timeIntervalSubject.listen(_listen);
  }

  @override
  void dispose() {
    _str?.cancel();
    controller.dispose();
    super.dispose();
  }

  bool filterFinances(Finance element) {
    if (controller.index == 0 && element.isFull == false) return false;
    if (controller.index == 1 && element.isFull == true) return false;
    if (controller.index == 2 && element.isFull == false) return false;
    final startTime = timeIntervalSubject.value.startTime;
    final endTime =
        timeIntervalSubject.value.endTime.add(const Duration(days: 1));
    return element.createdAt!.isAfter(startTime) &&
        element.createdAt!.isBefore(endTime);
  }

  void chooseTimeInterval(TimeIntervalEnum tEnum) async {
    if (TimeIntervalEnum.interval == tEnum) {
      DateTime? startTime;
      DateTime? endTime;
      if (timeIntervalSubject.value == TimeIntervalEnum.interval) {
        startTime = timeIntervalSubject.value.startTime;
        endTime = timeIntervalSubject.value.endTime;
      }
      final res = await showFlexibleBottomSheet<List<DateTime>>(
        minHeight: 0,
        initHeight: 0.95,
        maxHeight: 0.95,
        context: context,
        anchors: [0.95],
        builder: (ctx, controller, _) {
          return Material(
            color: Colors.transparent,
            child: ShowIntervalSheet(
              controller: controller,
              startTime: startTime,
              endTime: endTime,
            ),
          );
        },
      );
      if (res == null) return;
      timeIntervalSubject.value = TimeIntervalEnum.intervalFactory(
        res.first,
        res.last,
      );
      return;
    }
    timeIntervalSubject.value = tEnum;
  }

  void moveToLeft() {
    final dur = timeIntervalSubject.value.duration();
    final newStart = timeIntervalSubject.value.startTime.subtract(dur);
    final newEnd = timeIntervalSubject.value.endTime.subtract(dur);

    timeIntervalSubject.value = TimeIntervalEnum.intervalFactory(
      newStart,
      newEnd,
    );
  }

  void moveToRight() {
    final dur = timeIntervalSubject.value.duration();
    final newStart = timeIntervalSubject.value.startTime.add(dur);
    final newEnd = timeIntervalSubject.value.endTime.add(dur);

    timeIntervalSubject.value = TimeIntervalEnum.intervalFactory(
      newStart,
      newEnd,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const H4Text(
          'Финансы',
          color: Colors.white,
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        bottom: TabBar(
          controller: controller,
          labelStyle: const TextStyle(
            fontWeight: P1TextStyle.fontWeight,
            fontSize: P1TextStyle.fontSize,
            height: P1TextStyle.height,
            fontFamily: "InterTight",
          ),
          indicatorColor: Colors.white,
          indicatorWeight: 5,
          labelPadding: const EdgeInsets.only(
            bottom: 10,
            top: 15,
          ),
          tabs: const [
            Text('Компании'),
            Text('Сотрудников'),
            Text('По услугам'),
          ],
        ),
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 20),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: H5Text(
                    'История операций',
                    color: ColorData.colortTextSecondary,
                  ),
                ),
                const SizedBox(height: 10),
                FinanceTabs(
                  subject: timeIntervalSubject,
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  onTap: chooseTimeInterval,
                ),
                const SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: IntrinsicHeight(
                    child: Row(
                      children: [
                        SizeTapAnimation(
                          onTap: moveToLeft,
                          child: Container(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            height: double.infinity,
                            decoration: BoxDecoration(
                              color: ColorData.color3Percent,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: const Icon(
                              Icons.chevron_left,
                              color: ColorData.colorBlackInactive,
                              size: 24,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.symmetric(horizontal: 5),
                            decoration: BoxDecoration(
                              color: ColorData.color3Percent,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            width: double.infinity,
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 14),
                            child: Column(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                StreamBuilder(
                                  stream: timeIntervalSubject.stream,
                                  builder: (ctx, _) {
                                    return BlocBuilder<FinanceBloc,
                                        FinanceState>(
                                      bloc: _finBloc,
                                      builder: (context, state) {
                                        List<Finance> data = [];
                                        if (state is DataFinanceState) {
                                          data = [
                                            ...state.data.where(filterFinances),
                                          ];
                                        }
                                        final span = getAdaptiveTextSpan(
                                          NumberFormat('+###,###,000₸').format(
                                            data.fold<num>(
                                              0,
                                              (pr, e) => pr + e.value!,
                                            ),
                                          ),
                                          const TextStyle(
                                            fontSize: H4TextStyle.fontSize,
                                            fontWeight: H4TextStyle.fontWeight,
                                            height: H4TextStyle.height,
                                            color: Color(0xff00C738),
                                          ),
                                          context,
                                        );
                                        return RichText(text: span);
                                      },
                                    );
                                  },
                                ),
                                StreamBuilder(
                                  stream: timeIntervalSubject.stream,
                                  builder: (context, _) {
                                    final endTimeStr =
                                        timeIntervalSubject.value.endTimeStr;
                                    final startTimeStr =
                                        timeIntervalSubject.value.startTimeStr;

                                    final str = '$startTimeStr — $endTimeStr';

                                    return P1Text(
                                      str,
                                      color: ColorData.colorTextMain,
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizeTapAnimation(
                          onTap: moveToRight,
                          child: Container(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            height: double.infinity,
                            decoration: BoxDecoration(
                              color: ColorData.color3Percent,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: const Icon(
                              Icons.chevron_right,
                              color: ColorData.colorBlackInactive,
                              size: 24,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 5),
              ],
            ),
          ),
          if (controller.index == 0)
            FinanceBody(
              filter: filterFinances,
            ),
          if (controller.index == 1)
            FinanceTrainerBody(
              filter: filterFinances,
            ),
          if (controller.index == 2)
            FinanceServicePage(
              filter: filterFinances,
            ),
          const SliverToBoxAdapter(child: SizedBox(height: 200)),
        ],
      ),
    );
  }
}
