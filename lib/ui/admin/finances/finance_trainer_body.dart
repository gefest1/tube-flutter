// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:tube/logic/blocs/finance/finance_bloc.dart';
import 'package:tube/logic/blocs/finance/model/finance.model.dart';
import 'package:tube/logic/blocs/user/user.model.dart';

import 'package:tube/ui/admin/finances/mol/finance_trainer_card.dart';

class FinanceTrainerBody extends StatelessWidget {
  final bool Function(Finance element) filter;

  const FinanceTrainerBody({
    required this.filter,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Map<String, List<Finance>> trainerFinances = {};
    Map<String, User> trainerMap = {};
    final finBloc = getFinanceBloc(context);
    return BlocBuilder<FinanceBloc, FinanceState>(
      bloc: finBloc,
      builder: (context, state) {
        List<Finance> data = [];
        if (state is DataFinanceState) {
          data.addAll(
            state.data.where((element) => filter(element)),
          );
          for (final e in data) {
            trainerMap[e.trainerId!] = e.trainer!;
            trainerFinances[e.trainerId!] =
                (trainerFinances[e.trainerId!] ?? [])..add(e);
          }
          trainerFinances.forEach(
            (key, value) =>
                value.sort((f1, f2) => f2.createdAt!.compareTo(f1.createdAt!)),
          );
        }
        final entriesFin = trainerFinances.entries.toList();
        return SliverList(
          delegate: SliverChildBuilderDelegate(
            (ctx, i) {
              final trainerId = entriesFin[i].key;
              final finances = entriesFin[i].value;

              return FinanceTrainerAnimatedCard(
                finances: finances,
                user: trainerMap[trainerId]!,
              );
            },
            childCount: entriesFin.length,
          ),
        );
      },
    );
  }
}
