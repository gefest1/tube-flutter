import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class CardContainer extends StatelessWidget {
  final String title;
  final VoidCallback? onTap;

  const CardContainer({
    required this.title,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: const BoxDecoration(
          color: Color(0xffF3F4F6),
          borderRadius: BorderRadius.all(
            Radius.circular(6),
          ),
        ),
        height: 51,
        alignment: Alignment.center,
        child: Row(
          children: [
            Expanded(
              child: Text(
                title,
                style: const TextStyle(
                  color: ColorData.colorMainElements,
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  height: 19 / 16,
                ),
              ),
            ),
            const Icon(
              Icons.arrow_forward,
              size: 24,
              color: ColorData.colorElementsActive,
            )
          ],
        ),
      ),
    );
  }
}
