import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class WheelLabel extends StatelessWidget {
  final String data;
  const WheelLabel({
    required this.data,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      child: Center(
        child: Padding(
          padding: EdgeInsets.only(left: 30 + data.length * 12),
          child: P0Text(
            data,
            color: ColorData.colorElementsActive,
          ),
        ),
      ),
    );
  }
}
