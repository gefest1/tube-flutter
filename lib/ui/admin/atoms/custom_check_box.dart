import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/animated_switch.dart';
import 'package:tube/utils/colors.dart';

class CustomCheckBox extends StatelessWidget {
  final bool isCircle;
  final bool active;
  final EdgeInsets? margin;

  const CustomCheckBox({
    this.margin,
    this.active = false,
    this.isCircle = false,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 32,
      width: 32,
      margin: margin,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: const Color(0x08000000),
        border: Border.all(
          width: 1,
          color: ColorData.colorMainLink,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(isCircle ? 16 : 5),
        ),
      ),
      child: AnimatedSwitch(
        active: active,
        child: Container(
          height: 24,
          width: 24,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(isCircle ? 12 : 4),
            ),
            color: ColorData.colorMainLink,
          ),
          child: const Icon(
            Icons.done,
            size: 14,
          ),
        ),
      ),
    );
  }
}
