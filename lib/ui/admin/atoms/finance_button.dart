// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:tube/logic/model/status_enum.dart';
import 'package:tube/ui/admin/finances/finanace_trainer_page.dart';
import 'package:tube/ui/admin/finances/finance_admin_page.dart';
import 'package:tube/ui/admin/page/tariff/atoms/finance_card.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';

class FinanceButton extends StatelessWidget {
  const FinanceButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          late Widget newPage;
          if (MainDrawerState.currentParticipance?.status == StatusEnum.trainer) {
            newPage = const FinanceTrainerPage();
          } else if (MainDrawerState.currentParticipance?.status == StatusEnum.admin) {
            newPage = const FinanceAdminPage();
          }

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (ctx) => newPage,
            ),
          );
        },
        child: const Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: FinanceCard(title: 'Финансы'),
        ),
      ),
    );
  }
}
