import 'package:flutter/material.dart';
import 'package:tube/utils/mask_text_input.dart';

class ExpireDateController extends StatelessWidget {
  final TextEditingController controller;

  const ExpireDateController({
    required this.controller,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      inputFormatters: [
        MaskTextInputFormatter(
          mask: "##/##",
          filter: {
            "#": RegExp(r'[0-9]'),
          },
        ),
      ],
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        hintText: 'ММ/ГГ',
        hintStyle: TextStyle(
          color: Color(0xffAFAFAF),
          fontSize: 18,
          height: 21 / 18,
          fontWeight: FontWeight.w400,
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
            width: 1,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
            width: 2,
          ),
        ),
      ),
    );
  }
}
