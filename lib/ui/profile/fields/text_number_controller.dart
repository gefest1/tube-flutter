import 'package:flutter/material.dart';
import 'package:tube/utils/mask_text_input.dart';

class TextNumberController extends StatelessWidget {
  final TextEditingController controller;

  const TextNumberController({
    required this.controller,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      inputFormatters: [
        MaskTextInputFormatter(
          mask: "#### #### #### ####",
          filter: {
            "#": RegExp(r'[0-9]'),
          },
        )
      ],
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        hintText: '0000 0000 0000 0000',
        hintStyle: TextStyle(
          color: Color(0xffAFAFAF),
          fontSize: 18,
          height: 21 / 18,
          fontWeight: FontWeight.w400,
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
            width: 1,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
            width: 2,
          ),
        ),
      ),
    );
  }
}
