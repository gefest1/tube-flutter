import 'package:flutter/material.dart';
import 'package:tube/ui/main_page/profile_page.dart';
import 'package:tube/ui/profile/card_detail.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class AddCard extends StatelessWidget {
  const AddCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(
            Icons.arrow_back,
            size: 33,
          ),
          color: Colors.white,
        ),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(5),
          ),
        ),
        centerTitle: true,
        title: const Text(
          'Способ оплаты',
          style: TextStyle(
            fontSize: H4TextStyle.fontSize,
            fontWeight: H4TextStyle.fontWeight,
            height: H4TextStyle.height,
          ),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.only(top: 40, left: 20, right: 20),
        children: [
          const Text(
            "Кредитные и дебетовые карты",
            style: TextStyle(
              color: ColorData.colortTextSecondary,
              height: H5TextStyle.height,
              fontWeight: H5TextStyle.fontWeight,
              fontSize: H5TextStyle.fontSize,
            ),
          ),
          ProfileCard(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => const CardDetail(),
                ),
              );
            },
            icon: const Icon(
              Icons.credit_card,
              color: ColorData.colorElementsActive,
            ),
            label: 'Добавить карту',
          ),
        ],
      ),
    );
  }
}
