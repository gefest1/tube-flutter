import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/profile/fields/cvv_controller.dart';
import 'package:tube/ui/profile/fields/expire_date_controller.dart';
import 'package:tube/ui/profile/fields/full_name_controller.dart';
import 'package:tube/ui/profile/fields/text_number_controller.dart';
import 'package:tube/utils/colors.dart';

class CardDetail extends StatefulWidget {
  const CardDetail({Key? key}) : super(key: key);

  @override
  State<CardDetail> createState() => _CardDetailState();
}

class _CardDetailState extends State<CardDetail> {
  final TextEditingController textNumberController = TextEditingController();
  final TextEditingController fullNameController = TextEditingController();
  final TextEditingController expireDateController = TextEditingController();
  final TextEditingController cvvController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);

        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(5),
            ),
          ),
          centerTitle: true,
          title: const H4Text(
            'Способ оплаты',
            color: Colors.white,
          ),
        ),
        body: ListView(
          padding: const EdgeInsets.only(top: 40, left: 20, right: 20),
          children: [
            Container(
              decoration: const BoxDecoration(
                color: ColorData.colorTextMain,
                borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(2, 2),
                    blurRadius: 15,
                    spreadRadius: 0,
                    color: Color(0x14000000),
                  )
                ],
              ),
              padding: const EdgeInsets.only(
                left: 15,
                right: 15,
                bottom: 20,
                top: 50,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextNumberController(controller: textNumberController),
                  const SizedBox(height: 5),
                  const P1Text(
                    'Номер карты',
                    color: Colors.white,
                  ),
                  FullNameController(controller: fullNameController),
                  const SizedBox(height: 5),
                  const P1Text(
                    'Имя и Фамилия',
                    color: Colors.white,
                  ),
                  const SizedBox(height: 20),
                  Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ExpireDateController(
                              controller: expireDateController,
                            ),
                            const SizedBox(height: 5),
                            const P1Text(
                              'Срок действия',
                              color: Colors.white,
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CvvController(controller: cvvController),
                            const SizedBox(height: 5),
                            const P1Text(
                              'Код безопасности',
                              color: Colors.white,
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            const SizedBox(height: 20),
            const P1Text(
              'Вы можете использовать дебетовую или кредитную карту для оплаты услуг и абонементов через Checkallnow.',
              color: Color(0xff969696),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}
