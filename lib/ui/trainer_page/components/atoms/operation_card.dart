import 'package:flutter/material.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class OperationCard extends StatelessWidget {
  const OperationCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: const Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CircleAvatar(
            radius: 17,
            backgroundColor: Color(0xff7E7E7E),
            child: Icon(
              Icons.school,
              size: 16.5,
              color: Colors.white,
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        'Малыши',
                        style: TextStyle(
                          color: ColorData.colorTextMain,
                          fontSize: 18,
                          height: 21 / 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Text(
                      '+1500₸',
                      style: TextStyle(
                        fontSize: H4TextStyle.fontSize,
                        fontWeight: H4TextStyle.fontWeight,
                        height: H4TextStyle.height,
                        color: ColorData.colorElementsActive,
                      ),
                    ),
                  ],
                ),
                Text(
                  'За занятие 22.11.2021 11:00, групповая тренировка, 2 человека, ставка: 30%',
                  style: TextStyle(
                    fontSize: P2TextStyle.fontSize,
                    fontWeight: P2TextStyle.fontWeight,
                    height: P2TextStyle.height,
                    color: ColorData.colorElementsSecondary,
                  ),
                ),
                SizedBox(height: 16),
                Text(
                  '22 ноября, 11:22',
                  style: TextStyle(
                    fontSize: P2TextStyle.fontSize,
                    fontWeight: P2TextStyle.fontWeight,
                    height: P2TextStyle.height,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
