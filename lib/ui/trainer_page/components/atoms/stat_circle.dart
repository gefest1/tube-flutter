import 'package:flutter/material.dart';
import 'dart:math' as math;

class StatCircle extends StatelessWidget {
  const StatCircle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(
        maxHeight: 164,
        maxWidth: 164,
        minHeight: 80,
        minWidth: 80,
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: CustomPaint(
          painter: _CircularProgressIndicatorPainter(
            valueColor: const Color(0xff00C738),
            value: 0.6,
            offsetValue: 0,
            backgroundColor: const Color(0xffDDDDDD),
            strokeWidth: 32,
          ),
          child: Center(
            child: Container(
              height: 82,
              width: 82,
              decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xff141414),
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(2, 2),
                      blurRadius: 15,
                      color: Color(0x14000000),
                    ),
                  ]),
              child: const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '50%',
                    style: TextStyle(
                      fontSize: 24,
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                      height: 28 / 24,
                    ),
                  ),
                  Text(
                    'Посещений',
                    style: TextStyle(
                      fontSize: 10,
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                      height: 12 / 10,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _CircularProgressIndicatorPainter extends CustomPainter {
  _CircularProgressIndicatorPainter({
    this.backgroundColor,
    required this.valueColor,
    required this.value,
    required this.offsetValue,
    required this.strokeWidth,
  }) : arcSweep = value != null ? value.clamp(0.0, 1.0) * _sweep : _epsilon;

  final Color? backgroundColor;
  final Color valueColor;
  final double? value;

  final double offsetValue;

  final double strokeWidth;

  final double arcSweep;

  static const double _twoPi = math.pi * 2.0;
  static const double _epsilon = .001;
  static const double _sweep = _twoPi - _epsilon;

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = valueColor
      ..strokeWidth = strokeWidth
      ..style = PaintingStyle.stroke;
    if (backgroundColor != null) {
      final Paint backgroundPaint = Paint()
        ..color = backgroundColor!
        ..strokeWidth = strokeWidth
        ..style = PaintingStyle.stroke;
      canvas.drawArc(Offset.zero & size, 0, _sweep, false, backgroundPaint);
    }

    canvas.drawArc(
        Offset.zero & size, offsetValue * math.pi, arcSweep, false, paint);
  }

  @override
  bool shouldRepaint(_CircularProgressIndicatorPainter oldPainter) {
    return oldPainter.backgroundColor != backgroundColor ||
        oldPainter.valueColor != valueColor ||
        oldPainter.value != value ||
        oldPainter.offsetValue != offsetValue ||
        oldPainter.strokeWidth != strokeWidth;
  }
}
