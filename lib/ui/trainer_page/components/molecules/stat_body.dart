import 'package:flutter/material.dart';
import 'package:tube/ui/trainer_page/components/atoms/stat_circle.dart';
import 'package:tube/ui/trainer_page/components/column_chip.dart';
import 'package:tube/utils/colors.dart';

class StatBody extends StatelessWidget {
  const StatBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ColumnChip(
              iconData: Icons.face_unlock_outlined,
              title1: '12',
              title2: '24',
              label: 'Ученики',
            ),
            Flexible(
              child: StatCircle(),
            ),
            ColumnChip(
              iconData: Icons.face_unlock_outlined,
              title1: '5000 ₸',
              title2: '10 000 ₸',
              label: 'Финансы',
            ),
          ],
        ),
        SizedBox(height: 10),
        Center(
          child: Text(
            'Результаты за сегодня\n03.12.2021',
            style: TextStyle(
              color: ColorData.colorTextMain,
              fontWeight: FontWeight.w500,
              fontSize: 14,
              height: 17 / 14,
            ),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }
}
