import 'dart:ui';

import 'package:flutter/material.dart';

class MainTrainerBottom extends StatefulWidget {
  final TabController tabController;

  const MainTrainerBottom({
    Key? key,
    required this.tabController,
  }) : super(key: key);

  @override
  State<MainTrainerBottom> createState() => _MainTrainerBottomState();
}

class _MainTrainerBottomState extends State<MainTrainerBottom> {
  @override
  void initState() {
    super.initState();
    widget.tabController.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    // BackdropFilter(
    //   filter: ImageFilter.blur(sigmaX: 20.0, sigmaY: 20.0),
    // );
    final bottomPadding = MediaQuery.of(context).padding.bottom;
    return DecoratedBox(
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
            blurRadius: 0,
            offset: Offset(0, -0.5),
            color: Color(0x0d000000),
          ),
        ],
      ),
      child: ClipRRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
          child: Container(
            height: 60 + bottomPadding,
            padding: EdgeInsets.only(bottom: bottomPadding),
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.6),
            ),
            child: TabBar(
              controller: widget.tabController,
              labelColor: const Color(0xff404040),
              unselectedLabelColor: const Color(0xff969696),
              indicatorColor: const Color(0x00ffffff),
              tabs: [
                Tab(
                  icon: Icon(
                    Icons.work,
                    color: widget.tabController.index == 0
                        ? const Color(0xff404040)
                        : const Color(0xff969696),
                  ),
                  iconMargin: const EdgeInsets.only(top: 10),
                  text: 'Главная',
                ),
                Tab(
                  icon: Icon(
                    Icons.ballot,
                    color: widget.tabController.index == 1
                        ? const Color(0xff404040)
                        : const Color(0xff969696),
                  ),
                  iconMargin: const EdgeInsets.only(top: 10),
                  text: 'Расписание',
                ),
                Tab(
                  icon: Icon(
                    Icons.notifications,
                    color: widget.tabController.index == 2
                        ? const Color(0xff404040)
                        : const Color(0xff969696),
                  ),
                  iconMargin: const EdgeInsets.only(top: 10),
                  text: 'Уведомления',
                ),
                Tab(
                  icon: Icon(
                    Icons.supervised_user_circle,
                    color: widget.tabController.index == 3
                        ? const Color(0xff404040)
                        : const Color(0xff969696),
                  ),
                  iconMargin: const EdgeInsets.only(top: 10),
                  text: 'Профиль',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
