import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/trainer/big_event_trainer/big_event_trainer_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/ui/admin/page/big_event/big_event_detail_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/big_event_card.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_time_shift.dart';

class BigEventTrainerCard extends StatelessWidget {
  const BigEventTrainerCard({super.key});

  @override
  Widget build(BuildContext context) {
    final trainerBlocMap = Provider.of<TrainerBlocMap>(context, listen: false);
    final bigEventBloc = trainerBlocMap.getBigEvent(MainDrawerState.clubId!);

    return BlocBuilder<BigEventTrainerBloc, BigEventTrainerState>(
      bloc: bigEventBloc,
      builder: (context, state) {
        if (state is DataBigEventTrainerState && state.data.isNotEmpty) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 20),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: H5Text(
                  'Мероприятия',
                  color: ColorData.colortTextSecondary,
                ),
              ),
              ...List.generate(state.data.length * 2 - 1, (index) {
                if (index.isOdd) {
                  return const Divider(
                    endIndent: 15,
                    indent: 15,
                    height: 0,
                    thickness: 0.2,
                    color: ColorData.colortTextSecondary,
                  );
                }
                final bigEvent = state.data[index ~/ 2];
                return BigEventCard(
                  title: bigEvent.name,
                  subTitle: DateFormat('dd.MM.yyyy HH:mm').format(bigEvent.date.parseTimeGG),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => BigEventDetailPage(
                          entity: bigEvent,
                          isAdmin: false,
                        ),
                      ),
                    );
                  },
                );
              }),
            ],
          );
        }
        return const SizedBox(height: 30);
      },
    );
  }
}
