// ignore_for_file: prefer_function_declarations_over_variables

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/trainer_page/components/molecules/shedule_little_card.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/date_time_parse.dart';
import 'package:tube/utils/get_time_shift.dart';

typedef SheduleBigCardType = void Function({
  required String mapKeyUnique,
  required List<TariffSingle> list,
  required Booking booking,
  required DateTime dateTime,
});

extension DateOnlyCompare on DateTime {
  bool isSameDate(DateTime other) {
    return year == other.year && month == other.month && day == other.day;
  }
}

class SheduleBigCardModel {
  final List<MapEntry<String, List<TariffSingle>>> entries;
  final Map<String, Booking> bookingMap;
  final SheduleBigCardType? onTap;

  SheduleBigCardModel({
    required this.entries,
    required this.bookingMap,
    this.onTap,
  });

  int _currentIndex = 0;
  List<Widget> _widgets = [];

  MapEntry<String, List<TariffSingle>> get getLast {
    if (_currentIndex == 0) throw 'Error';
    return entries[_currentIndex - 1];
  }

  MapEntry<String, List<TariffSingle>> get currentEntry {
    return entries[_currentIndex];
  }

  DateTime get currentDateTime {
    return dateTimeParse(currentEntry.key.split("_").last)!;
  }

  List<Widget> getElements() {
    List<Widget> elements = [];
    DateTime? lastDate;
    while (_currentIndex < entries.length) {
      if (lastDate != null) {
        if (!lastDate.isSameDate(currentDateTime)) {
          break;
        }
      }
      lastDate = currentDateTime;
      final currentEntryParsed = currentEntry;
      final newCurrentDate = currentDateTime;
      elements.add(
        SheduleLittleCard(
          label: "Человек в группе: ${currentEntryParsed.value.length}",
          timeTitle: DateFormat("HH:mm").format(newCurrentDate.parseTimeGG),
          title: bookingMap[currentEntryParsed.key.split("_").first]!.name!,
          onTap: onTap == null
              ? null
              : () => onTap?.call(
                    list: currentEntryParsed.value,
                    booking: bookingMap[currentEntryParsed.key.split("_").first]!,
                    dateTime: newCurrentDate,
                    mapKeyUnique: currentEntryParsed.key,
                  ),
        ),
      );
      _currentIndex++;
    }
    if (elements.isEmpty) {
      return [];
    }
    return List.generate(elements.length * 2 - 1, (index) {
      if (index.isOdd) {
        return const Divider(
          color: Color(0xffCBCBCB),
          height: 0,
          thickness: 0.5,
        );
      }
      return elements[index ~/ 2];
    });
  }

  List<Widget> getWidgets() {
    _widgets = [];
    _currentIndex = 0;
    while (_currentIndex < entries.length) {
      _widgets.add(
        Container(
          decoration: const BoxDecoration(
            color: Color(0xffF3F4F6),
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          padding: const EdgeInsets.only(
            top: 20,
            bottom: 5,
            left: 15,
            right: 15,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              H4Text(
                toBeginningOfSentenceCase(
                  DateFormat('EEEEE, d MMMM yyyy').format(currentDateTime.parseTimeGG),
                ).toString(),
                color: ColorData.colorTextMain,
              ),
              ...getElements(),
            ],
          ),
        ),
      );
    }
    return _widgets;
  }
}

class SheduleBigCard extends StatelessWidget {
  final List<MapEntry<String, List<TariffSingle>>> entries;
  final Map<String, Booking> bookingMap;
  final SheduleBigCardType? onTap;

  const SheduleBigCard({
    required this.entries,
    required this.bookingMap,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final entries = this.entries.where((element) {
      final dd = DateTime.parse(element.key.split("_").last).difference(DateTime.now());

      return const Duration() < dd && dd < const Duration(days: 3);
    }).toList();
    if (entries.isEmpty) return const SizedBox();

    List<Widget> widgets = SheduleBigCardModel(
      entries: entries,
      bookingMap: bookingMap,
      onTap: onTap,
    ).getWidgets();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: List.generate(
        widgets.length * 2 - 1,
        (index) {
          if (index.isOdd) return const SizedBox(height: 10);
          return widgets[index ~/ 2];
        },
      ),
    );
  }
}
