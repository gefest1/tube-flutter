// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';

import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_time_shift.dart';

class SheduleCard extends StatelessWidget {
  final String? title;
  final String? label;
  final List<TariffSingle> trainTimeList;
  final void Function()? bookingTap;
  final void Function({
    required String mapKeyUnique,
    required List<TariffSingle> list,
    required DateTime dateTime,
  })? tariffTap;

  const SheduleCard({
    this.title,
    this.label = 'Абонемент',
    required this.trainTimeList,
    this.bookingTap,
    this.tariffTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final trainTimeList = [...this.trainTimeList]..sort((a, b) {
        return a.trainTimesSingle.date!.compareTo(b.trainTimesSingle.date!);
      });
    final Map<String, List<TariffSingle>> listGG = {};
    for (final dd in trainTimeList) {
      (listGG[dd.trainTimesSingle.date!.toUtc().toIso8601String()] ??= []).add(dd);
    }
    final showList = listGG.entries.toList();
    return Column(
      children: [
        SizeTapAnimation(
          child: GestureDetector(
            onTap: bookingTap,
            behavior: HitTestBehavior.opaque,
            child: Row(
              children: [
                const Icon(
                  Icons.fiber_smart_record,
                  color: ColorData.abonement,
                  size: 48,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (title != null)
                        H4Text(
                          title!,
                          color: ColorData.colorTextMain,
                        ),
                      if (label != null)
                        P1Text(
                          label!,
                          color: ColorData.colorTextMain,
                        ),
                    ],
                  ),
                ),
                const SizedBox(width: 10),
                // const Icon(
                //   Icons.chevron_right,
                //   size: 24,
                //   color: ColorData.colorElementsSecondary,
                // )
              ],
            ),
          ),
        ),
        const SizedBox(height: 10),
        if (trainTimeList.isNotEmpty)
          Container(
            decoration: const BoxDecoration(
              color: Color(0xffF3F4F6),
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            padding: const EdgeInsets.only(bottom: 5),
            child: Column(
              children: List.generate(
                showList.length * 2 - 1,
                (ind) {
                  if (ind.isOdd) {
                    return const Divider(
                      endIndent: 15,
                      indent: 15,
                      height: 0,
                      thickness: 0.5,
                      color: Color(0xffCBCBCB),
                    );
                  }
                  final index = ind ~/ 2;

                  return SizeTapAnimation(
                    onTap: tariffTap == null
                        ? null
                        : () => tariffTap?.call(
                              mapKeyUnique: showList[index].key,
                              list: showList[index].value,
                              dateTime: DateTime.parse(showList[index].key).toLocal(),
                            ),
                    child: Padding(
                      padding: const EdgeInsets.all(15),
                      child: Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                if (trainTimeList[index].trainTimesSingle.date != null)
                                  P0Text(
                                    DateFormat("HH:mm").format(
                                      DateTime.parse(showList[index].key).parseTimeGG,
                                    ),
                                    color: ColorData.colorElementsActive,
                                  ),
                                P2Text(
                                  'Человек в группе: ${showList[index].value.length}',
                                  color: ColorData.colortTextSecondary,
                                ),
                              ],
                            ),
                          ),
                          const Icon(
                            Icons.chevron_right,
                            color: ColorData.colorElementsSecondary,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
      ],
    );
  }
}
