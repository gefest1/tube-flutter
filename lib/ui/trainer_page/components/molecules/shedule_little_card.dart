import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class SheduleLittleCard extends StatelessWidget {
  final String timeTitle;
  final String title;
  final String label;
  final VoidCallback? onTap;

  const SheduleLittleCard({
    required this.label,
    required this.timeTitle,
    required this.title,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: IntrinsicHeight(
          child: Row(
            children: [
              Container(
                width: 47,
                alignment: Alignment.centerLeft,
                child: P2Text(
                  timeTitle,
                  // DateFormat("HH:mm").format(currentDateTime),
                  color: ColorData.colorElementsActive,
                ),
              ),
              Container(
                width: 0,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5,
                    color: ColorData.colorElementsActive,
                  ),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    P0Text(
                      title,
                      // bookingMap[currentEntry.key.split("_").first]!.name!,
                      color: ColorData.colorElementsActive,
                    ),
                    P0Text(
                      label,
                      // currentEntry.value.length.toString() + " человека",
                      color: ColorData.colortTextSecondary,
                    ),
                  ],
                ),
              ),
              const Icon(
                Icons.chevron_right,
                color: ColorData.colorElementsSecondary,
                size: 24,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
