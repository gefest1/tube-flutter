import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class CalendarCard extends StatelessWidget {
  const CalendarCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: const Color(0xffF3F4F6),
        borderRadius: BorderRadius.circular(6),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: BoxDecoration(
              color: const Color(0xffF3F4F6),
              borderRadius:
                  const BorderRadius.vertical(top: Radius.circular(5)),
              border: Border.all(
                width: 0,
                color: const Color(0xffF3F4F6),
                style: BorderStyle.solid,
              ),
            ),
            padding: const EdgeInsets.only(
              left: 15,
              right: 15,
              top: 20,
            ),
            child: const Text(
              'Сегодня, среда, 23 ноября',
              style: TextStyle(
                color: ColorData.colorTextMain,
                fontSize: H4TextStyle.fontSize,
                fontWeight: H4TextStyle.fontWeight,
                height: H4TextStyle.height,
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: List.generate(3 * 2 - 1, (ind) {
              if (ind % 2 == 1) {
                return const Divider(
                  indent: 15,
                  endIndent: 15,
                  color: Color(0xffCBCBCB),
                  height: 0,
                  thickness: 0.5,
                );
              }
              return SizeTapAnimation(
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {},
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: IntrinsicHeight(
                      child: Row(
                        children: [
                          const Text(
                            '16:00',
                            style: TextStyle(
                              fontSize: P2TextStyle.fontSize,
                              fontWeight: P2TextStyle.fontWeight,
                              height: P2TextStyle.height,
                              color: ColorData.colorElementsActive,
                            ),
                          ),
                          const SizedBox(width: 6),
                          Container(
                            width: 0,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: ColorData.colorElementsActive,
                                width: 0.5,
                              ),
                            ),
                          ),
                          const SizedBox(width: 10),
                          const Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Малыши',
                                  style: TextStyle(
                                    color: ColorData.colorElementsActive,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                    height: 21 / 18,
                                  ),
                                ),
                                Text(
                                  'Жандосова 55/3, персональная тренировка',
                                  style: TextStyle(
                                    color: ColorData.colortTextSecondary,
                                    fontSize: P2TextStyle.fontSize,
                                    fontWeight: P2TextStyle.fontWeight,
                                    height: P2TextStyle.height,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const Icon(
                            Icons.chevron_right_rounded,
                            color: ColorData.colorElementsSecondary,
                            size: 24,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }),
          ),
        ],
      ),
    );
  }
}
