import 'dart:async';
// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';

import 'package:tube/logic/blocs/trainer/work_time/work_time_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/work_time_graph.dart';
import 'package:tube/ui/admin/page/set_worktime_sheet.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/main_page/components/atoms/day_card.dart';
import 'package:tube/ui/main_page/components/organisms/active_time_card.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:tube/utils/load_popup_route.dart';
import 'package:tube/utils/week_day.dart';

class TrainerWorkerTime extends StatefulWidget {
  final String clubId;
  final String? trainerId;

  const TrainerWorkerTime({
    required this.clubId,
    this.trainerId,
    Key? key,
  }) : super(key: key);

  @override
  State<TrainerWorkerTime> createState() => _TrainerWorkerTimeState();
}

class _TrainerWorkerTimeState extends State<TrainerWorkerTime> {
  late ControllerWorkTimeBloc workTimeBloc = widget.trainerId != null
      ? Provider.of<AdminBlocMap>(context, listen: false).getControllerWorkTimeBloc(
          clubId: widget.clubId,
          trainerId: widget.trainerId!,
        )
      : Provider.of<TrainerBlocMap>(context, listen: false).getWorkTimeBloc(widget.clubId);
  Map<int, WorkTimeGraph?>? _workTimes;
  bool show = false;

  void _openSheet(int index) async {
    WorkTimeGraph? workTime = _workTimes![index] == null
        ? null
        : WorkTimeGraph(
            startTime: _workTimes![index]?.startTime!.subtract(Duration(days: index)),
            endTime: _workTimes![index]?.endTime!.subtract(Duration(days: index)),
          );

    final result = await showFlexibleBottomSheet<List<DateTime>>(
      minHeight: 0,
      initHeight: 0.95,
      maxHeight: 0.95,
      context: context,
      anchors: [0.95],
      builder: (ctx, controller, _) {
        return Material(
          color: Colors.transparent,
          child: SetWorktimeSheet(
            controller: controller,
            startTime: workTime?.startTime,
            endTime: workTime?.endTime,
            title: weeks[index],
          ),
        );
      },
    );
    if (result == null) return;
    if (result.isEmpty) {
      show = true;
      setState(() {
        _workTimes!.remove(index);
      });
      return;
    }
    if (result.last.difference(result.first).inDays >= 1) {
      result.last = result.last.subtract(const Duration(days: 1));
    }
    setState(() {
      show = true;
      _workTimes![index] = WorkTimeGraph(
        startTime: result.first.add(Duration(days: index)),
        endTime: result.last.add(Duration(days: index)),
      );
    });
  }

  void _sendData() async {
    if (_workTimes == null) return;
    final route = ModalRoute.of<dynamic>(context)!;

    Completer completer = Completer();
    final List<WorkTimeGraph> list = _workTimes!.values.whereType<WorkTimeGraph>().toList();

    workTimeBloc.add(
      SetDataWorkTimeEvent(
        workTimes: list,
        completer: completer,
      ),
    );
    Navigator.push(context, LoadPopupRoute(completer: completer));
    await completer.future;
    if (!mounted) return;
    Navigator.removeRoute(context, route);
    // Future(() {
    //   Navigator.pop(context);
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const Column(
          children: [
            H4Text(
              'Рабочее время',
              color: Colors.white,
            ),
            // P1Text(
            //   'Иван И.И.',
            //   color: Colors.white,
            // )
          ],
        ),
      ),
      body: BlocBuilder<ControllerWorkTimeBloc, WorkTimeState>(
          bloc: workTimeBloc,
          builder: (context, stateDirt) {
            if (stateDirt is LoadingWorkTimeState) {
              return const SizedBox();
            }
            final state = stateDirt as SuccessWorkTimeState;
            _workTimes ??= {...state.workTime};
            return Column(
              children: [
                Expanded(
                  child: ListView.separated(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                    itemBuilder: (context, index) {
                      if (_workTimes![index] == null) {
                        return DayCard(
                          title: weeks[index],
                          label: 'Выбрать время',
                          onTap: () => _openSheet(index),
                        );
                      }

                      final frstText =
                          DateFormat('HH:mm').format(_workTimes![index]!.startTime!.parseTimeGG);
                      final scndText =
                          DateFormat('HH:mm').format(_workTimes![index]!.endTime!.parseTimeGG);
                      return ActiveTimeCard(
                        onTap: () => _openSheet(index),
                        label: 'Изменить',
                        title: weeks[index],
                        subLabel: '$frstText — $scndText',
                      );
                    },
                    itemCount: 7,
                    separatorBuilder: (context, index) => const SizedBox(height: 10),
                  ),
                ),
                if (show)
                  BottomButton(
                    title: 'Завершить',
                    onTap: _sendData,
                  ),
              ],
            );
          }),
    );
  }
}
