import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/participance/participance_bloc.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/trainer/tariff_trainer_bloc/tariff_trainer_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/ui/admin/atoms/finance_button.dart';
import 'package:tube/ui/admin/atoms/head_titile.dart';
import 'package:tube/ui/admin/page/tariff/pages/tariff_trainer_detail_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/trainer_page/components/molecules/big_event_card.dart';
import 'package:tube/ui/trainer_page/components/molecules/shedule_big_card.dart';
import 'package:tube/ui/trainer_page/components/pages/about_us_page.dart';
import 'package:tube/utils/colors.dart';

class TrainBodyPage extends StatefulWidget {
  const TrainBodyPage({Key? key}) : super(key: key);

  @override
  State<TrainBodyPage> createState() => _TrainBodyPageState();
}

class _TrainBodyPageState extends State<TrainBodyPage> {
  final ScrollController scrollController = ScrollController();
  late final TariffTrainerBloc _tariffTrainerBloc =
      Provider.of<TrainerBlocMap>(context, listen: false)
          .getTariffTrainerBloc(MainDrawerState.currentParticipance!.clubId!);

  @override
  void initState() {
    super.initState();
    Future(() {
      _tariffTrainerBloc.add(const GetSoonTariffTrainerEvent());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          left: 0,
          right: 0,
          bottom: 0,
          top: 56 + MediaQuery.of(context).padding.top,
          child: CustomScrollView(
            controller: scrollController,
            slivers: [
              SliverToBoxAdapter(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 20),
                    const FinanceButton(),
                    const BigEventTrainerCard(),
                    // Padding(
                    //   padding: const EdgeInsets.only(top: 30),
                    //   child: SizeTapAnimation(
                    //     child: GestureDetector(
                    //       behavior: HitTestBehavior.opaque,
                    //       onTap: () {
                    //         Navigator.push(
                    //           context,
                    //           MaterialPageRoute(
                    //             builder: (_) => const FinancePage(),
                    //           ),
                    //         );

                    //       },
                    //       child: const FinanceCard(title: 'Финансы'),
                    //     ),
                    //   ),
                    // ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 20,
                        right: 20,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const H5Text(
                            'Ближайшие записи',
                            color: ColorData.colortTextSecondary,
                          ),
                          const SizedBox(height: 10),
                          BlocBuilder<TariffTrainerBloc, TariffTrainerState>(
                            bloc: _tariffTrainerBloc,
                            builder: (ctx, state) {
                              List<MapEntry<String, List<TariffSingle>>>
                                  newEntries = [];
                              final Map<String, Booking> bookingMap = {};

                              if (state is GetTariffTrainerState) {
                                bookingMap.addAll(state.bookingMap);
                                newEntries.addAll(state.sortedEntries);
                              }
                              if (bookingMap.isEmpty && newEntries.isEmpty) {
                                return const P0Text(
                                  'Новых записей нет',
                                  color: ColorData.colorTextMain,
                                );
                              }

                              return SheduleBigCard(
                                entries: newEntries,
                                bookingMap: bookingMap,
                                onTap: ({
                                  required String mapKeyUnique,
                                  required List<TariffSingle> list,
                                  required Booking booking,
                                  required DateTime dateTime,
                                }) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) {
                                        return TariffTrainerDetailPage(
                                          list: list,
                                          booking: booking,
                                          dateTime: dateTime,
                                          uniqueMapKey: mapKeyUnique,
                                        );
                                        // return TariffNoTrainerPage(
                                        //   list: list,
                                        //   booking: booking,
                                        //   dateTime: dateTime,
                                        //   uniqueMapKey: mapKeyUnique,
                                        // );
                                      },
                                    ),
                                  );
                                },
                              );
                            },
                          ),
                          const SizedBox(height: 200),
                          // const Padding(
                          //   padding: EdgeInsets.only(top: 5),
                          //   child: P0Text(
                          //     'Новых записей нет',
                          //     color: ColorData.colorTextMain,
                          //   ),
                          // )
                        ],
                      ),
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.only(top: 30),
                    //   child: Column(
                    //     crossAxisAlignment: CrossAxisAlignment.start,
                    //     children: const [
                    //       H5Text(
                    //         'Ваши услуги',
                    //         color: ColorData.colortTextSecondary,
                    //       ),
                    //       Padding(
                    //         padding: EdgeInsets.only(top: 5),
                    //         child: P0Text(
                    //           'За вами не закреплено услуг. Их добавляет администратор компании',
                    //           color: ColorData.colorTextMain,
                    //         ),
                    //       )
                    //     ],
                    //   ),
                    // ),
                    // Padding(
                    //   padding: const EdgeInsets.only(top: 30),
                    //   child: Column(
                    //     crossAxisAlignment: CrossAxisAlignment.start,
                    //     children: const [
                    //       H5Text(
                    //         'Ваши абонементы',
                    //         color: ColorData.colortTextSecondary,
                    //       ),
                    //       Padding(
                    //         padding: EdgeInsets.only(top: 5),
                    //         child: P0Text(
                    //           'За вами не закреплено абонементов. Их добавляет администратор компании',
                    //           color: ColorData.colorTextMain,
                    //         ),
                    //       )
                    //     ],
                    //   ),
                    // ),
                    const SizedBox(height: 100),
                  ],
                ),
              )
            ],
          ),
        ),
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: AppBar(
            automaticallyImplyLeading: false,
            leading: SizedBox(
              height: 56,
              width: 56,
              child: Stack(
                children: [
                  IconButton(
                    onPressed: () => Scaffold.of(context).openDrawer(),
                    icon: const Icon(
                      Icons.menu,
                      size: 33,
                    ),
                    iconSize: 33,
                    color: Colors.white,
                  ),
                  BlocBuilder<ParticipanceBloc, ParticipanceState>(
                    builder: (_, state) {
                      if (state is! DataParticipanceState ||
                          state.participances
                              .where((element) => element.accepted == false)
                              .isEmpty) {
                        return const SizedBox();
                      }
                      return const Positioned(
                        right: 16,
                        top: 8,
                        child: IgnorePointer(
                          child: CircleAvatar(
                            radius: 5,
                            backgroundColor: ColorData.colorMainNo,
                          ),
                        ),
                      );
                    },
                  )
                ],
              ),
            ),
            centerTitle: true,
            actions: const [SizedBox(width: 56)],
            title: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => const AboutUsPage(),
                  ),
                );
              },
              behavior: HitTestBehavior.opaque,
              child: const Column(
                children: [
                  HeadTitle(),
                  P2Text(
                    'Сотрудник',
                    color: ColorData.colorMainLink,
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
