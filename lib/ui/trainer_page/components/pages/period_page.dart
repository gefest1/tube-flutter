import 'package:flutter/material.dart';
import 'package:tube/ui/trainer_page/components/atoms/operation_card.dart';
import 'package:tube/ui/trainer_page/components/atoms/short_chip.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

const List<String> dayTitle = [
  'Неделя',
  'Месяц',
  'Год',
  'За период',
];

class PeriodPage extends StatelessWidget {
  const PeriodPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              color: const Color(0xff00C738),
              padding: const EdgeInsets.only(
                top: 36,
                bottom: 46,
              ),
              child: const Column(
                children: [
                  Text(
                    '25 000₸',
                    style: TextStyle(
                      fontWeight: H2TextStyle.fontWeight,
                      fontSize: H2TextStyle.fontSize,
                      height: H2TextStyle.height,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    'Баланс на 24 ноября, 13:15',
                    style: TextStyle(
                      fontSize: P1TextStyle.fontSize,
                      fontWeight: P1TextStyle.fontWeight,
                      height: P1TextStyle.height,
                      color: Color(0xffEDEDED),
                    ),
                  ),
                  Text(
                    'Выплата 30.11.2021',
                    style: TextStyle(
                      fontSize: P1TextStyle.fontSize,
                      fontWeight: P1TextStyle.fontWeight,
                      height: P1TextStyle.height,
                      color: Color(0xffEDEDED),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 20),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'История операций',
                    style: TextStyle(
                      fontSize: H5TextStyle.fontSize,
                      fontWeight: H5TextStyle.fontWeight,
                      height: H5TextStyle.height,
                      color: ColorData.colortTextSecondary,
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                SizedBox(
                  height: 40,
                  child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    itemBuilder: (context, index) => ShortChip(
                      title: dayTitle[index],
                    ),
                    separatorBuilder: (context, index) =>
                        const SizedBox(width: 5),
                    itemCount: dayTitle.length,
                  ),
                ),
                const SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Container(
                    decoration: BoxDecoration(
                      color: const Color(0xffF3F4F6),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 15,
                    ),
                    child: const Row(
                      children: [
                        Expanded(
                          child: Text(
                            'Поступления за период',
                            style: TextStyle(
                              fontSize: P1TextStyle.fontSize,
                              fontWeight: P1TextStyle.fontWeight,
                              height: P1TextStyle.height,
                              color: ColorData.colorElementsSecondary,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            '+5 000₸',
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontSize: H4TextStyle.fontSize,
                              fontWeight: H4TextStyle.fontWeight,
                              height: H4TextStyle.height,
                              color: Color(0xff00C738),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SliverPadding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            sliver: SliverList(
              delegate: SliverChildBuilderDelegate(
                (ctx, index) {
                  if (index % 2 == 1) {
                    return const Divider(
                      height: 0.5,
                      thickness: 0.5,
                      color: Color(0xffCBCBCB),
                    );
                  }
                  return const OperationCard();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
