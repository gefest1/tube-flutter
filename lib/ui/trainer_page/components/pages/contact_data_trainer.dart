import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql/client.dart';

import 'package:rxdart/subjects.dart';
import 'package:tube/icon/facebook.icon.dart';
import 'package:tube/icon/instagram.icon.dart';
import 'package:tube/icon/telegram.icon.dart';
import 'package:tube/icon/whatsapp.icon.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/participance/participance_bloc.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/load_popup_route.dart';
import 'package:tube/utils/mask_text_input.dart';

import 'dart:math' as math;

class ContactDataTrainer extends StatefulWidget {
  final String clubId;
  final String? instagram;
  final String? facebook;
  final String? whatsapp;
  final String? telegram;
  final String? description;

  const ContactDataTrainer({
    required this.clubId,
    this.instagram,
    this.facebook,
    this.whatsapp,
    this.telegram,
    this.description,
    Key? key,
  }) : super(key: key);

  @override
  State<ContactDataTrainer> createState() => _ContactDataTrainerState();
}

class _ContactDataTrainerState extends State<ContactDataTrainer> {
  late final TextEditingController instagramController = TextEditingController(
    text: widget.instagram == null ? null : widget.instagram!,
  )..addListener(() => _subject.add(null));
  late final TextEditingController facebookController = TextEditingController(
    text: widget.facebook == null ? null : widget.facebook!,
  )..addListener(() => _subject.add(null));
  late final TextEditingController whatsappController =
      TextEditingController(text: widget.whatsapp)
        ..addListener(() => _subject.add(null));
  late final TextEditingController telegramController =
      TextEditingController(text: widget.telegram)
        ..addListener(() => _subject.add(null));
  late final TextEditingController descriptionController =
      TextEditingController(text: widget.description)
        ..addListener(() => _subject.add(null));

  final BehaviorSubject _subject = BehaviorSubject.seeded(null);

  final _inst = MaskTextInputFormatter(
    mask: "@AAAAAAAAAAAAAAAAAAAAAAAAAAAA",
    filter: {"A": RegExp('.*')},
  );
  final _facebook = MaskTextInputFormatter(
    mask: "@AAAAAAAAAAAAAAAAAAAAAAAAAAAA",
    filter: {"A": RegExp('.*')},
  );

  bool isValid() =>
      instagramController.text.isNotEmpty ||
      facebookController.text.isNotEmpty ||
      whatsappController.text.isNotEmpty ||
      telegramController.text.isNotEmpty ||
      descriptionController.text.isNotEmpty;

  void submitButton() async {
    if (!isValid()) {
      Navigator.pop(context);
      return;
    }

    final Completer completer = Completer();

    Navigator.push(
      context,
      LoadPopupRoute(completer: completer),
    );
    try {
      final inp = Variables$Mutation$setContactData.fromJson(
          Variables$Mutation$setContactData(
        instagram: instagramController.text.replaceAll(" ", ""),
        facebook: facebookController.text.replaceAll(" ", ""),
        whatsapp: whatsappController.text.replaceAll(" ", ""),
        telegram: telegramController.text.replaceAll(" ", ""),
        description: descriptionController.text,
        clubId: widget.clubId,
      ).toJson()
            ..removeWhere((key, value) => value.isEmpty));
      final mutationRes = await graphqlClient.mutate$setContactData(
        Options$Mutation$setContactData(
          variables: inp,
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (mutationRes.hasException) throw mutationRes.exception!;
      if (!mounted) return;
      BlocProvider.of<ParticipanceBloc>(
        context,
        listen: false,
      ).add(
        const GetDataParticipanceEvent(),
      );

      completer.complete();
    } catch (e, s) {
      completer.completeError(e, s);
    }
  }

  @override
  void dispose() {
    instagramController.dispose();
    facebookController.dispose();
    whatsappController.dispose();
    telegramController.dispose();
    descriptionController.dispose();
    _subject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final focus = FocusScope.of(context);

        if (focus.hasFocus || focus.hasPrimaryFocus) {
          focus.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Column(
            children: [
              H4Text(
                'Контактная информация',
                color: Colors.white,
              )
            ],
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              size: 33,
            ),
          ),
        ),
        body: Stack(
          children: [
            ListView(
              padding: const EdgeInsets.all(20),
              children: [
                CustomTextField(
                  icon: const CustomPaint(
                    painter: InstagramGlyphCustomPainter(),
                    child: SizedBox(
                      height: 24,
                      width: 24,
                    ),
                  ),
                  scrollPadding: const EdgeInsets.only(bottom: 100),
                  hintText: '@example',
                  title: 'Instagram',
                  inputFormatters: [_inst],
                  controller: instagramController,
                ),
                const SizedBox(height: 40),
                CustomTextField(
                  inputFormatters: [_facebook],
                  icon: const CustomPaint(
                    painter: FacebookCustomPainter(),
                    child: SizedBox(
                      height: 24,
                      width: 24,
                    ),
                  ),
                  scrollPadding: const EdgeInsets.only(bottom: 200),
                  controller: facebookController,
                  hintText: '@example',
                  title: 'Facebook',
                ),
                const SizedBox(height: 40),
                CustomTextField(
                  icon: const CustomPaint(
                    painter: WhatsappGlyphCustomPainter(),
                    child: SizedBox(
                      height: 24,
                      width: 24,
                    ),
                  ),
                  controller: whatsappController,
                  inputFormatters: const [],
                  scrollPadding: const EdgeInsets.only(bottom: 300),
                  hintText: '+77877787272',
                  title: 'What’s App',
                ),
                const SizedBox(height: 40),
                CustomTextField(
                  icon: const CustomPaint(
                    painter: TelegramCustomPainter(),
                    child: SizedBox(
                      height: 24,
                      width: 24,
                    ),
                  ),
                  scrollPadding: const EdgeInsets.only(bottom: 300),
                  controller: telegramController,
                  hintText: '+77877787272',
                  title: 'Номер телефона Telegram',
                ),
                const SizedBox(height: 40),
                CustomTextField(
                  controller: descriptionController,
                  hintText: 'Расскажите о себе',
                  scrollPadding: const EdgeInsets.only(bottom: 300),
                  title:
                      'Описание. Расскажите о подходе, опишите как помогаете клиентам. Можно добавить позже',
                  maxLines: null,
                  minLines: 1,
                ),
                const SizedBox(height: 40),
              ],
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: Container(
                padding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                  bottom: math.max(MediaQuery.of(context).padding.bottom, 10),
                  top: 10,
                ),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(2),
                  ),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0, -4),
                      blurRadius: 15,
                      color: Color(0x0d000000),
                    )
                  ],
                ),
                child: StreamBuilder(
                    stream: _subject,
                    builder: (context, _) {
                      return CustomButton(
                        onTap: submitButton,
                        title: isValid() ? 'Сохранить' : 'Пропустить',
                        titleColor: Colors.white,
                        color: isValid()
                            ? ColorData.colorMainLink
                            : ColorData.colorElementsActive,
                      );
                    }),
              ),
            )
          ],
        ),
      ),
    );
  }
}
