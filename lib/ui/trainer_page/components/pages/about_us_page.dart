import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:provider/provider.dart';
import 'package:tube/icon/facebook.icon.dart';
import 'package:tube/icon/instagram.icon.dart';
import 'package:tube/icon/telegram.icon.dart';
import 'package:tube/icon/whatsapp.icon.dart';
import 'package:tube/logic/blocs/participance/model/sub_data.model.dart';
import 'package:tube/logic/blocs/trainer/tariff_trainer_bloc/tariff_trainer_bloc.dart';
import 'package:tube/logic/blocs/trainer/work_time/work_time_bloc.dart';
import 'package:tube/logic/blocs/user/user_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/ui/common/atoms/read_more_widget.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/trainer_page/components/pages/contact_data_trainer.dart';
import 'package:tube/ui/trainer_page/components/pages/trainer_worker_time.dart';

import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/get_time_shift.dart';

class AboutUsPage extends StatelessWidget {
  const AboutUsPage({Key? key}) : super(key: key);

  List<Widget> subWidget(SubData subData) {
    final List<Widget> widgets = [];

    if (subData.instagram != null) {
      widgets.add(
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Row(
            children: [
              const CustomPaint(
                painter: InstagramGlyphCustomPainter(
                  color: ColorData.colorMainLink,
                ),
                size: Size(24, 24),
              ),
              const SizedBox(width: 10),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const H5Text(
                    'Instagram',
                    color: ColorData.colorTextMain,
                  ),
                  const SizedBox(height: 2),
                  P0Text(
                    subData.instagram!,
                    color: ColorData.colorMainLink,
                  )
                ],
              )),
            ],
          ),
        ),
      );
    }
    if (subData.telegram != null) {
      widgets.add(
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Row(
            children: [
              const CustomPaint(
                painter: TelegramCustomPainter(
                  color: ColorData.colorMainLink,
                ),
                size: Size(24, 24),
              ),
              const SizedBox(width: 10),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const H5Text(
                    'Telegram',
                    color: ColorData.colorTextMain,
                  ),
                  const SizedBox(height: 2),
                  P0Text(
                    subData.telegram!,
                    color: ColorData.colorMainLink,
                  )
                ],
              )),
            ],
          ),
        ),
      );
    }
    if (subData.whatsapp != null) {
      widgets.add(
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Row(
            children: [
              const CustomPaint(
                painter: WhatsappGlyphCustomPainter(
                  color: ColorData.colorMainLink,
                ),
                size: Size(24, 24),
              ),
              const SizedBox(width: 10),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const H5Text(
                    'What’s App',
                    color: ColorData.colorTextMain,
                  ),
                  const SizedBox(height: 2),
                  P0Text(
                    subData.whatsapp!,
                    color: ColorData.colorMainLink,
                  )
                ],
              )),
            ],
          ),
        ),
      );
    }
    if (subData.facebook != null) {
      widgets.add(
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Row(
            children: [
              const CustomPaint(
                painter: FacebookCustomPainter(
                  color: ColorData.colorMainLink,
                ),
                size: Size(24, 24),
              ),
              const SizedBox(width: 10),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const H5Text(
                    'Facebook',
                    color: ColorData.colorTextMain,
                  ),
                  const SizedBox(height: 2),
                  P0Text(
                    subData.facebook!,
                    color: ColorData.colorMainLink,
                  )
                ],
              )),
            ],
          ),
        ),
      );
    }
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    late final TariffTrainerBloc tariffTrainerBloc =
        Provider.of<TrainerBlocMap>(context, listen: false)
            .getTariffTrainerBloc(MainDrawerState.currentParticipance!.clubId!);
    late ControllerWorkTimeBloc workTimeBloc = Provider.of<TrainerBlocMap>(context, listen: false)
        .getWorkTimeBloc(MainDrawerState.currentParticipance!.clubId!);

    final participanceSubject = MainDrawerState.currentParticipanceValue;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(
            Icons.arrow_back,
            size: 33,
          ),
          color: Colors.white,
        ),
        title: const H4Text(
          'О вас',
          color: Colors.white,
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.only(top: 20, bottom: 100),
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: BlocBuilder<UserBloc, UserState>(
              builder: (ctx, state) {
                if (state is DataUserState) {
                  if ((state.user.fullName ?? state.user.email) == null) {
                    return const SizedBox();
                  }
                  return H2Text(
                    (state.user.fullName ?? state.user.email)!,
                    color: ColorData.colorElementsActive,
                  );
                }
                return const SizedBox();
              },
            ),
          ),
          const SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BlocBuilder<UserBloc, UserState>(
                  builder: (ctx, state) {
                    if (state is! DataUserState) {
                      return const SizedBox(
                        height: 70,
                        width: 70,
                      );
                    }
                    if ((state.user.fullName ?? state.user.email) == null) {
                      return const SizedBox(
                        height: 70,
                        width: 70,
                      );
                    }
                    return CircleAvatar(
                      radius: 52,
                      backgroundColor: ColorData.color3Percent,
                      foregroundImage: state.user.photoUrl?.xl == null
                          ? null
                          : PCacheImage(
                              state.user.photoUrl!.xl!,
                              enableInMemory: true,
                            ),
                      child: const Center(
                        child: Icon(
                          Icons.perm_contact_cal,
                          size: 70,
                          color: ColorData.colorElementsActive,
                        ),
                      ),
                    );
                  },
                ),
                const SizedBox(width: 10),
                Flexible(
                  child: Container(
                    width: 104,
                    padding: const EdgeInsets.all(10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                      color: Colors.white,
                      boxShadow: blocksMain,
                    ),
                    child: LayoutBuilder(builder: (context, l) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          BlocBuilder<TariffTrainerBloc, TariffTrainerState>(
                            bloc: tariffTrainerBloc,
                            builder: (ctx, state) {
                              if (state is! GetTariffTrainerState) {
                                return const SizedBox();
                              }
                              return H2Text(
                                state.dirtTariffList.length.toString(),
                                color: Colors.black,
                              );
                            },
                          ),
                          const SizedBox(height: 14),
                          const P2Text(
                            'Записей к вам',
                            color: ColorData.colorMainLink,
                          ),
                        ],
                      );
                    }),
                  ),
                ),
                const SizedBox(width: 10),
                Flexible(
                  fit: FlexFit.tight,
                  child: SizeTapAnimation(
                    child: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => TrainerWorkerTime(
                              clubId: MainDrawerState.currentParticipance!.clubId!,
                            ),
                          ),
                        );
                      },
                      child: Container(
                          padding: const EdgeInsets.all(10),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                            color: Colors.white,
                            boxShadow: blocksMain,
                          ),
                          child: BlocBuilder<ControllerWorkTimeBloc, WorkTimeState>(
                            bloc: workTimeBloc,
                            builder: (ctx, state) {
                              if (state is! SuccessWorkTimeState) {
                                return const SizedBox();
                              }
                              final weekday = (DateTime.now().weekday + 6) % 7;
                              if (state.workTime[weekday] == null) {
                                return const Padding(
                                  padding: EdgeInsets.symmetric(vertical: 32),
                                  child: Center(
                                    child: P2Text(
                                      'Выходной',
                                      color: ColorData.colorMainLink,
                                    ),
                                  ),
                                );
                              }

                              final start = DateFormat("HH:mm")
                                  .format(state.workTime[weekday]!.startTime!.parseTimeGG);
                              final end = DateFormat("HH:mm")
                                  .format(state.workTime[weekday]!.endTime!.parseTimeGG);
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  H4Text(
                                    "$start — $end",
                                    color: Colors.black,
                                  ),
                                  const SizedBox(height: 8),
                                  const P2Text(
                                    'Рабочее время',
                                    color: ColorData.colorMainLink,
                                  ),
                                ],
                              );
                            },
                          )),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: SizeTapAnimation(
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => ContactDataTrainer(
                        description: participanceSubject.value?.subData?.description,
                        instagram: participanceSubject.value?.subData?.instagram,
                        facebook: participanceSubject.value?.subData?.facebook,
                        telegram: participanceSubject.value?.subData?.telegram,
                        whatsapp: participanceSubject.value?.subData?.whatsapp,
                        clubId: participanceSubject.value!.clubId!,
                      ),
                    ),
                  );
                },
                child: Container(
                  decoration: const BoxDecoration(
                    color: ColorData.color3Percent,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  padding: const EdgeInsets.only(
                    left: 15,
                    right: 15,
                    top: 15,
                    bottom: 20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const H5Text(
                            'Личная информация',
                            color: ColorData.colortTextSecondary,
                          ),
                          GestureDetector(
                            onTap: () {},
                            behavior: HitTestBehavior.opaque,
                            child: const IgnorePointer(
                              child: P2Text(
                                'Изменить',
                                color: ColorData.colorMainLink,
                              ),
                            ),
                          )
                        ],
                      ),
                      StreamBuilder(
                        stream: MainDrawerState.currentParticipanceValue,
                        builder: (ctx, _) {
                          if (participanceSubject.value?.subData?.description == null) {
                            return const SizedBox();
                          }
                          return Padding(
                            padding: const EdgeInsets.only(top: 15),
                            child: ReadMoreWidget(
                              participanceSubject.value!.subData!.description!,
                              trimLines: 4,
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          // BlocBuilder<TariffTrainerBloc, TariffTrainerState>(
          //   bloc: _tariffTrainerBloc,
          //   builder: (ctx, state) {
          //     if (state is! GetTariffTrainerState) {
          //       return const SizedBox();
          //     }
          //     final booking = state.bookingMap.values.toList();
          //     return SizedBox(
          //       height: 158,
          //       child: ListView.separated(
          //         padding: const EdgeInsets.symmetric(horizontal: 20),
          //         clipBehavior: Clip.none,
          //         scrollDirection: Axis.horizontal,
          //         itemBuilder: (context, index) {
          //           return BookingCard(
          //             onTap: () {},
          //             title: booking[index].name,
          //             subTitle: "Без сотрудников",
          //             label: booking[index].price == null
          //                 ? null
          //                 : NumberFormat('###,###,000₸')
          //                     .format(booking[index].price!),
          //             // '${booking[index].price!.toInt()} тг.',
          //             iconWidget: const Icon(
          //               Icons.fiber_smart_record,
          //               color: ColorData.colorMainLink,
          //               size: 32,
          //             ),
          //           );
          //         },
          //         separatorBuilder: (context, index) =>
          //             const SizedBox(width: 10),
          //         itemCount: booking.length,
          //       ),
          //     );
          //   },
          // ),
          const SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: SizeTapAnimation(
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => ContactDataTrainer(
                        description: participanceSubject.value?.subData?.description,
                        instagram: participanceSubject.value?.subData?.instagram,
                        facebook: participanceSubject.value?.subData?.facebook,
                        telegram: participanceSubject.value?.subData?.telegram,
                        whatsapp: participanceSubject.value?.subData?.whatsapp,
                        clubId: participanceSubject.value!.clubId!,
                      ),
                    ),
                  );
                },
                child: Container(
                  padding: const EdgeInsets.only(left: 15, right: 15, bottom: 5, top: 15),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: ColorData.color3Percent,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          H5Text(
                            'Контакты',
                            color: ColorData.colortTextSecondary,
                          ),
                          P2Text(
                            'Изменить',
                            color: ColorData.colorMainLink,
                          ),
                        ],
                      ),
                      StreamBuilder(
                        stream: MainDrawerState.currentParticipanceValue,
                        builder: (ctx, _) {
                          if (participanceSubject.value?.subData == null) {
                            return const SizedBox(height: 15);
                          }
                          final subData = participanceSubject.value!.subData!;
                          final widgets = subWidget(subData);

                          if (widgets.isEmpty) return const SizedBox();
                          return Column(
                            children: List.generate(
                              widgets.length * 2 - 1,
                              (index) => index.isOdd
                                  ? const Divider(
                                      color: ColorData.color5Percent,
                                      height: 0,
                                      thickness: 0.5,
                                    )
                                  : widgets[index ~/ 2],
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
