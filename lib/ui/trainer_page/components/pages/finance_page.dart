import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tube/ui/admin/page/tariff/molecules/edit_tariff_header.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:tube/utils/infinite_listview.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/tariff/model/train_time.model.dart';
import 'package:tube/logic/blocs/trainer/tariff_trainer_bloc/tariff_trainer_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/shedule_table.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/trainer_page/components/molecules/shedule_card.dart';
import 'package:tube/utils/colors.dart';

import 'package:tube/utils/is_same_day.dart';
import 'package:tube/ui/admin/page/tariff/pages/tariff_trainer_detail_page.dart';

class FinancePage extends StatefulWidget {
  const FinancePage({Key? key}) : super(key: key);

  @override
  State<FinancePage> createState() => _FinancePageState();
}

class _FinancePageState extends State<FinancePage> {
  final currentYear = DateTime.now().year;
  final currentMonth = DateTime.now().month;

  final lastIndex = BehaviorSubject.seeded(0);
  final BehaviorSubject<DateTime> dateTimeSubject =
      BehaviorSubject<DateTime>.seeded(DateTime.now());
  late final InfiniteScrollController infiniteScrollController = InfiniteScrollController();

  late TariffTrainerBloc tariffTrainerBloc = Provider.of<TrainerBlocMap>(context, listen: false)
      .getTariffTrainerBloc(MainDrawerState.currentParticipance!.clubId!);

  @override
  void initState() {
    tariffTrainerBloc.add(const GetSoonTariffTrainerEvent());
    super.initState();
  }

  @override
  void dispose() {
    infiniteScrollController.dispose();
    lastIndex.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: SizedBox(
                  height: MediaQuery.of(context).padding.top + 60,
                ),
              ),
              SliverToBoxAdapter(
                child: Column(
                  children: [
                    const SizedBox(height: 20),
                    BlocBuilder<TariffTrainerBloc, TariffTrainerState>(
                      bloc: Provider.of<TrainerBlocMap>(context, listen: false)
                          .getTariffTrainerBloc(MainDrawerState.currentParticipance!.clubId!),
                      builder: (ctx, state) {
                        final Map<String, Set<String>> dateTimeMapWidget = {};
                        if (state is GetTariffTrainerState) {
                          for (final single in state.dirtTariffList) {
                            for (TrainTime singleTrainTime in single.trainTimes ?? []) {
                              if (singleTrainTime.date == null) continue;
                              final key = dayOnlyTime(singleTrainTime.date!).toIso8601String();
                              (dateTimeMapWidget[key] ??= {}).add('booking');
                            }
                          }
                        }
                        return SheduleTable(
                          controller: infiniteScrollController,
                          lastIndexSubject: lastIndex,
                          horizontalPadding: const EdgeInsets.symmetric(horizontal: 20),
                          onTap: (DateTime dateTime) {
                            dateTimeSubject.value = dateTime;
                          },
                          dateTimeMapWidget: dateTimeMapWidget,
                          dateTimeSubject: dateTimeSubject,
                        );
                      },
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
              SliverFillRemaining(
                hasScrollBody: false,
                child: DecoratedBox(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 15,
                        offset: Offset(0, -4),
                        color: Color(0x0d000000),
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 20,
                      right: 20,
                      bottom: 100,
                      top: 15,
                    ),
                    child: BlocBuilder<TariffTrainerBloc, TariffTrainerState>(
                      bloc: Provider.of<TrainerBlocMap>(context, listen: false)
                          .getTariffTrainerBloc(MainDrawerState.currentParticipance!.clubId!),
                      builder: (ctx, state) {
                        final Map<String, Map<String, List<TariffSingle>>> tariffDayMap = {};
                        final Map<String, Booking> bookingMap = {};

                        if (state is GetTariffTrainerState) {
                          for (final single in state.dirtTariffList) {
                            for (TrainTime singleTrainTime in single.trainTimes ?? []) {
                              final currentDate = singleTrainTime.date;
                              if (currentDate == null) continue;
                              if (single.bookingId != null && single.booking != null) {
                                bookingMap[single.bookingId!] = single.booking!;
                              }

                              ((tariffDayMap[dayOnlyTime(currentDate).toIso8601String()] ??=
                                      {})[single.bookingId!] ??= [])
                                  .add(
                                TariffSingle.fromMapTariff(
                                  single,
                                  singleTrainTime,
                                ),
                              );
                            }
                          }
                        }
                        return StreamBuilder(
                          stream: dateTimeSubject,
                          builder: (ctx, _) {
                            final Map<String, List<TariffSingle>> tarifMap = tariffDayMap[
                                    dayOnlyTime(dateTimeSubject.value).toIso8601String()] ??
                                {};
                            final entries = tarifMap.entries.toList();
                            if (entries.isEmpty) {
                              return const Align(
                                alignment: Alignment.topCenter,
                                child: H4Text(
                                  'В этот день событий нет',
                                  color: ColorData.colortTextSecondary,
                                ),
                              );
                            }
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                H4Text(
                                  DateFormat("d MMMM yyyy")
                                      .format(dateTimeSubject.value.parseTimeGG),
                                  color: ColorData.colortTextSecondary,
                                ),
                                ...List.generate(entries.length * 2 - 1, (ind) {
                                  if (ind.isOdd) {
                                    return const Divider(
                                      color: Color(0xffEBEBEB),
                                      height: 0,
                                      thickness: 0.5,
                                    );
                                  }
                                  final index = ind ~/ 2;
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 20),
                                    child: SheduleCard(
                                      title: bookingMap[entries[index].key]?.name,
                                      trainTimeList: entries[index].value,
                                      // label: ,
                                      tariffTap: ({
                                        required String mapKeyUnique,
                                        required List<TariffSingle> list,
                                        required DateTime dateTime,
                                      }) {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (_) {
                                              return TariffTrainerDetailPage(
                                                list: list,
                                                booking: bookingMap[entries[index].key]!,
                                                dateTime: dateTime,
                                                uniqueMapKey: "${entries[index].key}_$mapKeyUnique",
                                              );
                                            },
                                          ),
                                        );
                                      },
                                    ),
                                  );
                                }),
                              ],
                            );
                          },
                          // builder: dateTime,
                        );
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
          Material(
            color: ColorData.colorMainElements,
            child: Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              child: EditTariffHeader(
                controller: infiniteScrollController,
                lastIndex: lastIndex,
                currentYear: currentYear,
                currentMonth: currentMonth,
              ),
              // SizedBox(
              //   height: 60,
              //   child: Center(
              //     child: Row(
              //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //       children: [
              //         IconButton(
              //           onPressed: () =>
              //               sheduleTableController.prevPage?.call(),
              //           icon: const Icon(Icons.arrow_back_ios),
              //           iconSize: 33,
              //         ),
              //         Center(
              //           child: StreamBuilder(
              //             stream: lastIndex,
              //             builder: (context, snapshot) {
              //               final currentDateTime = DateTime(
              //                   currentYear, currentMonth + lastIndex.value);
              //               final month = currentDateTime.month - 1;
              //               return Column(
              //                 mainAxisSize: MainAxisSize.min,
              //                 children: [
              //                   H4Text(
              //                     monthName[month],
              //                     color: Colors.white,
              //                   ),
              //                   P2Text(
              //                     currentDateTime.year.toString(),
              //                     color: ColorData.color3Percent,
              //                   )
              //                 ],
              //               );
              //             },
              //           ),
              //         ),
              //         IconButton(
              //           onPressed: () =>
              //               sheduleTableController.nextPage?.call(),
              //           icon: const Icon(Icons.arrow_forward_ios),
              //           iconSize: 33,
              //         ),
              //       ],
              //     ),
              //   ),
              // ),
            ),
          ),
        ],
      ),
    );
  }
}
