import 'package:flutter/material.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/register/enter_gmail.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class RegisterBottom extends StatelessWidget {
  const RegisterBottom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Container(
      decoration: const BoxDecoration(
        color: Color(0xffF8F8F8),
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(5),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(
            height: 52,
            child: Center(
              child: Text(
                'Войти',
                style: TextStyle(
                  fontWeight: H4TextStyle.fontWeight,
                  fontSize: H4TextStyle.fontSize,
                  height: H4TextStyle.height,
                  color: ColorData.colorTextMain,
                ),
              ),
            ),
          ),
          Container(
            color: Colors.white,
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                const SizedBox(height: 20),
                SizeTapAnimation(
                  child: GestureDetector(
                    onTap: () {
                      showFlexibleBottomSheet(
                        context: context,
                        minHeight: 0,
                        initHeight: 0.95,
                        maxHeight: 0.95,
                        isDismissible: true,
                        anchors: [0.95],
                        builder:
                            (context, scrollController, bottomSheetOffset) {
                          return EnterGmailPage(controller: scrollController);
                        },
                      );
                    },
                    child: Container(
                      height: 56,
                      decoration: BoxDecoration(
                        color: ColorData.colorElementsActive,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: const [
                          BoxShadow(
                            offset: Offset(2, 2),
                            blurRadius: 5,
                            color: Color(0x26000000),
                          )
                        ],
                      ),
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.alternate_email),
                          SizedBox(width: 5),
                          Text(
                            'По электронной почте',
                            style: TextStyle(
                              fontSize: P1TextStyle.fontSize,
                              fontWeight: P1TextStyle.fontWeight,
                              height: P1TextStyle.height,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                // Container(
                //   height: 56,
                //   decoration: BoxDecoration(
                //     color: ColorData.colorMainLink,
                //     borderRadius: BorderRadius.circular(5),
                //     boxShadow: const [
                //       BoxShadow(
                //         offset: Offset(2, 2),
                //         blurRadius: 5,
                //         color: Color(0x26000000),
                //       )
                //     ],
                //   ),
                //   child: Row(
                //     mainAxisAlignment: MainAxisAlignment.center,
                //     children: const [
                //       Icon(Icons.email),
                //       SizedBox(width: 5),
                //       Text(
                //         'По электронной почте',
                //         style: TextStyle(
                //           fontSize: P1TextStyle.fontSize,
                //           fontWeight: P1TextStyle.fontWeight,
                //           height: P1TextStyle.height,
                //         ),
                //       )
                //     ],
                //   ),
                // ),
                SizedBox(
                  height: mediaQuery.padding.bottom + 20,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
