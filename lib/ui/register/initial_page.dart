import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tube/icon/main_key.icon.dart';
import 'package:tube/ui/register/register_bottom.dart';
import 'package:tube/utils/colors.dart';
import 'dart:math' as math;

class InitialPage extends StatelessWidget {
  const InitialPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: Material(
        color: const Color(0xff141414),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Flexible(
                child: Center(
                  child: ConstrainedBox(
                    constraints: const BoxConstraints(
                      maxWidth: 400,
                      maxHeight: 400,
                    ),
                    child: const AspectRatio(
                      aspectRatio: 1,
                      child: CustomPaint(
                        painter: InitialKeyCustomPainter(),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              const Text(
                'Доступ к компаниям,\nуслугам и абонементам',
                style: TextStyle(
                  fontSize: 30,
                  color: Color(0xffD2D2D2),
                  height: 35 / 30,
                  fontWeight: FontWeight.w400,
                ),
                textAlign: TextAlign.center,
              ),
              Flexible(
                child: LayoutBuilder(builder: (context, constraints) {
                  return SizedBox(
                    height: math.min(constraints.maxHeight * 0.8, 146),
                  );
                }),
              ),
              const SizedBox(height: 16),
              GestureDetector(
                onTap: () {
                  showModalBottomSheet(
                    backgroundColor: Colors.transparent,
                    isScrollControlled: true,
                    isDismissible: true,
                    context: context,
                    builder: (ctx) => const RegisterBottom(),
                  );
                },
                child: Container(
                  height: 61,
                  decoration: BoxDecoration(
                    color: ColorData.colorElementsActive,
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: const [
                      BoxShadow(
                        offset: Offset(2, 2),
                        blurRadius: 5,
                        color: Color(0x26000000),
                      )
                    ],
                  ),
                  alignment: Alignment.center,
                  child: const Text(
                    'Войти',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                      height: 21 / 18,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 60),
              SizedBox(height: mediaQuery.padding.bottom),
            ],
          ),
        ),
      ),
    );
  }
}
