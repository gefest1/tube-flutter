import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';

import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/fast_app_bar.dart';
import 'package:tube/ui/register/molecules/bottom_finish_page.dart';
import 'package:tube/utils/colors.dart';
import 'package:http/http.dart' show MultipartFile;
import 'package:tube/utils/img_parser.dart';
import 'package:tube/utils/loader.dart';
import 'package:tube/logic/blocs/user/user_bloc.dart';
import 'package:tube/utils/restart_widget.dart';

class InputModel {
  late final TextEditingController fullNameController;
  late final TextEditingController dateController;
  late final TextEditingController phoneNumberController;
  late final TextEditingController iinController;

  InputModel(VoidCallback callback) {
    fullNameController = TextEditingController()..addListener(callback);
    dateController = TextEditingController()..addListener(callback);
    phoneNumberController = TextEditingController()..addListener(callback);
    iinController = TextEditingController()..addListener(callback);
  }
}

class RegisterCompany extends StatefulWidget {
  const RegisterCompany({Key? key}) : super(key: key);

  @override
  State<RegisterCompany> createState() => _RegisterCompanyState();
}

extension _Dis on _RegisterCompanyState {
  TextEditingController get fullNameController =>
      _inputModel.fullNameController;

  TextEditingController get phoneNumberController =>
      _inputModel.phoneNumberController;
  TextEditingController get iinController => _inputModel.iinController;
  void _dispose() {
    _strUpt.close();
    fullNameController.dispose();

    phoneNumberController.dispose();
    iinController.dispose();
  }
}

class _RegisterCompanyState extends State<RegisterCompany> {
  late final _media = MediaQuery.of(navigatorKeyGlobal.currentContext!);
  late final InputModel _inputModel = InputModel(() => _strUpt.add(null));
  final StreamController _strUpt = StreamController();
  XFile? _file;

  bool _validate() =>
      fullNameController.text.isNotEmpty &&
      phoneNumberController.text.length > 7 &&
      iinController.text.length == 12;

  @override
  void dispose() {
    _dispose();
    super.dispose();
  }

  void _register() async {
    loaderActivate(context);
    try {
      final mutationResult = await graphqlClient.mutate$registerUser(
        Options$Mutation$registerUser(
          variables: Variables$Mutation$registerUser(
            iin: iinController.text,
            fullName: fullNameController.text,
            phonNumber: phoneNumberController.text,
            photo: _file == null
                ? null
                : MultipartFile.fromBytes(
                    '',
                    await byeImageExif(await _file!.readAsBytes()),
                    filename: _file!.name,
                  ),
          ),
        ),
      );

      if (mutationResult.hasException) throw mutationResult.exception!;
      if (!mounted) return;
      final data = mutationResult.parsedData!;
      final user = User.fromMap(data.registerUser.toJson());
      BlocProvider.of<UserBloc>(context, listen: false)
          .add(InitUserEvent(user));
      await sharedBox.put('token', data.updateToken);
      if (!mounted) return;
      RestartWidget.restartApp(context);
    } catch (e) {
      log(e.toString());
    }
    killLoaderOverlay();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);
        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Container(
        height: (_media.size.height - 56 / 2 - _media.padding.top) * 0.98,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(10),
          ),
        ),
        alignment: Alignment.topCenter,
        child: Stack(
          children: [
            Column(
              children: [
                FastAppBar(
                  title: 'Регистрация',
                  suffIcon: Icons.close,
                  suffOnTap: () => Navigator.pop(context),
                ),
                Expanded(
                  child: ListView(
                    padding: const EdgeInsets.only(
                      left: 20,
                      right: 20,
                      top: 20,
                      bottom: 60,
                    ),
                    children: [
                      // Center(
                      //   child: LoadPhotoCircle(
                      //     file: _file,
                      //     size: 126,
                      //     onTap: () async {
                      //       final ImagePicker _picker = ImagePicker();
                      //       _file = await _picker.pickImage(
                      //           source: ImageSource.gallery);
                      //       setState(() {});
                      //     },
                      //   ),
                      // ),
                      // const SizedBox(height: 20),
                      // const Center(
                      //   child: Text(
                      //     'Tube',
                      //     style: TextStyle(
                      //       fontSize: H2TextStyle.fontSize,
                      //       fontWeight: H2TextStyle.fontWeight,
                      //       height: H2TextStyle.height,
                      //       color: ColorData.colorTextMain,
                      //     ),
                      //   ),
                      // ),
                      // const SizedBox(height: 40),
                      CustomTextField(
                        scrollPadding: const EdgeInsets.only(bottom: 400),
                        hintText: 'Иванов Иван',
                        title:
                            'Как к вам обращаться? Ваше имя будет отображаться для администрации и тренеров клуба.',
                        controller: fullNameController,
                      ),
                      const SizedBox(height: 40),
                      CustomTextField(
                        scrollPadding: const EdgeInsets.only(bottom: 400),
                        icon: const Icon(
                          Icons.beenhere,
                          color: ColorData.colorElementsActive,
                        ),
                        controller: iinController,
                        keyboardType: TextInputType.number,
                        hintText: '12 цифр',
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(12),
                          FilteringTextInputFormatter.digitsOnly,
                        ],
                        title: 'ИИН',
                      ),
                      const SizedBox(height: 36),
                      CustomTextField(
                        scrollPadding: const EdgeInsets.only(bottom: 400),
                        icon: const Icon(
                          Icons.call,
                          color: ColorData.colorElementsActive,
                        ),
                        controller: phoneNumberController,
                        keyboardType: TextInputType.phone,
                        hintText: '+7',
                        title:
                            'Номер телефона. Нужен для быстрой связи с вами.',
                      ),
                      if (_validate()) const SizedBox(height: 100),
                      SizedBox(
                        height: MediaQuery.of(context).viewInsets.bottom,
                      ),
                    ],
                  ),
                )
              ],
            ),
            StreamBuilder(
              builder: (ctx, _) {
                if (_validate()) {
                  return BottomFinishPage(
                    onTap: _register,
                    inputModel: _inputModel,
                  );
                }
                return const SizedBox();
              },
              stream: _strUpt.stream,
            ),
          ],
        ),
      ),
    );
  }
}
