// import 'dart:async';

// import 'package:flutter/material.dart';
// import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
// import 'package:tube/ui/common/atoms/load_photo_circle.dart';
// import 'package:tube/ui/common/molecules/custom_text_field.dart';
// import 'package:tube/ui/common/molecules/size_tap_animation.dart';
// import 'package:tube/ui/create_company/occupation_company_page.dart';
// import 'package:tube/utils/colors.dart';
// import 'package:tube/utils/const.dart';
// import 'package:image_picker/image_picker.dart';

// class NewCompany extends StatefulWidget {
//   final ScrollController controller;
//   const NewCompany({
//     required this.controller,
//     Key? key,
//   }) : super(key: key);

//   @override
//   State<NewCompany> createState() => _NewCompanyState();
// }

// class _NewCompanyState extends State<NewCompany> {
//   XFile? _file;
//   late final TextEditingController nameController = TextEditingController()
//     ..addListener(() {
//       streamController.add(null);
//     });
//   final StreamController streamController = StreamController();

//   void _open() async {
//     if (!(_file != null && nameController.text.isNotEmpty)) return;
//     showFlexibleBottomSheet(
//       minHeight: 0,
//       initHeight: 0.95,
//       maxHeight: 0.95,
//       context: context,
//       anchors: [0.95],
//       builder: (ctx, scroll, _) {
//         return Material(
//           color: Colors.transparent,
//           child: OccupationCompanyPage(
//             file: _file!,
//             fullName: nameController.text.trim(),
//             name: nameController.text.trim(),
//             scrollController: scroll,
//           ),
//         );
//       },
//     );
//   }

//   @override
//   void dispose() {
//     streamController.close();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     final media = MediaQuery.of(context);
//     return GestureDetector(
//       onTap: () {
//         final f = FocusScope.of(context);

//         if (f.hasFocus || f.hasPrimaryFocus) {
//           f.unfocus();
//         }
//       },
//       child: Material(
//         color: Colors.transparent,
//         child: Container(
//           decoration: const BoxDecoration(
//             color: Colors.white,
//             borderRadius: BorderRadius.vertical(
//               top: Radius.circular(10),
//             ),
//           ),
//           alignment: Alignment.topCenter,
//           child: CustomScrollView(
//             controller: widget.controller,
//             slivers: [
//               SliverToBoxAdapter(
//                 child: SizedBox(
//                   height: 52,
//                   child: Stack(
//                     children: [
//                       const Center(
//                         child: Text(
//                           'Создание компании',
//                           style: TextStyle(
//                             fontSize: H4TextStyle.fontSize,
//                             fontWeight: H4TextStyle.fontWeight,
//                             height: H4TextStyle.height,
//                             color: ColorData.colorTextMain,
//                           ),
//                         ),
//                       ),
//                       Positioned(
//                         top: 0,
//                         left: 0,
//                         bottom: 0,
//                         child: SizeTapAnimation(
//                           child: GestureDetector(
//                             behavior: HitTestBehavior.translucent,
//                             onTap: () => Navigator.pop(context),
//                             child: const Padding(
//                               padding: EdgeInsets.all(14.0),
//                               child: Icon(
//                                 Icons.arrow_back,
//                                 size: 24,
//                                 color: ColorData.colorMainElements,
//                               ),
//                             ),
//                           ),
//                         ),
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//               SliverPadding(
//                 padding: const EdgeInsets.symmetric(horizontal: 20),
//                 sliver: SliverToBoxAdapter(
//                   child: Column(
//                     children: [
//                       const SizedBox(height: 40),
//                       Center(
//                         child: LoadPhotoCircle(
//                           file: _file,
//                           onTap: () async {
//                             final file = await ImagePicker().pickImage(
//                               source: ImageSource.gallery,
//                             );
//                             if (_file != file) {
//                               setState(() {
//                                 _file = file;
//                               });
//                             }
//                           },
//                         ),
//                       ),
//                       const SizedBox(height: 40),
//                       CustomTextField(
//                         controller: nameController,
//                         scrollPadding: const EdgeInsets.only(bottom: 400),
//                         hintText: 'Название компании',
//                         title: 'Как будет называться ваша компания?',
//                       ),
//                       SizedBox(
//                         height: 20 + media.viewInsets.bottom,
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//               SliverPadding(
//                 padding: const EdgeInsets.symmetric(horizontal: 20),
//                 sliver: SliverFillRemaining(
//                   hasScrollBody: false,
//                   child: Align(
//                     alignment: Alignment.bottomCenter,
//                     child: StreamBuilder(
//                       stream: streamController.stream,
//                       builder: (context, AsyncSnapshot async) {
//                         return AnimatedOpacity(
//                           opacity:
//                               _file != null && nameController.text.isNotEmpty
//                                   ? 1
//                                   : 0,
//                           duration: const Duration(milliseconds: 300),
//                           child: Column(
//                             mainAxisSize: MainAxisSize.min,
//                             children: [
//                               SizeTapAnimation(
//                                 child: GestureDetector(
//                                   onTap: _open,
//                                   child: Container(
//                                     height: 56,
//                                     width: double.infinity,
//                                     alignment: Alignment.center,
//                                     decoration: BoxDecoration(
//                                       color: ColorData.colorElementsActive,
//                                       borderRadius: BorderRadius.circular(5),
//                                     ),
//                                     child: const Text(
//                                       'Продолжить',
//                                       style: TextStyle(
//                                         color: Colors.white,
//                                         fontSize: P1TextStyle.fontSize,
//                                         fontWeight: P1TextStyle.fontWeight,
//                                         height: P1TextStyle.height,
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                               const SizedBox(height: 10),
//                               RichText(
//                                 textAlign: TextAlign.center,
//                                 text: const TextSpan(
//                                   style: TextStyle(
//                                     fontSize: P2TextStyle.fontSize,
//                                     height: P2TextStyle.height,
//                                     fontWeight: P2TextStyle.fontWeight,
//                                   ),
//                                   children: [
//                                     TextSpan(
//                                       text:
//                                           'Продолжая создание компании, вы принимаете наши ',
//                                       style: TextStyle(
//                                         color: ColorData.colortTextSecondary,
//                                       ),
//                                     ),
//                                     TextSpan(
//                                       text:
//                                           'Условия использования для клиентов ',
//                                       style: TextStyle(
//                                         color: ColorData.colorMainLink,
//                                       ),
//                                     ),
//                                     TextSpan(
//                                       text: 'и ',
//                                       style: TextStyle(
//                                         color: ColorData.colortTextSecondary,
//                                       ),
//                                     ),
//                                     TextSpan(
//                                       text: 'Политику конфиденциальности',
//                                       style: TextStyle(
//                                         color: ColorData.colorMainLink,
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                               const SizedBox(height: 20),
//                             ],
//                           ),
//                         );
//                       },
//                     ),
//                   ),
//                 ),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
