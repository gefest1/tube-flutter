import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/model/status_enum.dart';
import 'package:tube/ui/add_compnay/pages/share_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/cards_center_share.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/uri_parse.dart';

// final inviteManyUsers = gql('''
// mutation inviteManyUsers(
//   \$clubId: String!
// 	\$emails: [String!]!
//   \$status: StatusEnumParticipance!
// ){
//   inviteUsers(
//     clubId: \$clubId
//     emails: \$emails
//     status: \$status
//   )
// }''');

class FinishPage extends StatefulWidget {
  final String clubId;
  final ScrollController controller;
  final StatusEnum statusEnum;
  final TypeInviteEnum enumType;
  final String title;
  final bool next;

  const FinishPage({
    required this.clubId,
    required this.controller,
    required this.statusEnum,
    required this.enumType,
    this.title = 'Род деятельности',
    this.next = true,
    Key? key,
  }) : super(key: key);

  @override
  State<FinishPage> createState() => _FinishPageState();
}

class _FinishPageState extends State<FinishPage> {
  void shareGmail() async {
    final newUsers = await showFlexibleBottomSheet<List<String>>(
      minHeight: 0,
      initHeight: 0.95,
      maxHeight: 0.95,
      context: context,
      anchors: [0.95],
      builder: (ctx, controller, _) {
        return Material(
          color: Colors.transparent,
          child: ShareSheetPage(
            controller: controller,
          ),
        );
      },
    );
    if (newUsers == null) return;
    if (!mounted) return;
    final mutRes = await graphqlClient.mutate$inviteManyUsers(
      Options$Mutation$inviteManyUsers(
        variables: Variables$Mutation$inviteManyUsers(
          clubId: widget.clubId,
          emails: newUsers,
          status: widget.enumType.getEnum,
        ),
      ),
    );

    if (mutRes.hasException) {
      log(mutRes.exception.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(10),
        ),
      ),
      child: CustomScrollView(
        controller: widget.controller,
        slivers: [
          SliverToBoxAdapter(
            child: SizedBox(
              height: 52,
              child: Stack(
                children: [
                  Center(
                    child: H4Text(
                      widget.title,
                      color: ColorData.colorTextMain,
                    ),
                  ),
                  if (widget.next)
                    Positioned(
                      right: 0,
                      top: 0,
                      bottom: 0,
                      child: SizeTapAnimation(
                        child: GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () => Navigator.pop(context),
                          child: const Padding(
                            padding: EdgeInsets.all(17.5),
                            child: H5Text(
                              'Далее',
                              color: ColorData.colorMainLink,
                            ),
                          ),
                        ),
                      ),
                    ),
                ],
              ),
            ),
          ),
          SliverPadding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 40),
            sliver: SliverToBoxAdapter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (widget.enumType == TypeInviteEnum.InviteTrainer)
                    const P0Text(
                      'Пригласите ваших работников в Checkallnow. Вы сможете назначить роли позже',
                      color: ColorData.colorElementsSecondary,
                    ),
                  if (widget.enumType == TypeInviteEnum.InviteClient)
                    const P0Text(
                      'Приглашайте клиентов по ссылке, отправляйте личным сообщением или выкладывайте в свои социальные сети',
                      color: ColorData.colorElementsSecondary,
                    ),
                  const SizedBox(height: 20),
                  CardsCenterShare(
                    emailTap: shareGmail,
                    shareLinkTap: () async {
                      Share.share(uriParse(
                        {
                          "clubId": widget.clubId,
                          "type": widget.enumType.toString(),
                        },
                      ));
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
