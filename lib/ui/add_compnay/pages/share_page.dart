// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:tube/ui/admin/molecules/add_mail_body.dart';
import 'package:tube/ui/common/molecules/fast_app_bar.dart';

class ShareSheetPage extends StatefulWidget {
  final ScrollController controller;

  const ShareSheetPage({
    required this.controller,
    Key? key,
  }) : super(key: key);

  @override
  State<ShareSheetPage> createState() => _ShareSheetPageState();
}

class _ShareSheetPageState extends State<ShareSheetPage> {
  final List<String> _list = [];
  late final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    // log(_list.toString());
    return GestureDetector(
      onTap: () {
        FocusScopeNode focusScope = FocusScope.of(context);

        if (focusScope.hasFocus || focusScope.hasPrimaryFocus) {
          focusScope.unfocus();
        }
      },
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(10),
          ),
        ),
        child: CustomScrollView(
          controller: widget.controller,
          slivers: [
            SliverToBoxAdapter(
              child: FastAppBar(
                title: 'Добавление по почте',
                preIcon: Icons.arrow_back,
                preOnTap: () => Navigator.pop(context),
                suffText: 'Отправить',
                suffTextOnTap: () => Navigator.pop(context, _list),
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              sliver: SliverToBoxAdapter(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 20),
                    AddMailBody(
                      controller: controller,
                      list: _list,
                      callBack: () {},
                    ),
                    const SizedBox(height: 20),
                  ],
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height: mediaQuery.viewInsets.bottom,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
