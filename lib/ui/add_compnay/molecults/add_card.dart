import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/animated_switch.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class AddCard extends StatelessWidget {
  final bool active;
  final String title;
  final String? subTitle;
  final VoidCallback? onTap;
  final Widget? subChild;

  const AddCard({
    required this.title,
    this.onTap,
    this.active = false,
    this.subTitle,
    this.subChild,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.only(
          left: 15,
          right: 15,
          top: 10,
          bottom: 15,
        ),
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(5)),
          border: Border.all(
            color: ColorData.colorMainElements,
            width: 1,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: Row(
                    children: [
                      const Icon(
                        Icons.mail,
                        color: ColorData.colorElementsSecondary,
                        size: 24,
                      ),
                      const SizedBox(width: 5),
                      Flexible(
                        child: Text(
                          title,
                          style: const TextStyle(
                            fontSize: P1TextStyle.fontSize,
                            fontWeight: P1TextStyle.fontWeight,
                            height: P1TextStyle.height,
                            color: ColorData.colorTextMain,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  height: 36,
                  width: 36,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: ColorData.colorElementsActive,
                      width: 1,
                    ),
                    shape: BoxShape.circle,
                  ),
                  child: AnimatedSwitch(
                    active: active,
                    child: Container(
                      height: 28,
                      width: 28,
                      decoration: const BoxDecoration(
                        color: ColorData.colorElementsActive,
                        shape: BoxShape.circle,
                      ),
                      alignment: Alignment.center,
                      child: const Icon(
                        Icons.done,
                        size: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            if (subTitle != null)
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Text(
                  subTitle!,
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                    fontSize: P2TextStyle.fontSize,
                    height: P2TextStyle.height,
                    fontWeight: P2TextStyle.fontWeight,
                    color: ColorData.colortTextSecondary,
                  ),
                ),
              ),
            if (subChild != null) subChild!,
          ],
        ),
      ),
    );
  }
}
