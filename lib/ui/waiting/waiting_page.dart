// import 'package:flutter/material.dart';
// import 'package:tube/ui/admin/atoms/head_titile.dart';
// import 'package:tube/ui/common/atoms/menu_drawer.dart';
// import 'package:tube/ui/common/atoms/texts_widgets.dart';
// import 'package:tube/ui/common/page/empty_page.dart';
// import 'package:tube/ui/common/templates/main_drawer.dart';
// import 'package:tube/utils/colors.dart';

// class WaitingPage extends StatefulWidget {
//   const WaitingPage({super.key});

//   @override
//   State<WaitingPage> createState() => _WaitingPageState();
// }

// class _WaitingPageState extends State<WaitingPage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       drawer: const MainDrawer(),
//       appBar: AppBar(
//         centerTitle: true,
//         automaticallyImplyLeading: false,
//         backgroundColor: const Color(0xff141414),
//         leading: const MenuDrawer(),
//         title: GestureDetector(
//           behavior: HitTestBehavior.opaque,
//           onTap: () {},
//           child: IgnorePointer(
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.center,
//               children: [
//                 const HeadTitle(),
//                 StreamBuilder(
//                   stream: MainDrawerState.currentParticipanceValue,
//                   builder: (ctx, snap) {
//                     if (MainDrawerState
//                             .currentParticipanceValue.value?.club?.type ==
//                         null) {
//                       return const SizedBox();
//                     }
//                     final part =
//                         MainDrawerState.currentParticipanceValue.value!;
//                     return P1Text(
//                       part.club!.type!,
//                       color: ColorData.colorMainLink,
//                     );
//                   },
//                 )
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
