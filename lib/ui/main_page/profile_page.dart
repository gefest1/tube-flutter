import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/common/atoms/user_title.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/page/delete_page.dart';
import 'package:tube/ui/main_page/components/organisms/user_edit_page.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/restart_widget.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';

class ProfileCard extends StatelessWidget {
  final Icon icon;
  final String label;
  final VoidCallback? onTap;

  const ProfileCard({
    required this.icon,
    required this.label,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: onTap,
        behavior: HitTestBehavior.opaque,
        child: Column(
          children: [
            Row(
              children: [
                icon,
                const SizedBox(width: 10),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: Text(
                      label,
                      style: const TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: ColorData.colorTextMain,
                      ),
                    ),
                  ),
                ),
                const Icon(
                  Icons.navigate_next,
                  size: 24,
                  color: ColorData.colortTextSecondary,
                ),
              ],
            ),
            const Divider(
              thickness: 0.5,
              height: 0,
              color: ColorData.color5Percent,
            )
          ],
        ),
      ),
    );
  }
}

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context);
    return Stack(
      children: [
        ListView(
          padding: EdgeInsets.only(
            top: media.padding.top + 64,
            left: 20,
            right: 20,
          ),
          children: [
            const SizedBox(height: 40),
            const Text(
              "Основные",
              style: TextStyle(
                color: ColorData.colortTextSecondary,
                height: H5TextStyle.height,
                fontWeight: H5TextStyle.fontWeight,
                fontSize: H5TextStyle.fontSize,
              ),
            ),
            const SizedBox(height: 5),
            ProfileCard(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => const UserEditPage(),
                  ),
                );
              },
              icon: const Icon(
                Icons.assignment_ind,
                color: ColorData.colorElementsActive,
              ),
              label: 'Личная информация',
            ),
            // ProfileCard(
            //   onTap: () {
            //     Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //         builder: (_) => const AddCard(),
            //       ),
            //     );
            //   },
            //   icon: const Icon(
            //     Icons.credit_card,
            //     color: ColorData.colorElementsActive,
            //   ),
            //   label: 'Способ оплаты',
            // ),
            // ProfileCard(
            //   onTap: () {},
            //   icon: const Icon(
            //     Icons.notification_important_rounded,
            //     color: ColorData.colorElementsActive,
            //   ),
            //   label: 'Уведомления',
            // ),
            ProfileCard(
              onTap: () async {
                await launchUrlString(
                  'https://t.me/checkallnow',
                  mode: LaunchMode.externalApplication,
                );
              },
              icon: const Icon(
                Icons.contact_support,
                color: ColorData.colorElementsActive,
              ),
              label: 'Поддержка',
            ),
            // ProfileCard(
            //   onTap: () {},
            //   icon: const Icon(
            //     Icons.translate,
            //     color: ColorData.colorElementsActive,
            //   ),
            //   label: 'Язык',
            // ),
            ProfileCard(
              onTap: () async {
                await launchUrl(Uri.parse('https://www.checkallnow.net/'));
              },
              icon: const Icon(
                Icons.share,
                color: ColorData.colorElementsActive,
              ),
              label: 'Пригласить друга в Checkallnow.',
            ),
            // ProfileCard(
            //   onTap: () async {
            //     log('message');
            //     await sharedBox.delete('token');
            //     RestartWidget.restartApp(context);
            //   },
            //   icon: const Icon(
            //     Icons.delete,
            //     color: ColorData.colorElementsActive,
            //   ),
            //   label: 'Выйти из аккаунта',
            // ),
            const SizedBox(height: 40),
            const Text(
              "Дополнительно",
              style: TextStyle(
                color: ColorData.colortTextSecondary,
                height: H5TextStyle.height,
                fontWeight: H5TextStyle.fontWeight,
                fontSize: H5TextStyle.fontSize,
              ),
            ),
            ProfileCard(
              onTap: () async {
                await launchUrl(Uri.parse('https://www.checkallnow.net/terms'));
              },
              icon: const Icon(
                Icons.assignment,
                color: ColorData.colorElementsActive,
              ),
              label: 'Правила использования сервиса',
            ),
            ProfileCard(
              onTap: () async {
                final url0 = Uri.parse('https://www.checkallnow.net/public');
                if (!await launchUrl(url0)) {
                  log('Could not launch $url0');
                }
              },
              icon: const Icon(
                Icons.assignment,
                color: ColorData.colorElementsActive,
              ),
              label: 'Для компаний',
            ),
            ProfileCard(
              onTap: () async {
                final url = Uri.parse('https://www.checkallnow.net/policy');
                if (!await launchUrl(url)) {
                  log('Could not launch $url');
                }
              },
              icon: const Icon(
                Icons.pan_tool,
                color: ColorData.colorElementsActive,
              ),
              label: 'Политика конфиденциальности',
            ),

            ProfileCard(
              onTap: () async {
                await sharedBox.delete('token');
                RestartWidget.restartApp(navigatorKeyGlobal.currentContext!);
              },
              icon: const Icon(
                Icons.meeting_room,
                color: ColorData.colorElementsActive,
              ),
              label: 'Выйти из профиля',
            ),
            ProfileCard(
              onTap: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const DeletePage(),
                  ),
                );
              },
              icon: const Icon(
                Icons.delete,
                color: ColorData.colorElementsActive,
              ),
              label: 'Удалить профиль',
            ),
            const SizedBox(height: 200),
          ],
        ),
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: Container(
            height: media.padding.top + 64,
            padding: EdgeInsets.only(top: media.padding.top),
            decoration: const BoxDecoration(
              color: Color(0xff141414),
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(5),
              ),
            ),
            child: const UserTitle(),
          ),
        )
      ],
    );
  }
}
