// ignore: unused_import
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/notification/model/notification.model.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/status_enum.dart';
import 'package:tube/ui/common/atoms/menu_drawer.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/notification_wrap.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/logic/blocs/notification/notification_bloc.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  NotificationBloc get notificationBloc {
    if (MainDrawerState.currentParticipance?.status == StatusEnum.admin) {
      return Provider.of<AdminBlocMap>(context, listen: false)
          .getNotifcationBloc(
        MainDrawerState.currentParticipance!.clubId!,
      );
    }
    if (MainDrawerState.currentParticipance?.status == StatusEnum.trainer) {
      return Provider.of<TrainerBlocMap>(context, listen: false)
          .getNotifcationBloc(
        MainDrawerState.currentParticipance!.clubId!,
      );
    }
    return Provider.of<UserBlocMap>(context, listen: false).getNotifcationBloc(
      MainDrawerState.currentParticipance!.clubId!,
    );
  }

  @override
  void initState() {
    super.initState();
    if (MainDrawerState.currentParticipance != null) {
      notificationBloc.add(const GetNotificationEvent());
    }
  }

  @override
  Widget build(BuildContext context) {
    // final _media = MediaQuery.of(context);

    return Scaffold(
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     notificationBloc.add(const GetNotificationEvent());
      //   },
      // ),
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: MenuDrawer(
          parentContext: context,
        ),
        centerTitle: true,
        title: const H4Text(
          'Уведомления',
          color: Colors.white,
        ),
      ),
      body: CustomScrollView(
        slivers: [
          if (MainDrawerState.currentParticipance == null)
            const SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                child: EmptyNotifcation(),
              ),
            )
          else
            BlocBuilder<NotificationBloc, NotificationState>(
              bloc: notificationBloc,
              builder: (ctx, state) {
                final data = <NotificationEntity>[];
                if (state is DataNotificationState) {
                  data.addAll(state.notificationEntity);
                }
                if (data.isEmpty) {
                  return const SliverToBoxAdapter(
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 40),
                      child: EmptyNotifcation(),
                    ),
                  );
                }
                return SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      final notification = data[data.length - 1 - index];
                      return NotificationWrap(
                        notification: notification,
                      );
                    },
                    childCount: data.length,
                  ),
                );
              },
            ),
          const SliverToBoxAdapter(child: SizedBox(height: 100)),
        ],
      ),
    );
  }
}

class EmptyNotifcation extends StatelessWidget {
  const EmptyNotifcation({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Icon(
          Icons.inbox,
          color: ColorData.colorElementsActive,
          size: 36,
        ),
        SizedBox(height: 20),
        H3Text(
          'Уведомлений пока нет',
          color: ColorData.colorTextMain,
        ),
        SizedBox(height: 5),
        P0Text(
          'Здесь будут выводиться уведомления обо всем, что будет происходить с вашим профилем.',
          color: ColorData.colortTextSecondary,
        ),
      ],
    );
  }
}
