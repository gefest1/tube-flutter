import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/big_event/entities/big_event_entity.dart';
import 'package:tube/logic/blocs/client/big_event_client/big_event_client_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/ui/admin/page/big_event/big_event_detail_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/main_page/components/organisms/big_event_ticket.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:tube/utils/local_page_scroll_physics.dart';

class ClientBigEventList extends StatefulWidget {
  const ClientBigEventList({super.key});

  @override
  State<ClientBigEventList> createState() => _ClientBigEventListState();
}

class _ClientBigEventListState extends State<ClientBigEventList> {
  UserBlocMap get userBlocMap => Provider.of<UserBlocMap>(context, listen: false);
  BigEventClientBloc get bigEventClientBloc =>
      userBlocMap.getBigEventClientBloc(MainDrawerState.clubId!);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final widthElement = constraints.maxWidth - 40;
        return BlocBuilder(
            bloc: bigEventClientBloc,
            builder: (context, state) {
              List<BigEventEntity> data = [];
              if (state is DataBigEventClientState) {
                data.addAll(state.events);
              }
              if (data.isEmpty) return const SizedBox();
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 30),
                    child: H5Text(
                      'Мероприятия',
                      color: ColorData.colorElementsSecondary,
                    ),
                  ),
                  const SizedBox(height: 10),
                  SizedBox(
                    height: 136,
                    child: ListView.separated(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      scrollDirection: Axis.horizontal,
                      clipBehavior: Clip.none,
                      physics: LocalPageScrollPhysics(
                        separateWidth: 10,
                        width: widthElement,
                      ),
                      itemBuilder: (context, index) => BigEventTicket(
                        width: widthElement,
                        title: data[index].name,
                        label: DateFormat('HH:mm').format(data[index].date.parseTimeGG),
                        subLabel: "${DateFormat('dd.MM.yyyy').format(data[index].date.parseTimeGG)}"
                            " - "
                            "${DateFormat('dd.MM.yyyy').format(data[index].finishDate.parseTimeGG)}",
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => BigEventDetailPage(
                                entity: data[index],
                              ),
                            ),
                          );
                        },
                      ),
                      separatorBuilder: (context, index) => const SizedBox(width: 10),
                      itemCount: data.length,
                    ),
                  ),
                ],
              );
            });
      },
    );
  }
}
