import 'package:flutter/material.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/main_page/components/organisms/buy_ticket/buy_ticket_body.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class BuyTicketBottomSheet extends StatefulWidget {
  const BuyTicketBottomSheet({Key? key}) : super(key: key);

  @override
  State<BuyTicketBottomSheet> createState() => _BuyTicketBottomSheetState();
}

class _BuyTicketBottomSheetState extends State<BuyTicketBottomSheet> {
  late final ValueNotifier<bool> active = ValueNotifier(false)
    ..addListener(() {
      setState(() {});
    });

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(navigatorKeyGlobal.currentContext!);
    return Container(
      height: (media.size.height - 56 / 2 - media.padding.top) * 0.98,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: GestureDetector(
        onTap: () {
          final focus = FocusScope.of(context);

          if (focus.hasFocus || focus.hasPrimaryFocus) {
            focus.unfocus();
          }
        },
        child: Stack(
          children: [
            BuyTicketBody(
              active: active,
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0, -4),
                      blurRadius: 15,
                      color: Color(0x0d000000),
                    ),
                  ],
                ),
                padding: EdgeInsets.only(
                  right: 20,
                  left: 20,
                  bottom: 10 + media.padding.bottom,
                  top: 10,
                ),
                child: SizeTapAnimation(
                  child: GestureDetector(
                    onTap: () {},
                    child: Container(
                      height: 56,
                      decoration: const BoxDecoration(
                        color: ColorData.colorElementsActive,
                        borderRadius: BorderRadius.all(Radius.circular(6)),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(2, 2),
                            blurRadius: 15,
                            color: Color(0x14000000),
                          ),
                        ],
                      ),
                      alignment: Alignment.center,
                      child: const Text(
                        'Продолжить',
                        style: TextStyle(
                          fontSize: P1TextStyle.fontSize,
                          height: P1TextStyle.height,
                          fontWeight: P1TextStyle.fontWeight,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
