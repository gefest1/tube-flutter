import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/main_page/components/molecules/buy_ticket_card.dart';
import 'package:tube/ui/main_page/components/organisms/family_body.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class BuyTicketBody extends StatelessWidget {
  final ValueNotifier<bool> active;

  const BuyTicketBody({
    required this.active,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        const SliverToBoxAdapter(
          child: Center(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 20),
              child: Text(
                'Покупка абонемента',
                style: TextStyle(
                  fontWeight: H4TextStyle.fontWeight,
                  fontSize: H4TextStyle.fontSize,
                  height: H4TextStyle.height,
                  color: ColorData.colorGrayscaleOffBlack,
                ),
              ),
            ),
          ),
        ),
        const SliverPadding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          sliver: SliverToBoxAdapter(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 12),
                Text(
                  'Чей это будет абонемент?',
                  style: TextStyle(
                    fontWeight: H4TextStyle.fontWeight,
                    fontSize: H4TextStyle.fontSize,
                    height: H4TextStyle.height,
                    color: Color(0xff14142B),
                  ),
                ),
                SizedBox(height: 40),
              ],
            ),
          ),
        ),
        SliverPadding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          sliver: SliverList(
            delegate: SliverChildListDelegate([
              SizeTapAnimation(
                child: BuyTicketCard(
                  onTap: () {
                    active.value = !active.value;
                  },
                  active: active.value,
                  title: 'Члена семьи',
                ),
              ),
              FamilyBody(
                active: active.value,
              ),
            ]),
          ),
        ),
        const SliverToBoxAdapter(
          child: SizedBox(height: 80),
        )
      ],
    );
  }
}
