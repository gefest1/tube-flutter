import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/tariff/model/stop_tariff.model.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/tariff/tariff_bloc.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/blocs/user/user_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/provider/booking/get_special_booking.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/main_page/components/molecules/abpo_card_wrapper.dart';
import 'package:tube/ui/main_page/components/molecules/buy_ticket.dart';
import 'package:tube/ui/main_page/components/pages/choose_tariff_page.dart';
import 'package:tube/ui/subscription/subscription_page.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/local_page_scroll_physics.dart';

class ClientTariffList extends StatefulWidget {
  const ClientTariffList({super.key});

  @override
  State<ClientTariffList> createState() => _ClientTariffListState();
}

class _ClientTariffListState extends State<ClientTariffList> {
  UserBlocMap get userBlocMap =>
      Provider.of<UserBlocMap>(context, listen: false);

  late TariffBloc tariffBloc = userBlocMap.getTariffBloc(
    MainDrawerState.clubId!,
  );
  @override
  void initState() {
    Future(() {
      tariffBloc.add(const GetDataTariffEvent());
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final state = Provider.of<UserBloc>(context).state;
    User? user;
    if (state is DataUserState) {
      user = state.user;
    }
    // tariffBloc.add(const GetDataTariffEvent());
    return LayoutBuilder(builder: (context, constraints) {
      final widthElement = constraints.maxWidth - 40;
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: H5Text(
              'Активные абонементы',
              color: ColorData.colorElementsSecondary,
            ),
          ),
          const SizedBox(height: 10),
          SizedBox(
            height: 185,
            child: BlocBuilder<TariffBloc, TariffState>(
              bloc: tariffBloc,
              builder: (ctx, state) {
                final List<Tariff> tariff = [];
                final List<StopTariff> stopTariff = [];
                if (state is DataTariffState) {
                  tariff.addAll(state.tariffs);
                  stopTariff.addAll(state.stopTariff);
                }
                // DecoratedSliver
                return CustomScrollView(
                  clipBehavior: Clip.none,
                  scrollDirection: Axis.horizontal,
                  physics: LocalPageScrollPhysics(
                    separateWidth: 10,
                    width: widthElement,
                  ),
                  slivers: [
                    const SliverToBoxAdapter(
                      child: SizedBox(width: 20),
                    ),
                    SliverList.separated(
                      itemCount: tariff.length,
                      itemBuilder: (context, index) {
                        final tarrifIndex = tariff[index];
                        return AbpoCardWrapper(
                          width: widthElement,
                          photoUrl: tarrifIndex.user?.photoUrl?.thumbnail ??
                              user?.photoUrl?.thumbnail,
                          userTitle: tarrifIndex.user?.fullName ??
                              tarrifIndex.user?.email ??
                              user?.fullName ??
                              user?.email,
                          title: tarrifIndex.booking?.name,
                          trainTimes: tarrifIndex.trainTimes,
                          trainCount: tarrifIndex.trainCount!,
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => SubscriptionPage(
                                  clubId: MainDrawerState
                                      .currentParticipance!.clubId!,
                                  bookingId: tarrifIndex.bookingId!,
                                  tariff: tarrifIndex,
                                ),
                              ),
                            );
                            Provider.of<GetSpecialBooking>(
                              context,
                              listen: false,
                            ).superLazyGetBooking(tarrifIndex.bookingId!);
                          },
                        );
                      },
                      separatorBuilder: (_, index) => const SizedBox(width: 10),
                    ),
                    if (tariff.isNotEmpty && stopTariff.isNotEmpty)
                      const SliverToBoxAdapter(
                        child: SizedBox(width: 10),
                      ),
                    SliverList.separated(
                      itemCount: stopTariff.length,
                      itemBuilder: (context, index) {
                        final tarrifIndex = stopTariff[index];
                        return AbpoCardWrapper(
                          stopTimes: (
                            stopStartTime: tarrifIndex.stopStartDate!,
                            stopEndTime: tarrifIndex.stopEndDate!,
                          ),
                          width: widthElement,
                          photoUrl: tarrifIndex.user?.photoUrl?.thumbnail ??
                              user?.photoUrl?.thumbnail,
                          userTitle: tarrifIndex.user?.fullName ??
                              tarrifIndex.user?.email ??
                              user?.fullName ??
                              user?.email,
                          title: tarrifIndex.booking?.name,
                          trainTimes: tarrifIndex.trainTimes,
                          trainCount: tarrifIndex.trainCount!,
                          onTap: () {
                            // Navigator.push(
                            //   context,
                            //   MaterialPageRoute(
                            //     builder: (_) => SubscriptionPage(
                            //       clubId: MainDrawerState
                            //           .currentParticipance!.clubId!,
                            //       bookingId: tarrifIndex.bookingId!,
                            //       tariff: tarrifIndex,
                            //     ),
                            //   ),
                            // );
                            // Provider.of<GetSpecialBooking>(
                            //   context,
                            //   listen: false,
                            // ).superLazyGetBooking(tarrifIndex.bookingId!);
                          },
                        );
                      },
                      separatorBuilder: (_, index) => const SizedBox(width: 10),
                    ),
                    if (tariff.isNotEmpty || stopTariff.isNotEmpty)
                      const SliverToBoxAdapter(
                        child: SizedBox(width: 10),
                      ),
                    SliverToBoxAdapter(
                      child: BuyTicket(
                        width: widthElement,
                        onTap: () async {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (ctx) => const ChooseTariffPage(),
                            ),
                          );
                        },
                      ),
                    ),
                    const SliverToBoxAdapter(
                      child: SizedBox(width: 20),
                    ),
                  ],
                );
                return ListView.separated(
                  clipBehavior: Clip.none,
                  physics: LocalPageScrollPhysics(
                    separateWidth: 10,
                    width: widthElement,
                  ),
                  itemCount: tariff.length + 1 + stopTariff.length,
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  scrollDirection: Axis.horizontal,
                  separatorBuilder: (_, index) => const SizedBox(width: 10),
                  itemBuilder: (_, index) {
                    if (tariff.length == index) {
                      return BuyTicket(
                        width: widthElement,
                        onTap: () async {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (ctx) => const ChooseTariffPage(),
                            ),
                          );
                        },
                      );
                    }
                    final tarrifIndex = tariff[index];
                    // _tarrif.

                    return AbpoCardWrapper(
                      width: widthElement,
                      photoUrl: tarrifIndex.user?.photoUrl?.thumbnail ??
                          user?.photoUrl?.thumbnail,
                      userTitle: tarrifIndex.user?.fullName ??
                          tarrifIndex.user?.email ??
                          user?.fullName ??
                          user?.email,
                      title: tarrifIndex.booking?.name,
                      trainTimes: tarrifIndex.trainTimes,
                      trainCount: tarrifIndex.trainCount!,
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => SubscriptionPage(
                              clubId:
                                  MainDrawerState.currentParticipance!.clubId!,
                              bookingId: tarrifIndex.bookingId!,
                              tariff: tarrifIndex,
                            ),
                          ),
                        );
                        Provider.of<GetSpecialBooking>(
                          context,
                          listen: false,
                        ).superLazyGetBooking(tarrifIndex.bookingId!);
                      },
                    );
                  },
                );
              },
            ),
          ),
        ],
      );
    });
  }
}
