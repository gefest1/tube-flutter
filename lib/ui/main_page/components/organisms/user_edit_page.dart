import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_picker/image_picker.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/blocs/user/user_bloc.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class UserEditPage extends StatefulWidget {
  const UserEditPage({
    Key? key,
  }) : super(key: key);

  @override
  State<UserEditPage> createState() => _UserEditPageState();
}

class _UserEditPageState extends State<UserEditPage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (ctx, UserState state) {
        final Widget? widget;
        final Widget? title;
        if (state is DataUserState) {
          widget = _InitUserEditPage(
            user: state.user,
          );
          title = H4Text(
            state.user.email ?? '',
            color: Colors.white,
          );
        } else {
          widget = const Center(
            child: CircularProgressIndicator(),
          );
          title = null;
        }
        return GestureDetector(
          onTap: () {
            final focusScope = FocusScope.of(context);

            if (focusScope.hasFocus || focusScope.hasPrimaryFocus) {
              focusScope.unfocus();
            }
          },
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              centerTitle: true,
              title: title,
              leading: IconButton(
                onPressed: () => Navigator.pop(context),
                icon: const Icon(
                  Icons.arrow_back,
                  size: 33,
                ),
                color: Colors.white,
              ),
            ),
            body: widget,
          ),
        );
      },
    );
  }
}

class Indicator extends StatelessWidget {
  final LayerLink link;
  final ValueNotifier<OverlayEntry?> valueOverlayEntry;
  final ValueNotifier<Uint8List?> uploadPhoto;

  const Indicator({
    required this.link,
    required this.valueOverlayEntry,
    required this.uploadPhoto,
    Key? key,
  }) : super(key: key);

  void killOverlay() {
    if (valueOverlayEntry.value != null) {
      valueOverlayEntry.value!.remove();
      valueOverlayEntry.value = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: killOverlay,
      behavior: HitTestBehavior.opaque,
      child: CompositedTransformFollower(
        offset: const Offset(3, 148),
        link: link,
        child: Align(
          alignment: Alignment.topLeft,
          child: DecoratedBox(
            decoration: const BoxDecoration(
              boxShadow: [
                BoxShadow(
                  offset: Offset(0, -3),
                  blurRadius: 2,
                  color: Color.fromRGBO(0, 0, 0, 0.01),
                ),
                BoxShadow(
                  offset: Offset(-3, 0),
                  blurRadius: 2,
                  color: Color.fromRGBO(0, 0, 0, 0.01),
                ),
                BoxShadow(
                  offset: Offset(3, 0),
                  blurRadius: 2,
                  color: Color.fromRGBO(0, 0, 0, 0.01),
                ),
                BoxShadow(
                  offset: Offset(0, 5),
                  blurRadius: 20,
                  color: Color.fromRGBO(0, 0, 0, 0.05),
                ),
              ],
            ),
            child: Material(
              color: const Color.fromRGBO(255, 255, 255, 0.9),
              child: IntrinsicWidth(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizeTapAnimation(
                      child: GestureDetector(
                        onTap: () async {
                          final XFile? xFile = await ImagePicker().pickImage(
                            source: ImageSource.camera,
                          );
                          if (xFile == null) return;
                          uploadPhoto.value =
                              (await FlutterImageCompress.compressWithFile(
                            xFile.path,
                            format: CompressFormat.jpeg,
                          ))!;
                          // uploadPhoto.value = xFile;
                          killOverlay();
                        },
                        behavior: HitTestBehavior.opaque,
                        child: const Padding(
                          padding: EdgeInsets.all(15),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: P1Text(
                              'Камера',
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizeTapAnimation(
                      child: GestureDetector(
                        onTap: () async {
                          final XFile? xFile = await ImagePicker().pickImage(
                            source: ImageSource.gallery,
                          );
                          if (xFile == null) return;

                          uploadPhoto.value =
                              (await FlutterImageCompress.compressWithFile(
                            xFile.path,
                            format: CompressFormat.jpeg,
                          ))!;
                          killOverlay();
                        },
                        behavior: HitTestBehavior.opaque,
                        child: const Padding(
                          padding: EdgeInsets.all(15),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: P1Text(
                              'Выбрать из галереи',
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                  // color: Colors.green,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _InitUserEditPage extends StatefulWidget {
  final User user;

  const _InitUserEditPage({
    required this.user,
    Key? key,
  }) : super(key: key);

  @override
  State<_InitUserEditPage> createState() => _InitUserEditPageState();
}

class _InitUserEditPageState extends State<_InitUserEditPage> {
  late TextEditingController fullNameController =
      TextEditingController(text: widget.user.fullName)
        ..addListener(() {
          setState(() {});
        });

  late TextEditingController phoneNumberController =
      TextEditingController(text: widget.user.phoneNumber)
        ..addListener(() {
          setState(() {});
        });
  final LayerLink layerLink = LayerLink();

  final ValueNotifier<OverlayEntry?> valueOverlay = ValueNotifier(null);
  late final ValueNotifier<Uint8List?> uploadPhoto =
      ValueNotifier<Uint8List?>(null)
        ..addListener(() {
          setState(() {});
        });

  void _onTap() async {
    if (valueOverlay.value != null) return;
    Overlay.of(context).insert(
      valueOverlay.value ??= OverlayEntry(
        builder: (ctx) => Indicator(
          uploadPhoto: uploadPhoto,
          link: layerLink,
          valueOverlayEntry: valueOverlay,
        ),
      ),
    );
  }

  bool isValid() {
    if (uploadPhoto.value != null) return true;
    final newUser = widget.user.copyWith(
      fullName:
          fullNameController.text.isEmpty ? null : fullNameController.text,
      phoneNumber: phoneNumberController.text.isEmpty
          ? null
          : phoneNumberController.text,
    );
    if (newUser != widget.user) return true;
    return false;
  }

  @override
  void dispose() {
    valueOverlay.value?.remove();
    valueOverlay.dispose();
    uploadPhoto.dispose();
    fullNameController.dispose();
    phoneNumberController.dispose();

    super.dispose();
  }

  ImageProvider? getImageProvider() {
    if (uploadPhoto.value != null) {
      return MemoryImage(uploadPhoto.value!);
    }

    if (widget.user.photoUrl?.xl != null) {
      return PCacheImage(
        widget.user.photoUrl!.xl!,
        enableInMemory: true,
      );
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: ListView(
            padding: const EdgeInsets.only(top: 40, left: 20, right: 20),
            children: [
              Center(
                child: CompositedTransformTarget(
                  link: layerLink,
                  child: SizeTapAnimation(
                    child: GestureDetector(
                      onTap: _onTap,
                      child: CircleAvatar(
                        radius: 162 / 2,
                        backgroundColor: const Color(0xffF3F4F6),
                        child: Stack(
                          children: [
                            const SizedBox(
                              height: 162,
                              width: 162,
                              child: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.add_a_photo,
                                      color: ColorData.colorTextMain,
                                      size: 24,
                                    ),
                                    SizedBox(height: 5),
                                    P2Text(
                                      'Загрузить\nфото',
                                      color: Color.fromARGB(255, 76, 60, 60),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            if (widget.user.photoUrl?.thumbnail != null)
                              SizedBox(
                                height: 162,
                                width: 162,
                                child: ClipOval(
                                  child: Image(
                                    fit: BoxFit.cover,
                                    image: PCacheImage(
                                      widget.user.photoUrl!.xl!,
                                      enableInMemory: true,
                                    ),
                                  ),
                                ),
                              ),
                            if (uploadPhoto.value != null)
                              SizedBox(
                                height: 162,
                                width: 162,
                                child: ClipOval(
                                  child: Image.memory(
                                    uploadPhoto.value!,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                          ],
                        ),
                        // foregroundImage: getImageProvider(),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              // const Center(
              //   child: P1Text(
              //     'Фото можно\nзагрузить позже',
              //     color: ColorData.colorElementsSecondary,
              //     textAlign: TextAlign.center,
              //   ),
              // ),
              // const SizedBox(height: 10),
              CustomTextField(
                controller: fullNameController,
                hintText: 'Иван Иванов',
                title: 'Полное Имя',
              ),
              const SizedBox(height: 30),
              CustomTextField(
                hintText: '+7 707 777 77 77',
                controller: phoneNumberController,
                keyboardType: TextInputType.phone,
                icon: const Icon(
                  Icons.call,
                  color: Color.fromARGB(255, 9, 8, 8),
                ),
                title: 'Номер телефона для быстрой связи',
              ),
            ],
          ),
        ),
        if (isValid())
          Container(
            padding: EdgeInsets.only(
              top: 10,
              left: 20,
              right: 20,
              bottom: MediaQuery.of(context).padding.bottom + 10,
            ),
            decoration: const BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Color(0x1a000000),
                  offset: Offset(0, -0.5),
                ),
              ],
            ),
            child: CustomButton(
              title: 'Сохранить',
              onTap: () {
                final user = User(
                  fullName: fullNameController.text.isEmpty
                      ? null
                      : fullNameController.text,
                  phoneNumber: phoneNumberController.text.isEmpty
                      ? null
                      : phoneNumberController.text,
                );

                Provider.of<UserBloc>(
                  context,
                  listen: false,
                ).add(
                  EditUserEvent(
                    user: user,
                    file: uploadPhoto.value,
                  ),
                );
              },
              color: ColorData.colorMainLink,
            ),
          ),
      ],
    );
  }
}
