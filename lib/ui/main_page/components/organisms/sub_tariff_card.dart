import 'package:flutter/material.dart';
import 'package:tube/ui/admin/atoms/custom_check_box.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class SubTariffCard extends StatelessWidget {
  final VoidCallback? onTap;
  final bool active;
  final String? title;
  final String? label;

  const SubTariffCard({
    this.onTap,
    this.active = false,
    this.title,
    this.label,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Row(
          children: [
            CustomCheckBox(
              isCircle: true,
              active: active,
              margin: const EdgeInsets.only(right: 10),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (title != null)
                    P1Text(
                      title!,
                      color: ColorData.colorTextMain,
                    ),
                  if (label != null)
                    P2Text(
                      label!,
                      color: ColorData.colortTextSecondary,
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
