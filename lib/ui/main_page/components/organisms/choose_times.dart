import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/logic/model/workt_time_obj.dart';
import 'package:tube/ui/admin/page/tariff/molecules/trainer_detail_sheet.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/fast_app_bar.dart';
import 'package:tube/ui/main_page/components/molecules/day_card.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_next_week_date.dart';
import 'package:tube/utils/get_time_shift.dart';

class ChooseTime extends StatefulWidget {
  final String title;
  final ScrollController controller;
  final List<WorkTimeObj> dateTimes;
  final String? userId;
  final Map<String, bool> workTimes;

  const ChooseTime({
    required this.dateTimes,
    required this.controller,
    required this.workTimes,
    this.userId,
    this.title = 'Понедельник',
    Key? key,
  }) : super(key: key);

  @override
  State<ChooseTime> createState() => _ChooseTimeState();
}

class _ChooseTimeState extends State<ChooseTime> {
  late final BehaviorSubject<Map<String, bool>> workTimesSubject =
      BehaviorSubject.seeded(widget.workTimes);
  Map<String, bool> get workTimes => workTimesSubject.value;
  set workTimes(Map<String, bool> val) {
    workTimesSubject.value = val;
    setState(() {});
  }

  @override
  void dispose() {
    workTimesSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<WorkTimeObj> dateTimes = widget.dateTimes;
    return GestureDetector(
      onTap: () {
        FocusScopeNode focusScope = FocusScope.of(context);

        if (focusScope.hasFocus || focusScope.hasPrimaryFocus) {
          focusScope.unfocus();
        }
      },
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(10),
          ),
        ),
        child: Stack(
          children: [
            CustomScrollView(
              physics: const ClampingScrollPhysics(),
              controller: widget.controller,
              slivers: [
                SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FastAppBar(
                        title: 'Выбор времени',
                        suffIcon: Icons.close,
                        suffOnTap: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
                SliverPadding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  sliver: SliverToBoxAdapter(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 20),
                        H2Text(
                          widget.title, // 'Понедельник',
                          color: ColorData.colorTextMain,
                        ),
                        const SizedBox(height: 20),
                      ],
                    ),
                  ),
                ),
                SliverPadding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  sliver: SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (context, ind) {
                        if (ind.isOdd) return const SizedBox(height: 10);
                        final index = ind ~/ 2;
                        final workTime = dateTimes[index];
                        // TODO
                        final userKey =
                            workTime is WorkTimeObjTrainer ? workTime.trainer.id! : widget.userId!;
                        final key = "${userKey}_${workTime.date!.toIso8601String()}";
                        return LittleDayCard(
                          properties: [
                            DiagnosticsProperty(
                                'work times', workTime.date!.toUtc().toIso8601String()),
                          ],
                          value: workTimes[key] ?? false,
                          onTap: () {
                            final newValue = {...workTimes};

                            newValue[key] = !(newValue[key] ?? false);
                            workTimes = newValue;
                          },
                          subLabel: workTime is WorkTimeObjTrainer
                              ? "Сотрудик: ${workTime.trainer.fullName ?? workTime.trainer.email!}"
                              : null,
                          title: DateFormat("HH:mm").format(workTime.date!.parseTimeGG),
                          label: 'Человек в группе: ${workTime.now}/${workTime.max}',
                          active: (widget.workTimes[key] != null) || workTime.now! < workTime.max,
                        );
                      },
                      childCount: dateTimes.length * 2 - 1,
                    ),
                  ),
                ),
                const SliverToBoxAdapter(
                  child: SizedBox(height: 160),
                ),
              ],
            ),
            if (!const DeepCollectionEquality(DefaultEqualityTime()).equals(
              {...widget.workTimes}..removeWhere((key, value) => value == false),
              {...workTimes}..removeWhere((key, value) => value == false),
            ))
              Positioned(
                left: 0,
                right: 0,
                bottom: 0,
                child: BottomButton(
                  onTap: () {
                    Navigator.pop(context, workTimes);
                  },
                ),
              ),
          ],
        ),
      ),
    );
  }
}
