// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tube/icon/add_aboniment.icon.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/molecules/sliver_space.dart';
import 'package:tube/ui/main_page/components/organisms/client_big_event_list.dart';
import 'package:tube/ui/main_page/components/organisms/client_tariff_list.dart';
import 'package:tube/ui/main_page/components/organisms/new_client_tariff_list.dart';
import 'package:tube/ui/main_page/components/pages/choose_tariff_page.dart';
import 'package:tube/utils/colors.dart';

class MainBody extends StatefulWidget {
  const MainBody({Key? key}) : super(key: key);

  @override
  State<MainBody> createState() => _MainBodyState();
}

class _MainBodyState extends State<MainBody> {
  UserBlocMap get userBlocMap =>
      Provider.of<UserBlocMap>(context, listen: false);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Container(
            decoration: const BoxDecoration(
              color: Color(0xffF3F4F6),
            ),
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: const Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                NewClientTariffList(),
                ClientTariffList(),
                ClientBigEventList(),
              ],
            ),
          ),
        ),
        const SliverSpace(height: 20),
        SliverToBoxAdapter(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const H5Text(
                  'Абонемент',
                  color: Color(0xff959595),
                ),
                const SizedBox(height: 10),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (ctx) => const ChooseTariffPage(),
                      ),
                    );
                  },
                  child: SizeTapAnimation(
                    child: Container(
                      height: 45,
                      decoration: const BoxDecoration(
                        color: Color(0xffF5F5F5),
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      alignment: Alignment.center,
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CustomPaint(
                            painter: AddAbonementCustomPainter(
                              color: ColorData.colorMainLink,
                            ),
                            size: Size(18, 18),
                          ),
                          SizedBox(width: 5),
                          P1Text(
                            'Купить абонемент',
                            color: ColorData.colorMainLink,
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        const SliverSpace(height: 80),
      ],
    );
  }
}
