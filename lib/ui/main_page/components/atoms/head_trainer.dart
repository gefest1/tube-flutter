import 'package:flutter/material.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class HeadTrainer extends StatelessWidget {
  final String? title;
  final String? label;
  final String? photoUrl;

  const HeadTrainer({
    required this.title,
    required this.label,
    this.photoUrl,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (title != null)
                P2Text(
                  title!,
                  color: ColorData.colortTextSecondary,
                ),
              if (label != null)
                H3Text(
                  label!,
                  color: ColorData.colorTextMain,
                ),
            ],
          ),
        ),
        const SizedBox(width: 10),
        if (photoUrl != null)
          CircleAvatar(
            maxRadius: 22.5,
            backgroundColor: Colors.black38,
            backgroundImage: PCacheImage(
              photoUrl!,
              enableInMemory: true,
            ),
          )
      ],
    );
  }
}
