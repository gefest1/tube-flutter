import 'package:flutter/material.dart';
import 'package:tube/ui/main_page/components/atoms/head_trainer.dart';
import 'package:tube/ui/main_page/components/organisms/active_time_card.dart';
import 'dart:math' as math;

import 'package:tube/utils/week_day.dart';
import 'package:tube/utils/week_day_str.dart';

class TrainerBookingModel {
  final String? name;
  final String? photoURl;
  final List<DateTime>? dateTimes;

  const TrainerBookingModel({
    this.name,
    this.photoURl,
    this.dateTimes,
  });
}

class TrainerBookingCard extends StatelessWidget {
  final List<TrainerBookingModel> arrays;

  const TrainerBookingCard({
    this.arrays = const [],
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
        math.max(0, arrays.length * 2 - 1),
        (ind) {
          if (ind.isOdd) return const SizedBox(height: 40);
          final index = ind ~/ 2;
          final Map<int, List<DateTime>> weekMap = {};

          for (DateTime dateTime in arrays[index].dateTimes ?? []) {
            (weekMap[(dateTime.weekday + 6) % 7] ??= []).add(dateTime);
          }
          List<MapEntry<int, List<DateTime>>> mapList = weekMap.entries.toList()
            ..sort((a, b) {
              return a.key.compareTo(b.key);
            });

          return Column(
            children: [
              HeadTrainer(
                title: 'Тренер',
                label: arrays[index].name,
              ),
              const SizedBox(height: 10),
              if (mapList.isNotEmpty)
                ...List.generate(mapList.length * 2 - 1, (ind) {
                  if (ind.isOdd) return const SizedBox(height: 10);
                  final subIndex = ind ~/ 2;

                  return ActiveTimeCard(
                    title: weeks[mapList[subIndex].key],
                    subLabel: getWeekStr(mapList[subIndex].value),
                  );
                }),
            ],
          );
        },
      ),
    );
  }
}
