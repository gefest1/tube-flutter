import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/animated_switch.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class BuyTicketCard extends StatelessWidget {
  final bool active;
  final String title;
  final VoidCallback? onTap;

  const BuyTicketCard({
    this.active = false,
    required this.title,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.opaque,
      child: Row(
        children: [
          Container(
            height: 36,
            width: 36,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border.all(
                color: ColorData.colorMainLink,
                width: 1,
              ),
              shape: BoxShape.circle,
            ),
            child: AnimatedSwitch(
              active: active,
              child: Container(
                height: 28,
                width: 28,
                decoration: const BoxDecoration(
                  color: ColorData.colorMainLink,
                  shape: BoxShape.circle,
                ),
                alignment: Alignment.center,
                child: const Icon(
                  Icons.done,
                  size: 16,
                ),
              ),
            ),
          ),
          const SizedBox(width: 10),
          Text(
            title,
            style: const TextStyle(
              fontSize: P1TextStyle.fontSize,
              height: P1TextStyle.height,
              fontWeight: P1TextStyle.fontWeight,
              color: Colors.black,
            ),
          )
        ],
      ),
    );
  }
}
