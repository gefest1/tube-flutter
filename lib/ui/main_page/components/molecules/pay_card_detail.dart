import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/bottom_data_card.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/main_page/components/molecules/trainer_booking_card.dart';
import 'package:tube/utils/colors.dart';

class PayCardDetail extends StatelessWidget {
  final String? title;
  final String? priceTitle;
  final String? description;
  final String? applyUntilTitle;
  final String? countLessonTitle;
  final String? minDuration;
  final String? startTime;
  final List<TrainerBookingModel>? trainerBookingModels;

  const PayCardDetail({
    this.title,
    this.priceTitle,
    this.description,
    this.applyUntilTitle,
    this.countLessonTitle,
    this.minDuration,
    this.trainerBookingModels,
    this.startTime,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: ColorData.colorElementsSecondary,
          width: 0.5,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(6),
        ),
      ),
      padding: const EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Row(
              children: [
                if (title != null)
                  Expanded(
                    child: H4Text(
                      title!,
                      color: ColorData.colorTextMain,
                    ),
                  ),
                if (title != null && priceTitle != null)
                  const SizedBox(width: 10),
                if (priceTitle != null)
                  H4Text(
                    priceTitle!,
                    // '31 000 ₸',
                    color: ColorData.colorTextMain,
                  ),
              ],
            ),
          ),
          if (description != null && description!.isNotEmpty)
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: P1Text(
                description!,
                color: ColorData.colorTextMain,
              ),
            ),
          const SizedBox(height: 10),
          BottomDataCard(
            applyUntil: applyUntilTitle, // '1 мес',
            countLesson: countLessonTitle,
            minDuration: minDuration,
          ),
          if (startTime != null)
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const P1Text(
                          "начало",
                          color: ColorData.colortTextSecondary,
                          textAlign: TextAlign.start,
                        ),
                        const SizedBox(height: 2),
                        P1Text(
                          startTime!,
                          color: ColorData.colorTextMain,
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          const SizedBox(height: 10),
          const DecoratedBox(
            decoration: BoxDecoration(
              color: Color(0xffF3F4F6),
              borderRadius: BorderRadius.all(
                Radius.circular(5),
              ),
            ),
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: Icon(
                      Icons.report_gmailerrorred,
                      size: 24,
                      color: ColorData.colorElementsActive,
                    ),
                  ),
                  Expanded(
                    child: P1Text(
                      'Непосещенные уроки сгорают. Надо сходить на все уроки в течение срока действия абонемента',
                      color: ColorData.colorMainBlack,
                    ),
                  )
                ],
              ),
            ),
          ),
          const SizedBox(height: 16),
          const Divider(
            height: 0,
            thickness: 0.3,
            color: ColorData.colorElementsSecondary,
          ),
          const SizedBox(height: 20),
          if (trainerBookingModels != null && trainerBookingModels!.isNotEmpty)
            TrainerBookingCard(arrays: trainerBookingModels!),
        ],
      ),
    );
  }
}
