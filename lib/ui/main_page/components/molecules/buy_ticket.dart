import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class BuyTicket extends StatelessWidget {
  final double? width;
  final VoidCallback onTap;

  const BuyTicket({
    this.width,
    required this.onTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.opaque,
      child: SizeTapAnimation(
        child: Container(
          height: 185,
          width: width ?? double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
            boxShadow: const [
              BoxShadow(
                offset: Offset(2, 2),
                blurRadius: 15,
                color: Color(0x14000000),
              )
            ],
          ),
          child: const Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.control_point_duplicate,
                color: ColorData.colorElementsActive,
                size: 40,
              ),
              SizedBox(height: 5),
              Text(
                "Купить абонемент",
                style: TextStyle(
                  color: ColorData.colorTextMain,
                  height: P1TextStyle.height,
                  fontSize: P1TextStyle.fontSize,
                  fontWeight: P1TextStyle.fontWeight,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
