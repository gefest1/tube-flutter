import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/ui/main_page/components/atoms/day_card.dart';
import 'package:tube/ui/main_page/components/atoms/head_trainer.dart';
import 'package:tube/ui/main_page/components/organisms/active_time_card.dart';
import 'package:tube/ui/main_page/components/organisms/choose_times.dart';
import 'package:tube/utils/date_time_parse.dart';
import 'package:tube/utils/week_day.dart';
import 'package:tube/utils/week_day_str.dart';

class TrainerCardTariff extends StatefulWidget {
  final User trainer;
  final List<MapEntry<int, List<WorkTimeObj>>> trainerBookings;
  final BehaviorSubject<Map<String, bool>> workTimes;

  const TrainerCardTariff({
    required this.trainer,
    required this.trainerBookings,
    required this.workTimes,
    Key? key,
  }) : super(key: key);

  @override
  State<TrainerCardTariff> createState() => _TrainerCardTariffState();
}

class _TrainerCardTariffState extends State<TrainerCardTariff> {
  final Map<int, List<DateTime>> _mapCount = {};
  late StreamSubscription streamSubscription;
  late List<MapEntry<int, List<WorkTimeObj>>> trainerBookings = [
    ...widget.trainerBookings,
  ];

  void chooseTime(BuildContext context, int index) async {
    final element = [...trainerBookings[index].value]..sort((a, b) {
        return a.date!.compareTo(b.date!);
      });
// Map<String, bool> get workTimes
    final weekDay = trainerBookings[index].key;
    final result = await showFlexibleBottomSheet<Map<String, bool>>(
      context: context,
      minHeight: 0,
      initHeight: 0.95,
      maxHeight: 0.95,
      anchors: [0.95],
      builder: (ctx, ScrollController controller, _) {
        return Material(
          color: Colors.transparent,
          child: ChooseTime(
            controller: controller,
            dateTimes: element,
            workTimes: {...widget.workTimes.value},
            userId: widget.trainer.id!,
            title: weeks[weekDay],
          ),
        );
      },
    );
    if (result == null) return;
    widget.workTimes.value = result;
  }

  @override
  void initState() {
    _initS();
    streamSubscription = widget.workTimes.stream.listen((_) {
      _initS();
      setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    streamSubscription.cancel();
    super.dispose();
  }

  void _initS() {
    _mapCount.clear();
    final entries = widget.workTimes.value.entries;
    for (final entry in entries) {
      final key = entry.key.split('_').first;

      final dateTime = dateTimeParse(entry.key.split('_').last)!;

      if (key != widget.trainer.id || !entry.value) {
        continue;
      }

      (_mapCount[(dateTime.weekday + 6) % 7] ??= []).add(dateTime);
    }
  }

  @override
  void didUpdateWidget(covariant TrainerCardTariff oldWidget) {
    trainerBookings = [...widget.trainerBookings]..sort(
        (a, b) {
          return a.key.compareTo(b.key);
        },
      );
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    trainerBookings = [...widget.trainerBookings]..sort(
        (a, b) {
          return a.key.compareTo(b.key);
        },
      );
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    if (trainerBookings.isEmpty) return const SizedBox();

    return Column(
      children: [
        HeadTrainer(
          title: 'Тренер',
          label: widget.trainer.fullName ?? widget.trainer.email,
          photoUrl: widget.trainer.photoUrl?.thumbnail,
        ),
        const SizedBox(height: 15),
        ...List.generate(
          trainerBookings.length * 2 - 1,
          (ind) {
            if (ind.isOdd) return const SizedBox(height: 10);
            final index = ind ~/ 2;

            final key = trainerBookings[index].key;

            if ((_mapCount[key] ?? []).isNotEmpty) {
              return ActiveTimeCard(
                onTap: () => chooseTime(context, index),
                label: 'Изменить',
                title: weeks[trainerBookings[index].key],
                subLabel: getWeekStr(_mapCount[key]!),
              );
            }
            return DayCard(
              title: weeks[key],
              label: 'Выбрать время',
              onTap: () => chooseTime(context, index),
            );
          },
        ),
        //
        //
        //
        //
        // ...List.generate([].length * 2 + 1, (_) {
        //   if (_ % 2 == 1) {
        //     return const SizedBox(height: 10);
        //   }

        //   if (_ % 4 == 0) {
        //     return ActiveTimeCard(
        //       onTap: () => chooseTime(context),
        //       label: 'Изменить',
        //       title: 'Понедельник',
        //       subLabel: '10:00 | 12:00 | 13:00',
        //     );
        //   }
        //   return DayCard(
        //     title: 'Понедельник',
        //     label: 'Выбрать время',
        //     onTap: () => chooseTime(context),
        //   );
        // }),
      ],
    );
  }
}
