import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tube/logic/blocs/tariff/model/train_time.model.dart';
import 'package:tube/ui/common/molecules/abo_card.dart';
import 'package:tube/utils/get_time_shift.dart';

class AbpoCardWrapper extends StatelessWidget {
  final String? userTitle;
  final String? title;
  final VoidCallback? onTap;
  final List<TrainTime>? trainTimes;
  final String? photoUrl;
  final double? width;
  final bool isNew;
  final int trainCount;
  final ({
    DateTime stopStartTime,
    DateTime stopEndTime,
  })? stopTimes;

  const AbpoCardWrapper({
    this.userTitle,
    this.title,
    this.onTap,
    this.trainTimes,
    this.photoUrl,
    this.width,
    this.isNew = false,
    required this.trainCount,
    this.stopTimes,
    // this.stopStartTime,
    Key? key,
  }) : super(key: key);

  TrainTime? getTrainTime() {
    try {
      return trainTimes?.firstWhere(
        (element) {
          return DateTime.now().microsecondsSinceEpoch < element.endTime!.microsecondsSinceEpoch &&
              element.come != true;
        },
      );
    } catch (e) {
      return null;
    }
  }

  String? getDayTitle() {
    try {
      final trainTime = getTrainTime();
      if (trainTime == null) return null;
      return toBeginningOfSentenceCase(
        DateFormat('EEEEE HH:mm').format(trainTime.date!.parseTimeGG),
      );
    } catch (e) {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    final notActive = (trainTimes ?? []).where(
      (element) {
        DateTime dateTime;
        if (stopTimes?.stopStartTime != null) {
          dateTime = stopTimes!.stopStartTime;
        } else {
          dateTime = DateTime.now();
        }
        final bOp = dateTime.microsecondsSinceEpoch > element.endTime!.microsecondsSinceEpoch ||
            element.come == true;
        return bOp;
      },
    ).toList();
    int count = trainCount - notActive.length;

    return AbpoCard(
      activeTrains: count,
      userTitle: userTitle,
      title: title,
      onTap: count == 0 ? null : onTap,
      dayTitle: getDayTitle(),
      photoUrl: photoUrl,
      width: width,
      isNew: isNew,
      stopTime: stopTimes?.stopEndTime,
    );
  }
}
