import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/provider/booking/get_special_booking.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_next_week_date.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:tube/utils/week_day.dart';

class _WeekDay {
  final List<WorkActiveEntity> trainTimes;
  final int weekDay;

  const _WeekDay({
    required this.trainTimes,
    required this.weekDay,
  });
}

class WorkActiveEntity {
  final String trainerId;
  final DateTime date;

  const WorkActiveEntity({
    required this.trainerId,
    required this.date,
  });
}

class WorkActiveDay extends StatelessWidget {
  final Map<int, List<WorkActiveEntity>> activeWeekDay;
  final Tariff tariff;
  final Map<String, User> trainers;
  final void Function(BuildContext context, int index)? chooseTime;

  const WorkActiveDay({
    required this.activeWeekDay,
    required this.tariff,
    required this.trainers,
    this.chooseTime,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_WeekDay> weekDays = activeWeekDay.entries
        .map(
          (e) => _WeekDay(trainTimes: e.value, weekDay: e.key),
        )
        .toList();
    weekDays.sort((a, b) => a.weekDay.compareTo(b.weekDay));

    if (weekDays.isEmpty) return const SizedBox();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const H5Text(
          'Ваши еженедельные занятия',
          color: ColorData.colorElementsSecondary,
        ),
        const SizedBox(height: 10),
        Column(
          children: List.generate(
            weekDays.length * 2 - 1,
            (index) {
              if (index.isOdd) return const SizedBox(height: 10);
              final _WeekDay weekDay = weekDays[index ~/ 2];
              return SizeTapAnimation(
                child: GestureDetector(
                  onTap: () => chooseTime?.call(context, weekDay.weekDay),
                  child: Container(
                    decoration: BoxDecoration(
                      color: const Color(0xffF3F4F6),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding: const EdgeInsets.only(
                      left: 15,
                      right: 15,
                      bottom: 5,
                      top: 15,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: H4Text(
                                weeks[weekDay.weekDay],
                                color: ColorData.colorTextMain,
                              ),
                            ),
                            const P2Text(
                              'Изменить',
                              color: ColorData.colorMainLink,
                            )
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: List.generate(
                            weekDay.trainTimes.length * 2 - 1,
                            (index) {
                              if (index.isOdd) {
                                return const Divider(
                                  height: 0,
                                  thickness: 0.5,
                                  color: Color(0xffCBCBCB),
                                );
                              }
                              final trainTime = weekDay.trainTimes[index ~/ 2];

                              return Padding(
                                padding: const EdgeInsets.symmetric(vertical: 15),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    P0Text(
                                      DateFormat("HH:mm")
                                          .format(dateTimeOnNextWeek(trainTime.date).parseTimeGG),
                                      color: ColorData.colorElementsActive,
                                    ),
                                    // if (trainers[_trainTime.trainerId]
                                    //             ?.fullName !=
                                    //         null ||
                                    //     trainers[_trainTime.trainerId]?.email !=
                                    //         null)
                                    Consumer<GetSpecialBooking>(
                                      builder: (
                                        context,
                                        value,
                                        child,
                                      ) {
                                        final number = value.getNumberOfUser(
                                          bookingId: tariff.bookingId!,
                                          trainerId: trainTime.trainerId,
                                          dateTime: trainTime.date,
                                        );

                                        if (number == null) {
                                          return const SizedBox();
                                        }

                                        final count =
                                            value.bookings[tariff.bookingId]?.studentNumber ?? 10;
                                        return P2Text(
                                          'Человек в группе: $number/$count',
                                          color: ColorData.colortTextSecondary,
                                        );
                                      },
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
