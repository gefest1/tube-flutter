import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/circle_choose.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class LittleDayCard extends StatefulWidget {
  final String? title;
  final String? label;
  final String? subLabel;
  final bool value;
  final VoidCallback? onTap;
  final bool active;
  final List<DiagnosticsNode>? properties;

  const LittleDayCard({
    this.title,
    this.label,
    this.value = false,
    this.onTap,
    this.subLabel,
    this.active = true,
    this.properties,
    Key? key,
  }) : super(key: key);

  @override
  State<LittleDayCard> createState() => _LittleDayCardState();
}

class _LittleDayCardState extends State<LittleDayCard> {
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    if (widget.properties != null) {
      for (var element in widget.properties!) {
        properties.add(element);
      }
    }
    super.debugFillProperties(properties);
  }

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: widget.active ? widget.onTap : null,
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          border: Border.all(
            width: 0.5,
            color: ColorData.colortTextSecondary,
          ),
          borderRadius: const BorderRadius.all(
            Radius.circular(6),
          ),
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (widget.title != null)
                    Text(
                      widget.title!,
                      style: const TextStyle(
                        fontSize: H3TextStyle.fontSize,
                        height: H3TextStyle.height,
                        fontWeight: H3TextStyle.fontWeight,
                        color: ColorData.colorTextMain,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  if (widget.title != null && widget.label != null) const SizedBox(height: 10),
                  if (widget.subLabel != null)
                    Padding(
                      padding: const EdgeInsets.only(bottom: 2),
                      child: P1Text(
                        widget.subLabel!,
                        color: ColorData.colorTextMain,
                        textAlign: TextAlign.left,
                      ),
                    ),
                  if (widget.label != null)
                    Text(
                      widget.label!,
                      style: TextStyle(
                        fontSize: P1TextStyle.fontSize,
                        height: P1TextStyle.height,
                        fontWeight: P1TextStyle.fontWeight,
                        color: widget.active ? ColorData.colorTextMain : ColorData.colorMainNo,
                      ),
                      textAlign: TextAlign.left,
                    ),
                ],
              ),
            ),
            const SizedBox(width: 10),
            if (widget.active)
              CircleChoose(
                littleSize: 24,
                bigSize: 32,
                iconSize: 14,
                active: widget.value,
                color: Colors.white,
              ),
          ],
        ),
      ),
    );
  }
}
