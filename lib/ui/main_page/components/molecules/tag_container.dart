import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';

class TagAppContainer extends StatelessWidget {
  final String title;
  final bool active;
  final VoidCallback? onTap;

  const TagAppContainer({
    required this.title,
    this.active = false,
    this.onTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        height: 35,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 7),
        decoration: ShapeDecoration(
          color: active ? Colors.white : null,
          shape: const StadiumBorder(
            side: BorderSide(
              width: 0.5,
              color: Colors.white,
            ),
          ),
        ),
        child: P0Text(
          title,
          color: active ? Colors.black : Colors.white,
        ),
      ),
    );
  }
}
