import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tube/logic/provider/create_tariff/create_tariff.dart';
import 'package:provider/provider.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/main_page/components/pages/create_tariff.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/ui/main_page/components/organisms/sub_tariff_card.dart';

class SubTariffPage extends StatefulWidget {
  const SubTariffPage({super.key});

  @override
  State<SubTariffPage> createState() => _SubTariffPageState();
}

class _SubTariffPageState extends State<SubTariffPage> {
  late CreateTariffProvider crTariff =
      Provider.of<CreateTariffProvider>(context);
  int? choosenIndex;

  void onTap(int index) {
    setState(() {
      choosenIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final subBooking = crTariff.booking.subBooking;
    final booking = crTariff.booking;
    return Scaffold(
      appBar: AppBar(
        title: const H4Text(
          'Покупка абонемента',
          color: Colors.white,
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
      ),
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          CustomScrollView(
            slivers: [
              const SliverToBoxAdapter(
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: H2Text(
                    'Купить пробное занятие или полный абонемент?',
                    color: ColorData.colorTextMain,
                  ),
                ),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (context, index) {
                    if (index == subBooking.length) {
                      return SubTariffCard(
                        active: choosenIndex == index,
                        onTap: () => onTap(index),
                        title:
                            "Абонемент — ${NumberFormat('###,###,000₸').format(booking.price)}",
                        label: 'Полноценный абонемент',
                      );
                    }

                    return SubTariffCard(
                      onTap: () => onTap(index),
                      active: choosenIndex == index,
                      title:
                          "Пробное занятие — ${NumberFormat('###,###,000₸').format(subBooking[index].price)}",
                      label:
                          '${subBooking[index].count} занятие, чтобы попробовать',
                    );
                  },
                  childCount: crTariff.booking.subBooking.length + 1,
                ),
              ),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: BottomButton(
              active: choosenIndex != null,
              onTap: () async {
                final setSubBooking = choosenIndex == subBooking.length
                    ? null
                    : subBooking[choosenIndex!];
                crTariff.update(
                  subBooking: setSubBooking,
                );

                await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const CreateTariff(),
                  ),
                );
                crTariff.clean(subBooking: true);
              },
            ),
          ),
        ],
      ),
    );
  }
}
