import 'package:flutter/material.dart';
import 'package:tube/ui/admin/page/tariff/molecules/edit_tariff_header.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:tube/utils/infinite_listview.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/blocs/gifts/provider/gift_provider.dart';
import 'package:tube/logic/provider/create_tariff/create_tariff.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/fast_app_bar.dart';
import 'package:tube/ui/common/molecules/shedule_table.dart';
import 'package:tube/ui/main_page/components/pages/pay_tarif_page.dart';

import 'package:tube/utils/colors.dart';
import 'dart:developer' as dev;

class ChooseStartDayPage extends StatefulWidget {
  const ChooseStartDayPage({super.key});

  @override
  State<ChooseStartDayPage> createState() => _ChooseStartDayPageState();
}

class _ChooseStartDayPageState extends State<ChooseStartDayPage> {
  final TextEditingController dateController = TextEditingController();
  DateTime? dateTime;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: const H4Text(
            'Начало абонемента',
            color: Colors.white,
          ),
          leading: IconButton(
            onPressed: () => Navigator.pop(context),
            icon: const Icon(
              Icons.arrow_back,
              size: 33,
            ),
            color: Colors.white,
          ),
        ),
        body: Stack(
          children: [
            ListView(
              padding: const EdgeInsets.all(25),
              children: [
                const H2Text(
                  'С какого числа активировать абонемент?',
                  color: ColorData.colorTextMain,
                ),
                const SizedBox(height: 40),
                GestureDetector(
                  onTap: () async {
                    final date = await showFlexibleBottomSheet<DateTime>(
                      minHeight: 0,
                      initHeight: 0.95,
                      maxHeight: 0.95,
                      context: context,
                      anchors: [0.95],
                      builder: (context, scrollController, bottomSheetOffset) {
                        return StartShedulePage(
                          controller: scrollController,
                          stopDate: context.read<CreateTariffProvider>().booking.stopDate,
                        );
                      },
                    );
                    if (date == null) return;
                    setState(() {
                      dateTime = date;
                    });

                    dateController.text = DateFormat("dd/MM/yyyy").format(date.parseTimeGG);
                  },
                  behavior: HitTestBehavior.opaque,
                  child: IgnorePointer(
                    child: CustomTextField(
                      controller: dateController,
                      hintText: 'ДД/ММ/ГГ',
                      title: 'Дата старта абонемента',
                      icon: const Icon(
                        Icons.date_range,
                        size: 24,
                        color: ColorData.colorElementsActive,
                      ),
                    ),
                  ),
                )
              ],
            ),
            if (dateTime != null)
              Positioned(
                left: 0,
                right: 0,
                bottom: 0,
                child: BottomButton(
                  title: 'Продолжить',
                  onTap: () async {
                    final crtProvider = context.read<CreateTariffProvider?>() ??
                        context.read<GiftTariffProvider?>();

                    if (crtProvider is CreateTariffProvider) {
                      crtProvider.update(dateTime: dateTime);
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => const PayTarifPage(),
                        ),
                      );
                      crtProvider.clean(dateTime: true);
                    } else if (crtProvider is GiftTariffProvider) {
                      crtProvider.update(dateTime: dateTime);
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => const PayTarifPage(),
                        ),
                      );
                      crtProvider.clean(dateTime: true);
                    }
                  },
                ),
              ),
          ],
        ),
      ),
    );
  }
}

class StartShedulePage extends StatefulWidget {
  final ScrollController controller;
  final DateTime? stopDate;

  const StartShedulePage({
    this.stopDate,
    required this.controller,
    super.key,
  });

  @override
  State<StartShedulePage> createState() => _StartShedulePageState();
}

class _StartShedulePageState extends State<StartShedulePage> {
  late final InfiniteScrollController infiniteScrollController = InfiniteScrollController();
  final lastIndex = BehaviorSubject.seeded(0);

  final _startTime = DateTime.now();
  late final BehaviorSubject<DateTime?> dateTimeSubject = BehaviorSubject<DateTime?>.seeded(null);

  @override
  void dispose() {
    infiniteScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(10),
          ),
        ),
        alignment: Alignment.topCenter,
        child: Stack(
          children: [
            ListView(
              controller: widget.controller,
              children: [
                FastAppBar(
                  title: 'Дата начала',
                  suffIcon: Icons.close,
                  suffOnTap: () {
                    Navigator.pop(context);
                  },
                ),
                const SizedBox(height: 20),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  decoration: const BoxDecoration(
                    color: ColorData.color3Percent,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: EditTariffHeader(
                          controller: infiniteScrollController,
                          lastIndex: lastIndex,
                          currentYear: _startTime.year,
                          currentMonth: _startTime.month,
                        ),
                      ),
                      SheduleTable(
                        validation: widget.stopDate == null
                            ? null
                            : (time) {
                                return time.millisecondsSinceEpoch <=
                                    widget.stopDate!.millisecondsSinceEpoch;
                              },
                        controller: infiniteScrollController,
                        lastIndexSubject: lastIndex,
                        dateTimeSubject: dateTimeSubject,
                        onTap: (_) {
                          dateTimeSubject.value = _;
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: StreamBuilder(
                stream: dateTimeSubject,
                builder: (context, _) {
                  return BottomButton(
                    active: dateTimeSubject.value != null,
                    title: 'Выбрать',
                    onTap: () {
                      Navigator.pop(context, dateTimeSubject.value);
                    },
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
