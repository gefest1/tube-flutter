import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/logic/provider/create_tariff/create_tariff.dart';
import 'package:tube/logic/tariff/get_tariff.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/main_page/components/molecules/tag_container.dart';
import 'package:tube/ui/main_page/components/molecules/tariff_card.dart';
import 'package:tube/ui/main_page/components/pages/create_tariff.dart';
import 'package:tube/ui/main_page/components/pages/tag_bottom_sheet.dart';
import 'package:tube/utils/nested_logic_route.dart';
import 'package:tube/ui/main_page/components/pages/sub_tariff_page.dart';

class ChooseTariffPage extends StatefulWidget {
  const ChooseTariffPage({Key? key}) : super(key: key);

  @override
  State<ChooseTariffPage> createState() => _ChooseTariffPageState();
}

class _ChooseTariffPageState extends State<ChooseTariffPage> {
  List<Booking> bookings = [];
  List<String> tags = [];
  Map<String, bool> tagsMap = {};
  _initS() async {
    final data = await getTariffWithTrainer(
      clubId: MainDrawerState.currentParticipance!.clubId!,
    );
    final bookings = data;
    this.bookings = bookings;
    // final stopTariffs = data.stopTariffs;
    tagsMap = <String, bool>{};
    for (final book in bookings) {
      for (final tag in book.tags) {
        tagsMap[tag] = false;
      }
    }
    tags = tagsMap.entries.map((e) => e.key).toList();
    if (!mounted) return;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    Future(() {
      _initS();
    });
  }

  void openTagSheet() async {
    final newVal = await showFlexibleBottomSheet(
      minHeight: 0,
      initHeight: 0.95,
      maxHeight: 0.95,
      context: context,
      anchors: [0.95],
      builder: (ctx, controller, _) {
        return Material(
          color: Colors.transparent,
          child: TagBottomSheet(
            controller: controller,
            tagMap: tagsMap,
          ),
        );
      },
    );
    if (newVal == null) return;
    setState(() {
      tagsMap = newVal;
    });
  }

  @override
  Widget build(BuildContext context) {
    final filteredTags = tagsMap.entries
        .where((element) => element.value)
        .map((e) => e.key)
        .toList();
    final bookings = this.bookings.where((book) {
      if (filteredTags.isEmpty) return true;
      for (final singleTag in filteredTags) {
        if (!book.tags.contains(singleTag)) return false;
      }
      return true;
    }).toList();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: openTagSheet,
            icon: const Icon(
              Icons.tune,
              color: Colors.white,
            ),
          ),
        ],
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
        centerTitle: true,
        title: const H4Text(
          'Выбор абонемента',
          color: Colors.white,
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(55),
          child: SizedBox(
            height: 50,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              padding: const EdgeInsets.only(
                  right: 20, left: 20, bottom: 10, top: 5),
              itemBuilder: (context, index) => TagAppContainer(
                title: tags[index],
                active: tagsMap[tags[index]] ?? false,
                onTap: () {
                  setState(() {
                    tagsMap[tags[index]] = !(tagsMap[tags[index]] ?? false);
                  });
                },
              ),
              // Container(
              //   height: 35,
              //   padding:
              //       const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              //   child: P0Text(
              //     tags[index],
              //     color: Colors.white,
              //   ),
              //   decoration: ShapeDecoration(
              //     shape: StadiumBorder(),
              //   ),
              // ),
              separatorBuilder: (context, index) => const SizedBox(width: 5),
              itemCount: tags.length,
            ),
          ),
        ),
      ),
      body: ListView.separated(
        padding:
            const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 100),
        separatorBuilder: (ctx, _) => const SizedBox(height: 20),
        itemCount: bookings.length,
        itemBuilder: (ctx, _) => TariffCard(
          onTap: () async {
            // log(message) bookings[_].stopDate;
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (__) => MultiProvider(
                  providers: [
                    ChangeNotifierProvider(
                      create: (ctx) =>
                          CreateTariffProvider(booking: bookings[_]),
                    )
                  ],
                  child: NestedLogicRoute(
                    child: bookings[_].subBooking.isEmpty
                        ? const CreateTariff()
                        : const SubTariffPage(),
                  ),
                ),
              ),
            );
          },
          title: bookings[_].name,
          priceCost: NumberFormat('###,###,000₸').format(bookings[_].price!),
          applyUntil: "${bookings[_].deadlineDays} дней",
          countLesson: bookings[_].count?.toInt().toString(),
          minDuration:
              '${bookings[_].timePerMinute?.toInt().toString() ?? '0'} мин',
          description: bookings[_].description,
          tags: bookings[_].tags,
        ),
      ),
    );
  }
}
