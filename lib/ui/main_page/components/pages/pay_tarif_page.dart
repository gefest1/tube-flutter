import 'dart:async';
import 'dart:developer';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/gifts/gift_bloc.dart';
import 'package:tube/logic/blocs/gifts/provider/gift_provider.dart';
import 'package:tube/logic/blocs/tariff/tariff_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/create_tarriff.dart';
import 'package:tube/logic/provider/create_tariff/create_tariff.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/main_page/components/molecules/pay_card_detail.dart';
import 'package:tube/ui/main_page/components/molecules/trainer_booking_card.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/get_time_shift.dart';
import 'package:tube/utils/load_popup_route.dart';
import 'package:url_launcher/url_launcher_string.dart';

class PayTarifPage extends StatelessWidget {
  const PayTarifPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final crtProvider = Provider.of<CreateTariffProvider?>(context, listen: false);
    final giftProvider = context.read<GiftTariffProvider?>();
    final booking = crtProvider?.booking ?? giftProvider!.booking;
    final dateTimesMap = crtProvider?.dateTimesMap ?? giftProvider?.dateTimesMap ?? {};
    final mapUsers = crtProvider?.mapUsers ?? giftProvider?.mapUsers ?? {};
    final owner = crtProvider?.owner;
    final dateTime = crtProvider?.dateTime ?? giftProvider?.dateTime;
    final userBlocMap = Provider.of<UserBlocMap>(context, listen: false);

    String? countLessonTitle = crtProvider?.subBooking?.count.toString() ??
        giftProvider?.gift.count.toString() ??
        booking.count?.toString();
    int? applyUntilTitleDirt = crtProvider?.subBooking?.deadlineDays ?? booking.deadlineDays;
    String? applyUntilTitle = "$applyUntilTitleDirt дней";

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        centerTitle: true,
        title: const Text(
          'Оплата',
          style: TextStyle(
            fontSize: H4TextStyle.fontSize,
            fontWeight: H4TextStyle.fontWeight,
            height: H4TextStyle.height,
            color: Colors.white,
          ),
        ),
      ),
      body: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverPadding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                sliver: SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      PayCardDetail(
                        title: booking.name,
                        priceTitle: NumberFormat('###,###,000₸')
                            .format(crtProvider?.subBooking?.price ?? booking.price),
                        applyUntilTitle: applyUntilTitle,
                        countLessonTitle: countLessonTitle,
                        description: booking.description,
                        minDuration:
                            booking.timePerMinute == null ? null : "${booking.timePerMinute} мин",
                        trainerBookingModels: dateTimesMap.entries
                            .map(
                              (e) => TrainerBookingModel(
                                name: mapUsers[e.key]?.fullName ?? mapUsers[e.key]?.email,
                                photoURl: mapUsers[e.key]?.photoUrl?.m,
                                dateTimes: e.value,
                              ),
                            )
                            .toList(),
                        startTime: DateFormat("dd.MM.yyyy")
                            .format((dateTime ?? DateTime.now()).parseTimeGG),
                      ),
                      // const SizedBox(height: 40),
                      // const H4Text(
                      //   "Способ оплаты",
                      //   color: ColorData.colorElementsActive,
                      // ),
                      // SizeTapAnimation(
                      //   child: GestureDetector(
                      //     onTap: () {
                      //       Navigator.push(
                      //         context,
                      //         MaterialPageRoute(
                      //           builder: (_) => const CardDetail(),
                      //         ),
                      //       );
                      //     },
                      //     behavior: HitTestBehavior.opaque,
                      //     child: Padding(
                      //       padding: const EdgeInsets.symmetric(vertical: 15),
                      //       child: Row(
                      //         children: const [
                      //           Icon(
                      //             Icons.credit_card,
                      //             size: 24,
                      //             color: ColorData.colorElementsActive,
                      //           ),
                      //           SizedBox(width: 10),
                      //           Expanded(
                      //             child: Text(
                      //               'Добавить карту',
                      //               style: TextStyle(
                      //                 fontSize: P1TextStyle.fontSize,
                      //                 fontWeight: P1TextStyle.fontWeight,
                      //                 height: P1TextStyle.height,
                      //                 color: ColorData.colorTextMain,
                      //               ),
                      //             ),
                      //           ),
                      //           SizedBox(width: 10),
                      //           Icon(
                      //             Icons.navigate_next,
                      //             size: 24,
                      //             color: ColorData.colorTextMain,
                      //           ),
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      // ),
                      // const Divider(
                      //   color: ColorData.colortTextSecondary,
                      //   thickness: 0.2,
                      //   height: 0,
                      // ),
                      const SizedBox(height: 280),
                    ],
                  ),
                ),
              )
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: BottomButton(
              title: giftProvider == null ? "Оплатить" : "Заврешить",
              textSpan: TextSpan(
                children: [
                  const TextSpan(
                    text: 'Нажимая "Оплатить" вы принимаете ',
                  ),
                  TextSpan(
                    style: const TextStyle(color: ColorData.colorMainLink),
                    text: 'Условия использования для клиентов',
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        launchUrlString(
                          "https://docs.google.com/document/d/1043GEkJI4FZHzCYrQR_Qe_xx_pmBrx2RyzBSqX_K-5w/edit",
                        );
                      },
                  ),
                ],
              ),
              onTap: () async {
                final Completer completer = Completer();
                Navigator.push(
                  context,
                  LoadPopupRoute(completer: completer),
                );
                final List<InputTrainGraph> inputs = [];
                for (final entry in dateTimesMap.entries) {
                  for (final date in entry.value) {
                    inputs.add(InputTrainGraph(
                      date: date,
                      endTime: date.add(
                        Duration(
                          minutes: booking.timePerMinute!.toInt(),
                        ),
                      ),
                      trainerId: entry.key,
                    ));
                  }
                }
                inputs.sort((a, b) {
                  return a.date.compareTo(b.date);
                });
                if (giftProvider != null) {
                  final giftBloc = userBlocMap.getGiftBloc(
                    MainDrawerState.currentParticipance!.clubId!,
                  );
                  giftBloc.add(
                    UseGiftEvent(
                      inputTrains: inputs,
                      giftId: giftProvider.gift.id,
                      completer: completer,
                    ),
                  );
                  await completer.future;
                  userBlocMap
                      .getTariffBloc(
                        MainDrawerState.currentParticipance!.clubId!,
                      )
                      .add(const GetDataTariffEvent());
                  giftBloc.add(const GetGiftEvent());

                  // ignore: use_build_context_synchronously
                  Navigator.of(context, rootNavigator: true).popUntil((route) => route.isFirst);
                }
                if (crtProvider != null) {
                  try {
                    userBlocMap
                        .getTariffBloc(
                          MainDrawerState.currentParticipance!.clubId!,
                        )
                        .add(
                          AddTariffEvent(
                            booking: booking,
                            completer: completer,
                            inputTrains: inputs,
                            dateTime: dateTime,
                            owner: owner,
                            subBooking: crtProvider.subBooking,
                          ),
                        );

                    await completer.future;
                    // ignore: use_build_context_synchronously
                    Navigator.of(context, rootNavigator: true).popUntil((route) => route.isFirst);
                  } catch (e) {
                    log(e.toString());
                  }
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
