import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/main_page/components/organisms/buy_ticket_body.dart';
import 'package:tube/ui/main_page/components/pages/choose_tariff_page.dart';
import 'package:tube/utils/const.dart';

class BuyTicketPage extends StatefulWidget {
  const BuyTicketPage({Key? key}) : super(key: key);

  @override
  State<BuyTicketPage> createState() => _BuyTicketPageState();
}

class _BuyTicketPageState extends State<BuyTicketPage> {
  final StreamController<InputModelBuyTicket?> inputModelBuyTicket =
      StreamController<InputModelBuyTicket?>();

  @override
  void dispose() {
    inputModelBuyTicket.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        final f = FocusScope.of(context);

        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              size: 33,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: const Text(
            'Покупка абонемента',
            style: TextStyle(
              fontSize: H4TextStyle.fontSize,
              fontWeight: H4TextStyle.fontWeight,
              height: H4TextStyle.height,
              color: Colors.white,
            ),
          ),
        ),
        body: Stack(
          children: [
            CustomScrollView(
              slivers: [
                const SliverPadding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  sliver: SliverToBoxAdapter(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 20),
                        Text(
                          'Чей это будет абонемент?',
                          style: TextStyle(
                            fontWeight: H2TextStyle.fontWeight,
                            fontSize: H2TextStyle.fontSize,
                            height: H2TextStyle.height,
                            color: Color(0xff14142B),
                          ),
                        ),
                        SizedBox(height: 30),
                      ],
                    ),
                  ),
                ),
                SliverPadding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  sliver: SliverList(
                    delegate: SliverChildListDelegate([
                      BuyTicketAdaptiveBody(
                        streamController: inputModelBuyTicket,
                      ),
                    ]),
                  ),
                ),
                const SliverToBoxAdapter(
                  child: SizedBox(height: 160),
                )
              ],
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: StreamBuilder(
                stream: inputModelBuyTicket.stream,
                builder: (context, snapshot) {
                  if (snapshot.data == null) {
                    return const SizedBox();
                  }
                  return BottomButton(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => const ChooseTariffPage(),
                        ),
                      );
                    },
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
