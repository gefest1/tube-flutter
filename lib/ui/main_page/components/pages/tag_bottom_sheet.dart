import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/active_tag.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/fast_app_bar.dart';

class TagBottomSheet extends StatefulWidget {
  final ScrollController controller;
  final Map<String, bool> tagMap;

  const TagBottomSheet({
    required this.tagMap,
    required this.controller,
    super.key,
  });

  @override
  State<TagBottomSheet> createState() => _TagBottomSheetState();
}

class _TagBottomSheetState extends State<TagBottomSheet> {
  late final tagsMap = {...widget.tagMap};
  @override
  Widget build(BuildContext context) {
    List<String> tagList = tagsMap.entries.map((e) => e.key).toList();
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(10),
        ),
      ),
      child: Stack(
        children: [
          CustomScrollView(
            controller: widget.controller,
            slivers: [
              SliverToBoxAdapter(
                child: FastAppBar(
                  title: 'Атрибуты',
                  suffIcon: Icons.close,
                  suffOnTap: () => Navigator.pop(context),
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
                  child: Wrap(
                    spacing: 10,
                    runSpacing: 10,
                    alignment: WrapAlignment.center,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: List.generate(
                      tagList.length,
                      (index) => ActiveTag(
                        active: tagsMap[tagList[index]] ?? false,
                        title: tagList[index],
                        onTap: () {
                          setState(() {
                            tagsMap[tagList[index]] =
                                !(tagsMap[tagList[index]] ?? false);
                          });
                        },
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: BottomButton(
              title: 'Применить',
              onTap: () {
                Navigator.pop(context, tagsMap);
              },
            ),
          )
        ],
      ),
    );
  }
}
