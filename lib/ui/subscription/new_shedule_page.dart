import 'dart:async';
import 'dart:developer';

import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:graphql/client.dart';

import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/tariff/model/train_time.model.dart';
import 'package:tube/logic/blocs/tariff/tariff_bloc.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/logic/model/workt_time_obj.dart';
import 'package:tube/logic/provider/booking/get_special_booking.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/main_page/components/molecules/not_active_work_day.dart';
import 'package:tube/ui/main_page/components/molecules/work_day.dart';
import 'package:tube/ui/main_page/components/organisms/choose_times.dart';
import 'package:tube/utils/date_time_parse.dart';
import 'package:tube/utils/load_popup_route.dart';
import 'package:tube/utils/overlay_error.dart';
import 'package:tube/utils/parse_time.dart';
import 'package:tube/utils/week_day.dart';

class NewShedulePage extends StatefulWidget {
  final String clubId;
  final Tariff tariff;

  const NewShedulePage({
    required this.clubId,
    required this.tariff,
    super.key,
  });

  @override
  State<NewShedulePage> createState() => _NewShedulePageState();
}

extension _Booking on _NewShedulePageState {
  Booking get _booking =>
      Provider.of<GetSpecialBooking>(context, listen: false).bookings[widget.tariff.bookingId]!;

  List<TrainTime> get activeTraining {
    return (widget.tariff.trainTimes ?? [])
        .where(
          (element) => (element.come == false && (element.date?.isAfter(DateTime.now()) ?? false)),
        )
        .toList();
  }
}

class _NewShedulePageState extends State<NewShedulePage> {
  late TariffBloc tariffBloc = Provider.of<UserBlocMap>(context, listen: false).getTariffBloc(
    MainDrawerState.currentParticipance!.clubId!,
  );

  late final Map<String, User> trainerMap = Map<String, User>.fromEntries(
    (_booking.trainers ?? []).map((e) => MapEntry(e.id!, e)),
  );
  //

  late Map<String, bool> initWorkTimes = initActiveF(activeTraining);
  late final BehaviorSubject<Map<String, bool>> workTimes =
      BehaviorSubject<Map<String, bool>>.seeded(initWorkTimes);

  //

  void chooseTime(BuildContext context, int index) async {
    log(workTimeObjTrainer[index]!.toString());
    log(workTimes.value.toString());

    final setWorkTime = Map.fromEntries(
      workTimes.value.entries.where((element) {
        final trainerId = element.key.split("_").first;
        final date = dateTimeParse(element.key.split("_").last)!;
        if ((date.weekday + 6) % 7 != index) return true;
        final needed = workTimeObjTrainer[index]!
            .firstWhereOrNull((wobj) => wobj.trainer.id == trainerId && wobj.date == date);
        return needed != null;
      }),
    );
    final Map<String, bool>? result = await showFlexibleBottomSheet(
      context: context,
      minHeight: 0,
      initHeight: 0.95,
      maxHeight: 0.95,
      anchors: [0.95],
      builder: (ctx, ScrollController controller, _) {
        return Material(
          color: Colors.transparent,
          child: ChooseTime(
            title: weeks[index],
            controller: controller,
            dateTimes: workTimeObjTrainer[index]!,
            workTimes: setWorkTime,
          ),
        );
      },
    );

    if (result == null) return;
    setState(() {
      workTimes.value = result;
    });
  }

  Map<int, List<WorkActiveEntity>> get activeWeekDay {
    final Map<int, List<WorkActiveEntity>> toReturn = {};

    // for (final entry in workTimeObjTrainer.entries) {
    //   for (final workTimeObj in entry.value) {
    //     String trainerId = workTimeObj.trainer.id!;
    //     DateTime date = workTimeObj.date!;
    //     final key = trainerId + "_" + date.toIso8601String();
    //     if (workTimes.value[key] != true) continue;

    //     final _weekday = (date.weekday + 6) % 7;
    //     (toReturn[_weekday] ??= []).add(WorkActiveEntity());
    //   }
    // }
    for (final entry in workTimes.value.entries) {
      if (!entry.value) continue;
      final trainerId = entry.key.split("_").first;
      final date = DateTime.parse(entry.key.split("_").last);

      final weekday = (date.weekday + 6) % 7;
      (toReturn[weekday] ??= []).add(
        WorkActiveEntity(
          date: date,
          trainerId: trainerId,
        ),
      );
    }

    // final Map<String, bool> wasTime = {};

    // for (final singelTrain in activeTraining) {
    //   final _str = parseTime(singelTrain.date!).toString();
    //   if (wasTime[_str] == true) continue;
    //   wasTime[_str] = true;

    //   final _weekday = (singelTrain.date!.weekday + 6) % 7;

    //   (toReturn[_weekday] ??= []).add(singelTrain);
    // }
    return toReturn;
  }

  Tariff get tariff {
    return widget.tariff;
  }

  late Map<int, List<WorkTimeObjTrainer>> workTimeObjTrainer =
      initWOrkTimeObj(_booking, trainerMap);

  @override
  Widget build(BuildContext context) {
    log(initWorkTimes.toString());
    final wellDone = initWorkTimes.entries.map((val) => val.key.split("_")[1]);
    // log(Map.fromEntries(
    //   workTimes.value.entries.where((element) => element.value),
    // ).toString());
    return Stack(
      children: [
        CustomScrollView(
          slivers: [
            const SliverToBoxAdapter(
              child: SizedBox(
                height: 30,
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: WorkActiveDay(
                  activeWeekDay: activeWeekDay,
                  tariff: widget.tariff,
                  trainers: Map.fromEntries(
                    (widget.tariff.trainers ?? []).map((e) => MapEntry(e.id!, e)),
                  ),
                  chooseTime: chooseTime,
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 20,
                  right: 20,
                  top: 30,
                ),
                child: NotActiveWorkDay(
                  getActiveWeekDay: activeWeekDay,
                  chooseTime: chooseTime,
                  workTimeObjTrainer: workTimeObjTrainer,
                ),
              ),
            ),
            const SliverToBoxAdapter(child: SizedBox(height: 200)),
          ],
        ),
        Positioned(
          left: 0,
          right: 0,
          bottom: 0,
          child: BottomButton(
            title: 'Сохранить',
            active: !mapEquals(
              initWorkTimes,
              Map.fromEntries(
                workTimes.value.entries.where((element) => element.value),
              ),
            ),
            onTap: () {
              List<InputTrain> inputTrain =
                  workTimes.value.entries.where((element) => element.value).map(
                (e) {
                  final trainerId = e.key.split("_").first;
                  final date = DateTime.parse(e.key.split("_").last);

                  return InputTrain(
                    date: date,
                    endTime: date.add(
                      Duration(minutes: _booking.timePerMinute!.toInt()),
                    ),
                    trainerId: trainerId,
                  );
                },
              ).toList();
              log(inputTrain.toString());
              final completer = Completer();
              Navigator.push(context, LoadPopupRoute(completer: completer));
              tariffBloc.add(
                EditTariffEvent(
                  completer: completer,
                  tariffId: widget.tariff.id!,
                  inputs: inputTrain,
                ),
              );
              completer.future.catchError((e, s) {
                log(e.toString(), stackTrace: s);
                if (e is OperationException) {
                  for (final errors in e.graphqlErrors) {
                    navigatorKeyGlobal.currentState?.overlay?.insert(
                      OverlayError(
                        text: errors.message,
                      ),
                    );
                  }
                }
              });
            },
          ),
        ),
      ],
    );
  }
}

Map<String, bool> initActiveF(List<TrainTime> activeTraining) => Map.fromEntries(
      activeTraining.map((singlTrain) {
        log(singlTrain.date!.toUtc().toIso8601String());
        final str = parseTime(singlTrain.date!).toIso8601String();
        return MapEntry("${singlTrain.trainerId!}_$str", true);
      }),
    );

Map<int, List<WorkTimeObjTrainer>> initWOrkTimeObj(
  Booking booking,
  Map<String, User> trainerMap,
) {
  final Map<int, List<WorkTimeObjTrainer>> workTimeObj =
      Map.fromEntries(List.generate(7, (index) => MapEntry(index, [])));

  for (final MapEntry<String, List<WorkTimeObj>> singleEntry in booking.workTimes?.entries ?? []) {
    for (final everyWorkTime in singleEntry.value) {
      if (everyWorkTime.date == null) continue;
      (workTimeObj[(everyWorkTime.date!.weekday + 6) % 7] ??= []).add(
        WorkTimeObjTrainer(
          date: everyWorkTime.date,
          max: everyWorkTime.max,
          now: everyWorkTime.now,
          trainer: trainerMap[singleEntry.key]!,
        ),
      );
    }
  }
  // ignore: avoid_function_literals_in_foreach_calls
  workTimeObj.values.forEach((element) {
    element.sort((a, b) {
      return a.date!.compareTo(b.date!);
    });
  });

  return workTimeObj;
}
