import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/tariff/model/train_time.model.dart';
import 'package:tube/logic/blocs/tariff/tariff_bloc.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/logic/model/workt_time_obj.dart';
import 'package:tube/logic/provider/booking/get_special_booking.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';

import 'package:tube/ui/main_page/components/organisms/choose_times.dart';
import 'package:tube/ui/subscription/new_shedule_page.dart';

import 'package:tube/utils/load_popup_route.dart';

import 'package:tube/utils/parse_time.dart';
import 'package:tube/utils/week_day.dart';

class ShedulePage extends StatelessWidget {
  final String clubId;
  final String tariffId;

  const ShedulePage({
    required this.clubId,
    required this.tariffId,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TariffBloc, TariffState>(
      bloc: Provider.of<UserBlocMap>(context, listen: false).getTariffBloc(
        MainDrawerState.currentParticipance!.clubId!,
      ),
      builder: (context, state) {
        if (state is DataTariffState) {
          try {
            final tariff =
                state.tariffs.firstWhere((element) => element.id == tariffId);
            return NewShedulePage(
              clubId: clubId,
              tariff: tariff,
            );
          } catch (e) {
            return const SizedBox();
          }
        }
        return const SizedBox();
      },
    );
  }
}

class InnerShedulePage extends StatefulWidget {
  final String clubId;
  final Tariff tariff;

  const InnerShedulePage({
    required this.clubId,
    required this.tariff,
    Key? key,
  }) : super(key: key);

  @override
  State<InnerShedulePage> createState() => _InnerShedulePageState();
}

class _InnerShedulePageState extends State<InnerShedulePage> {
  late Booking _booking = Provider.of<GetSpecialBooking>(context, listen: false)
      .bookings[widget.tariff.bookingId]!;

  //

  late Map<int, List<TrainTime>> getActiveWeekDay = getActiveWeekDayF();
  late Map<String, bool> initWorkTimes = initActiveF(activeTraining);
  late final BehaviorSubject<Map<String, bool>> workTimes =
      BehaviorSubject<Map<String, bool>>.seeded(initWorkTimes);
  late final Map<String, User> trainerMap = Map<String, User>.fromEntries(
    (_booking.trainers ?? []).map((e) => MapEntry(e.id!, e)),
  );
  late Map<int, List<WorkTimeObjTrainer>> workTimeObjTrainer =
      initWOrkTimeObj(_booking, trainerMap);

  @override
  void dispose() {
    workTimes.close();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant InnerShedulePage oldWidget) {
    if (widget.tariff != oldWidget.tariff) {
      Future(update);
    }
    super.didUpdateWidget(oldWidget);
  }

  void update() async {
    final completer = Completer();
    Navigator.push(context, LoadPopupRoute(completer: completer));
    try {
      _booking = await Provider.of<GetSpecialBooking>(context, listen: false)
          .getBooking(widget.tariff.bookingId!);
      getActiveWeekDay = getActiveWeekDayF();
      workTimes.value = initActiveF(activeTraining);
      workTimeObjTrainer = initWOrkTimeObj(_booking, trainerMap);
      setState(() {});
    } finally {
      completer.complete();
    }
  }

  void chooseTime(BuildContext context, int index) async {
    log(workTimes.value.toString());
    final Map<String, bool>? result = await showFlexibleBottomSheet(
      context: context,
      minHeight: 0,
      initHeight: 0.95,
      maxHeight: 0.95,
      anchors: [0.95],
      builder: (ctx, ScrollController controller, _) {
        return Material(
          color: Colors.transparent,
          child: ChooseTime(
            title: weeks[index],
            controller: controller,
            dateTimes: workTimeObjTrainer[index]!,
            workTimes: {...workTimes.value},
          ),
        );
      },
    );
    if (result == null) return;
    log(result.toString());
    // workTimes.value =
    // newInputTrain[index] = _result.entries
    //     .where((element) => element.value)
    //     .map(
    //       (e) => WorkTimeObjTrainer(
    //         trainer: trainerMap[e.key.split("_").first]!,
    //         date: dateTimeParse(e.key.split("_").last)!,
    //       ),
    //     )
    //     .toList();
    // log(workTimeObjTrainer[index].toString());
    // log(newInputTrain[index].toString());

    // final Map<String, List<DateTime>> _dateTimesMap = {};

    // ignore: avoid_function_literals_in_foreach_calls
    // _result.entries.forEach((e) {
    //   if (e.value == false) return;
    //   final _splitted = e.key.split('_');
    //   final userId = _splitted.first;
    //   final DateTime _dateTime = dateTimeParse(_splitted.last)!;
    //   (_dateTimesMap[userId] ??= []).add(_dateTime);
    // });

    // final List<InputTrainGraph> inputs = [];
    // for (final _entry in _dateTimesMap.entries) {
    //   for (final _date in _entry.value) {
    //     inputs.add(InputTrainGraph(
    //       date: _date,
    //       endTime: _date.add(
    //         Duration(
    //           minutes: (widget.tariff.booking?.timePerMinute ?? 0).toInt(),
    //         ),
    //       ),
    //       trainerId: _entry.key,
    //     ));
    //   }
    // }
    // inputs.sort((a, b) {
    //   return a.date.compareTo(b.date);
    // });
    // final _count = widget.tariff.trainTimes?.fold(
    //       0,
    //       (num previousValue, TrainTime element) {
    //         if (element.come == false &&
    //             element.endTime!.compareTo(DateTime.now()) == 1) {
    //           return previousValue + 1;
    //         }
    //         return previousValue;
    //       },
    //     ).toInt() ??
    //     0;
    // final _prefixCount = (widget.tariff.trainTimes?.length ?? 0) - _count;
    // final _listTrainTime = getTrainTime(
    //   inputs,
    //   _count,
    //   (widget.tariff.booking?.timePerMinute ?? 0).toInt(),
    // );

    // //

    // final List<TrainTime> _newTrainTime = [
    //   ...(widget.tariff.trainTimes?.sublist(0, _prefixCount) ?? []),
    //   ..._listTrainTime,
    // ];

    // final Completer completer = Completer();

    // final _tariffBloc =
    //     Provider.of<UserBlocMap>(context, listen: false).getTariffBloc(
    //   MainDrawerState.currentParticipance!.clubId!,
    // );
    // _tariffBloc.add(
    //   UpdateTrainTimeEvent(
    //     completer: completer,
    //     tariffId: widget.tariff.id!,
    //     trainTime: _newTrainTime,
    //   ),
    // );

    // Navigator.push(context, LoadPopupRoute(completer: completer));
  }

  List<TrainTime> get activeTraining {
    return (widget.tariff.trainTimes ?? [])
        .where(
          (element) => (element.come == false &&
              (element.date?.isAfter(DateTime.now()) ?? false)),
        )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    log(workTimeObjTrainer.toString());
    // final getActiveWeekDay = this.getActiveWeekDay;
    return Stack(
      children: [
        const CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: SizedBox(
                height: 30,
              ),
            ),
            // SliverToBoxAdapter(
            //   child: Padding(
            //     padding: const EdgeInsets.symmetric(horizontal: 20),
            //     child: WorkActiveDay(
            //       activeWeekDay: getActiveWeekDay,
            //       tariff: widget.tariff,
            //       trainers: Map.fromEntries(
            //         (widget.tariff.trainers ?? [])
            //             .map((e) => MapEntry(e.id!, e)),
            //       ),
            //       chooseTime: chooseTime,
            //     ),
            //   ),
            // ),
            // SliverToBoxAdapter(
            //   child: Padding(
            //     padding: const EdgeInsets.only(
            //       left: 20,
            //       right: 20,
            //       top: 30,
            //     ),
            //     child: NotActiveWorkDay(
            //       getActiveWeekDay: getActiveWeekDay,
            //       chooseTime: chooseTime,
            //       workTimeObjTrainer: workTimeObjTrainer,
            //     ),
            //   ),
            // ),
            // SliverPadding(
            //   padding: const EdgeInsets.symmetric(horizontal: 20),
            //   sliver: SliverList(
            //     delegate: SliverChildBuilderDelegate(
            //       (context, index) {
            //         if (!index.isEven) {
            //           return const SizedBox(height: 10);
            //         }
            //         return Container(
            //           decoration: BoxDecoration(
            //             color: Colors.white,
            //             borderRadius: BorderRadius.circular(5),
            //             boxShadow: blocksMain,
            //           ),
            //           padding: const EdgeInsets.all(15),
            //           child: Row(
            //             children: [
            //               const Expanded(
            //                 child: H4Text(
            //                   'Понедельник',
            //                   color: ColorData.colorTextMain,
            //                 ),
            //               ),
            //               GestureDetector(
            //                 behavior: HitTestBehavior.opaque,
            //                 onTap: () => _openBottom(context),
            //                 child: const P2Text(
            //                   'Выбрать время',
            //                   color: ColorData.colorMainLink,
            //                 ),
            //               )
            //             ],
            //           ),
            //         );
            //       },
            //       childCount: math.max(0, 2 * 2 - 1),
            //     ),
            //   ),
            // ),
            SliverToBoxAdapter(child: SizedBox(height: 40)),
          ],
        ),
        Positioned(
          left: 0,
          right: 0,
          bottom: 0,
          child: BottomButton(
            title: 'Сохранить',
            active: false,
            onTap: () {},
          ),
        ),
      ],
    );
  }
}

// ignore: library_private_types_in_public_api
extension InitWorkTimeObjTrainer on _InnerShedulePageState {
  Map<int, List<TrainTime>> getActiveWeekDayF() {
    final activeTraining = this.activeTraining;
    final Map<int, List<TrainTime>> toReturn = {};
    final Map<String, bool> wasTime = {};

    for (final singelTrain in activeTraining) {
      final str = parseTime(singelTrain.date!).toString();
      if (wasTime[str] == true) continue;
      wasTime[str] = true;

      final weekday = (singelTrain.date!.weekday + 6) % 7;

      (toReturn[weekday] ??= []).add(singelTrain);
    }
    return toReturn;
  }

  Map<int, List<WorkTimeObjTrainer>> initWOrkTimeObj() {
    final Map<int, List<WorkTimeObjTrainer>> workTimeObj =
        Map.fromEntries(List.generate(7, (index) => MapEntry(index, [])));

    for (final MapEntry<String, List<WorkTimeObj>> singleEntry
        in _booking.workTimes?.entries ?? []) {
      for (final everyWorkTime in singleEntry.value) {
        if (everyWorkTime.date == null) continue;
        (workTimeObj[(everyWorkTime.date!.weekday + 6) % 7] ??= []).add(
          WorkTimeObjTrainer(
            date: everyWorkTime.date,
            max: everyWorkTime.max,
            now: everyWorkTime.now,
            trainer: trainerMap[singleEntry.key]!,
          ),
        );
      }
    }
    // ignore: avoid_function_literals_in_foreach_calls
    workTimeObj.values.forEach((element) {
      element.sort((a, b) {
        return a.date!.compareTo(b.date!);
      });
    });

    return workTimeObj;
  }
}
