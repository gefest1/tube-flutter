import 'dart:async';

// import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/blocs/participance/model/participance.model.dart';
import 'package:tube/logic/blocs/participance/participance_bloc.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/blocs/user/user_bloc.dart';
import 'package:tube/logic/model/status_enum.dart';
import 'package:tube/ui/admin/page/create_new_company_page/create_new_company_page.dart';
import 'package:tube/ui/admin/page/main_admin_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/little_button_card.dart';
import 'package:tube/ui/common/organisms/drawer_boxes.dart';
import 'package:tube/ui/common/page/empty_page.dart';
import 'package:tube/ui/common/page/iin_page_sheet.dart';
import 'package:tube/ui/common/templates/main_template.dart';
import 'package:tube/ui/trainer_page/components/pages/contact_data_trainer.dart';
import 'package:tube/ui/trainer_page/components/pages/trainer_worker_time.dart';
import 'package:tube/ui/trainer_page/main_page.dart';
import 'package:tube/utils/colors.dart';

class MainDrawer extends StatefulWidget {
  const MainDrawer({Key? key}) : super(key: key);

  @override
  State<MainDrawer> createState() => MainDrawerState();
}

class MainDrawerState extends State<MainDrawer> {
  late final userBloc = BlocProvider.of<UserBloc>(context, listen: true);
  static int? index;
  static BehaviorSubject<Participance?> currentParticipanceValue =
      BehaviorSubject<Participance?>.seeded(null);
  late final participanceBloc =
      BlocProvider.of<ParticipanceBloc>(context, listen: false);

  static String? get clubId => currentParticipance?.clubId;

  static Participance? get currentParticipance =>
      currentParticipanceValue.value;
  static set currentParticipance(Participance? part) {
    currentParticipanceValue.value = part;
  }

  void openPage(Participance participance) {
    final Widget newPage;
    if (participance.status == StatusEnum.admin) {
      final clubId = participance.club!.id!;

      newPage = MainAdminPage(
        clubId: clubId,
      );
    } else if (participance.status == StatusEnum.trainer) {
      newPage = const TrainerMainPage();
    } else if (participance.status == StatusEnum.waiting) {
      newPage = const EmptyPage();
    } else {
      newPage = MainTemplate(
        clubId: participance.clubId,
        status: participance.status,
      );
    }
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => newPage,
      ),
    );
  }

  Future<void> acceptParticipance({
    required Participance participance,
    String? iinData,
  }) async {
    final Completer completer = Completer();
    BlocProvider.of<ParticipanceBloc>(context, listen: false).add(
      AcceptParticipanceEvent(
        clubId: participance.clubId!,
        participance: participance.status!,
        accept: true,
        completer: completer,
        iin: iinData,
      ),
    );
    await completer.future;
  }

  @override
  Widget build(BuildContext context) {
    final state = userBloc.state;
    User? user;
    if (state is DataUserState) {
      user = state.user;
    }
    return Drawer(
      backgroundColor: Colors.white,
      child: Column(
        children: [
          Container(
            height: 56 + MediaQuery.of(context).padding.top,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.08),
                  blurRadius: 15,
                  offset: const Offset(2, 2),
                ),
              ],
            ),
            alignment: Alignment.bottomLeft,
            padding: const EdgeInsets.symmetric(
              vertical: 10,
              horizontal: 20,
            ),
            child: const H2Text(
              'Компании',
              color: ColorData.colorElementsActive,
            ),
          ),
          Expanded(
            child: BlocBuilder<ParticipanceBloc, ParticipanceState>(
              bloc: participanceBloc,
              builder: (context, state) {
                if (state is LoadingParticipanceState) {
                  return const Center(child: CircularProgressIndicator());
                }
                List<Participance> participances = [];

                if (state is DataParticipanceState) {
                  participances.addAll(state.participances);
                }

                if (participances.isNotEmpty) {
                  currentParticipance = participances[index ??= 0];
                  return DrawerBoxes(
                    active: index ??= 0,
                    participances: participances,
                    onTap: (__) {
                      setState(() {
                        index = __;
                      });
                      Navigator.pop(context);
                      openPage(participances[__]);
                    },
                    acceptTap: (Participance participance) async {
                      String? iinData;
                      if (participance.status == StatusEnum.participance) {
                        iinData = await showFlexibleBottomSheet<String>(
                          minHeight: 0,
                          initHeight: 0.95,
                          maxHeight: 0.95,
                          context: context,
                          anchors: [0.95],
                          builder: (ctx, controller, _) {
                            return Material(
                              color: Colors.transparent,
                              child: IinPageSheet(
                                participance: participance,
                                controller: controller,
                              ),
                            );
                          },
                        );
                        if (iinData == null) return;
                        await acceptParticipance(
                            participance: participance, iinData: iinData);
                        return;
                      }

                      if (participance.status == StatusEnum.trainer) {
                        await acceptParticipance(participance: participance);
                        if (!mounted) return;
                        await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => ContactDataTrainer(
                              clubId: participance.clubId!,
                            ),
                          ),
                        );
                        if (!mounted) return;
                        await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => TrainerWorkerTime(
                              clubId: participance.clubId!,
                            ),
                          ),
                        );
                      }
                      if (participance.status == StatusEnum.admin) {
                        await acceptParticipance(participance: participance);
                      }
                    },
                    declineTap: (Participance participance) {
                      BlocProvider.of<ParticipanceBloc>(context, listen: false)
                          .add(
                        AcceptParticipanceEvent(
                          clubId: participance.clubId!,
                          participance: participance.status!,
                          accept: false,
                        ),
                      );
                    },
                  );
                }
                return const Padding(
                  padding: EdgeInsets.all(20),
                  child: P0Text(
                    'Здесь будут компании, к которым у вас есть доступ. Пока таких нет.',
                    color: ColorData.colortTextSecondary,
                  ),
                );
              },
            ),
          ),
          // if (user?.isAdmin ?? false)
          Container(
            height: 120 + MediaQuery.of(context).padding.bottom,
            padding: const EdgeInsets.only(
              top: 20,
              left: 20,
              right: 20,
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  offset: const Offset(0, -4),
                  color: Colors.black.withOpacity(0.05),
                  blurRadius: 15,
                ),
              ],
            ),
            child: Column(
              children: [
                LittleButtonCard(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const CreateNewCompanyPage(),
                      ),
                    );
                  },
                  icon: const Icon(
                    Icons.add_circle_outline,
                    color: Colors.white,
                    size: 24,
                  ),
                  label: 'Создать компанию',
                  textColor: Colors.white,
                  backgroundColor: ColorData.colorElementsActive,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
