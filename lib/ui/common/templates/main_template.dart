import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tube/logic/blocs/participance/model/participance.model.dart';
import 'package:tube/logic/blocs/participance/participance_bloc.dart';
import 'package:tube/logic/model/status_enum.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/admin/page/main_admin_page.dart';
import 'package:tube/ui/common/page/client_body.dart';
import 'package:tube/ui/common/page/empty_page.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/main_page.dart';
import 'package:tube/ui/trainer_page/main_page.dart';
import 'package:tube/ui/waiting/waiting_page.dart';
import 'package:tube/utils/restart_widget.dart';

class MainTemplate extends StatefulWidget {
  final String? clubId;
  final StatusEnum? status;

  const MainTemplate({
    this.clubId,
    this.status,
    Key? key,
  }) : super(key: key);

  @override
  MainTemplateState createState() => MainTemplateState();
}

class MainTemplateState extends State<MainTemplate> {
  @override
  Widget build(BuildContext context) {
    assert(!((widget.status == null && widget.clubId != null) ||
        (widget.status != null && widget.clubId == null)));
    if (widget.status != null && widget.clubId != null) {
      StatusEnum status = widget.status!;
      String clubId = widget.clubId!;
      final Widget child;
      if (status == StatusEnum.participance) {
        child = MainPage(
          child: ClientBody(clubId: clubId),
        );
      } else if (status == StatusEnum.trainer) {
        child = const TrainerMainPage();
      } else if (status == StatusEnum.admin) {
        child = MainAdminPage(clubId: clubId);
      } else {
        child = const SizedBox();
      }

      return Scaffold(
        body: child,
        backgroundColor: Colors.white,
        drawer: const MainDrawer(),
      );
    }
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: const MainDrawer(),
      body: BlocBuilder<ParticipanceBloc, ParticipanceState>(
        builder: (context, state) {
          if (state is LoadingParticipanceState && state.prevState == null) {
            return const DecoratedBox(
              decoration: BoxDecoration(color: Colors.white),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
          if (state is LoadingParticipanceState) {
            state = state.prevState!;
          }
          final List<Participance> participances = [];
          final Map<String, Participance> mapParticipances = {};
          if (state is DataParticipanceState) {
            participances.addAll(state.participances);
            mapParticipances.addAll(state.mapParticipances);
          }
          if (participances.where((element) {
            return element.accepted ?? false;
          }).isEmpty) {
            return const MainPage(
              child: EmptyPage(),
            );
          }

          final StatusEnum status = participances.first.status!;
          final String clubId = participances.first.clubId!;
          MainDrawerState.currentParticipance ??= participances.first;
          if (status == StatusEnum.participance) {
            return MainPage(
              child: ClientBody(clubId: clubId),
            );
          }
          if (status == StatusEnum.trainer) {
            return const TrainerMainPage();
          }
          if (status == StatusEnum.admin) {
            return MainAdminPage(clubId: clubId);
          }
          if (status == StatusEnum.waiting) {
            return const EmptyPage();
          }
          return const SizedBox();
        },
      ),
    );
  }
}
