import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/const.dart';

class LittleButtonCard extends StatelessWidget {
  final Icon icon;
  final String label;
  final Color textColor;
  final Color? backgroundColor;
  final VoidCallback? onTap;

  const LittleButtonCard({
    required this.icon,
    required this.label,
    required this.textColor,
    this.backgroundColor,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          height: 61,
          decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: BorderRadius.circular(5),
          ),
          width: double.infinity,
          padding: const EdgeInsets.all(10),
          constraints: const BoxConstraints(minHeight: 44),
          alignment: Alignment.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              icon,
              const SizedBox(width: 5),
              Flexible(
                child: Text(
                  label,
                  style: TextStyle(
                    fontSize: P1TextStyle.fontSize,
                    fontWeight: P1TextStyle.fontWeight,
                    height: P1TextStyle.height,
                    color: textColor,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
