import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class BigEventCard extends StatelessWidget {
  final String title;
  final String subTitle;
  final VoidCallback? onTap;

  const BigEventCard({
    required this.title,
    required this.subTitle,
    this.onTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.only(top: 10, bottom: 15),
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  H4Text(
                    title,
                    color: ColorData.colorTextMain,
                  ),
                  const SizedBox(height: 5),
                  P1Text(
                    subTitle,
                    color: ColorData.colorTextMain,
                  ),
                ],
              ),
            ),
            const Icon(
              Icons.navigate_next,
              color: ColorData.colorTextMain,
            )
          ],
        ),
      ),
    );
  }
}
