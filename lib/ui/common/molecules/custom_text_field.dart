import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class CustomTextField extends StatelessWidget {
  final String hintText;
  final String title;
  final Widget? icon;
  final TextEditingController? controller;
  final void Function(String)? onSubmitted;
  final TextInputType? keyboardType;
  final EdgeInsets scrollPadding;
  final int? maxLength;
  final List<TextInputFormatter>? inputFormatters;
  final InputCounterWidgetBuilder? buildCounter;
  final BoxConstraints? prefixIconConstraints;
  final int? maxLines;
  final int? hintMaxLines;
  final int? minLines;

  const CustomTextField({
    required this.hintText,
    required this.title,
    this.icon,
    this.controller,
    this.onSubmitted,
    this.keyboardType,
    this.scrollPadding = const EdgeInsets.all(20.0),
    this.maxLength,
    this.inputFormatters,
    this.buildCounter,
    this.prefixIconConstraints = const BoxConstraints(
      maxHeight: 24,
      maxWidth: 29,
      minHeight: 24,
      minWidth: 29,
    ),
    this.maxLines,
    this.minLines,
    this.hintMaxLines,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextField(
          inputFormatters: inputFormatters,
          scrollPadding: scrollPadding,
          keyboardType: keyboardType,
          onSubmitted: onSubmitted,
          controller: controller,
          cursorColor: Colors.black,
          style: const TextStyle(
            color: Colors.black,
            fontSize: P0TextStyle.fontSize,
            height: P0TextStyle.height,
            fontWeight: P0TextStyle.fontWeight,
          ),
          onTapOutside: (event) =>
              FocusManager.instance.primaryFocus?.unfocus(),
          maxLines: maxLines,
          minLines: minLines,
          maxLength: maxLength,
          buildCounter: buildCounter,
          decoration: InputDecoration(
            hintMaxLines: hintMaxLines,
            hintText: hintText,
            prefixIconConstraints: prefixIconConstraints,
            prefixIcon: icon == null
                ? null
                : Padding(
                    padding: const EdgeInsets.only(right: 5.0),
                    child: icon,
                  ),
            hintStyle: const TextStyle(
              color: Color(0xff969696),
              fontSize: 18,
              height: 21 / 18,
              fontWeight: FontWeight.w400,
            ),
            enabledBorder: const UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.black,
                width: 0.5,
              ),
            ),
            focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.black,
                width: 0.5,
              ),
            ),
          ),
        ),
        const SizedBox(height: 5),
        P1Text(
          title,
          color: ColorData.colorElementsActive,
        ),
      ],
    );
  }
}
