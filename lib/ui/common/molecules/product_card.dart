import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class ProductCard extends StatelessWidget {
  final VoidCallback onTap;
  final String title;
  final String subTitle;

  const ProductCard({
    Key? key,
    required this.title,
    required this.subTitle,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: SizeTapAnimation(
        child: Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            color: ColorData.colorGrayishBackground,
          ),
          padding: const EdgeInsets.all(15),
          child: const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Записаться на массаж',
                style: TextStyle(
                  fontSize: 18,
                  height: 21 / 18,
                  fontWeight: FontWeight.w400,
                  color: ColorData.colorButtonMain,
                ),
              ),
              SizedBox(height: 15),
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Icon(
                      Icons.add_to_photos_rounded,
                      size: 32,
                      color: ColorData.colorElementsActive,
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: Text(
                          '5000 тг',
                          style: TextStyle(
                            fontSize: 18,
                            height: 21 / 18,
                            fontWeight: FontWeight.w400,
                            color: ColorData.colorButtonMain,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
