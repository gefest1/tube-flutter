import 'package:flutter/material.dart';
import 'package:tube/utils/const.dart';

class SearchTextField extends StatefulWidget {
  final TextEditingController? controller;

  const SearchTextField({this.controller, super.key});

  @override
  State<SearchTextField> createState() => _SearchTextFieldState();
}

class _SearchTextFieldState extends State<SearchTextField> {
  final FocusNode _focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: const Color(0xff303030),
      ),
      width: double.infinity,
      height: 36,
      child: Row(
        children: [
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              _focusNode.requestFocus();
            },
            child: const Padding(
              padding: EdgeInsets.only(
                left: 10,
                top: 8,
                bottom: 8,
                right: 6,
              ),
              child: Icon(
                Icons.search,
                color: Color(0xff969696),
              ),
            ),
          ),
          Expanded(
            child: TextField(
              controller: widget.controller,
              focusNode: _focusNode,
              maxLines: 1,
              style: const TextStyle(
                fontSize: P1TextStyle.fontSize,
                height: P1TextStyle.height,
                fontWeight: P1TextStyle.fontWeight,
              ),
              decoration: const InputDecoration(
                border: InputBorder.none,
                hintStyle: TextStyle(
                  fontSize: P1TextStyle.fontSize,
                  height: P1TextStyle.height,
                  fontWeight: P1TextStyle.fontWeight,
                ),
                contentPadding: EdgeInsets.only(bottom: 10),
                hintText: 'Искать по имени',
              ),
            ),
          ),
        ],
      ),
    );
  }
}
