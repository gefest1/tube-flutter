import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class CustomToggleButton extends StatefulWidget {
  final EdgeInsetsGeometry margin;
  final ValueNotifier<int?> valueNotifier;
  final List<String> values;

  const CustomToggleButton({
    this.margin = EdgeInsets.zero,
    required this.valueNotifier,
    required this.values,
    super.key,
  });

  @override
  State<CustomToggleButton> createState() => _CustomToggleButtonState();
}

class _CustomToggleButtonState extends State<CustomToggleButton> {
  @override
  Widget build(BuildContext context) {
    if (widget.values.isEmpty) return const SizedBox();
    return Padding(
      padding: widget.margin,
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            width: 0.5,
            color: Colors.white,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Row(
              children: List.generate(
            widget.values.length * 2 - 1,
            (index) => index.isOdd
                ? const VerticalDivider(
                    width: 0,
                    thickness: 0.5,
                    color: Colors.white,
                  )
                : Expanded(
                    child: ValueListenableBuilder(
                      valueListenable: widget.valueNotifier,
                      builder: (context, value, _) {
                        return DecoratedBox(
                          decoration: BoxDecoration(
                            color: value == index ~/ 2
                                ? Colors.white
                                : Colors.transparent,
                          ),
                          child: SizeTapAnimation(
                            onTap: () {
                              widget.valueNotifier.value = index ~/ 2;
                            },
                            child: Container(
                              alignment: Alignment.center,
                              padding: const EdgeInsets.symmetric(vertical: 8),
                              child: P1Text(
                                widget.values[index ~/ 2],
                                color: value == index ~/ 2
                                    ? ColorData.colorMainElements
                                    : ColorData.colorMainLink,
                                style: TextStyle(
                                  fontWeight: value == index ~/ 2
                                      ? FontWeight.w700
                                      : FontWeight.w400,
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
          )

              // [
              //   Expanded(
              //     child: Container(
              //       alignment: Alignment.center,
              //       padding: const EdgeInsets.symmetric(vertical: 8),
              //       child: P1Text(
              //         'Физ. лицо',
              //       ),
              //     ),
              //   ),
              //   Expanded(
              //     child: Container(
              //       alignment: Alignment.center,
              //       padding: const EdgeInsets.symmetric(vertical: 8),
              //       child: P1Text('Юр. лицо'),
              //     ),
              //   ),
              // ],
              ),
        ),
      ),
    );
  }
}
