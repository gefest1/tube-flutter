import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/utils/colors.dart';
import 'dart:math' as math;

import 'package:tube/utils/const.dart';

class BottomButton extends StatelessWidget {
  final VoidCallback? onTap;
  final String title;
  final bool active;

  final String? subTitle;
  final TextSpan? textSpan;

  const BottomButton({
    this.onTap,
    this.title = 'Продолжить',
    this.active = true,
    this.subTitle,
    this.textSpan,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
        bottom: math.max(MediaQuery.of(context).padding.bottom, 10),
        top: 10,
      ),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(2),
        ),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -4),
            blurRadius: 15,
            color: Color(0x0d000000),
          )
        ],
      ),
      child: Column(
        children: [
          CustomButton(
            onTap: active ? onTap : null,
            title: title,
            titleColor: active ? Colors.white : ColorData.colortTextSecondary,
            color: active
                ? ColorData.colorMainLink
                : ColorData.colorElementsLightMenu,
          ),
          const SizedBox(height: 10),
          if (textSpan != null)
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: const TextStyle(
                  fontSize: P2TextStyle.fontSize,
                  fontWeight: P2TextStyle.fontWeight,
                  height: P2TextStyle.height,
                  color: ColorData.colorTextMain,
                ),
                children: [textSpan!],
              ),
            ),
          if (subTitle != null)
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: P2Text(
                subTitle!,
                color: ColorData.colorTextMain,
                textAlign: TextAlign.center,
              ),
            ),
        ],
      ),
    );
  }
}
