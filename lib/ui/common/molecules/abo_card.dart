import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/ui/common/atoms/stop_card.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class LittleBodyCard extends StatelessWidget {
  final Icon icon;
  final String label;
  final Color textColor;

  const LittleBodyCard({
    required this.icon,
    required this.label,
    required this.textColor,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: const Color(0xffF3F4F6),
        borderRadius: BorderRadius.circular(10),
      ),
      width: double.infinity,
      padding: const EdgeInsets.all(10),
      constraints: const BoxConstraints(minHeight: 44),
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          icon,
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: AutoSizeText(
                label,
                maxFontSize: 16,
                minFontSize: 8,
                presetFontSizes: const [16, 14, 10, 8],
                style: TextStyle(
                  color: textColor,
                ),
                maxLines: 1,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class AbpoCard extends StatelessWidget {
  final String? userTitle;
  final VoidCallback? onTap;
  final int activeTrains;
  final String? title;
  final String? dayTitle;
  final String? photoUrl;
  final double? width;
  final bool isNew;
  final DateTime? stopTime;

  const AbpoCard({
    this.userTitle,
    this.activeTrains = 0,
    this.onTap,
    this.dayTitle,
    this.title,
    this.photoUrl,
    this.width,
    this.isNew = false,
    this.stopTime,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget? stopCard;
    if (stopTime != null) {
      stopCard = StopCard(
        stopTime: stopTime!,
      );
    }
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        width: width,
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              offset: const Offset(2, 2),
              color: Colors.black.withOpacity(0.08),
              blurRadius: 15,
            )
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (title != null)
              H3Text(
                title!,
                color: Colors.black,
              ),
            const SizedBox(height: 10),
            Expanded(
              child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Владелец',
                        style: TextStyle(
                          fontSize: 12,
                          color: ColorData.colortTextSecondary,
                        ),
                      ),
                      const SizedBox(height: 5),
                      Flexible(
                        child: Container(
                          decoration: BoxDecoration(
                            color: const Color(0xffF3F4F6),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          constraints:
                              const BoxConstraints(minHeight: 98, maxWidth: 95),
                          padding: const EdgeInsets.symmetric(
                            vertical: 12,
                          ),
                          alignment: Alignment.center,
                          child: Column(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 30),
                                child: CircleAvatar(
                                  maxRadius: 18,
                                  backgroundColor: Colors.transparent,
                                  foregroundImage: photoUrl == null
                                      ? null
                                      : PCacheImage(
                                          photoUrl!,
                                          enableInMemory: true,
                                        ),
                                  child: const Icon(
                                    Icons.face,
                                    color: ColorData.colorElementsActive,
                                    size: 33,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 3),
                              if (userTitle != null)
                                Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: AutoSizeText(
                                      userTitle!,
                                      presetFontSizes: const [12, 10, 8],
                                      style: const TextStyle(
                                        color: Color(0xff141414),
                                      ),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                    // Text(
                                    //   userTitle!,
                                    //   style: const TextStyle(
                                    //     color: Color(0xff141414),
                                    //     fontSize: 12,
                                    //   ),
                                    //   maxLines: 2,
                                    //   textAlign: TextAlign.center,
                                    // ),
                                  ),
                                )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Ближайшее занятие',
                            style: TextStyle(
                              fontSize: 12,
                              color: ColorData.colortTextSecondary,
                            ),
                          ),
                          const SizedBox(height: 5),
                          if (dayTitle != null)
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.only(bottom: 5.0),
                                child: stopCard ??
                                    LittleBodyCard(
                                      label:
                                          isNew ? 'Выставьте время' : dayTitle!,
                                      textColor: isNew
                                          ? ColorData.colorMainLink
                                          : ColorData.colorElementsActive,
                                      icon: Icon(
                                        Icons.event,
                                        color: isNew
                                            ? ColorData.colorMainLink
                                            : ColorData.colorElementsActive,
                                        size: 24,
                                      ),
                                    ),
                              ),
                            ),
                          Flexible(
                            child: LittleBodyCard(
                              label: '$activeTrains занятий осталось',
                              textColor: const Color(0xff00CA2C),
                              icon: const Icon(
                                Icons.bubble_chart,
                                color: Color(0xff00CA2C),
                                size: 24,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
