import 'package:flutter/material.dart';
import 'package:tube/utils/infinite_listview.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

import 'package:tube/utils/is_same_day.dart';

const monthName = [
  "Январь",
  "Февраль",
  "Март",
  "Апрель",
  "Май",
  "Июнь",
  "Июль",
  "Август",
  "Сентябрь",
  "Октябрь",
  "Ноябрь",
  "Декабрь",
];
const weeksShort = [
  "Пон",
  "Вт",
  "Ср",
  "Чт",
  "Пт",
  "Сб",
  "Вс",
];

class SheduleTable extends StatefulWidget {
  final InfiniteScrollController controller;
  final BehaviorSubject<int> lastIndexSubject;

  final EdgeInsets horizontalPadding;
  final void Function(DateTime date)? onTap;
  final Map<String, Set<String>> dateTimeMapWidget;
  final BehaviorSubject<DateTime?> dateTimeSubject;
  final EdgeInsetsGeometry padding;
  final bool Function(DateTime time)? validation;

  const SheduleTable({
    required this.controller,
    required this.lastIndexSubject,
    this.onTap,
    this.horizontalPadding = EdgeInsets.zero,
    this.dateTimeMapWidget = const {},
    required this.dateTimeSubject,
    this.padding = EdgeInsets.zero,
    this.validation,
    Key? key,
  }) : super(key: key);

  @override
  State<SheduleTable> createState() => _SheduleTableState();
}

class _SheduleTableState extends State<SheduleTable> {
  final currentYear = DateTime.now().year;
  final currentMonth = DateTime.now().month;
  double maxWidth = 1;

  void _listen() {
    final currentInd = (widget.controller.position.pixels / maxWidth).round();
    if (widget.lastIndexSubject.value != currentInd) {
      widget.lastIndexSubject.value = currentInd;
    }
  }

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(_listen);

    Future(() {
      Future(() {
        widget.controller.jumpTo(widget.lastIndexSubject.value * maxWidth);
      });
    });
  }

  @override
  void dispose() {
    widget.controller.removeListener(_listen);
    super.dispose();
  }

  DateTime? get parentDateTime => widget.dateTimeSubject.value;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding,
      child: Column(
        children: [
          Padding(
            padding: widget.horizontalPadding,
            child: Row(
              children: List.generate(
                weeksShort.length,
                (index) => Expanded(
                  child: Center(
                    child: H6Text(
                      weeksShort[index],
                      color: ColorData.colortTextSecondary,
                    ),
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 20),
          StreamBuilder(
            stream: widget.dateTimeSubject,
            builder: (context, _) {
              return SizedBox(
                height: 302,
                child: LayoutBuilder(
                  builder: (context, box) {
                    maxWidth = box.maxWidth;
                    return InfiniteListView.builder(
                      controller: widget.controller,
                      physics: const PageScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (ctx, index) {
                        final currentStartTime =
                            DateTime(currentYear, currentMonth + index);
                        final currentEndTime =
                            DateTime(currentYear, currentMonth + index + 1);
                        return SizedBox(
                          height: 302,
                          width: box.maxWidth,
                          child: Padding(
                            padding: widget.horizontalPadding,
                            child: Column(
                              children: List.generate(
                                6 * 2 - 1,
                                (ind) {
                                  if (ind.isOdd) {
                                    return const SizedBox(height: 10);
                                  }
                                  final index = ind ~/ 2;
                                  return Row(
                                    children: List.generate(
                                      7,
                                      (subIndex) {
                                        final currentDay = 1 +
                                            index * 7 +
                                            subIndex -
                                            ((currentStartTime.weekday + 6) %
                                                7);
                                        final lastDay = currentEndTime
                                            .difference(currentStartTime)
                                            .inDays;

                                        if (!(currentDay > 0 &&
                                            currentDay <= lastDay)) {
                                          return const Expanded(
                                            child: SizedBox(height: 42),
                                          );
                                        }
                                        final subDay = DateTime(
                                          currentStartTime.year,
                                          currentStartTime.month,
                                          currentDay,
                                        );
                                        final subWidget =
                                            widget.dateTimeMapWidget[
                                                subDay.toIso8601String()];
                                        final bool choosen =
                                            isSameDay(parentDateTime, subDay);
                                        bool isValid = true;
                                        if (widget.validation != null) {
                                          isValid = !widget.validation!(
                                            DateTime(
                                              currentStartTime.year,
                                              currentStartTime.month,
                                              currentDay,
                                            ),
                                          );
                                        }
                                        return Expanded(
                                          child: GestureDetector(
                                            behavior: HitTestBehavior.opaque,
                                            onTap: !isValid
                                                ? null
                                                : () => widget.onTap?.call(
                                                      DateTime(
                                                        currentStartTime.year,
                                                        currentStartTime.month,
                                                        currentDay,
                                                      ),
                                                    ),
                                            child: SizedBox(
                                              height: 42,
                                              child: Center(
                                                child: Container(
                                                  // height: 42,
                                                  width: 32,
                                                  // margin: EdgeInsets.only(),
                                                  constraints:
                                                      const BoxConstraints(
                                                    maxWidth: 32,
                                                  ),
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    color: !isValid
                                                        ? Colors.transparent
                                                        : choosen
                                                            ? ColorData
                                                                .colorMainLink
                                                            : Colors.white,
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      const SizedBox(height: 5),
                                                      P0Text(
                                                        currentDay.toString(),
                                                        color: choosen
                                                            ? Colors.white
                                                            : ColorData
                                                                .colorMainBlack,
                                                      ),
                                                      if (subWidget != null)
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .only(top: 2),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: [
                                                              // CustomPaint(
                                                              //   painter:
                                                              //       ServiceIconCustomPainter(),
                                                              //   size: Size(8, 8),
                                                              // ),
                                                              // SizedBox(width: 2),
                                                              if (subWidget
                                                                  .contains(
                                                                      'booking'))
                                                                Icon(
                                                                  Icons
                                                                      .fiber_smart_record,
                                                                  size: 8,
                                                                  color: choosen
                                                                      ? Colors
                                                                          .white
                                                                      : ColorData
                                                                          .abonement,
                                                                ),
                                                              // SizedBox(width: 2),
                                                              // Icon(
                                                              //   Icons.today,
                                                              //   size: 8,
                                                              //   color:
                                                              //       ColorData.event,
                                                              // ),
                                                            ],
                                                          ),
                                                        ),
                                                      //  const [
                                                      //   CustomPaint(
                                                      //     painter:
                                                      //         ServiceIconCustomPainter(),
                                                      //     size: Size(8, 8),
                                                      //   ),
                                                      //   SizedBox(width: 2),
                                                      //   Icon(
                                                      //     Icons.fiber_smart_record,
                                                      //     size: 8,
                                                      //     color: ColorData.abonement,
                                                      //   ),
                                                      //   SizedBox(width: 2),
                                                      //   Icon(
                                                      //     Icons.today,
                                                      //     size: 8,
                                                      //     color: ColorData.event,
                                                      //   ),
                                                      // ],
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  },
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
