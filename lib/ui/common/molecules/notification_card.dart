import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/logic/model/photourl.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_time_shift.dart';

class NotificationCard extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final PhotoUrl? photoUrl;
  final String? description;
  final EdgeInsets padding;
  final DateTime? dateTime;

  const NotificationCard({
    this.title,
    this.subTitle,
    this.photoUrl,
    this.description,
    this.padding = const EdgeInsets.symmetric(
      horizontal: 20,
      vertical: 15,
    ),
    this.dateTime,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: padding,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 32,
                    width: 32,
                    margin: const EdgeInsets.only(right: 6.0),
                    decoration: const ShapeDecoration(
                      shape: CircleBorder(),
                      color: ColorData.color3Percent,
                    ),
                    alignment: Alignment.center,
                    child: Stack(
                      children: [
                        const Center(
                          child: Icon(
                            Icons.face,
                            color: ColorData.colorElementsActive,
                          ),
                        ),
                        if (photoUrl?.thumbnail != null)
                          SizedBox(
                            height: double.infinity,
                            width: double.infinity,
                            child: ClipOval(
                              child: Image(
                                image: PCacheImage(
                                  photoUrl!.thumbnail!,
                                  enableInMemory: true,
                                ),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        if (photoUrl?.m != null)
                          SizedBox(
                            height: double.infinity,
                            width: double.infinity,
                            child: ClipOval(
                              child: Image(
                                image: PCacheImage(
                                  photoUrl!.m!,
                                  enableInMemory: true,
                                ),
                                fit: BoxFit.cover,
                              ),
                            ),
                          )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (title != null)
                          H4Text(
                            title!,
                            color: ColorData.colorTextMain,
                          ),
                        if (subTitle != null)
                          P2Text(
                            subTitle!,
                            color: ColorData.colortTextSecondary,
                          )
                      ],
                    ),
                  ),
                  if (dateTime != null)
                    Column(
                      children: [
                        P2Text(
                          DateFormat("dd.MM.yyyy\nHH:mm").format(dateTime!.parseTimeGG),
                          textAlign: TextAlign.end,
                          color: ColorData.colortTextSecondary,
                        ),
                      ],
                    )
                ],
              ),
              if (description != null)
                Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: P1Text(
                    description!,
                    color: ColorData.colorTextMain,
                  ),
                )
            ],
          ),
        ),
        Divider(
          endIndent: padding.right,
          indent: padding.left,
          height: 0,
          color: ColorData.color5Percent,
          thickness: 0.5,
        ),
      ],
    );
  }
}
