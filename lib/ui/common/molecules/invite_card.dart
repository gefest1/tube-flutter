import 'package:flutter/material.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/ui/common/atoms/small_button.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class InviteCard extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final String? networkImage;
  final VoidCallback? acceptTap;
  final VoidCallback? declineTap;

  const InviteCard({
    this.title,
    this.subTitle,
    this.networkImage,
    this.acceptTap,
    this.declineTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(2),
        border: Border.all(
          color: const Color(0x1a000000),
          width: 0.5,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (networkImage != null) ...[
                Container(
                  height: 40,
                  width: 40,
                  alignment: Alignment.center,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(6),
                    child: Image(
                      image: PCacheImage(
                        networkImage!,
                        enableInMemory: true,
                      ),
                      height: 42,
                      width: 42,
                    ),
                  ),
                ),
                const SizedBox(width: 10),
              ],
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (title != null)
                      H3Text(
                        title!,
                        color: ColorData.colorElementsActive,
                      ),
                    if (subTitle != null)
                      P2Text(
                        subTitle!,
                        color: const Color(0xff9C9C9C),
                      ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Row(
            children: [
              Expanded(
                child: SmallButton(
                  onTap: acceptTap,
                  child: const Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        Icons.check,
                        color: ColorData.colorMainLink,
                        size: 16,
                      ),
                      SizedBox(width: 5),
                      Flexible(
                        child: P2Text(
                          'Принять',
                          color: ColorData.colorMainLink,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(width: 5),
              Expanded(
                child: SmallButton(
                  onTap: declineTap,
                  child: const Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        Icons.close,
                        color: ColorData.colorMainNo,
                        size: 16,
                      ),
                      SizedBox(width: 5),
                      Flexible(
                        child: P2Text(
                          'Отклонить',
                          color: ColorData.colorMainNo,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
