import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class CardsCenterShare extends StatelessWidget {
  final VoidCallback? emailTap;
  final VoidCallback? shareLinkTap;

  const CardsCenterShare({
    this.emailTap,
    this.shareLinkTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizeTapAnimation(
          child: GestureDetector(
            onTap: shareLinkTap,
            child: Container(
              height: 56,
              decoration: BoxDecoration(
                color: ColorData.colorMainLink,
                borderRadius: BorderRadius.circular(5),
                boxShadow: const [
                  BoxShadow(
                    offset: Offset(2, 2),
                    blurRadius: 5,
                    color: Color(0x26000000),
                  )
                ],
              ),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.share),
                  SizedBox(width: 10),
                  Flexible(
                    child: Text(
                      'Поделиться ссылкой',
                      style: TextStyle(
                        fontSize: P1TextStyle.fontSize,
                        fontWeight: P1TextStyle.fontWeight,
                        height: P1TextStyle.height,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        const SizedBox(height: 10),
        SizeTapAnimation(
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: emailTap,
            child: const SizedBox(
              height: 56,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.mail,
                    color: ColorData.colorMainLink,
                  ),
                  SizedBox(width: 10),
                  Flexible(
                    child: P1Text(
                      'Добавить по электронной почте',
                      color: ColorData.colorMainLink,
                    ),
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
