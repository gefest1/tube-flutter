import 'package:flutter/material.dart';
import 'package:tube/utils/colors.dart';

class FrameCard extends StatelessWidget {
  final Widget child;
  const FrameCard({required this.child, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      decoration: const BoxDecoration(
        color: ColorData.colorGrayishBackground,
        borderRadius: BorderRadius.all(
          Radius.circular(6),
        ),
      ),
      alignment: Alignment.center,
      child: child,
    );
  }
}
