import 'package:flutter/material.dart';

class MainBottom extends StatefulWidget {
  final TabController tabController;
  final int tabIndex;

  const MainBottom({
    super.key,
    required this.tabController,
    required this.tabIndex,
  });

  @override
  State<MainBottom> createState() => _MainBottomState();
}

class _MainBottomState extends State<MainBottom> {
  @override
  Widget build(BuildContext context) {
    final bottomPadding = MediaQuery.of(context).padding.bottom;
    return Material(
      child: DecoratedBox(
        decoration: const BoxDecoration(
          boxShadow: [
            BoxShadow(
              blurRadius: 0,
              offset: Offset(0, -0.5),
              color: Color(0x0d000000),
            ),
          ],
        ),
        child: ClipRRect(
          child: Container(
            height: 60 + bottomPadding,
            padding: EdgeInsets.only(bottom: bottomPadding),
            decoration: const BoxDecoration(
              color: Colors.white,
            ),
            child: TabBar(
              controller: widget.tabController,
              labelColor: const Color(0xff404040),
              unselectedLabelColor: const Color(0xff969696),
              indicatorColor: Colors.transparent,
              tabs: [
                Tab(
                  icon: Icon(
                    Icons.dashboard,
                    size: 24,
                    color: widget.tabIndex == 0
                        ? const Color(0xff404040)
                        : const Color(0xff969696),
                  ),
                  iconMargin: const EdgeInsets.only(top: 10),
                  text: 'Главная',
                ),
                Tab(
                  icon: Icon(
                    Icons.notifications,
                    size: 24,
                    color: widget.tabIndex == 1
                        ? const Color(0xff404040)
                        : const Color(0xff969696),
                  ),
                  iconMargin: const EdgeInsets.only(top: 10),
                  text: 'Уведомления',
                ),
                Tab(
                  icon: Icon(
                    Icons.supervised_user_circle,
                    size: 24,
                    color: widget.tabIndex == 2
                        ? const Color(0xff404040)
                        : const Color(0xff969696),
                  ),
                  iconMargin: const EdgeInsets.only(top: 10),
                  text: 'Профиль',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
