// ignore: unused_import
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tube/logic/blocs/notification/model/notification.model.dart';
import 'package:tube/logic/blocs/notification/model/notify_type.model.dart';

import 'package:tube/ui/common/molecules/notification_card.dart';
import 'package:tube/utils/get_date_time_object_id.dart';
import 'package:tube/utils/get_time_shift.dart';

class NotificationWrap extends StatelessWidget {
  final NotificationEntity notification;

  const NotificationWrap({required this.notification, super.key});

  String? getTitle() {
    if (NotificationType.boughtBookingTrainer == notification.type) {
      return notification.club?.fullName;
    }
    if (NotificationType.joiningTrainer == notification.type) {
      return notification.club?.fullName;
    }
    if (NotificationType.joiningParticipance == notification.type) {
      return notification.club?.fullName;
    }
    if (NotificationType.boughtBookingParticipance == notification.type) {
      return notification.club?.fullName;
    }
    if (NotificationType.inviteParticipance == notification.type) {
      return notification.club?.fullName;
    }
    if (NotificationType.createCompanyAdmin == notification.type) {
      return notification.club?.fullName;
    }
    if (NotificationType.createBookingAdmin == notification.type) {
      return notification.club?.fullName;
    }
    if (NotificationType.createServiceAdmin == notification.type) return null;
    if (NotificationType.createEventAdmin == notification.type) return null;
    if (NotificationType.deleteBookingAdmin == notification.type) return null;
    if (NotificationType.deleteServiceAdmin == notification.type) return null;
    if (NotificationType.deleteEventAdmin == notification.type) return null;
    if (NotificationType.boughtBookingAdmin == notification.type) {
      return notification.club?.fullName;
    }
    if (NotificationType.boughtServiceAdmin == notification.type) return null;
    if (NotificationType.joiningCompanyAdmin == notification.type) {
      return notification.club?.fullName;
    }
    return null;
  }

  String? getSubTitle() {
    if (NotificationType.boughtBookingTrainer == notification.type) {
      return notification.club?.description;
    }
    if (NotificationType.joiningTrainer == notification.type) {
      return notification.club?.description;
    }
    if (NotificationType.joiningParticipance == notification.type) {
      return notification.club?.description;
    }
    if (NotificationType.boughtBookingParticipance == notification.type) {
      return notification.club?.description;
    }
    if (NotificationType.inviteParticipance == notification.type) {
      return notification.club?.description;
    }
    if (NotificationType.createCompanyAdmin == notification.type) {
      return notification.club?.description;
    }
    if (NotificationType.createBookingAdmin == notification.type) {
      return notification.club?.description;
    }
    if (NotificationType.createServiceAdmin == notification.type) return null;
    if (NotificationType.createEventAdmin == notification.type) return null;
    if (NotificationType.deleteBookingAdmin == notification.type) return null;
    if (NotificationType.deleteServiceAdmin == notification.type) return null;
    if (NotificationType.deleteEventAdmin == notification.type) return null;
    if (NotificationType.boughtBookingAdmin == notification.type) {
      return "Администратор";
    }
    if (NotificationType.boughtServiceAdmin == notification.type) return null;
    if (NotificationType.joiningCompanyAdmin == notification.type) {
      return notification.club?.description;
    }
    return null;
  }

  String? getDescription() {
    if (NotificationType.boughtBookingTrainer == notification.type) {
      notification.user?.fullName;
      return 'Новая запись. ${notification.user?.fullName ?? notification.user?.email ?? ""} — абонемент «${notification.booking?.name ?? ''}».';
    }
    if (NotificationType.joiningTrainer == notification.type) {
      return 'Добро пожаловать в ${notification.club?.fullName ?? ""}!';
    }
    if (NotificationType.joiningParticipance == notification.type) {
      return 'Добро пожаловать в ${notification.club?.fullName ?? ""}!';
    }
    if (NotificationType.boughtBookingParticipance == notification.type) {
      final startDate = notification.tariff?.id == null
          ? null
          : DateFormat("dd.MM.yyyy")
              .format(getDateTimefromObjectId(notification.tariff!.id!).parseTimeGG);

      return 'Вы оплатили абонемент ${notification.booking?.name ?? ""}. Он действует c $startDate. Осталось ${notification.booking?.count ?? ''} уроков.';
    }
    if (NotificationType.inviteParticipance == notification.type) {
      return 'Добро пожаловать в ${notification.club?.fullName ?? ""}!';
    }
    if (NotificationType.createCompanyAdmin == notification.type) {
      return "Компания «${notification.club?.fullName ?? ''}» создана! Создайте абонементы и услуги, назначьте сотрудников и принимайте клиентов";
    }
    if (NotificationType.createBookingAdmin == notification.type) {
      return "Создан абонемент «${notification.booking?.name}»";
    }
    if (NotificationType.createServiceAdmin == notification.type) return null;
    if (NotificationType.createEventAdmin == notification.type) return null;
    if (NotificationType.deleteBookingAdmin == notification.type) return null;
    if (NotificationType.deleteServiceAdmin == notification.type) return null;
    if (NotificationType.deleteEventAdmin == notification.type) return null;
    if (NotificationType.boughtBookingAdmin == notification.type) {
      final clientName = notification.user?.fullName ?? notification.user?.email ?? '';
      final bookingName = notification.booking?.name ?? '';
      final startDate = notification.tariff?.id == null
          ? null
          : DateFormat("dd.MM.yyyy")
              .format(getDateTimefromObjectId(notification.tariff!.id!).parseTimeGG);

      return "Клиент $clientName — покупка абонемента «$bookingName». ${notification.booking?.count ?? ''} посещений. Действует c $startDate";
    }
    if (NotificationType.boughtServiceAdmin == notification.type) return null;
    if (NotificationType.joiningCompanyAdmin == notification.type) {
      return 'Добро пожаловать в ${notification.club?.fullName ?? ""}!';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return NotificationCard(
      dateTime: notification.date,
      title: getTitle(),
      subTitle: getSubTitle(),
      description: getDescription(),
      photoUrl: notification.avatar,
    );
  }
}
