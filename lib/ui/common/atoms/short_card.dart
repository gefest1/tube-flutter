import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class ShortGrayCard extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final VoidCallback? onTap;

  const ShortGrayCard({
    this.title,
    this.subTitle,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: const Color(0xffF3F4F6),
          borderRadius: BorderRadius.circular(6),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (title != null)
                  Text(
                    title!,
                    style: const TextStyle(
                      fontSize: H4TextStyle.fontSize,
                      height: H4TextStyle.height,
                      fontWeight: H4TextStyle.fontWeight,
                      color: ColorData.colorTextMain,
                    ),
                  ),
                if (subTitle != null)
                  Text(
                    subTitle!,
                    style: const TextStyle(
                      fontSize: P2TextStyle.fontSize,
                      height: P2TextStyle.height,
                      fontWeight: P2TextStyle.fontWeight,
                      color: ColorData.colorElementsSecondary,
                    ),
                  ),
              ],
            ),
            const SizedBox(height: 10),
            const Icon(
              Icons.arrow_forward,
              size: 24,
              color: ColorData.colorElementsActive,
            )
          ],
        ),
      ),
    );
  }
}
