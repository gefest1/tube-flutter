import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/logic/blocs/user/user_bloc.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';

class UserTitle extends StatelessWidget {
  const UserTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        if (state is DataUserState) {
          final user = state.user;
          return GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              Scaffold.of(context).openDrawer();
            },
            child: Row(
              children: [
                const SizedBox(width: 20),
                if (user.photoUrl?.thumbnail != null)
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: CircleAvatar(
                      radius: 16,
                      backgroundColor: Colors.grey[600],
                      backgroundImage: PCacheImage(
                        user.photoUrl!.thumbnail!,
                        enableInMemory: true,
                      ),
                    ),
                  ),
                if (user.email != null)
                  Expanded(
                    child: H3Text(
                      user.email!,
                      color: Colors.white,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                const SizedBox(width: 20),
              ],
            ),
          );
        }
        return const SizedBox();
      },
    );
  }
}
