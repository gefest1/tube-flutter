import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tube/logic/blocs/participance/participance_bloc.dart';
import 'package:tube/utils/colors.dart';

class MenuDrawer extends StatelessWidget {
  final BuildContext? parentContext;
  const MenuDrawer({this.parentContext, super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 56,
      width: 56,
      child: Stack(
        children: [
          IconButton(
            onPressed: () => Scaffold.of(parentContext ?? context).openDrawer(),
            icon: const Icon(
              Icons.menu,
              size: 33,
            ),
            iconSize: 33,
            color: Colors.white,
          ),
          BlocBuilder<ParticipanceBloc, ParticipanceState>(
            builder: (_, state) {
              if (state is! DataParticipanceState ||
                  state.participances
                      .where((element) => element.accepted == false)
                      .isEmpty) {
                return const IgnorePointer(child: SizedBox());
              }
              return const Positioned(
                right: 16,
                top: 8,
                child: IgnorePointer(
                  child: CircleAvatar(
                    radius: 5,
                    backgroundColor: ColorData.colorMainNo,
                  ),
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
