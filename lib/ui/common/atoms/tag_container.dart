import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class TagContainer extends StatelessWidget {
  final String title;
  final VoidCallback? onTap;

  const TagContainer({
    required this.title,
    this.onTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        decoration: const BoxDecoration(
          color: Color(0x1a969696),
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            Expanded(
              child: P0Text(
                title,
                color: ColorData.colorButtonMain,
              ),
            ),
            const SizedBox(width: 5),
            const Icon(
              Icons.more_vert,
              color: ColorData.colorTextMain,
            )
          ],
        ),
      ),
    );
  }
}
