import 'package:flutter/material.dart';
import 'package:tube/logic/model/photourl.dart';
import 'package:tube/utils/colors.dart';

class CirclePhoto extends StatelessWidget {
  final double size;
  final PhotoUrl? photoUrl;

  const CirclePhoto({
    this.size = 24,
    this.photoUrl,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: size,
      width: size,
      child: Stack(
        children: [
          Container(
            height: size,
            width: size,
            decoration: const ShapeDecoration(
              shape: CircleBorder(),
              color: ColorData.color3Percent,
            ),
            alignment: Alignment.center,
            child: Icon(
              Icons.face,
              color: ColorData.colorElementsActive,
              size: 0.75 * size,
            ),
          ),
          if (photoUrl?.thumbnail != null)
            ClipOval(
              child: Image.network(
                photoUrl!.thumbnail!,
                height: size,
                width: size,
              ),
            ),
          if (photoUrl?.m != null)
            ClipOval(
              child: Image.network(
                photoUrl!.m!,
                height: size,
                width: size,
              ),
            ),
        ],
      ),
    );
  }
}
