import 'package:flutter/material.dart';
import 'package:tube/utils/colors.dart';

class LittleTitle extends StatelessWidget {
  final String title;

  const LittleTitle({
    required this.title,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: const TextStyle(
        fontSize: 12,
        fontWeight: FontWeight.w400,
        height: 14 / 12,
        color: ColorData.colortTextSecondary,
      ),
    );
  }
}
