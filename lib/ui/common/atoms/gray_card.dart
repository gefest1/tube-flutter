import 'package:flutter/material.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class GrayCard extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final VoidCallback? onTap;
  final String? imageNetwork;

  const GrayCard({
    this.title,
    this.subTitle,
    this.onTap,
    this.imageNetwork,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: const Color(0xffF3F4F6),
          borderRadius: BorderRadius.circular(6),
        ),
        child: IntrinsicHeight(
          child: Row(
            children: [
              if (imageNetwork != null)
                Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: ClipOval(
                      child: Image(
                        image: PCacheImage(
                          imageNetwork!,
                          enableInMemory: false,
                        ),
                        height: 43,
                        width: 43,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (title != null)
                      Text(
                        title!,
                        style: const TextStyle(
                          fontSize: H3TextStyle.fontSize,
                          height: H3TextStyle.height,
                          fontWeight: H3TextStyle.fontWeight,
                          color: ColorData.colorTextMain,
                        ),
                      ),
                    if (subTitle != null)
                      Text(
                        subTitle!,
                        style: const TextStyle(
                          fontSize: P1TextStyle.fontSize,
                          height: P1TextStyle.height,
                          fontWeight: P1TextStyle.fontWeight,
                          color: ColorData.colorElementsSecondary,
                        ),
                      ),
                  ],
                ),
              ),
              const Icon(
                Icons.arrow_forward,
                size: 24,
                color: ColorData.colorElementsActive,
              )
            ],
          ),
        ),
      ),
    );
  }
}
