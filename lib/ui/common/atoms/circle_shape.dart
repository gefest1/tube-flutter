import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class CircleShape extends StatelessWidget {
  final String title;
  final IconData icon;
  final bool shadowActive;

  final Color backgroundColor;

  final Color iconColor;
  final Color titleColor;

  final VoidCallback? onTap;

  const CircleShape({
    required this.title,
    required this.icon,
    this.backgroundColor = Colors.white,
    this.shadowActive = true,
    this.iconColor = ColorData.colorElementsActive,
    this.titleColor = Colors.black,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            color: backgroundColor,
            shape: BoxShape.circle,
            boxShadow: [
              if (shadowActive)
                const BoxShadow(
                  offset: Offset(2, 2),
                  blurRadius: 15,
                  color: Color(0x14000000),
                ),
            ],
          ),
          constraints: const BoxConstraints(
            maxHeight: 104,
            maxWidth: 104,
          ),
          alignment: Alignment.center,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                icon,
                color: iconColor,
                size: 20,
              ),
              Text(
                title,
                style: TextStyle(
                  fontSize: 12,
                  color: titleColor,
                  fontWeight: FontWeight.w400,
                  height: 14 / 12,
                ),
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      ),
    );
  }
}
