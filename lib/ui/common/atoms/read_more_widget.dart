import 'package:flutter/material.dart';
import 'package:readmore/readmore.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class ReadMoreWidget extends StatelessWidget {
  final String text;
  final int trimLines;

  const ReadMoreWidget(
    this.text, {
    Key? key,
    this.trimLines = 2,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ReadMoreText(
      text,
      trimLines: trimLines,
      delimiter: '',
      colorClickableText: ColorData.colorMainLink,
      trimMode: TrimMode.Line,
      trimCollapsedText: ' ...показать полностью',
      trimExpandedText: ' скрыть',
      style: const TextStyle(
        fontSize: P0TextStyle.fontSize,
        fontWeight: P0TextStyle.fontWeight,
        height: P0TextStyle.height,
        color: ColorData.colorTextMain,
      ),
      moreStyle: const TextStyle(
        fontSize: P0TextStyle.fontSize,
        fontWeight: P0TextStyle.fontWeight,
        height: P0TextStyle.height,
        color: ColorData.colorMainLink,
      ),
    );
  }
}
