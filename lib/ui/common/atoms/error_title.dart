// DangerousTitle
import 'package:flutter/material.dart';

import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/shadow.dart';

class ErrorTitle extends StatelessWidget {
  final String title;

  const ErrorTitle({
    required this.title,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: blocksMains,
        color: Colors.white,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        border: Border.all(
          color: ColorData.colorMainNo,
          width: 0.5,
        ),
      ),
      padding: const EdgeInsets.all(15),
      child: Material(
        color: Colors.transparent,
        child: Row(
          children: [
            const Padding(
              padding: EdgeInsets.only(right: 10),
              child: Icon(
                Icons.warning_amber,
                size: 24,
                color: ColorData.colorMainNo,
              ),
            ),
            Expanded(
              child: P1Text(
                title,
                color: ColorData.colorTextMain,
              ),
            )
          ],
        ),
      ),
    );
  }
}
