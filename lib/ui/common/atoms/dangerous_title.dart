// DangerousTitle
import 'package:flutter/material.dart';

import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class DangerousTitle extends StatelessWidget {
  final String title;
  const DangerousTitle({
    required this.title,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        border: Border.all(
          color: const Color(0xffA5A5A5),
          width: 0.5,
        ),
      ),
      padding: const EdgeInsets.all(15),
      child: Row(
        children: [
          const Padding(
            padding: EdgeInsets.only(right: 10),
            child: Icon(
              Icons.warning_amber,
              size: 24,
              color: ColorData.colorElementsActive,
            ),
          ),
          Expanded(
            child: P1Text(
              title,
              color: ColorData.colorTextMain,
            ),
          )
        ],
      ),
    );
  }
}
