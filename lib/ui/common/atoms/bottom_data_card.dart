import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class BottomDataCard extends StatelessWidget {
  final String? applyUntil;
  final String? countLesson;
  final String? minDuration;

  const BottomDataCard({
    this.applyUntil,
    this.countLesson,
    this.minDuration,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        children: [
          if (applyUntil != null) ...[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const P1Text(
                    "действует",
                    color: ColorData.colortTextSecondary,
                    textAlign: TextAlign.start,
                  ),
                  const SizedBox(height: 2),
                  P1Text(
                    applyUntil!,
                    color: ColorData.colorTextMain,
                    textAlign: TextAlign.start,
                  ),
                ],
              ),
            ),
            const SizedBox(
              width: 0.6,
              height: double.infinity,
              child: DecoratedBox(
                decoration:
                    BoxDecoration(color: ColorData.colorElementsSecondary),
              ),
            ),
          ],
          if (countLesson != null) ...[
            Expanded(
              child: Column(
                children: [
                  const P1Text(
                    "уроков",
                    color: ColorData.colortTextSecondary,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 2),
                  P1Text(
                    countLesson!,
                    color: ColorData.colorTextMain,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            const SizedBox(
              width: 0.6,
              height: double.infinity,
              child: DecoratedBox(
                decoration:
                    BoxDecoration(color: ColorData.colorElementsSecondary),
              ),
            ),
          ],
          if (minDuration != null) ...[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  const P1Text(
                    "один урок",
                    color: ColorData.colortTextSecondary,
                    textAlign: TextAlign.end,
                  ),
                  const SizedBox(height: 2),
                  P1Text(
                    minDuration!,
                    color: ColorData.colorTextMain,
                    textAlign: TextAlign.end,
                  ),
                ],
              ),
            ),
          ],
        ],
      ),
    );
  }
}
