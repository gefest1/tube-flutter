import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_time_shift.dart';

class StopCard extends StatelessWidget {
  final DateTime stopTime;

  const StopCard({
    required this.stopTime,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: const Color(0xffF3F4F6),
        borderRadius: BorderRadius.circular(10),
      ),
      width: double.infinity,
      padding: const EdgeInsets.all(10),
      constraints: const BoxConstraints(minHeight: 44),
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(
            Icons.pause_circle_filled,
            size: 24,
            color: ColorData.colorMainBlack,
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: AutoSizeText(
                'На Паузе до ${DateFormat('dd.MM.yyyy').format(stopTime.parseTimeGG)}',
                maxFontSize: 16,
                minFontSize: 8,
                presetFontSizes: const [16, 14, 10, 8],
                style: const TextStyle(
                  color: ColorData.colorElementsActive,
                ),
                maxLines: 1,
              ),
            ),
          )
        ],
      ),
    );
  }
}
