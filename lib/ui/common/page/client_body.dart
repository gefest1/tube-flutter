import 'package:flutter/material.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/ui/admin/atoms/head_titile.dart';
import 'package:tube/ui/common/atoms/menu_drawer.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/page/club_sheet.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/main_page/components/organisms/main_body.dart';
import 'package:tube/utils/colors.dart';

class ClientBody extends StatefulWidget {
  final String clubId;
  const ClientBody({
    required this.clubId,
    Key? key,
  }) : super(key: key);

  @override
  State<ClientBody> createState() => _ClientBodyState();
}

class _ClientBodyState extends State<ClientBody> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AppBar(
          centerTitle: true,
          automaticallyImplyLeading: false,
          backgroundColor: const Color(0xff141414),
          // toolbarHeight: 65,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(5),
            ),
          ),
          actions: const [SizedBox(width: 56)],
          leading: const MenuDrawer(),
          title: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              final club = MainDrawerState.currentParticipanceValue.value?.club;
              if (club == null) return;
              showFlexibleBottomSheet(
                minHeight: 0,
                initHeight: 0.6,
                maxHeight: 0.6,
                context: context,
                anchors: [0.6],
                builder: (ctx, controller, _) {
                  return Material(
                    color: Colors.transparent,
                    child: ClubSheet(
                      controller: controller,
                      club: club,
                    ),
                  );
                },
              );
            },
            child: IgnorePointer(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const HeadTitle(),
                  StreamBuilder(
                    stream: MainDrawerState.currentParticipanceValue,
                    builder: (ctx, snap) {
                      if (MainDrawerState
                              .currentParticipanceValue.value?.club?.type ==
                          null) {
                        return const SizedBox();
                      }
                      final part =
                          MainDrawerState.currentParticipanceValue.value!;
                      return P1Text(
                        part.club!.type!,
                        color: ColorData.colorMainLink,
                      );
                    },
                  )
                ],
              ),
            ),
          ),
        ),
        const Expanded(
          child: MainBody(),
        ),
      ],
    );
  }
}
