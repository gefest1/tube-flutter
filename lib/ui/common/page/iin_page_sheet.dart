import 'package:flutter/material.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/logic/blocs/participance/model/participance.model.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/common/atoms/animated_switch.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/fast_app_bar.dart';
import 'package:tube/utils/colors.dart';

class IinPageSheet extends StatefulWidget {
  final ScrollController controller;
  final Participance participance;

  const IinPageSheet({
    required this.controller,
    required this.participance,
    Key? key,
  }) : super(key: key);

  @override
  State<IinPageSheet> createState() => _IinPageSheetState();
}

class _IinPageSheetState extends State<IinPageSheet> {
  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(navigatorKeyGlobal.currentContext!);

    return GestureDetector(
      onTap: () {
        FocusScopeNode focusScope = FocusScope.of(context);

        if (focusScope.hasFocus || focusScope.hasPrimaryFocus) {
          focusScope.unfocus();
        }
      },
      child: Material(
        color: Colors.white,
        borderRadius: const BorderRadius.vertical(
          top: Radius.circular(10),
        ),
        child: CustomScrollView(
          physics: const ClampingScrollPhysics(),
          controller: widget.controller,
          slivers: [
            SliverToBoxAdapter(
              child: FastAppBar(
                title: 'Регистрация в компании',
                suffIcon: Icons.close,
                suffTextOnTap: () => Navigator.pop(context),
              ),
            ),
            SliverToBoxAdapter(
              child: Column(
                children: [
                  const SizedBox(height: 20),
                  CircleAvatar(
                    radius: 63,
                    backgroundImage:
                        widget.participance.club?.photoUrl?.thumbnail == null
                            ? null
                            : PCacheImage(
                                widget.participance.club!.photoUrl!.thumbnail!,
                                enableInMemory: true,
                              ),
                    foregroundImage:
                        widget.participance.club?.photoUrl?.xl == null
                            ? null
                            : PCacheImage(
                                widget.participance.club!.photoUrl!.xl!,
                                enableInMemory: true,
                              ),
                  ),
                  if (widget.participance.club?.fullName != null)
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20, left: 20, right: 20),
                      child: H2Text(
                        widget.participance.club!.fullName!,
                        color: ColorData.colorTextMain,
                      ),
                    ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 40, left: 20, right: 20),
                    child: CustomTextField(
                      controller: controller,
                      hintText: '12 цифр',
                      title: 'ИИН',
                      maxLength: 12,
                      icon: const Icon(
                        Icons.beenhere,
                        color: ColorData.colorElementsActive,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              fillOverscroll: true,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: ValueListenableBuilder(
                  valueListenable: controller,
                  child: Container(
                    margin: const EdgeInsets.only(top: 20),
                    decoration: const BoxDecoration(
                      color: Color(0xccffffff),
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, -0.5),
                          color: Color(0x1a000000),
                        ),
                      ],
                    ),
                    padding: EdgeInsets.only(
                      top: 20,
                      left: 20,
                      right: 20,
                      bottom: mediaQuery.padding.bottom != 0
                          ? mediaQuery.padding.bottom
                          : 20,
                    ),
                    child: CustomButton(
                      onTap: () {
                        Navigator.pop<String>(context, controller.text);
                      },
                      title: 'Продолжить',
                    ),
                  ),
                  builder: (ctx, _, child) => AnimatedSwitch(
                    active: controller.text.length == 12,
                    child: child!,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
