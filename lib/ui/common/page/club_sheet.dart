import 'package:flutter/material.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/logic/blocs/club/club.model.dart';
import 'package:tube/ui/common/atoms/read_more_widget.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class ClubSheet extends StatelessWidget {
  final ScrollController controller;
  final Club club;

  const ClubSheet({
    required this.controller,
    required this.club,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return GestureDetector(
      onTap: () {
        FocusScopeNode f = FocusScope.of(context);

        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(5),
          ),
        ),
        child: CustomScrollView(
          physics: const ClampingScrollPhysics(),
          controller: controller,
          slivers: [
            SliverToBoxAdapter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 52,
                    width: double.infinity,
                    child: Material(
                      borderRadius:
                          const BorderRadius.vertical(top: Radius.circular(5)),
                      color: ColorData.colorMainBlack,
                      child: Stack(
                        children: [
                          const Center(
                            child: H4Text(
                              'Страница компании',
                              color: Colors.white,
                            ),
                          ),
                          Positioned(
                            right: 0,
                            top: 0,
                            bottom: 0,
                            child: IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              iconSize: 24,
                              icon: const Icon(
                                Icons.close,
                                color: Colors.white,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Center(
                    child: CircleAvatar(
                      backgroundImage: club.photoUrl?.thumbnail == null
                          ? null
                          : PCacheImage(
                              club.photoUrl!.thumbnail!,
                              enableInMemory: true,
                            ),
                      foregroundImage: club.photoUrl?.xl == null
                          ? null
                          : PCacheImage(
                              club.photoUrl!.xl!,
                              enableInMemory: true,
                            ),
                      radius: 52,
                    ),
                  ),
                  const SizedBox(height: 8),
                  if (club.fullName != null)
                    Center(
                      child: H2Text(
                        club.fullName!,
                        color: ColorData.colorMainBlack,
                      ),
                    ),
                  const SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Container(
                      width: double.infinity,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: ColorData.color3Percent,
                      ),
                      padding: const EdgeInsets.all(15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const H4Text(
                            'О компании',
                            color: ColorData.colortTextSecondary,
                          ),
                          ReadMoreWidget(
                            club.description!,
                            trimLines: 5,
                          ),
                        ],
                      ),
                    ),
                  ),
                  // AddMailBody(
                  //   keyboardType: TextInputType.text,
                  //   hintText: 'Новая должность',
                  //   title: 'Введите название новой должности',
                  //   controller: controller,
                  //   list: _list,
                  //   callBack: () {
                  //     log(_list.toString());
                  //   },
                  // ),
                  const SizedBox(height: 20),
                ],
              ),
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height: mediaQuery.viewInsets.bottom,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
