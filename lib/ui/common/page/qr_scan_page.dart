import 'dart:async';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/tariff/model/train_time.model.dart';
import 'package:tube/logic/blocs/tariff/tariff_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/page/success_page.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'dart:math' as math;

import 'package:tube/utils/colors.dart';
import 'package:tube/utils/date_time_parse.dart';
import 'package:tube/utils/load_popup_route.dart';

class QrScanPage extends StatefulWidget {
  final Booking booking;
  final TrainTime trainTime;
  final String tariffId;

  const QrScanPage({
    required this.tariffId,
    required this.trainTime,
    required this.booking,
    Key? key,
  }) : super(key: key);

  @override
  State<QrScanPage> createState() => _QrScanPageState();
}

class _QrScanPageState extends State<QrScanPage>
    with SingleTickerProviderStateMixin {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  final AudioPlayer audioCache = AudioPlayer();

  final mobileController = MobileScannerController(
    facing: CameraFacing.back,
  );
  late AnimationController animationController = AnimationController(
    vsync: this,
    lowerBound: 1,
    upperBound: 1.05,
    duration: const Duration(milliseconds: 900),
    reverseDuration: const Duration(milliseconds: 900),
  );
  // QRViewController? controller;
  StreamSubscription? _streamSub;
  bool isScanning = false;

  @override
  void initState() {
    animationController.forward();
    animationController.repeat(reverse: true);
    super.initState();
  }

  @override
  void dispose() {
    _streamSub?.cancel();

    mobileController.dispose();
    animationController.dispose();
    super.dispose();
  }

  void _listen(Barcode scanData, MobileScannerArguments? args) async {
    if (isScanning && scanData.rawValue != null) return;

    final uri = Uri.tryParse(scanData.rawValue!);
    if (uri == null) return;

    final params = uri.queryParameters;

    if (params['bookingId'] != widget.booking.id ||
        dateTimeParse(params['date'])!.compareTo(widget.trainTime.date!) != 0) {
      return;
    }
    isScanning = true;
    await mobileController.stop();

    Completer completer = Completer();
    if (!mounted) return;
    Navigator.push(
      context,
      LoadPopupRoute(
        completer: completer,
      ),
    );
    try {
      final mutationResult = await graphqlClient.mutate$startSession(
        Options$Mutation$startSession(
          variables: Variables$Mutation$startSession(
            bookingId: widget.booking.id!,
            trainTimeId: widget.trainTime.id!,
          ),
        ),
      );

      if (mutationResult.hasException) mutationResult.exception!;

      completer.complete();
      await audioCache.play(AssetSource('assets/sound.wav'));
      final List<TrainTime> trainTimes =
          (mutationResult.data!['startTraining']['trainTimes'] as List<dynamic>)
              .map((e) => TrainTime.fromMap(e))
              .toList();
      if (!mounted) return;
      final TariffBloc tariffBloc =
          Provider.of<UserBlocMap>(context, listen: false).getTariffBloc(
        MainDrawerState.currentParticipance!.clubId!,
      );
      tariffBloc.add(
        SetNewTrainTimeEvent(
          trainTime: trainTimes,
          tariffId: widget.tariffId,
        ),
      );
      Navigator.popUntil(context, (route) => route.isFirst);
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => SuccessPage(
            tariffId: widget.tariffId,
          ),
        ),
      );
      return;
    } catch (e, s) {
      completer.completeError(e, s);
    }
    await mobileController.start();
    // await controller?.resumeCamera();
    isScanning = false;
  }

  // @override
  // void reassemble() async {
  //   super.reassemble();
  //   if (Platform.isAndroid) {
  //     await controller!.pauseCamera();
  //     await controller!.resumeCamera();
  //   } else if (Platform.isIOS) {
  //     controller!.resumeCamera();
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final cubit =
        math.min(mediaQuery.size.height, mediaQuery.size.width) * 0.64;

    return Material(
      child: Stack(
        children: [
          MobileScanner(
            key: qrKey,
            // cameraFacing: CameraFacing.back,
            // overlay: null,
            controller: mobileController,
            onDetect: _listen,
            // onQRViewCreated: (QRViewController _controller) {
            //   controller = _controller;
            //   _streamSub = controller!.scannedDataStream.listen(_listen);
            // },
          ),
          Positioned(
            top: MediaQuery.of(context).padding.top + 40,
            left: 20,
            child: SizeTapAnimation(
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  height: 48,
                  width: 48,
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: ColorData.colorMainElements,
                  ),
                  child: const Icon(
                    Icons.arrow_back,
                    size: 33,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).padding.top + 92,
            bottom: 0,
            right: 0,
            left: 0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  decoration: BoxDecoration(
                    color: const Color(0xb2000000),
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: const P0Text(
                    'Отсканируйте QR код, чтобы начать',
                    color: Colors.white,
                  ),
                ),
                Expanded(
                  child: Center(
                    child: ScaleTransition(
                      scale: animationController,
                      child: CustomPaint(
                        painter: ScanAreaPainter(),
                        child: SizedBox(
                          height: cubit,
                          width: cubit,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 100),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ScanAreaPainter extends CustomPainter {
  draw(Canvas canvas, Size size, bool x, bool y) {
    canvas.drawPath(
      Path()
        ..moveTo((x ? 0 : 1) * size.width, (y ? 0.2 : 0.8) * size.height)
        ..lineTo((x ? 0 : 1) * size.width, (y ? 0.1 : 0.9) * size.height)
        ..cubicTo(
          (x ? 0 : 1) * size.width,
          (y ? 0.05 : 0.95) * size.height,
          (x ? 0.05 : 0.95) * size.width,
          (y ? 0 : 1) * size.height,
          (x ? 0.1 : 0.9) * size.width,
          (y ? 0 : 1) * size.height,
        )
        ..lineTo((x ? 0.2 : 0.8) * size.width, (y ? 0 : 1) * size.height),
      Paint()
        ..strokeWidth = 10
        ..color = Colors.white
        ..strokeCap = StrokeCap.round
        ..style = PaintingStyle.stroke,
    );
  }

  @override
  void paint(Canvas canvas, Size size) {
    draw(canvas, size, false, false);
    draw(canvas, size, false, true);
    draw(canvas, size, true, false);
    draw(canvas, size, true, true);
  }

  @override
  bool shouldRepaint(ScanAreaPainter oldDelegate) => false;

  @override
  bool shouldRebuildSemantics(ScanAreaPainter oldDelegate) => false;
}
