import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/tariff/model/train_time.model.dart';
import 'package:tube/ui/common/atoms/circle_shape.dart';
import 'package:tube/ui/common/atoms/little_title.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/frame_card.dart';
import 'package:tube/ui/common/page/qr_scan_page.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/get_time_shift.dart';

class TrainPage extends StatelessWidget {
  final String clubId;
  final Tariff tariff;
  final DateTime deadlineDate;

  const TrainPage({
    required this.tariff,
    required this.clubId,
    required this.deadlineDate,
    Key? key,
  }) : super(key: key);

  TrainTime? getTrainTime(List<TrainTime>? trainTimes) {
    return trainTimes?.firstWhereOrNull((element) {
      return DateTime.now().compareTo(element.endTime!) == -1 && element.come != true;
    });
  }

  String? getDayTitle(List<TrainTime>? trainTimes) {
    try {
      final trainTime = getTrainTime(trainTimes);

      if (trainTime == null) return null;
      final str = toBeginningOfSentenceCase(
        DateFormat('EEEEE HH:mm. dd MMM').format(trainTime.date!.parseTimeGG),
      );
      return str;
    } catch (e) {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    // final trainTime = getTrainTime(tariff.trainTimes);
    final notActive = (tariff.trainTimes ?? []).where(
      (element) {
        return DateTime.now().microsecondsSinceEpoch > element.endTime!.microsecondsSinceEpoch ||
            element.come == true;
      },
    ).toList();
    final count = tariff.trainCount! - notActive.length;
    // .trainTimes!.length -
    //     tariff.trainTimes!.indexOf(getTrainTime(tariff.trainTimes)!);

    final title = getDayTitle(tariff.trainTimes);

    return ListView(
      padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
      children: [
        const LittleTitle(title: 'Владелец'),
        const SizedBox(height: 6),
        FrameCard(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (tariff.user?.photoUrl?.thumbnail != null)
                CircleAvatar(
                  radius: 15,
                  backgroundColor: Colors.grey[600],
                  backgroundImage: PCacheImage(
                    tariff.user!.photoUrl!.thumbnail!,
                    enableInMemory: true,
                  ),
                ),
              const SizedBox(width: 6),
              if (tariff.user?.fullName != null || tariff.user?.email != null)
                Flexible(
                  child: P1Text(
                    tariff.user?.fullName ?? tariff.user!.email!,
                    color: const Color(0xff141414),
                  ),
                )
            ],
          ),
        ),
        const SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Flexible(
              child: SizedBox(
                width: 104,
              ),
            ),
            const SizedBox(width: 10),
            if (getTrainTime(tariff.trainTimes) != null)
              Flexible(
                child: CircleShape(
                  onTap: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => QrScanPage(
                          booking: tariff.booking!,
                          trainTime: getTrainTime(tariff.trainTimes)!,
                          tariffId: tariff.id!,
                        ),
                      ),
                    );
                  },
                  icon: Icons.center_focus_strong,
                  title: 'Сканировать\nQR',
                  shadowActive: false,
                  backgroundColor: ColorData.colorMainElements,
                  iconColor: Colors.white,
                  titleColor: Colors.white,
                ),
              ),
            const SizedBox(width: 10),
            const Flexible(
              child: SizedBox(
                width: 104,
              ),
              // child: CircleShape(
              //   icon: Icons.pin_drop,
              //   title: 'Построить маршрут',
              // ),
            ),
          ],
        ),
        const SizedBox(height: 20),
        const LittleTitle(title: 'Ближайшее занятие'),
        const SizedBox(height: 5),
        if (title != null)
          Padding(
            padding: const EdgeInsets.only(bottom: 5),
            child: FrameCard(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const Icon(
                    Icons.event,
                    size: 24,
                    color: ColorData.colorElementsActive,
                  ),
                  const SizedBox(width: 6),
                  Flexible(
                    child: P1Text(
                      title,
                      color: const Color(0xff141414),
                    ),
                  )
                ],
              ),
            ),
          ),
        FrameCard(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Icon(
                Icons.bubble_chart,
                size: 24,
                color: Color(0xff00CA2C),
              ),
              const SizedBox(width: 6),
              Flexible(
                child: P1Text(
                  '$count занятий осталось',
                  color: const Color(0xff00CA2C),
                ),
              )
            ],
          ),
        ),
        const SizedBox(height: 5),
        FrameCard(
          child: P1Text(
            'Сгорает ${DateFormat("dd.MM.yyyy в HH:mm").format(deadlineDate.parseTimeGG)}',
            color: ColorData.colorElementsActive,
          ),
        ),
      ],
    );
  }
}
