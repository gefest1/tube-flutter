import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/gis_icon.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class InfoPage extends StatelessWidget {
  const InfoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 30),
      children: [
        const Text(
          'Адрес',
          style: TextStyle(
            fontSize: H5TextStyle.fontSize,
            height: H5TextStyle.height,
            fontWeight: H5TextStyle.fontWeight,
            color: ColorData.colortTextSecondary,
          ),
        ),
        const SizedBox(height: 5),
        const Text(
          'РК, г. Алматы, ул.Юрия Кима (между ул. Правды и ул. Берегового), при спортивном, Алматы',
          style: TextStyle(
            fontSize: H5TextStyle.fontSize,
            height: H5TextStyle.height,
            fontWeight: H5TextStyle.fontWeight,
            color: ColorData.colorTextMain,
          ),
        ),
        const SizedBox(height: 10),
        Container(
          height: 54,
          decoration: BoxDecoration(
            color: ColorData.colorElementsActive,
            borderRadius: BorderRadius.circular(6),
          ),
          alignment: Alignment.center,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              CustomPaint(
                size: const Size(51, 22),
                painter: TwoGisCustomPainter(),
              ),
              const SizedBox(width: 10),
              const Flexible(
                child: Text(
                  'Построить маршрут',
                  style: TextStyle(
                    fontSize: P1TextStyle.fontSize,
                    height: P1TextStyle.height,
                    fontWeight: P1TextStyle.fontWeight,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 30),
        const Text(
          'Тренер',
          style: TextStyle(
            fontSize: H5TextStyle.fontSize,
            height: H5TextStyle.height,
            fontWeight: H5TextStyle.fontWeight,
            color: ColorData.colortTextSecondary,
          ),
        ),
        const SizedBox(height: 5),
        Container(
          padding: const EdgeInsets.all(20),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(6)),
            boxShadow: [
              BoxShadow(
                offset: Offset(2, 2),
                blurRadius: 15,
                color: Color(0x14000000),
              ),
            ],
          ),
          child: const Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CircleAvatar(
                radius: 28.5,
                backgroundColor: Colors.grey,
              ),
              SizedBox(width: 15),
              Expanded(
                child: Text(
                  'Игнатов Михаил Борисович',
                  style: TextStyle(
                    color: ColorData.colorTextMain,
                    height: H3TextStyle.height,
                    fontSize: H3TextStyle.fontSize,
                    fontWeight: H3TextStyle.fontWeight,
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 10),
        SizeTapAnimation(
          child: GestureDetector(
            onTap: () {},
            child: Container(
              height: 56,
              decoration: const BoxDecoration(
                color: Color(0xff00CB20),
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(2, 2),
                    blurRadius: 5,
                    color: Color(0x26000000),
                  )
                ],
              ),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.call,
                    color: Colors.white,
                  ),
                  SizedBox(width: 5),
                  Flexible(
                    child: Text(
                      'Позвонить',
                      style: TextStyle(
                        height: P1TextStyle.height,
                        fontSize: P1TextStyle.fontSize,
                        fontWeight: P1TextStyle.fontWeight,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
