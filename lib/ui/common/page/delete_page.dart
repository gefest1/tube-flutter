import 'package:flutter/material.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/restart_widget.dart';

class DeletePage extends StatelessWidget {
  const DeletePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const H4Text(
          'Удаление профиля',
          color: Colors.white,
        ),
      ),
      body: Stack(
        children: [
          ListView(
            padding: const EdgeInsets.all(20),
            children: const [
              H2Text(
                'Профиль будет удален через 30 дней, после отправки запроса',
                color: ColorData.colorTextMain,
              ),
              SizedBox(height: 20),
              P0Text(
                'Ваш профиль будет удален, когда истекут все абонементы. Вы всегда можете восстановить профиль, если опять в него зайдете в течение срока удаления',
                color: ColorData.colorTextMain,
              )
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: BottomButton(
              active: true,
              onTap: () async {
                await sharedBox.delete('token');
                RestartWidget.restartApp(navigatorKeyGlobal.currentContext!);
              },
              title: 'Запросить удаление',
            ),
          ),
        ],
      ),
    );
  }
}
