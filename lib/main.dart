// ignore: unused_import
import 'dart:collection';
import 'dart:developer';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/logic/bloc_wrapper.dart';
import 'package:tube/logic/provider_wrapper.dart';
import 'package:tube/utils/check_version.dart';

import 'package:tube/utils/navigator.dart';
import 'package:tube/utils/restart_widget.dart';
import 'package:tube/utils/routes.observer.dart';
export './logic/api/graphql_client.dart';

late CustomRouterDelegate routerDelegate;
late final Box sharedBox;
late MyNavigatorObserver myNavigatorObserver;
late CustomRouteInformationParser routeParser;

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  log((message.notification?.title).toString());
}

void listenBackgroundMessage() {
  // if (kDebugMode) return;

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
}

Future init() async {
  WidgetsFlutterBinding.ensureInitialized();
  Intl.defaultLocale = 'ru';

  await Future.wait(
    [
      Firebase.initializeApp(),
      Hive.initFlutter(),
      PCacheImage.init(
        enableCache: true,
        maxInMemoryImages: 500,
      ),
    ],
  );
  initializeDateFormatting();

  sharedBox = await Hive.openBox('box');
}

void main() async {
  await init();

  await Hive.initFlutter();
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: await getApplicationDocumentsDirectory(),
  );
  listenBackgroundMessage();
  runApp(
    const RestartWidget(
      child: ProviderWrapper(
        child: BlocWrapper(
          child: MyApp(),
        ),
      ),
    ),
  );
}

late GlobalKey<NavigatorState> navigatorKeyGlobal;

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    if (!kDebugMode) {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
      ]);
    }
    navigatorKeyGlobal = GlobalKey<NavigatorState>();
    routeParser = CustomRouteInformationParser();
    routerDelegate = CustomRouterDelegate(
      navigatorKey: navigatorKeyGlobal,
    );

    getPermisson();
    log(sharedBox.get('token') ?? '');
    myNavigatorObserver = MyNavigatorObserver();
    super.initState();
    checkVersion();
  }

  getPermisson() async {
    NotificationSettings settings = await FirebaseMessaging.instance.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
    log('User granted ${settings}');

    FirebaseMessaging.instance.getToken().then((value) {
      log(value ?? '', name: "TOKEN");
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationParser: routeParser,
      routerDelegate: routerDelegate,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'InterTight',
        primaryColor: const Color(0xff141414),
        colorScheme: const ColorScheme.dark(
          surface: Color(0xff141414),
          surfaceTint: Colors.transparent,
        ),
      ),
    );
  }
}
