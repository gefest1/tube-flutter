import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tube/utils/drop_down_route.dart';

class CustomDropdownMenuRouteLayout<T> extends SingleChildLayoutDelegate {
  CustomDropdownMenuRouteLayout({
    required this.buttonRect,
    required this.route,
    required this.textDirection,
  });

  final Rect buttonRect;
  final CustomDropdownRoute<T> route;
  final TextDirection? textDirection;

  @override
  BoxConstraints getConstraintsForChild(BoxConstraints constraints) {
    double leftPoint = 0;
    double rightPoint = constraints.maxWidth;
    double topPoint = 0;
    double bottomPoint = constraints.maxHeight;

    if (!buttonRect.left.isInfinite) {
      leftPoint = buttonRect.left;
    }
    if (!buttonRect.right.isInfinite) {
      rightPoint = buttonRect.right;
    }
    if (!buttonRect.top.isInfinite) {
      topPoint = buttonRect.top;
    }
    if (!buttonRect.bottom.isInfinite) {
      topPoint = buttonRect.bottom;
    }
    final maxWidth = rightPoint - leftPoint;
    return BoxConstraints(
      maxWidth: maxWidth,
      maxHeight: bottomPoint - topPoint,
    );
  }

  @override
  Offset getPositionForChild(Size size, Size childSize) {
    assert(textDirection != null);
    final double left;
    switch (textDirection!) {
      case TextDirection.rtl:
        left = clampDouble(buttonRect.right, 0.0, size.width) - childSize.width;
        break;
      case TextDirection.ltr:
        left = clampDouble(buttonRect.left, 0.0, size.width);
        break;
    }
    double top = clampDouble(buttonRect.top, 0, size.height);
    log(childSize.width.toString());
    return Offset(left, top);
  }

  @override
  bool shouldRelayout(CustomDropdownMenuRouteLayout<T> oldDelegate) {
    return buttonRect != oldDelegate.buttonRect ||
        textDirection != oldDelegate.textDirection;
  }
}
