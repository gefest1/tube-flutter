import 'package:flutter/material.dart';

abstract class ColorData {
  static const Color colorBlack = Color(0xff424242);
  static const Color colorButtonMain = Color(0xff141414);
  static const Color colorMainElements = Color(0xff141414);
  static const Color colorMainBlack = Color(0xff141414);
  static const Color colorTextMain = Color(0xff404040);
  static const Color colortTextSecondary = Color(0xff959595);
  static const Color colorElementsActive = Color(0xff404040);
  static const Color colorElementsSecondary = Color(0xff959595);
  static const Color colorElementsLightMenu = Color(0xffD1D1D1);
  static const Color colorWhiteBackground = Color(0xffE5E5E5);
  static const Color colorGrayishBackground = Color(0xffF3F4F6);
  static const Color colorGrayscaleOffBlack = Color(0xff14142B);
  static const Color colorMainLink = Color(0xff2E9BFF);
  static const Color colorMainNo = Color(0xffFF2F2F);
  static const Color color3Percent = Color(0xffF7F7F7);
  static const Color color5Percent = Color(0xffEBEBEB);
  static const Color service = Color(0xffF1BC00);
  static const Color abonement = Color(0xff00F1A9);
  static const Color event = Color(0xff1300F1);
  static const Color colorBlackInactive = Color(0x8a000000);
  static const Color colorStrokeIncorrect = Color(0xffff0000);
}
