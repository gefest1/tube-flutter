DateTime getDateTimefromObjectId(String objectId) {
  return DateTime.fromMillisecondsSinceEpoch(
    int.parse(objectId.substring(0, 8), radix: 16) * 1000,
  ).toLocal();
}

DateTime? getDateTimefromObjectIdNullable(String? objectId) {
  if (objectId == null) return null;
  return DateTime.fromMillisecondsSinceEpoch(
    int.parse(objectId.substring(0, 8), radix: 16) * 1000,
  ).toLocal();
}
