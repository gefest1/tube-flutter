import 'dart:async';
import 'dart:developer' as dev;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/participance/participance_bloc.dart';
import 'package:tube/logic/model/status_enum.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/common/templates/main_template.dart';
import 'package:tube/ui/register/initial_page.dart';

import 'package:tube/utils/load_popup_route.dart';
import 'package:tube/utils/restart_widget.dart';
import 'package:tube/utils/uri_parse.dart';

class CustomRoutePath {
  final Uri? uri;
  const CustomRoutePath({this.uri});
}

class CustomRouteInformationParser
    extends RouteInformationParser<CustomRoutePath> {
  @override
  Future<CustomRoutePath> parseRouteInformation(
    RouteInformation routeInformation,
  ) async {
    return CustomRoutePath(uri: routeInformation.uri);
  }

  @override
  // ignore: avoid_renaming_method_parameters
  RouteInformation restoreRouteInformation(CustomRoutePath path) {
    return RouteInformation();
  }
}

class CustomRouterDelegate extends RouterDelegate<CustomRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<CustomRoutePath> {
  @override
  final GlobalKey<NavigatorState> navigatorKey;

  // parse

  List<Page> pages = [
    MaterialPage(
      child: sharedBox.containsKey('token')
          ? const MainTemplate()
          : const InitialPage(),
    )
  ];

  CustomRouterDelegate({
    GlobalKey<NavigatorState>? navigatorKey,
  }) : navigatorKey = navigatorKey ?? GlobalKey<NavigatorState>();

  Future<void> fastAccept({
    required String clubId,
  }) async {
    try {
      await graphqlClient.mutate$inviteUserMy(
        Options$Mutation$inviteUserMy(
          variables: Variables$Mutation$inviteUserMy(
            clubId: clubId,
            status: StatusEnum.participance.genratedValue,
          ),
        ),
      );
    } catch (e) {
      return fastAccept(clubId: clubId);
    }
  }

  @override
  Future<void> setNewRoutePath(CustomRoutePath configuration) async {
    dev.log(configuration.toString());
    if (configuration.uri?.queryParameters.isEmpty ?? true) return;

    final params = configuration.uri!.queryParameters;

    if (params['token'] != null &&
        params['type'] == 'InviteClient' &&
        params['clubId'] != null) {
      await sharedBox.put('token', params['token']);
      await fastAccept(clubId: params['clubId']!);

      RestartWidget.restartApp(navigatorKey.currentContext!);
      return;
    }
    if (params['type'] == null || params['clubId'] == null) return;
    final parBloc = Provider.of<ParticipanceBloc>(
      navigatorKeyGlobal.currentContext!,
      listen: false,
    );
    final Completer completer = Completer();

    final status = TypeInviteEnum.fromMap(params['type']);
    if (status == null) return;
    navigatorKey.currentState?.push(
      LoadPopupRoute(completer: completer),
    );
    if (status == TypeInviteEnum.InviteTrainer) {
      parBloc.add(
        InviteTrainerParticipanceEvent(
          clubId: params['clubId']!,
          completer: completer,
        ),
      );
    }
    if (status == TypeInviteEnum.InviteClient) {
      parBloc.add(
        InviteClientParticipanceEvent(
          clubId: params['clubId']!,
          completer: completer,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      observers: [myNavigatorObserver],
      key: navigatorKey,
      onPopPage: (route, result) {
        final didPop = route.didPop(result);
        if (!didPop) {
          return false;
        }

        return true;
      },
      pages: pages,
    );
  }
}
