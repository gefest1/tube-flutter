import 'dart:ui' as ui;

import 'package:flutter/material.dart';

class LocalPageScrollPhysics extends ScrollPhysics {
  final double width;
  final double separateWidth;
  const LocalPageScrollPhysics(
      {required this.width, required this.separateWidth, ScrollPhysics? parent})
      : super(parent: parent);

  @override
  LocalPageScrollPhysics applyTo(ScrollPhysics? ancestor) {
    return LocalPageScrollPhysics(
      width: width,
      separateWidth: separateWidth,
      parent: buildParent(ancestor),
    );
  }

  double _getPage(ScrollMetrics position) {
    return position.pixels / (width + separateWidth);
  }

  double _getPixels(ScrollMetrics position, double page) {
    return page * (width + separateWidth);
  }

  double _getTargetPixels(
      ScrollMetrics position, Tolerance tolerance, double velocity) {
    double page = _getPage(position);
    if (velocity < -tolerance.velocity) {
      page -= 0.5;
    } else if (velocity > tolerance.velocity) {
      page += 0.5;
    }
    return _getPixels(position, page.roundToDouble());
  }

  @override
  Simulation? createBallisticSimulation(
    ScrollMetrics position,
    double velocity,
  ) {
    if ((velocity <= 0.0 && position.pixels <= position.minScrollExtent) ||
        (velocity >= 0.0 && position.pixels >= position.maxScrollExtent)) {
      return super.createBallisticSimulation(position, velocity);
    }

    final Tolerance tolerance = toleranceFor(FixedScrollMetrics(
      minScrollExtent: null,
      maxScrollExtent: null,
      pixels: null,
      viewportDimension: null,
      axisDirection: AxisDirection.down,
      devicePixelRatio:
          ui.PlatformDispatcher.instance.implicitView?.devicePixelRatio ?? 1,
    ));
    final double target = _getTargetPixels(position, tolerance, velocity);
    if (target != position.pixels) {
      return ScrollSpringSimulation(spring, position.pixels, target, velocity,
          tolerance: tolerance);
    }
    return null;
  }

  @override
  bool get allowImplicitScrolling => false;
}
