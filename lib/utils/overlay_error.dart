import 'dart:async';

import 'package:flutter/material.dart';

import 'package:tube/ui/common/atoms/error_title.dart';

class OverlayError extends OverlayEntry {
  final String text;

  OverlayError({required this.text})
      : super(
          builder: (_) {
            return const SizedBox();
          },
        );

  @override
  OverlayWidgetError Function(BuildContext) get builder => (context) {
        return OverlayWidgetError(overlay: this);
      };
}

class OverlayWidgetError extends StatefulWidget {
  final OverlayError overlay;
  const OverlayWidgetError({
    required this.overlay,
    super.key,
  });

  @override
  State<OverlayWidgetError> createState() => _OverlayWidgetErrorState();
}

class _OverlayWidgetErrorState extends State<OverlayWidgetError>
    with SingleTickerProviderStateMixin {
  late Timer timerKill;
  late Timer timerHide;
  late AnimationController animation;
  late Animation<Offset> animationOffset;
  late Animation<double> animationOpacity;

  @override
  void initState() {
    animation = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    animationOffset = Tween<Offset>(
      begin: const Offset(0, -0.4),
      end: Offset.zero,
    ).animate(animation);
    animationOpacity = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: animation,
        curve: Curves.easeIn,
      ),
    );

    timerKill = Timer(
      const Duration(seconds: 4, milliseconds: 300),
      hideAnimation,
    );
    timerHide = Timer(
      const Duration(seconds: 4, milliseconds: 800),
      killOverlay,
    );

    animation.forward();
    super.initState();
  }

  void hideAnimation() {
    if (!mounted) return;
    animation.reverse();
  }

  void killOverlay() {
    widget.overlay.remove();
  }

  @override
  void dispose() {
    animation.dispose();

    if (timerHide.isActive) timerHide.cancel();
    if (timerKill.isActive) timerKill.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final top = MediaQuery.of(context).padding.top;
    return Align(
      alignment: Alignment.topCenter,
      child: FadeTransition(
        opacity: animationOpacity,
        child: SlideTransition(
          position: animationOffset,
          child: Padding(
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
              top: top + 20,
            ),
            child: ErrorTitle(
              title: widget.overlay.text,
            ),
          ),
        ),
      ),
    );
  }
}
