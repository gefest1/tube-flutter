Duration getTimeShift() {
  return DateTime.now().timeZoneOffset;
}

extension ParseGlobalTime on DateTime {
  DateTime get parseTimeGG => parseGlobalTime(this);
}

DateTime parseGlobalTime(DateTime date) {
  return date.toUtc().add(getTimeShift());
}
