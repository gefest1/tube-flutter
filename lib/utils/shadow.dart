import 'package:flutter/material.dart';

const blocksMains = [
  BoxShadow(
    offset: Offset(0, -3),
    blurRadius: 2,
    spreadRadius: 0,
    color: Color(0x03000000),
  ),
  BoxShadow(
    offset: Offset(-3, 0),
    blurRadius: 2,
    spreadRadius: 0,
    color: Color(0x03000000),
  ),
  BoxShadow(
    offset: Offset(3, 0),
    blurRadius: 2,
    spreadRadius: 0,
    color: Color(0x03000000),
  ),
  BoxShadow(
    offset: Offset(0, 5),
    blurRadius: 20,
    spreadRadius: 0,
    color: Color(0x0d000000),
  ),
];
