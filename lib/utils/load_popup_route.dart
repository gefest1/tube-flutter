import 'dart:async';
import 'dart:developer';
import 'package:flutter/material.dart';

class _LoadBlockWidget extends StatefulWidget {
  final Completer completer;
  final LoadPopupRoute route;

  const _LoadBlockWidget({
    required this.completer,
    required this.route,
    Key? key,
  }) : super(key: key);

  @override
  State<_LoadBlockWidget> createState() => _LoadBlockWidgetState();
}

class _LoadBlockWidgetState extends State<_LoadBlockWidget> {
  @override
  void initState() {
    super.initState();
    _initS();
  }

  void _initS() async {
    try {
      await widget.completer.future;
    } catch (e) {
      log('Loader got error');
    } finally {
      Navigator.removeRoute(context, widget.route);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: const Center(
        child: CircularProgressIndicator.adaptive(),
      ),
    );
  }
}

class LoadPopupRoute<T> extends PopupRoute<T> {
  LoadPopupRoute({
    required this.completer,
    // required this.capturedThemes,
    RouteSettings? settings,
    this.transitionAnimationController,
  }) : super(settings: settings);

  final Completer completer;
  // final CapturedThemes capturedThemes;
  final AnimationController? transitionAnimationController;

  @override
  Duration get transitionDuration => const Duration(milliseconds: 250);

  @override
  Duration get reverseTransitionDuration => const Duration(milliseconds: 200);

  @override
  bool get barrierDismissible => false;

  @override
  final String? barrierLabel = null;

  @override
  Color get barrierColor => Colors.black54;

  AnimationController? _animationController;

  @override
  AnimationController createAnimationController() {
    if (transitionAnimationController != null) {
      _animationController = transitionAnimationController;
      willDisposeAnimationController = false;
    } else {
      return AnimationController(
        duration: const Duration(milliseconds: 250),
        reverseDuration: const Duration(milliseconds: 200),
        debugLabel: 'LoadPopup',
        vsync: navigator!,
      );
    }
    return _animationController!;
  }

  @override
  Widget buildPage(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
  ) {
    return _LoadBlockWidget(
      completer: completer,
      route: this,
    );
  }
}
