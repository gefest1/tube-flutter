import 'package:intl/intl.dart';

DateTime dateTimeOnNextWeek(DateTime time) {
  int dayFiff = DateTime.now().difference(time).inDays;
  return time.add(Duration(days: dayFiff + 7 - dayFiff % 7)).toLocal();
}
