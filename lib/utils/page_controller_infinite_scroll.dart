// import 'package:flutter/material.dart';

// class PageInfiniteController extends ScrollController {
//   PageInfiniteController({
//     this.initialPage = 0,
//     this.keepPage = true,
//     this.viewportFraction = 1.0,
//   }) : assert(viewportFraction > 0.0);
//   final int initialPage;
//   final bool keepPage;
//   final double viewportFraction;

//   double? get page {
//     assert(
//       positions.isNotEmpty,
//       'PageController.page cannot be accessed before a PageView is built with it.',
//     );
//     assert(
//       positions.length == 1,
//       'The page property cannot be read when multiple PageViews are attached to '
//       'the same PageController.',
//     );
//     final _PagePosition position = this.position as _PagePosition;
//     return position.page;
//   }

//   Future<void> animateToPage(
//     int page, {
//     required Duration duration,
//     required Curve curve,
//   }) {
//     final _PagePosition position = this.position as _PagePosition;
//     if (position._cachedPage != null) {
//       position._cachedPage = page.toDouble();
//       return Future<void>.value();
//     }

//     return position.animateTo(
//       position.getPixelsFromPage(page.toDouble()),
//       duration: duration,
//       curve: curve,
//     );
//   }

//   void jumpToPage(int page) {
//     final _PagePosition position = this.position as _PagePosition;
//     if (position._cachedPage != null) {
//       position._cachedPage = page.toDouble();
//       return;
//     }

//     position.jumpTo(position.getPixelsFromPage(page.toDouble()));
//   }

//   Future<void> nextPage({required Duration duration, required Curve curve}) {
//     return animateToPage(page!.round() + 1, duration: duration, curve: curve);
//   }

//   Future<void> previousPage(
//       {required Duration duration, required Curve curve}) {
//     return animateToPage(page!.round() - 1, duration: duration, curve: curve);
//   }

//   @override
//   ScrollPosition createScrollPosition(ScrollPhysics physics,
//       ScrollContext context, ScrollPosition? oldPosition) {
//     return _PagePosition(
//       physics: physics,
//       context: context,
//       initialPage: initialPage,
//       keepPage: keepPage,
//       viewportFraction: viewportFraction,
//       oldPosition: oldPosition,
//     );
//   }

//   @override
//   void attach(ScrollPosition position) {
//     super.attach(position);
//     final _PagePosition pagePosition = position as _PagePosition;
//     pagePosition.viewportFraction = viewportFraction;
//   }
// }

// class _PagePosition extends ScrollPositionWithSingleContext
//     implements PageMetrics {
//   _PagePosition({
//     required super.physics,
//     required super.context,
//     this.initialPage = 0,
//     bool keepPage = true,
//     double viewportFraction = 1.0,
//     super.oldPosition,
//   })  : assert(viewportFraction > 0.0),
//         _viewportFraction = viewportFraction,
//         _pageToUseOnStartup = initialPage.toDouble(),
//         super(
//           initialPixels: null,
//           keepScrollOffset: keepPage,
//         );

//   final bool negativeScroll;
//   void _forceNegativePixels(double value) {
//     super.forcePixels(-value);
//   }

//   @override
//   void saveScrollOffset() {
//     if (!negativeScroll) {
//       super.saveScrollOffset();
//     }
//   }

//   @override
//   void restoreScrollOffset() {
//     if (!negativeScroll) {
//       super.restoreScrollOffset();
//     }
//   }

//   @override
//   double get minScrollExtent => double.negativeInfinity;

//   @override
//   double get maxScrollExtent => double.infinity;
// }

