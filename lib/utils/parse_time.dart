const weekTimeDateTime = 3600000 * 24 * 7;
const startTimeDateTime = 345600000;

DateTime getStratTime() {
  return DateTime(1970, 1, 5);
}

DateTime parseTime(DateTime _) {
  return DateTime.fromMillisecondsSinceEpoch(
      (_.millisecondsSinceEpoch - startTimeDateTime) % weekTimeDateTime + startTimeDateTime);
}
