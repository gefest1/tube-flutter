import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:http/http.dart' as http;
import 'package:tube/main.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/const.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'dart:io' show Platform;
import 'colors.dart';
import 'dart:math' as math;

// import 'package:html/parser.dart' show parse;

void checkVersion() async {
  PackageInfo packageInfo = await PackageInfo.fromPlatform();

  final uri = Uri.parse("$httpUrlPath/version");

  final res = await http.get(
    uri.replace(
      queryParameters: {
        "operatingSystem": Platform.operatingSystem,
      },
    ),
  );

  if (int.parse(packageInfo.buildNumber) >= int.parse(res.body)) return;
  showDialog(
    context: navigatorKeyGlobal.currentContext!,
    useSafeArea: false,
    builder: (context) {
      return const UpdateDialog();
    },
  );
}

class UpdateDialog extends StatelessWidget {
  const UpdateDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context);
    return Material(
      color: Colors.black.withOpacity(0.8),
      child: Column(
        children: [
          const Expanded(
            child: Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.developer_board,
                      color: Colors.white,
                      size: 45,
                    ),
                    SizedBox(height: 10),
                    H2Text(
                      'Доступно обновление',
                      color: Colors.white,
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
            ),
          ),
          ClipRRect(
            child: BackdropFilter(
              filter: ImageFilter.blur(
                sigmaX: 20,
                sigmaY: 20,
              ),
              child: Container(
                color: Colors.white.withOpacity(0.8),
                padding: EdgeInsets.only(
                  top: 10,
                  left: 20,
                  right: 20,
                  bottom: math.max(20, media.padding.bottom),
                ),
                child: Column(
                  children: [
                    CustomButton(
                      onTap: () {
                        launchUrlString('https://www.checkallnow.net/');
                      },
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.cloud_download,
                            color: Colors.white,
                            size: 20,
                          ),
                          SizedBox(width: 5),
                          P0Text(
                            'Загрузить',
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                    const SizedBox(height: 10),
                    SizeTapAnimation(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: const P1Text(
                        'Позже',
                        color: ColorData.colorMainLink,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
