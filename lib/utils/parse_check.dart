import 'package:tube/logic/blocs/tariff/model/train_time.model.dart';
import 'package:tube/logic/model/create_tarriff.dart';
import 'package:tube/utils/parse_time.dart';

List<TrainTime> getTrainTime(
  List<InputTrainGraph> inputTrainDirt,
  int count,
  int timePerMinute,
) {
  final parsedTime = parseTime(DateTime.now());
  final inputTrain = [...inputTrainDirt]..sort((a, b) {
      return a.date.compareTo(b.date); //- b.date!.getTime();
    });
  final mapsFast = inputTrain.map((e) {
    return parsedTime.compareTo(e.date) == 1 ? 1 : -1;
    // parsedTime.getTime() - e.date.getTime() > 0 ? 1 : -1,
  }).toList();

  int firstIndex = 0;

  int weekCount =
      DateTime.now().difference(parseTime(DateTime.now())).inDays ~/ 7;
  if (mapsFast.contains(-1) && mapsFast.contains(1)) {
    // _mapsFast.indexWhere((element) => false)
    firstIndex = mapsFast.indexWhere((val) => val == -1);
  } else if (mapsFast.contains(1)) {
    weekCount++;
  }
  final List<TrainTime> newInputRet = [];

  for (int countIndex = 0; countIndex < count; ++countIndex, firstIndex++) {
    final currentTrainTime = inputTrain[firstIndex % inputTrain.length];

    if (newInputRet.isNotEmpty &&
        newInputRet[newInputRet.length - 1].date!.compareTo(
                currentTrainTime.date.add(Duration(days: 7 * weekCount))) !=
            -1) {
      weekCount++;
    }
    final date = currentTrainTime.date.add(Duration(days: 7 * weekCount));

    newInputRet.add(
      TrainTime(
        come: false,
        date: date,
        endTime: date.add(Duration(minutes: timePerMinute)),
        trainerId: currentTrainTime.trainerId,
      ),
    );
  }
  return newInputRet;
}
