import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/logic/bloc_wrapper.dart';
import 'package:tube/logic/blocs/participance/model/participance.model.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/loader.dart';

class RestartWidget extends StatefulWidget {
  final Widget child;

  const RestartWidget({required this.child, Key? key}) : super(key: key);

  static void restartApp(BuildContext context) {
    context.findAncestorStateOfType<_RestartWidgetState>()?.restartApp();
  }

  @override
  State<RestartWidget> createState() => _RestartWidgetState();
}

class _RestartWidgetState extends State<RestartWidget> {
  Key key = UniqueKey();

  void restartApp() {
    userBloc?.close();
    participanceBloc?.close();
    clubBloc?.close();
    workerBloc?.close();
    adminBlocMap?.close();
    trainerBlocMap?.close();
    userBlocMap?.close();

    userBlocMap = null;

    MainDrawerState.currentParticipanceValue =
        BehaviorSubject<Participance?>.seeded(null);
    MainDrawerState.index = null;

    userBloc = null;
    participanceBloc = null;
    clubBloc = null;
    workerBloc = null;
    adminBlocMap = null;
    trainerBlocMap = null;
    routeClean?.call();
    financeBloc = null;
    setState(() {
      key = UniqueKey();
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KeyedSubtree(
      key: key,
      child: widget.child,
    );
  }
}
