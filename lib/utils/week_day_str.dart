import 'package:intl/intl.dart';
import 'package:tube/utils/get_next_week_date.dart';
import 'package:tube/utils/get_time_shift.dart';

String? getWeekStr(List<DateTime> val) {
  String? str;
  final value = [...val]..sort((a, b) => a.compareTo(b));
  if (value.isNotEmpty) {
    for (int ind = 0; ind < value.length * 2 - 1; ++ind) {
      if (ind.isOdd) {
        (str ??= '');
        str += ' | ';
        continue;
      }
      str ??= '';
      str += DateFormat('HH:mm').format(dateTimeOnNextWeek(value[ind ~/ 2]).parseTimeGG);
    }
  }
  return str;
}
