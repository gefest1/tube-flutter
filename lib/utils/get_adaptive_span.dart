import 'dart:ui';

import 'package:flutter/material.dart';

Size calcTextSize(String text, TextStyle style) {
  final TextPainter textPainter = TextPainter(
    text: TextSpan(text: text, style: style),
    textDirection: TextDirection.ltr,
    textScaleFactor: PlatformDispatcher.instance.textScaleFactor,
  )..layout();

  return textPainter.size;
}

TextSpan getAdaptiveTextSpan(
    String text, TextStyle? style, BuildContext context) {
  final DefaultTextStyle defaultTextStyle = DefaultTextStyle.of(context);
  TextStyle? effectiveTextStyle = style;
  if (style == null || style.inherit) {
    effectiveTextStyle = defaultTextStyle.style.merge(style);
  }
  if (MediaQuery.boldTextOf(context)) {
    effectiveTextStyle =
        effectiveTextStyle!.merge(const TextStyle(fontWeight: FontWeight.bold));
  }

  List<InlineSpan> listInlineSpan = [];
  String localText = '';
  for (final ll in text.characters) {
    if (calcTextSize(ll, effectiveTextStyle!.copyWith(fontSize: 50)).width ==
        0) {
      listInlineSpan.add(TextSpan(text: localText));
      listInlineSpan.add(TextSpan(
          text: ll, style: effectiveTextStyle.copyWith(fontFamily: '')));
      localText = '';
    } else {
      localText += ll;
    }
  }
  listInlineSpan.add(TextSpan(text: localText));
  return TextSpan(children: listInlineSpan, style: effectiveTextStyle);
}
