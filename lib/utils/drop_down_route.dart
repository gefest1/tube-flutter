import 'package:flutter/material.dart';

import 'package:tube/utils/custom_dropdown_menu_route_layout.dart';

class CustomDropdownRoute<T> extends PopupRoute<T> {
  CustomDropdownRoute({
    this.barrierLabel,
    this.rect = Rect.zero,
    this.alignment,
    this.child,
  });

  final Widget? child;
  final Rect rect;
  final Alignment? alignment;

  @override
  Color? get barrierColor => null;

  @override
  bool get barrierDismissible => true;

  @override
  final String? barrierLabel;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return FadeTransition(
      opacity: CurvedAnimation(
        parent: animation,
        curve: Curves.easeIn,
      ),
      child: LayoutBuilder(
        builder: (context, constraints) {
          return DropdownRoutePage(
            route: this,
            alignment: alignment,
          );
        },
      ),
    );
  }

  @override
  Duration get transitionDuration => const Duration(milliseconds: 100);
}

class DropdownRoutePage<T> extends StatelessWidget {
  const DropdownRoutePage({
    super.key,
    required this.route,
    this.alignment,
  });

  final Alignment? alignment;
  final CustomDropdownRoute<T> route;

  @override
  Widget build(BuildContext context) {
    final TextDirection? textDirection = Directionality.maybeOf(context);
    return CustomSingleChildLayout(
      delegate: CustomDropdownMenuRouteLayout(
        buttonRect: route.rect,
        route: route,
        textDirection: textDirection,
      ),
      child: Align(
        alignment: alignment ?? Alignment.center,
        child: route.child,
      ),
    );
  }
}
